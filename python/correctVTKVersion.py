''' Quickly replace the 'version="2.0"' to 'version="1.0"' in the VTI and VTU files created by Slurm.
    This is necessary for files to be opened by ParaView 4.3 and lower.

'''
import os, glob

path = 'C:/Temp/Ubuntu-shared/KH/data0'

if __name__ == '__main__':
    if not os.path.exists(path):
        print 'No such directory: ', path
    
    filenames = glob.glob(os.path.join(path, '*.vti'))
    filenames += glob.glob(os.path.join(path, '*.vtu'))
    filenames += glob.glob(os.path.join(path, '*.vtr'))
    
    if not filenames:
        print 'No VTI or VTU files found in: ', path
    
    for fname in filenames:
        fd = os.open(fname, os.O_RDWR)
        buf = os.read(fd, 200)
        p = buf.find('version="2.0"')
        if p > 0:
            os.lseek(fd, p, os.SEEK_SET)
            os.write(fd, 'version="1.0"')
        os.close(fd)
        print fname