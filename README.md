# README #

* Slurm: a Lagrangian particle-in-cell HD/MHD solver
* Version 1.1
* C++, Ubuntu linux x64 (Must run on Windows/Mac as well)
* V. Olshevsky & F. Bacchini
* (c) 2015-2018

### Dependencies ###

* Boost library (http://www.boost.org/).
* sudo apt-get install libboost-all-dev
* CMake
* apt-get install cmake

### Installation instructions (Ubuntu) ###

* Clone the repository from bitbucket.
* mkdir build
* cd build
* cmake ../path-to-working-copy
* make
* ./Slurm KH.cfg

### Known issues ###

### Contribution guidelines ###

* 2 spaces for each nesting level. NO TABS!
* Precede each method with a commentary. A comment of a new method MUST begin with /**
* Please use readable and understandable names for variables, we're not in Fortran 77!

### Who do I talk to? ###

* Vyacheslav Olshevsky (slavik@kth.se)