#pragma once

#include "GridCommon.h"
#include "BoundaryConditions.h"
#include "Fields.h"
#include "InitialConditions.h"

// Defines to simplify cycling over the grid
#define for_cell_indices for (CellArray::index ic = 0; ic < ncx; ++ic) for (CellArray::index jc = 0; jc < ncy; ++jc) for (CellArray::index kc = 0; kc < ncz; ++kc)
#define for_cells for (GridCell *c = cells.data(); c < (cells.data() + cells.num_elements()); ++c)
#define for_node_indices for (NodeArray::index in = 0; in < nnx; ++in) for (NodeArray::index jn = 0; jn < nny; ++jn) for (NodeArray::index kn = 0; kn < nnz; ++kn)
#define for_nodes for (GridNode *n = nodes.data(); n < (nodes.data() + nodes.num_elements()); ++n)

/**
  Class Grid represents a rectilinear grid for the Slurm project.
  Connectivity is stored in the grid elements; we assume the connectivity doesn't change with time.

  Grid shape is [ncx+2][ncy+2][ncz+2] cells. 
  One layer on each side are ghost cells. Cell indexing therefore starts from -1 and ends at ncx.
  There are no ghost nodes.

  V. Olshevsky, Nov 2014, sya@mao.kiev.ua.
*/
class Grid: public GridCommon
{
private:

  /// Number of cells and nodes, normally nn = nc + 1.
  SlurmInt ncx, ncy, ncz, nnx, nny, nnz;
  /// Domain dimensions
  SlurmDouble Lx, Ly, Lz;
  /// Grid steps: dx = Lx/ncx
  //TODO: make them SlDoubleVector
  SlurmDouble dx, dy, dz;
  /// Inverted dx, dy, dz
  SlurmDouble dxInv, dyInv, dzInv;
  /// Contains node data
  NodeArray nodes;
  /// Contains cell data
  CellArray cells;
  /// Artificial viscosity represents the buk viscosity coefficient 
  ArtificialViscosityModes ArtificialViscosity;
  /// Kinematic shear viscosity mu, according to Kundu & Kohen "Fluid Mechanics" 5th edition (2012), page 113, eqns 4.31, 4.34, 4.36.
  SlurmDouble KinematicShearViscosity = 0;
  /// Kinematic bulk viscosity mu_v = lambda + 2/3mu. Usually it is a good idea to keep KinematicBulkViscosity = 0, and only use artificial, e.g., Kuropatenko, bulk viscosity. It is responsible for shock and sound handling.
  SlurmDouble KinematicBulkViscosity = 0;
  /// Particle volume evolution strategy
  VolumeEvolutionMethods VolumeEvolution;
  /// Total energy of particles
  SlurmDouble InternalEnergy = 0.;
  /// Lock of the internal energy
  omp_lock_t InternalEnergyLock;
  /// Total kinetic energy
  SlurmDouble KineticEnergy = 0.;
  /// Lock of the kinetic energy
  omp_lock_t KineticEnergyLock;
  /// Total energy of magnetic field
  SlurmDouble MagneticEnergy = 0.;
  /// Lock of the magnetic energy
  omp_lock_t MagneticEnergyLock;
  /// Whether magnetic field is present, or the system is purely hydrodynamic.
  bool FieldsEnabled = true;
  /// Whether to keep track of node neighbor particles
  bool TrackNodeNeighborParticles = true;

protected:

  /// Adjust (copy) neighbor particles in case of periodic BCs. Really, in other BCs it is trivial.  
  void adjustNodeNeighborParticles(GridNode * n);

  /// Boundary conditions. First 6 items define the walls of the rectangular box (bcx0, bcx1, bcy0, bcy1, bcz0, bcz1).
  vector<BoundaryCondition *> boundary_conditions;

  /// Initial condition
  InitialCondition * initial;

  /// Internal fields object
  Fields * fields;

public:

  // Property getters/setters
  /// Domain extent along X
  SlurmDouble get_Lx() { return Lx; }
  /// Domain extent along Y
  SlurmDouble get_Ly() { return Ly; }
  /// Domain extent along Z
  SlurmDouble get_Lz() { return Lz; }
  /// Cell extent (grid step) along X
  SlurmDouble get_dx() { return dx; }
  /// Cell extent (grid step) along Y
  SlurmDouble get_dy() { return dy; }
  /// Cell extent (grid step) along Z
  SlurmDouble get_dz() { return dz; }
  /// Number of cells along X
  SlurmInt get_ncx() { return ncx; }
  /// Number of cells along Y
  SlurmInt get_ncy() { return ncy; }
  /// Number of cells along Z
  SlurmInt get_ncz() { return ncz; }
  /// Number of nodes along X 
  SlurmInt get_nnx() { return nnx; }
  /// Number of nodes along Y 
  SlurmInt get_nny() { return nny; }
  /// Number of nodes along Z
  SlurmInt get_nnz() { return nnz; }
  /// Cell's center X coordinate
  SlurmDouble get_xc(SlurmInt ic, SlurmInt jc, SlurmInt kc) { return cells[ic][jc][kc].get_x(); }
  /// Cell's center Y coordinate
  SlurmDouble get_yc(SlurmInt ic, SlurmInt jc, SlurmInt kc) { return cells[ic][jc][kc].get_y(); }
  /// Cell's center Z coordinate
  SlurmDouble get_zc(SlurmInt ic, SlurmInt jc, SlurmInt kc) { return cells[ic][jc][kc].get_z(); }
  /// Returns the cell object by indices
  GridCell* get_cell(SlurmInt ic, SlurmInt jc, SlurmInt kc) { return &cells[ic][jc][kc]; }
  /// Fields getter
  Fields * get_fields() { return fields; }
  /// Initial conditions getter
  InitialCondition * get_initial() { return initial; }

  /// Get the BC with the specific index
  inline BoundaryCondition* get_bc(SlurmInt index) { return boundary_conditions[index]; }

  /// Add a new boundary condition to the end of the vector of BCs
  void add_bc(BoundaryCondition * bc) { boundary_conditions.push_back(bc); }

  /// Returns the number of boundary conditions in the domain
  inline SlurmInt get_bcCount() { return boundary_conditions.size(); }

  /// Get Left X boundary condition 
  BoundaryCondition* get_bcx0() { return get_bc(0); }

  /// Get Right X boundary condition
  BoundaryCondition* get_bcx1() { return get_bc(1); }

  /// Get Left Y boundary condition 
  BoundaryCondition* get_bcy0() { return get_bc(2); }

  /// Get Right Y boundary condition
  BoundaryCondition* get_bcy1() { return get_bc(3); }

  /// Get Left Z boundary condition
  BoundaryCondition* get_bcz0() { return get_bc(4); }

  /// Get Right Z boundary condition
  BoundaryCondition* get_bcz1() { return get_bc(5); }

  /// indices denote which neighbor should be set: 000, 100, 010, 110, etc.
  void set_NodeNeighborParticle(Particle* p, SlurmInt i, SlurmInt j, SlurmInt k, short dims);
  /// Return total energy summed over all cells 
  SlurmDouble get_InternalEnergy() { return InternalEnergy; }
  /// Return kinetic energy summed over all cells 
  SlurmDouble get_KineticEnergy() { return KineticEnergy; }
  /// Return total energy summed over all cells
  SlurmDouble get_MagneticEnergy() { return MagneticEnergy; }
  /// Return whether node neighbor particles should be tracked
  bool get_TrackNodeNeighborParticles() { return TrackNodeNeighborParticles; }
  /// Way the code evolves magnetic field
  VolumeEvolutionMethods get_VolumeEvolutionMethod() { return VolumeEvolution; }

  /// Locks and sets the value
  void set_InternalEnergy(SlurmDouble value);
  /// Locks and sets the value
  void set_MagneticEnergy(SlurmDouble value);
  /// Locks and sets the value
  void set_KineticEnergy(SlurmDouble value);
  /// Locks and increases the value
  void inc_InternalEnergy(SlurmDouble value);
  /// Locks and increases the value
  void inc_MagneticEnergy(SlurmDouble value);
  /// Locks and increases the value
  void inc_KineticEnergy(SlurmDouble value);

  // Numerical scheme

  /// Advance grid by one timestep 
  void advanceExplicit(SlurmDouble t, SlurmDouble dt);

  /// Normalize cell quantities interpolated from particles.
  void normalizeCells();
  /// Normalize node quantities after BCs are set.
  void normalizeNodes();
  /// Deal with empty cells and nodes
  void fillEmptyElements(); 
  /// Impose cell BC on the given quantity on all cells
  void imposeBCCells(unsigned long quants, SlurmDouble t = 0);
  /// Impose BC for the given quantity for all nodes
  void imposeBCNodes(unsigned long quants, SlurmDouble t);
  /// Adjust boundary velocities after they've been updated
  void adjustBCNodes(SlurmDouble t);
  /// Copy values to the ghost cells or impose periodicity on the nodes
  void imposeBoundaryConditions(SlurmDouble t);
  /// Assign proper values to bcx0, bcy1, bcz0...
  void initBoundaryConditions(ConfigFile * config, InitialCondition * initial);
  /// Store the quantities in the "old" ones before advancing the grid.
  void saveToOld();
  /// Copies necessary pointers to neighbor particles, e.g., after particles are initialized.
  void updateBoundaryNeighborParticles();
  /// Interpolate magnetic field according to the MF evolution strategy.
  void interpolateVectorPotential();
  /// Compute B from A; compute currents; impose the corresponding BC.
  void prepareCellFields(SlurmDouble t);

  /// Find node's neighboring cells
  void initNodeNeighbors(GridNode* n);
  /// Initialize nighbors in the begining of simulation
  void initCellNeighbors(GridCell* c);
  /// Initialize quadratic interpolation stuff
  void initCellsInterpolationStuff();

  /// The node closest to the given point 
  GridNode* getClosestNode(GridPoint* p);
  /// The cell including the given point
  GridCell* getClosestCell(GridPoint* p);

  /// Compute bulk and shear viscosity at each cell before cells are updated. Maybe this could move to the cell itself?
  void computeViscosity();
  /// Computes the divergence of B at all nodes
  void computeDivB();
  /// Check CFL condition
  SlurmDouble getCFL();

  // Auxilliary/convenience

  /// Fill cells with the specified value.
  void fillCells(SlurmDouble value, unsigned long quants);
  /// Fill nodes with the specified values
  void fillNodes(SlurmDouble value, unsigned long quants);
  /// Fill zeros for default node and cell quants
  void fillZeros();
  /// Look for the closest nodes and update their neighbor particles accordingly
  void updateParticleNodeNeighbors(Particle* p);

  // Input/output

  /// Return cell data and names in the form of a 4D boost multi_array for multiple quantities for a 3D grid.
  SlurmDoubleArray4D getCellScalarsArray(unsigned long quants, vector<string> *cell_names, bool export_ghost = false);
  /// Return node data in the form of a boost multi_array for multiple quantities for a 3D grid
  SlurmDoubleArray4D getNodeScalarsArray(unsigned long quants, vector<string> *node_names, bool export_ghost = false);

  /// Print node info.
  void printNodes();
  /// Print cell info.
  void printCells();
  /// Print both cell and node data
  void print();

  /// Create cells, nodes arrays and initialize them using initial condition and fields objects
  Grid(MFEvolutionMethods MFEvolution, ConfigFile * config, InitialCondition * initial, Fields * fields);

  /// Destructor
  ~Grid();
};

