#include "InitialConditions.h"

// For root finding necessary for Parker solar wind
#include <boost/math/tools/roots.hpp>
#include <tuple> // for std::tuple and std::make_tuple.
#include <boost/math/special_functions/cbrt.hpp> // For boost::math::cbrt.
#include <limits> //for std::numeric_limits;

using namespace std;

InitialCondition * InitialCondition::makeInitial(ConfigFile * config)
{
  string testcase = config->read <string>("testcase");
  if (boost::iequals(testcase, "OrszagTang")) return new InitialConditionOT(config);
  else if (boost::iequals(testcase, "OrszagTang3D")) return new InitialConditionOT3D(config);
  else if (boost::iequals(testcase, "Perturbation")) return new InitialConditionPerturbation(config);
  else if (boost::iequals(testcase, "KelvinHelmholtz")) return new InitialConditionKH(config);
  else if (boost::iequals(testcase, "MagneticLoop")) return new InitialConditionML(config);
  else if (boost::iequals(testcase, "MagneticLoop3D")) return new InitialConditionML3D(config);
  else if (boost::iequals(testcase, "RayleighTaylor")) return new InitialConditionRT(config);
  else if (boost::iequals(testcase, "Reconnection")) return new InitialConditionMR(config);
  else if (boost::iequals(testcase, "Implosion")) return new InitialConditionImplosion(config);
  else if (boost::iequals(testcase, "Shock1D")) return new InitialConditionShock1D(config);
  else if (boost::iequals(testcase, "SphBlast")) return new InitialConditionSB(config);
  else if (boost::iequals(testcase, "ShrinkingSheet")) return new InitialConditionSS(config);
  else if (boost::iequals(testcase, "TaylorGreen")) return new InitialConditionTG(config);
  else if (boost::iequals(testcase, "InletOutlet")) return new InitialConditionIO(config);
  else if (boost::iequals(testcase, "FanGibson")) return new InitialConditionFG(config);
  else if (boost::iequals(testcase, "FluxRope")) return new InitialConditionFR(config);
  else if (boost::iequals(testcase, "SolarWind")) return new InitialConditionSW(config);
  else if (boost::iequals(testcase, "SolarWind1")) return new InitialConditionSW1(config);
  else if (boost::iequals(testcase, "Test")) return new InitialConditionTest(config);
  else if (boost::iequals(testcase, "BrioWu")) return new InitialConditionBrioWu(config);
  else if (boost::iequals(testcase, "TwoBlasts")) return new InitialConditionTB(config);
  else if (boost::iequals(testcase, "TwoBlastsPeriodic")) return new InitialConditionTBPeriodic(config);
  else if (boost::iequals(testcase, "DoubleHarris")) return new InitialConditionDH(config);
  else 
  {
    message("ERROR: Initial condition is undefined!\n\n");
    throw SLURM_ERROR_WRONG_IC;
  }
}

// Base class InitialCondition

/** Initializes particle data from the surrounding grid using the same interpolation as in the main grid->particles exchange */
void InitialCondition::initParticleInterpolate(Particle * p, Interpolator * interpolator) 
{
  GridCell *c = interpolator->interpolateCells(p, QUANT_RHO | QUANT_ENERGY);
  GridNode *n = interpolator->interpolateNodes(p, QUANT_VX | QUANT_VY | QUANT_VZ | QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ);    

  p->set_m(c->get_rho() * p->get_vol());
  p->set_e(c->get_e() * p->get_m());
  for (SlurmInt i = 0; i < 3; i++)
  {
    p->set_u(i, n->get_u(i));
    p->set_A(i, n->get_A(i));
  }
  p->set_color(0);

  delete c;
  delete n;    
}

/// Default initialization is based on advection of the grid quantities to the particle
void InitialCondition::initParticle(Particle * p, Interpolator * interpolator) 
{
  initParticleInterpolate(p, interpolator);
}

/// Initialization of injected particles
void InitialCondition::initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t) 
{
  initParticleInterpolate(p, interpolator);
}

/** 
  By default, uniform density and pressure is set
*/
void InitialCondition::initCell(GridCell * c)
{
  // density, momentum, energy
  c->set_rho(rho0);
  c->set_p(p0);
  c->set_e(p0 / (GasGamma_1 * rho0));
};

/// Constructor
InitialCondition::InitialCondition(ConfigFile * config)
{
  Lxyz[0] = config->read<SlurmDouble>("Lx");
  Lxyz[1] = config->read<SlurmDouble>("Ly");
  Lxyz[2] = config->read<SlurmDouble>("Lz");

  rho0 = config->read<SlurmDouble>("rho0", rho0);
  p0 = config->read<SlurmDouble>("p0", p0);
  set_GasGamma(config->read<SlurmDouble>("gamma", GasGamma));

  // Gravity
  GravityAcceleration[0] = config->read<SlurmDouble>("gravityX", 0.);
  GravityAcceleration[1] = config->read<SlurmDouble>("gravityY", 0.);
  GravityAcceleration[2] = config->read<SlurmDouble>("gravityZ", 0.);
  
  IsothermalSoundSpeedSq = p0 / rho0;
  AdiabaticSoundSpeedSq = GasGamma * IsothermalSoundSpeedSq;

  u0 = config->read<SlurmDouble>("u0", u0);
  B0 = config->read<SlurmDouble>("B0", B0);

  name = config->read <string>("testcase");
  messageline("Initial condition (testcase): ", name);
  messageline("");
  messageline("Default physical parameters:");
  messageline("rho0 = ", rho0);
  messageline("p0 = ", p0);
  messageline("Adiabatic Sound Speed = ", pow(AdiabaticSoundSpeedSq, 0.5));
  messageline("Isothermal Sound Speed = ", pow(IsothermalSoundSpeedSq, 0.5));
  messageline("gamma  = ", GasGamma);
  messageline("u0 = ", u0);
  messageline("B0 = ", B0);  
  message("Gravity = ");
  for (int i = 0; i < 3; ++i) message(GravityAcceleration[i], " ");
  messageline("\n");
}

/** Also change the gamma - 1 */
void InitialCondition::set_GasGamma(SlurmDouble value)
{
  GasGamma = value;
  GasGamma_1 = value - 1;
}

/** 
  By default, constant gravity is applied to all nodes.
*/
void InitialCondition::addGravity(GridNode * n, SlurmDouble dt)
{
  n->inc_u(dt * GravityAcceleration[0], dt * GravityAcceleration[1], dt * GravityAcceleration[2]);
}

/** Isothermal EOS */
void InitialCondition::computePressureIsothermal(GridCell * c)
{
  c->set_p(c->get_rho() * IsothermalSoundSpeedSq);
}

/** Adiabatic EOS */
void InitialCondition::computePressureAdiabatic(GridCell * c)
{
  c->set_p(c->get_e() * GasGamma_1 * c->get_rho());
}


// Inlet/outlet

InitialConditionIO::InitialConditionIO(ConfigFile * config) : InitialCondition(config)
{
  InjectSpeed = config->read<SlurmDouble>("InjectSpeed", u0);
  u0 = config->read<SlurmDouble>("u0", u0);
  p0 = config->read<SlurmDouble>("p0", p0);
  rho0 = config->read<SlurmDouble>("rho0", rho0);
  // dxp / u0
  InjectPeriod = get_Lx() / config->read<SlurmInt>("GridCellsX") / config->read<SlurmDouble>("ParticlesPerCellX") / InjectSpeed;
  LastInjectTime = 0.;
  // Should only shift by a half of a particle before the first new population pops up
  NextInjectTime = 0.5 * InjectPeriod;
  InjectPosition = 0.;
  messageline("Inlet/outlet initial condition parameters:");
  messageline("InjectSpeed = ", InjectSpeed);
  messageline("InjectPosition = ", InjectPosition);
}

/** Additionally, mass? */
void InitialConditionIO::initCell(GridCell * c)
{
  InitialCondition::initCell(c);
  c->set_m(rho0 * c->get_vol());
}

/** Only velocity? */
void InitialConditionIO::initNode(GridNode * n) 
{
  n->set_m(rho0 * n->get_vol());
  n->set_u(u0, 0, 0);
}

/** Only velocity? */
void InitialConditionIO::initNode(GridNode * n, SlurmDouble t) 
{
  n->set_m(rho0 * n->get_vol());
  n->set_u(u0, 0, 0);
}

/** In the beginning, all particles have the same color */
void InitialConditionIO::initParticle(Particle * p, Interpolator * interpolator) 
{
  p->set_m(rho0 * p->get_vol());
  computeEnergy(p, p0);

  p->set_u(u0, 0, 0);
  // Two colors
  p->set_color(0);
}

/** Injected particles have a different color */
void InitialConditionIO::initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t) 
{
  // Because the boundary layer is twice lighter?
  p->set_m(rho0 * p->get_vol());
  computeEnergy(p, p0);

  p->set_u(InjectSpeed, 0, 0);  

  // Set the coordinate if the bastard needs to appear inside the domain
  //p->x = (t - LastInjectTime) * InjectSpeed;
  p->set_x(InjectPosition);

  // Two colors
  p->set_color(1);
}

/** 
  LastInjectTime is the time when last injection has happened.
  This checks whether it is time to inject particles and also sets their initial position.

*/
bool InitialConditionIO::checkInjectMoment(SlurmDouble t) 
{
  bool res = false;
  if (t >= NextInjectTime)
  {
    InjectPosition = InjectSpeed * (t - NextInjectTime);
    res = true;
  }
  return res;
}

/** 
  Call this method only after the particles have been injected.
  Otherwise they will be initialized with a wrong X coordinate!
  t is the time of _this_ injection, current time.
*/
void InitialConditionIO::resetInjectMoment(SlurmDouble t)
{
  LastInjectTime = t;
  NextInjectTime = t + InjectPeriod - InjectPosition / InjectSpeed;
}

// Fan & Gibson

/** 
  Arcade parameters from the "CME injection challenge" of SWIFF and Soteria projects.
    tg(Theta) == By0/Bx0 = alpha/L
*/
InitialConditionFG::InitialConditionFG(ConfigFile * config) : InitialConditionIO(config)
{    

  InjectPeriod = get_Lz() / config->read<SlurmInt>("GridCellsZ") / config->read<SlurmDouble>("ParticlesPerCellZ") / InjectSpeed;

  // Arcade parameters from the "CME injection challenge" of SWIFF and Soteria projects.
  // tg(Theta) == By0/Bx0 = alpha/L
  /*
  // Original set of parameters derived by me and Fabio from CME challenge
  K = PI / get_Ly();
  L = ((2. / get_Lz()) < K) ? (2. / get_Lz()) : K;
  alpha = pow((K*K - L*L), 0.5);
  */

  // Torus parameters
  // Field amplitude
  Bt = 9 * B0;
  // Normalization factor inside the exponent of the loop. Basically, toroidal loop radius.
  a = config->read<SlurmDouble>("a", 0.1);
  // Radius of the torus, I guess
  Rg = config->read<SlurmDouble>("Rg", 0.375);
  // wat is tis?
  q = config->read<SlurmDouble>("Q", -1);

  // Center of the torus
  xc = 0.5 * get_Lx();
  yc = 0.5 * get_Ly();
  // Initially, loop is under the bottom. However, in F&G paper it is at -0.675.
  zc = -Rg - 3 * a;  

  // Set of parameters for non-twisted periodic in both Y and X arcade like in F&G
  K = PI / get_Ly();     // pi
  L = K; //((2. / get_Lz()) < K) ? (2. / get_Lz()) : K;
  alpha = 0; //pow((K*K - L*L), 0.5);

  messageline("Force-free arcade parameters:");
  messageline("k = ", K);
  messageline("l = ", L);
  messageline("a = ", alpha);
}

/** 
  By default, uniform density and pressure is set
*/
void InitialConditionFG::initCell(GridCell * c)
{
  // density, momentum, energy
  c->set_rho(rho0);
  c->set_p(p0);
  c->set_e(p0 / (GasGamma - 1) / rho0);
  c->set_m(rho0 * c->get_vol());
  // B to be computed later using ghost nodes
  
};

/** 
  A sheared arcade from Markus Ashwanden's book "Physics of the Solar Corona"
  See equation (5.3.17).
  Bx = Bx0*sin(kx)*exp(-lz)
  By = By0*sin(kx)*exp(-lz)
  Bz = B0*cos(kx)*exp(-lz)   -> Bz0 == B0

  Where
  Bx0 = l/k*B0
  By0 = a/k*B0
  k**2 - l**2 - a**2 = 0
  tan(Theta) = a/l = By0/Bx0

  In terms of vector potential, let's set Ax = 0 then it is easy to derive
  Ax = 0
  Ay = B0/k * sin(kx) * exp(-lz)
  Az = a/k**2 * B0 * cos(kx) * exp(-lz)
  
*/
void InitialConditionFG::initNode(GridNode * n) 
{
  n->set_m(rho0 * n->get_vol());
  n->set_u(0, 0, u0);
  
  /*
  // Original setup derived by me and Fabio from the Soteria/Swiff CME challenge.
  n->set_A(0, B0 / K * sin(K * n->get_x()) * exp(-L * n->get_z()), alpha / K / K * B0 * cos(K * n->get_x()) * exp(-L * n->get_z()));
  */
  
  // Arcade periodic in X and Y; no twist, like in the paper by F&G. Bloody MF is NOT PERIODIC!
  n->set_A(B0 / K * sin(K * n->get_y()) * exp(-L * n->get_z()), 0, alpha / K / K * B0 * cos(K * n->get_x()) * exp(-L * n->get_z()));
}

/// Add arcade
void InitialConditionFG::initNode(GridNode * n, SlurmDouble t) 
{
  // Coordinates of the node relative to the emerging loop's center
  SlurmDouble x = n->get_x() - 0.5 * get_Lx();
  SlurmDouble y = n->get_y() - 0.5 * get_Ly();
  SlurmDouble z = n->get_z() - (zc + u0 * t);
 
  // Squared distance to the center of the torus
  SlurmDouble r2 = pow(x, 2) + pow(y, 2) + pow(z, 2);
  SlurmDouble r = pow(r2, 0.5);

  SlurmDouble CosTheta = y / (r + 1.e-10);
  SlurmDouble SinTheta = pow(1 - CosTheta * CosTheta, 0.5);

  // Omega is the distance to the axis of the toroidal tube.
  SlurmDouble Omega2 = r2 + Rg*Rg - 2. * r * Rg * SinTheta * SinTheta;
  SlurmDouble Omega = pow(Omega2, 0.5);
 
  if (Omega < 3 * a)
  {
    n->set_u(0, 0, u0);
  }
  else
    n->set_u(0, 0, u0);
}


/** Sheared arcade, see initNode() */
void InitialConditionFG::initParticle(Particle * p, Interpolator * interpolator) 
{
  p->set_m(rho0 * p->get_vol());
  computeEnergy(p, p0);
  p->set_u(0, 0, 0);
  
  /*
  // Original setup derived by me and Fabio
  p->set_A(0, B0 / K * sin(K * p->get_x()) * exp(-L * p->get_z()), alpha / K / K * B0 * cos(K * p->get_x()) * exp(-L * p->get_z()));
  */
  
  //p->set_A(0, 0, 0);
  // Arcade periodic in X and Y; no twist, like in the paper by F&G. Bloody MF is NOT PERIODIC!
  p->set_A(B0 / K * sin(K * p->get_y()) * exp(-L * p->get_z()), 0, alpha / K / K * B0 * cos(K * p->get_x()) * exp(-L * p->get_z()));
}

/** Injected particles have a different color */
void InitialConditionFG::initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t) 
{
  // Because the boundary layer is twice lighter?
  p->set_m(rho0 * p->get_vol());
  computeEnergy(p, p0);

  p->set_u(0, 0, u0);  

  // Set the coordinate if the bastard needs to appear inside the domain
  //p->x = (t - LastInjectTime) * InjectSpeed;
  p->set_z(InjectPosition);

  // Two colors
  if (t > 0)
    p->set_color(1);

  // Potential arcade field
  p->set_A(B0 / K * sin(K * p->get_y()) * exp(-L * p->get_z()), 0, alpha / K / K * B0 * cos(K * p->get_x()) * exp(-L * p->get_z()));  

  // Add some more vector potential.

  // Coordinates of the particle ralitive to the emerging loop's center
  SlurmDouble x = p->get_x() - 0.5 * get_Lx();
  SlurmDouble y = p->get_y() - 0.5 * get_Ly();
  SlurmDouble z = p->get_z() - (zc + u0 * t);

  /**
    The polar axis is along Y axis in this case.
    Also, Theta is mixed up with Phi here, in comparison to 
    http://mathworld.wolfram.com/SphericalCoordinates.html

    In Wolfram (r, Theta, Phi), r = (x**2 + y**2 + z**2)**0.5, tanTheta = y/x, cosPhi = z/r.
      x = r cosTheta sinPhi
      y = r sinTheta sinPhi
      z = r cosPhi
    
    Line element
      ds = [dr, r*dPhi, r*sinPhi*dTheta]

    Hence, in our notation [r, Phi, Theta]
      x = r sinTheta cosPhi
      y = r cosTheta
      z = r sinTheta sinPhi
      ds = [dr, r*dTheta, r*sinTheta*dPhi]

    The gradient (in our notation, [r, Phi, Theta]) is
      nabla = [d/dr, 1/(r*sinTheta)*d/dPhi, 1/r*d/dTheta]

  */
  // Squared distance to the center of the torus
  SlurmDouble r2 = pow(x, 2) + pow(y, 2) + pow(z, 2);
  SlurmDouble r = pow(r2, 0.5);

  SlurmDouble CosTheta = y / (r + 1.e-10);
  SlurmDouble SinTheta = pow(1 - CosTheta * CosTheta, 0.5);

  SlurmDouble CosPhi = x / (r * SinTheta + 1e-10);
  SlurmDouble SinPhi = z / (r * SinTheta + 1e-10);

  // Omega is the distance to the axis of the toroidal tube.
  SlurmDouble Omega2 = r2 + Rg*Rg - 2. * r * Rg * SinTheta * SinTheta;
  SlurmDouble Omega = pow(Omega2, 0.5);

  //p->inc_A(0.5 * q * a * a * Bt * exp(-Omega2 / (a*a)));


  // Partial derivatives of the torus vector potential
  SlurmDouble dAdOmega = -q * Bt * Omega * exp(-Omega2 / (a*a));
  // dOmega / dTheta
  SlurmDouble pOmpth = -2. / Omega * r * Rg * SinTheta * CosTheta;
  // dOmega / dr
  SlurmDouble pOmpr = 1. / Omega * (r - Rg * SinTheta * SinTheta);
  
  SlurmDouble Bphi = a * Bt / (r * SinTheta + 1e-10) * exp(-Omega2 / (a*a));
  SlurmDouble Br = dAdOmega * pOmpth / (r2 * SinTheta + 1e-10);
  SlurmDouble Btheta = -dAdOmega * pOmpr / (r * SinTheta + 1e-10);

  /**
  // Field as defined in FlipMHD setup
  bx_ini = -Bphi * SinPhi +  Btheta * CosTheta * CosPhi + Br * SinTheta * CosPhi;
  by_ini = -Btheta * SinTheta + Br * CosTheta - exp(-0.0625 * (n->get_z() - zc) / (a * a));  // What is the exponent here???
  bz_ini = Bphi * CosPhi + Btheta * CosTheta * SinPhi + Br * SinTheta * SinPhi;
  */

  
}


// Orszag-Tang

void InitialConditionOT::initNode(GridNode * n)
{
  n->set_u(-sin(2. * PI * n->get_y()), sin(2. * PI * n->get_x()), 0);
  n->set_A(0, 0, 1. / sqrt(4. * PI) * (cos(4. * PI * n->get_x()) / (4. * PI) + cos(2. * PI * n->get_y()) / (2. * PI)));
}

/// Default initialization is based on advection of the grid quantities to the particle
void InitialConditionOT::initParticle(Particle * p, Interpolator * interpolator) 
{
  p->set_m(rho0 * p->get_vol());
  computeEnergy(p, p0);
  p->set_u(-sin(2. * PI * p->get_y()), sin(2. * PI * p->get_x()), 0);
  p->set_A(0, 0, 1. / sqrt(4. * PI) * (cos(4. * PI * p->get_x()) / (4. * PI) + cos(2. * PI * p->get_y()) / (2. * PI)));
}

///
void InitialConditionOT3D::initNode(GridNode * n)
{
  n->set_u(-2. * sin(2. * PI * n->get_y()), 2. * sin(2. * PI * n->get_x()), 0);
  n->set_A(1. / sqrt(4. * PI) * (-cos(2. * PI * n->get_z()) / (2. * PI) + cos(2. * PI * n->get_y()) / (2. * PI)),
           1. / sqrt(4. * PI) * (cos(2. * PI * n->get_z()) / (2. * PI) - cos(2. * PI * n->get_x()) / (2. * PI)),
           1. / sqrt(4. * PI) * (-2. * cos(4. * PI * n->get_y()) / (4. * PI) + 2. * cos(2. * PI * n->get_x()) / (2. * PI)));
}


// Perturbation

InitialConditionPerturbation::InitialConditionPerturbation(ConfigFile * config) : InitialCondition(config)
{
  rho1 = config->read<SlurmDouble>("rho1", 2. * rho0);
  i0 = config->read<SlurmInt>("GridCellsX") / 2;
  j0 = config->read<SlurmInt>("GridCellsY") / 2;
  k0 = config->read<SlurmInt>("GridCellsZ") / 2;
}

void InitialConditionPerturbation::initCell(GridCell * c)
{
  // density, momentum, energy
  c->set_rho(rho0);
  c->set_p(p0);
  c->set_e(p0 / (get_GasGamma() - 1) / rho0);

  // Perturbation
  if ((c->get_i() == i0) && (c->get_j() == j0) && (c->get_k() == k0)) 
    c->set_rho(rho1);
}

// Kelvin-Helmholtz

InitialConditionKH::InitialConditionKH(ConfigFile * config) : InitialCondition(config)
{
  rho1 = config->read<SlurmDouble>("rho1", rho1);
  rhom = 0.5 * (rho0 - rho1);
  L = config->read<SlurmDouble>("L0", L);
  messageline("Kelvin-Helmholtz parameters");
  messageline("rho1 = ", rho1);
  messageline("L = ", L);
}

void InitialConditionKH::initCell(GridCell * c)
{
  c->set_p(p0);

  // Athena
  //if ((c->get_y() < 0.25 * get_Ly()) || (c->get_y() > 0.75 * get_Ly())) c->rho = rho0;
  //else c->rho = rho1;

  // McNally
  SlurmDouble rho;
  if (c->get_y() < 0.25 * get_Ly()) 
    rho = rho0 - rhom * exp((c->get_y() - 0.25) / L);
  else if ((c->get_y() >= 0.25 * get_Ly()) && (c->get_y() < 0.5 * get_Ly())) 
    rho = rho1 + rhom * exp((-c->get_y() + 0.25) / L);
  else if ((c->get_y() >= 0.5 * get_Ly()) && (c->get_y() < 0.75 * get_Ly())) 
    rho = rho1 + rhom * exp((c->get_y() - 0.75) / L);
  else 
    rho = rho0 - rhom * exp((-c->get_y() + 0.75) / L);

  c->set_rho(rho);
  c->set_e(p0 / (get_GasGamma() - 1) / rho);
}

void InitialConditionKH::initNode(GridNode * n) 
{
  // Athena
	//if ((n->get_y() < 0.25 * get_Ly()) || (n->get_y() > 0.75 * get_Ly())) n->vx = -u0 + 1e-2 * (((SlurmDouble) rand()) / RAND_MAX - 0.5);
  //else n->vx = u0 + 1e-2 * (((SlurmDouble) rand()) / RAND_MAX - 0.5);
  //n->vy = 1e-2 * (((SlurmDouble) rand()) / RAND_MAX - 0.5);

  // McNally
  SlurmDouble ux;
  if (n->get_y() < 0.25 * get_Ly()) 
    ux = u0 * (1. - exp((n->get_y() - 0.25) / L));
  else if ((n->get_y() >= 0.25 * get_Ly()) && (n->get_y() < 0.5 * get_Ly())) 
    ux = u0 * (-1. + exp((-n->get_y() + 0.25) / L));
  else if ((n->get_y() >= 0.5 * get_Ly()) && (n->get_y() < 0.75 * get_Ly())) 
    ux = u0 * (-1. + exp((n->get_y() - 0.75) / L));
  else 
    ux = u0 * (1. - exp((-n->get_y() + 0.75) / L));

  n->set_u(ux, 0.01 * sin(4. * PI * n->get_x() / get_Lx()), 0);

  // Velocity in Z
  /*
  SlurmDouble uz;
  if (n->get_y() < 0.25 * get_Ly()) 
    uz = u0 * (1. - exp((n->get_y() - 0.25) / L));
  else if ((n->get_y() >= 0.25 * get_Ly()) && (n->get_y() < 0.5 * get_Ly())) 
    uz = u0 * (-1. + exp((-n->get_y() + 0.25) / L));
  else if ((n->get_y() >= 0.5 * get_Ly()) && (n->get_y() < 0.75 * get_Ly()))
    n->uz = u0 * (-1. + exp((n->get_y() - 0.75) / L));
  else 
    n->uz = u0 * (1. - exp((-n->get_y() + 0.75) / L));

  n->set_u(0, 0.01 * sin(4. * PI * n->get_z() / get_Lz()), uz);
  */
}

/** Three particle colors */
void InitialConditionKH::initParticle(Particle * p, Interpolator * interpolator) 
{
  initParticleInterpolate(p, interpolator);

  SlurmDouble ux;
  if (p->get_y() < 0.25 * get_Ly()) 
    ux = u0 * (1. - exp((p->get_y() - 0.25) / L));
  else if ((p->get_y() >= 0.25 * get_Ly()) && (p->get_y() < 0.5 * get_Ly())) 
    ux = u0 * (-1. + exp((-p->get_y() + 0.25) / L));
  else if ((p->get_y() >= 0.5 * get_Ly()) && (p->get_y() < 0.75 * get_Ly())) 
    ux = u0 * (-1. + exp((p->get_y() - 0.75) / L));
  else 
    ux = u0 * (1. - exp((-p->get_y() + 0.75) / L));
  p->set_u(ux, 0.01 * sin(4. * PI * p->get_x() / get_Lx()), 0);

  short color;
  if (p->get_y() < 0.25 * get_Ly()) 
    color = 0;
  else if ((p->get_y() >= 0.25 * get_Ly()) && (p->get_y() < 0.75 * get_Ly())) 
    color = 1;
  else 
    color = 2;
  p->set_color(color);
}


// Magnetic loop

InitialConditionML::InitialConditionML(ConfigFile * config) : InitialCondition(config)
{
  LoopRadius = config->read<SlurmDouble>("L0", LoopRadius);
  B0 = config->read<SlurmDouble>("B0", B0);
    
  u0[0] = config->read<SlurmDouble>("u0x", u0[0]);
  u0[1] = config->read<SlurmDouble>("u0y", u0[1]);
  u0[2] = 0;
}

SlurmDouble InitialConditionML::computeAz(GridPoint * p)
{  
  // Classical 2D setting
  SlurmDouble Az = B0 * (LoopRadius - sqrt(pow(p->get_x() / get_Lx() - 0.5, 2) + pow(p->get_y() / get_Ly()- 0.5, 2)));
  if (Az < 0) 
    Az = 0.;
  return Az;
}

void InitialConditionML::initNode(GridNode * n) 
{
  n->set_u(u0);
  // Classical 2D case
  n->set_A(0, 0, computeAz(n));
}

/** Default initialization is based on advection of the grid quantities to the particle */
void InitialConditionML::initParticle(Particle * p, Interpolator * interpolator) 
{
  p->set_m(rho0 * p->get_vol());
  computeEnergy(p, p0);
  p->set_u(u0);
  p->set_A(0, 0, computeAz(p));
}


// 3D Magnetic loop

InitialConditionML3D::InitialConditionML3D(ConfigFile * config) : InitialCondition(config)
{
  LoopRadius = config->read<SlurmDouble>("L0", LoopRadius);
  B0 = config->read<SlurmDouble>("B0", B0);

  u0[0] = config->read<SlurmDouble>("u0x", u0[0]);
  u0[1] = config->read<SlurmDouble>("u0y", u0[1]);
  u0[2] = config->read<SlurmDouble>("u0z", u0[2]);

  // Axis of the flux tube
  A1 = new GridPoint();
  A2 = new GridPoint();  
  // Axis lies in the XZ plane
  y0 = 0.5 * get_Ly();
  
  messageline("3D magnetic loop parameters:");
  messageline("Loop radius = ", LoopRadius);
  message("u0 = ", u0[0]);
  message(", ", u0[1]);
  messageline(", ", u0[2]);
}

/**
  When distance to the loop axis is less than R0,
    Ax = Az = A0 * (R0 - dist)
  The problem is triple-periodic, therefore in addition to the XZ diagonal, there are two other (parallel_) pieces.
  
*/
SlDoubleVector InitialConditionML3D::computeA(GridPoint * p)
{
  SlDoubleVector A = {0, 0, 0};

  // The loop is inclined by 45 deg; Az = Ax, and Ay = 0.
  // 3D inclined loop in the middle of the box
  SlDoubleVector A1 = {0, y0, 0};
  SlDoubleVector A2 = {get_Lx(), y0, get_Lz()};

  // Distance to the loop axis
  SlurmDouble d = p->distanceToLine(A1, A2);
  if (d < LoopRadius) 
    A[2] = LoopRadius - d;

  // Impose periodicity (infinitely long loop): add two more pieces at the top and bottom edges
  A1[0] = -get_Lx();
  A1[2] = 0;
  A2[0] = 0;
  A2[2] = get_Lz();
  d = p->distanceToLine(A1, A2);
  if (d < LoopRadius) 
    A[2] = LoopRadius - d;

  // Last diagonal
  A1[0] = 0;
  A1[2] = -get_Lz();
  A2[0] = get_Lx();
  A2[2] = 0;
  d = p->distanceToLine(A1, A2);
  if (d < LoopRadius)
    A[2] = LoopRadius - d;

  // Now, loop inclined by 45 deg
  A[2] = A[2] * B0 * pow(2., -0.5);
  A[0] = A[2];
  return A;
}

void InitialConditionML3D::initNode(GridNode * n) 
{
  n->set_u(u0);
  // The loop is inclined by 45 deg; Az = Ax, and Ay = 0.
  n->set_A(computeA(n));
}

/** Default initialization is based on advection of the grid quantities to the particle */
void InitialConditionML3D::initParticle(Particle * p, Interpolator * interpolator) 
{
  p->set_m(rho0 * p->get_vol());
  computeEnergy(p, p0);
  p->set_u(u0);

  p->set_A(computeA(p));
}

InitialConditionML3D::~InitialConditionML3D()
{
  delete A1;
  delete A2;
}


// Rayleigh-Taylor

InitialConditionRT::InitialConditionRT(ConfigFile * config) : InitialCondition(config)
{
  messageline("Rayleigh-Taylor parameters");
  messageline("u0 = ", u0);
}

void InitialConditionRT::initCell(GridCell * c)
{
/*
  // XY plane
  if (c->get_y() > 0.5 * get_Ly()) 
    c->set_rho(2 * rho0);
  else 
    c->set_rho(rho0);

  c->set_p(p0 - GravityAcceleration[1] * c->get_rho() * (c->get_y() - 0.75 * get_Ly()));
  c->set_e(c->get_p() / (get_GasGamma() - 1) / c->get_rho());
*/


  // XZ plane
  if (c->get_z() > 0.5 * get_Lz()) 
    c->set_rho(2 * rho0);
  else 
    c->set_rho(rho0);

  c->set_p(p0 - GravityAcceleration[2] * c->get_rho() * (c->get_z() - 0.75 * get_Lz()));
  c->set_e(c->get_p() / (get_GasGamma() - 1) / c->get_rho());


/*
  // YZ plane
  if (c->get_y() > 0.5 * get_Ly()) 
    c->set_rho(2 * rho0);
  else 
    c->set_rho(rho0);

  c->set_p(p0 - GravityAcceleration[1] * c->get_rho() * (c->get_y() - 0.75 * get_Ly()));
  c->set_e(c->get_p() / (get_GasGamma() - 1) / c->get_rho());
*/
}

void InitialConditionRT::initNode(GridNode * n) 
{

  // XY
  //n->set_u(0, u0 * 0.125 * (1 + cos(4. * PI * (n->get_x() - 0.5 * get_Lx()))) * (1 + cos(3. * PI * (n->get_y() - 0.5 * get_Ly()))) * (1 + cos(4. * PI * (n->get_z() - 0.5 * get_Lz()))), 0);

  // XZ
  n->set_u(0, 0, u0 * 0.125 * (1 + cos(4. * PI * (n->get_x() - 0.5 * get_Lx()))) * (1 + cos(4. * PI * (n->get_y() - 0.5 * get_Ly()))) * (1 + cos(3. * PI * (n->get_z() - 0.5 * get_Lz()))));

  // XZ multi-mode
  //n->set_u(0, 0, (2 * (((SlurmDouble) rand()) / RAND_MAX - 0.5)) * u0 * 0.5 * (1 + cos(8./3. * PI * (n->get_z() - 0.5) / get_Lz())));

  // YZ
  //n->set_u(0, u0 * 0.125 * (1 + cos(4. * PI * (n->get_x() - 0.5 * get_Lx()))) * (1 + cos(3. * PI * (n->get_y() - 0.5 * get_Ly()))) * (1 + cos(4. * PI * (n->get_z() - 0.5 * get_Lz()))), 0);
}

/** Two particle colors? */
void InitialConditionRT::initParticle(Particle * p, Interpolator * interpolator) 
{
  initParticleInterpolate(p, interpolator);
  //p->set_uy(0, u0 * 0.125 * (1 + cos(4. * PI * (p->get_x() - 0.5 * get_Lx()))) * (1 + cos(3. * PI * (p->get_y() - 0.5 * get_Ly()))) * (1 + cos(4. * PI * (p->get_z() - 0.5 * get_Lz()))), 0);

  // Two colors
  short color;
  if (p->get_y() > 0.5 * get_Ly()) 
    color = 1;
  else 
    color = 2;
  p->set_color(color);
}


// Magnetic Reconnection

void InitialConditionMR::initNode(GridNode * n) 
{
  n->set_u(u0 * sin(2. * PI * (n->get_y() - 0.5)), 0, 0);

  SlurmDouble Az = -1. * n->get_x();
  if ((n->get_x() >= 0.25 * get_Lx()) && (n->get_x() < 0.75 * get_Lx())) 
    Az = -0.25 + 1. * (n->get_x() - 0.25 * get_Lx());
  if (n->get_x() >= 0.75 * get_Lx())
    Az= 0.25 - 1. * (n->get_x() - 0.75 * get_Lx());    
  n->set_A(0, 0, Az);
}


// Implosion

void InitialConditionImplosion::initCell(GridCell * c)
{
  if (c->get_x() + c->get_y() > 0.5 * get_Lx())
  {
    c->set_rho(1.0);
    c->set_p(1.0);
  }    
  else
  {
    c->set_rho(rho0);
    c->set_p(p0);
  }

  c->set_e(c->get_p() / (get_GasGamma() - 1.) / c->get_rho());
};

void InitialConditionImplosion::initNode(GridNode * n) 
{
  n->set_u(0, 0, 0);
};


// 1D hydro shock

void InitialConditionShock1D::initCell(GridCell * c)
{
  c->set_p(p0);

  if (c->get_x() > 0.5 * get_Lx())
    c->set_rho(0.1 * rho0);
  else
    c->set_rho(rho0);

  c->set_e(p0 / (get_GasGamma() - 1.) / c->get_rho());
}

void InitialConditionShock1D::initNode(GridNode * n) 
{
  n->set_u(0, 0, 0);
}


// Spherical Blast

void InitialConditionSB::initCell(GridCell * c)
{
  c->set_rho(rho0);
  c->set_p(p0);
  if (pow(pow((c->get_x() - 0.5 * get_Lx()), 2.) + pow((c->get_y() - 0.5 * get_Ly()), 2.) + pow((c->get_z() - 0.5 * get_Lz()), 2.), 0.5) < 0.1)
    c->set_p(100 * p0);
  c->set_e(c->get_p() / (get_GasGamma() - 1.) / rho0);
}

void InitialConditionSB::initNode(GridNode * n) 
{
  n->set_u(0, 0, 0);
}

void InitialConditionSB::initParticle(Particle * ptcl, Interpolator * interpolator) 
{
  ptcl->set_u(0, 0, 0);
  ptcl->set_m(rho0 * ptcl->get_vol());

  SlurmDouble press = p0;
  if (pow(pow((ptcl->get_x() - 0.5 * get_Lx()), 2.) + pow((ptcl->get_y() - 0.5 * get_Ly()), 2.) + pow((ptcl->get_z() - 0.5 * get_Lz()), 2.), 0.5) < 0.1)
    press = 100 * p0;
  computeEnergy(ptcl, press);
}


// Shrinking shit

InitialConditionSS::InitialConditionSS(ConfigFile * config) : InitialCondition(config)
{
  a0 = config->read<SlurmDouble>("a0", a0);
  D = config->read<SlurmDouble>("D", D);
}

void InitialConditionSS::initNode(GridNode * n) 
{
  n->set_u(u0 * sin(2. * PI * (n->get_y() / get_Ly() - 0.5)), 0, 0);

  SlurmDouble Az = B0 * a0 * (- log(cosh((- (n->get_x() - 0.5 * get_Lx()) - D) / a0)) + log(cosh(- D / a0)));
  if (n->get_x() >= 0.5 * get_Lx()) 
    Az = B0 * a0 * (log(cosh((- (n->get_x() - 0.5 * get_Lx()) + D) / a0)) - log(cosh(- D / a0)));
  n->set_A(0, 0, Az);
}

void InitialConditionSS::initParticle(Particle * p, Interpolator * interpolator) 
{
  //initParticleInterpolate(p, interpolator);
  GridCell *c = interpolator->interpolateCells(p, QUANT_RHO | QUANT_ENERGY);

  p->set_m(c->get_rho() * p->get_vol());
  p->set_e(c->get_e() * p->get_m());
  p->set_u(u0 * sin(2. * PI * (p->get_y() / get_Ly() - 0.5)), 0, 0);

  SlurmDouble Az = B0 * a0 * (- log(cosh((- (p->get_x() - 0.5 * get_Lx()) - D) / a0)) + log(cosh(- D / a0)));
  if (p->get_x() >= 0.5 * get_Lx()) 
    Az = B0 * a0 * (log(cosh((- (p->get_x() - 0.5 * get_Lx()) + D) / a0)) - log(cosh(- D / a0)));
  p->set_A(0, 0, Az);
  delete c;
}


// Taylor-Green

InitialConditionTG::InitialConditionTG(ConfigFile * config) : InitialCondition(config)
{
  setting = config->read<SlurmDouble>("setting", setting);

  // Set density in order to achieve initial energy equipartition depending on the case
  if (setting == 1) rho0 = 3.0808825908;
  else if (setting == 2) rho0 = 2.715861974;
  else if (setting == 3) rho0 = 0.905606539;
  else rho0 = 1.;

  messageline("Magnetized Taylor-Green case ", setting);
  messageline("rho0 = ", rho0);
}

void InitialConditionTG::initNode(GridNode * n) 
{
  n->set_u(u0 * sin(2. * PI / get_Lx() * n->get_x()) * cos(2. * PI / get_Ly() * n->get_y()) * cos(2. * PI / get_Lz() * n->get_z()),
     -1. * u0 * cos(2. * PI / get_Lx() * n->get_x()) * sin(2. * PI / get_Ly() * n->get_y()) * cos(2. * PI / get_Lz() * n->get_z()),
      0);

  if (setting == 1) 
  {
    // The first config, eqs 3, 4, 5
    n->set_A(-1. * B0 / 2. / PI * sin(2. * PI / get_Lx() * n->get_x()) * cos(2. * PI / get_Ly() * n->get_y()) * cos(2. * PI / get_Lz() * n->get_z()),
                   B0 / 2. / PI * cos(2. * PI / get_Lx() * n->get_x()) * sin(2. * PI / get_Ly() * n->get_y()) * cos(2. * PI / get_Lz() * n->get_z()),
              0);
  }
  else if (setting == 2)
  {
    // Second one, eqs 6, 7, 8
    n->set_A(B0 / 4. / PI * cos(4. * PI / get_Lx() * n->get_x()) * sin(4. * PI / get_Ly() * n->get_y()) * sin(4. * PI / get_Lz() * n->get_z()),
       -1. * B0 / 4. / PI * sin(4. * PI / get_Lx() * n->get_x()) * cos(4. * PI / get_Ly() * n->get_y()) * sin(4. * PI / get_Lz() * n->get_z()),
        0);
  }
  else if (setting == 3)
  {
    // Third case, equations 9, 10, 11. Note that here magnetic field amplitude should be sqrt(3) larger to have the same energy as cases 1, 2.
    n->set_A(B0 / 4. / PI * sin(4. * PI / get_Lx() * n->get_x()) * cos(4. * PI / get_Ly() * n->get_y()) * cos(4. * PI / get_Lz() * n->get_z()),
             B0 / 4. / PI * cos(4. * PI / get_Lx() * n->get_x()) * sin(4. * PI / get_Ly() * n->get_y()) * cos(4. * PI / get_Lz() * n->get_z()),
             0);
  }
}

// FluxRope in the uniform backgorund field

/** 
  Uniform field so far.
  Particle inflow from the bottom.
*/
InitialConditionFR::InitialConditionFR(ConfigFile * config) : InitialConditionIO(config)
{    
  p0 = config->read<SlurmDouble>("p0", p0);

  InjectPeriod = get_Lz() / config->read<SlurmInt>("GridCellsZ") / config->read<SlurmDouble>("ParticlesPerCellZ") / InjectSpeed;

  // Like in the magnetic loop test
  LoopRadius = config->read<SlurmDouble>("L0", LoopRadius);
  u0 = config->read<SlurmDouble>("u0", u0);

  // Flux rope / loop amplitude
  B0 = config->read<SlurmDouble>("B0", B0);

  // Background field
  B0xyz[0] = config->read<SlurmDouble>("B0x", 0.);
  B0xyz[1] = config->read<SlurmDouble>("B0y", 0.);
  B0xyz[2] = config->read<SlurmDouble>("B0z", 0.);

  // Initial coordinates of the loop center
  xyzc[0] = 0.5 * get_Lx();
  xyzc[1] = 0.5 * get_Ly();
  xyzc[2] = -LoopRadius;

  messageline("Flux rope parameters:");
  messageline("Loop radius = ", LoopRadius);
  messageline("Loop field = ", B0);
  message("Background Field Bxyz = ", B0xyz[0]);
  message(", ", B0xyz[1]);
  messageline(", ", B0xyz[2]);
}

/** 
  By default, uniform density and pressure is set
*/
void InitialConditionFR::initCell(GridCell * c)
{
  // density, momentum, energy
  c->set_rho(rho0);
  c->set_p(p0);
  c->set_e(p0 / (GasGamma_1 * rho0));
  c->set_m(rho0 * c->get_vol());
  //c->set_B(B0xyz[0], B0xyz[1], B0xyz[2]);
};

/** Add the loop to the boundary node. */
void InitialConditionFR::initNode(GridNode * n, SlurmDouble t) 
{
  n->set_m(rho0 * n->get_vol());
  n->set_u(0, 0, u0);
  n->set_A(0, computeLoopA(n, t), 0);
}

/** Injected particles have a different color */
void InitialConditionFR::initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t) 
{
  p->set_m(rho0 * p->get_vol());
  computeEnergy(p, p0);
  p->set_u(0, 0, u0);
  p->set_A(0, computeLoopA(p, t), 0);
  
  p->set_color(0);
  if (t > 0)
  {
    if (computeLoopA(p, t) > 0)
      p->set_color(2);
    else
      p->set_color(1);
  }
}

/**
  Magnetic loop periodic parallel to Y axis.
*/
SlurmDouble InitialConditionFR::computeLoopA(GridPoint * p, SlurmDouble t)
{
  // d = sqrt(x**2 + z**2) - distance to the loop.
  SlurmDouble d = pow(pow(p->get_x() - xyzc[0], 2) + pow(p->get_z() - (xyzc[2] + InjectSpeed * t), 2), 0.5);  

  if (d < LoopRadius) 
    return B0 * (LoopRadius - d);
  else
    return 0;
}

// Test

InitialConditionTest::InitialConditionTest(ConfigFile * config) : InitialCondition(config)
{
  setting = config->read<SlurmDouble>("setting", setting);
}

void InitialConditionTest::initNode(GridNode * n) 
{
  n->set_u(u0 * n->get_x() / get_Lx(), u0 * n->get_y() / get_Ly(), u0 * n->get_z() / get_Lz());
}

// SolarWind. Parker quiet solar wind model

/** 
  Parker's solution and its derivative

  (u/uc)**2 - 2*ln(u/uc) + 4*ln(rc/r) - 4*rc/r + 3 = 0

  where u is the solar wind speed, uc is the critical speed (of sound), rc is the critical radius.
  Here, u is the unknown, r is the parameter.
  Assume the arguments are already normalized, i.e., r/rc and u/uc
*/

template <class T>
struct parker_functor_deriv
{
  parker_functor_deriv(T const& r)
  { 
    // r is radius divided by the critical radius rc
    // Store the right-hand-side of the equation
    rhs = 4 * ( log(r) + 1. / r) - 3;
  }
  pair<T, T> operator()(T const& u)
  { 
    // Return both f(x) and f'(x).
    T fx = u*u - 2 * log(u) - rhs;                // Difference (estimate x^3 - value).
    T dx =  2 * (u - 1./u);                 // 1st derivative = 3x^2.
    return make_pair(fx, dx);   // 'return' both fx and dx.
  }
private:
  T rhs; // Right hand side of Parker's solution
};

/** 
  Find solar wind speed at given radius.
  Assume r is already divided by the critical radius rc.

  Usage:
  SlurmDouble r = 0.5 * rc;
  SlurmDouble u = parker_speed(r);

  This template was created using an example from
  libs/math/example/root_finding_example.cpp
*/

template <class T>
T parker_speed(T r)
{ 
  // return cube root of x using 1st derivative and Newton_Raphson.
  using namespace boost::math::tools;
  int exponent;
  frexp(r, &exponent);                                // Get exponent of z (ignore mantissa).
  T guess = ldexp(1., exponent/3);                    // Rough guess is to divide the exponent by three.
  T min = ldexp(0.5, exponent/3);                     // Minimum possible value is half our guess.
  T max = ldexp(2., exponent/3);                      // Maximum possible value is twice our guess.
  const int digits = numeric_limits<T>::digits;       // Maximum possible binary digits accuracy for type T.
  int get_digits = static_cast<int>(digits * 0.6);    // Accuracy doubles with each step, so stop when we have
                                                      // just over half the digits correct.
  const boost::uintmax_t maxit = 20;
  boost::uintmax_t it = maxit;
  T result = newton_raphson_iterate(parker_functor_deriv<T>(r), guess, min, max, get_digits, it);
  return result;
}

/**
  We expect the length in the inputfile to be normalized to 1 AU.
  Velocity normalization will be computed given the temperature in Kelvin.

    (u/uc)**2 - 2*ln(u/uc) + 4*ln(rc/r) - 4*rc/r + 3 = 0
    r**2 * rho * u = const
    uc**2 = p / rho = kB * T / mp

  Units for normalization 
  L = 1 AU
  u = uc
  t = L / u
  rho = density at the Earth's orbit, 6-7e6 * mproton [kg m^-3].
  Temperature - ?
*/
InitialConditionSW::InitialConditionSW(ConfigFile * config) : InitialConditionIO(config)
{
  // Density in the inputfile is already normalized to SW density at Earth's orbit
  rho0 = config->read<SlurmDouble>("rho0", rho0);

  T = config->read<SlurmDouble>("T", T);
  // [m/s]
  uc = pow(T * BOLTZMANN_CONSTANT / PROTON_MASS, 0.5);
  // rc normalized to 1 AU
  rc = 0.5 * GRAVITATIONAL_CONSTANT * SOLAR_MASS / (uc * uc) / ASTRONOMICAL_UNIT;
  // Solar wind speed at the critical point
  uL = parker_speed(1.);

  // Magnetic field properties
  B0 = config->read<SlurmDouble>("B0", B0);

  // Angular rotation speed in code units
  AngularSpeed = 2. * PI / (SOLAR_EQUATORIAL_PERIOD * uc / ASTRONOMICAL_UNIT);

  // Center of the sphere
  Center.set_xyz(0.5 * get_Lx(), 0.5 * get_Ly(), 0.5 * get_Lz());

  // Radius of the internal boundary
  R = config->read<SlurmDouble>("R0");
  InjectSpeed = parker_speed(R / rc);
  // Solar wind density at the inner boundary
  uInner = InjectSpeed;
  rhoInner = rho0 * uL / (uInner * R * R);

  // Estimate the number of particles in the cylindrical coordinate system
  // N = 2/pi * nphi * nr * nz, where nz == ncz * npz. N is the total number of particles.
  SlurmInt N = config->read<SlurmInt>("GridCellsX") * config->read<SlurmDouble>("ParticlesPerCellX") * config->read<SlurmInt>("GridCellsY") * config->read<SlurmDouble>("ParticlesPerCellY");
  SlurmDouble R1 = 0.5 * pow(pow(get_Lx(), 2) + pow(get_Ly(), 2), 0.5);

  // Put a condition on the distance between particles along phi and r at the outermost corner dr == dphi * R1.
  // Then the number of particles on each circle
  nphi = PI * pow(2 * N * (R1*R1 - R*R) / (get_Lx() * get_Ly()), 0.5);
  dphi = 2 * PI / nphi;
  // Distance between particles along radius
  dr = dphi * R1;
  nr = (R1 - R) / dr;

  // Inject period is different than in the inlet
  InjectPeriod = dr / InjectSpeed;
  NextInjectTime = InjectPeriod;

  messageline("\nParker Solar Wind parameters:");
  messageline("T [K] = ", T);
  messageline("rc [AU] = ", rc);
  messageline("uc [m/s] = ", uc);
  messageline("Internal Radius = ", R);
  messageline("dr = ", dr);
  messageline("nphi = ", nphi);
  messageline("InjectSpeed = ", InjectSpeed);
  messageline("InjectPosition = ", R);
  messageline("Rotation period = ", 2. * PI / AngularSpeed);
  messageline("Asimuthal speed = ", AngularSpeed * R);
  messageline("Inner speed = ", uInner);
  messageline("Inner density = ", rhoInner);
  messageline("Solar dipole A0 = ", A0);
}

/** 
  Compute speed and density of Parker solar wind at given point.
  r, u, rho are returned in code units: r is normalized to 1 AU, 
  u is normalized to the critical speed uc, and rho is normalized 
  to rho0, density at 1 AU (?).
*/
void InitialConditionSW::computeParkerProfile(GridPoint * p, SlurmDouble & r, SlurmDouble & u, SlurmDouble & rho)
{
  r = Center.distance(p);
  if (r >= R)
  {
    u = parker_speed(r / rc);
    rho = rho0 * uL / (u * r * r);
  }
  else
  {
    u = uInner;
    rho = rhoInner;
  }
}

/** 
  Compute magnetic vector potential of the Sun.
  The formulas are taken from [Webb et al. 2010, JGR 115, doi:10.1029/2010JA015513].

  The coordinate system associated with the solar rotation axis is tilted by Inclination to the Y axis of the Slurm coordinates.
  It is denoted with prime.
  In spherical coords, 
  polar angle sin(theta) = (x^2 + z^2) / r
  azimuthal angle phi = atan2(z, x)
*/
SlDoubleVector InitialConditionSW::computeA(GridPoint * p, SlurmDouble r)
{
  if (r < 0)
    r = Center.distance(p);

  // Solar multipole tilted by Inclination to the Z axis. Basically, Inclination rotated around Y axis.
  GridPoint * shifted = p->clone();
  shifted->shift_coords_inv(&Center);
  GridPoint * rotated = shifted->rotate_coords(1, Inclination);

  // Azimuthal angle in the tilted coords
  SlurmDouble phi_prime = atan2(rotated->get_y(), rotated->get_x()) + PI;
  SlurmDouble A = 0.;

  /*
  // Monopole A = B0 * r0**2 (1-|cos(Theta')| / (r sin(Theta')) e_phi'
  // A = B0 * R * R * (1.0 - fabs(rotated->get_x()/r)) / pow(rotated->get_x()*rotated->get_x() + rotated->get_y()*rotated->get_y(), 0.5);
  if (r > 0.5 * R)
    A = B0 * (1.0 - fabs(rotated->get_x()/r)) / pow(rotated->get_x()*rotated->get_x() + rotated->get_y()*rotated->get_y(), 0.5);
  else
    A = B0 * (1.0 - fabs(rotated->get_x()/R)) / pow(rotated->get_x()*rotated->get_x() + rotated->get_y()*rotated->get_y(), 0.5);
  */
  
  // Dipole A = - Q * (sin(Theta') / r^2) e_phi'
  SlurmDouble theta_prime = acos(rotated->get_z() / r);
  if (r > 0)
    A = - B0 * sin(theta_prime) / (r * r);
  else
    A = 0; //- B0 * sin(theta_prime) / (R * R);

  // Set only the out-of-plane Z component of vector potential?
  //SlDoubleVector A_real = {A * cos(phi_prime) * cos(Inclination), A * sin(phi_prime), -A * cos(phi_prime) * sin(Inclination)};
  SlDoubleVector A_real = {0, 0, -A * cos(phi_prime) * sin(Inclination)};

  delete shifted;
  delete rotated;

  return A_real;
}

/** 
  Parker's values?
*/
void InitialConditionSW::initCell(GridCell * c)
{
  SlurmDouble r, u;
  computeParkerProfile(c, r, u, c->fetch_rho());
  c->set_m(c->get_rho() * c->get_vol());
  c->set_e(IsothermalSoundSpeedSq / GasGamma_1);
  computePressureIsothermal(c);
};

/** Parker's velocity, density, and mass derived from volume and rho. */
void InitialConditionSW::initNode(GridNode * n, SlurmDouble t) 
{
  SlurmDouble r, u, rho;
  computeParkerProfile(n, r, u, rho);
  n->set_u(u * (n->get_x() - Center.get_x()) / r, u * (n->get_y() - Center.get_y()) / r, 0); 
  n->set_m(rho * n->get_vol());

  n->set_A(computeA(n, r));
}

/** Injected particles have radial speed */
void InitialConditionSW::initParticle(Particle * p, Interpolator * interpolator) 
{
  SlurmDouble r, u, rho;
  computeParkerProfile(p, r, u, rho);
  p->set_u(u * (p->get_x() - Center.get_x()) / r, u * (p->get_y() - Center.get_y()) / r, 0); 
  p->set_m(rho * p->get_vol());
  computeEnergy(p, rho * IsothermalSoundSpeedSq);

  p->set_A(computeA(p, r));

  p->set_color(0);
}

/** 
  Injected particles have radial speed. 
  It expects phi instead of X, and r is computed here.
*/
void InitialConditionSW::initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t) 
{
  /*
  // Shift the particle acording to InjectPosition
  SlurmDouble r = (R + InjectPosition) / R;
  p->set_x(Center.get_x() + (p->get_x() - Center.get_x()) * r);
  p->set_y(Center.get_y() + (p->get_y() - Center.get_y()) * r);
  */

  p->set_color((short) (128. * p->get_x() / PI));
  
  // In case we have received phi from ParticleManager
  SlurmDouble phi = p->get_x() + t * AngularSpeed;

  SlurmDouble r = (R + InjectPosition);
  // Add asimuthal component?
  p->set_u(0, - AngularSpeed * r * sin(phi));
  p->set_u(1, AngularSpeed * r * cos(phi));

  p->set_x(Center.get_x() + r * cos(phi));
  p->set_y(Center.get_y() + r * sin(phi));

  SlurmDouble u, rho;
  computeParkerProfile(p, r, u, rho);
  //p->set_u(InjectSpeed * (p->get_x() - Center.get_x()) / r, InjectSpeed * (p->get_y() - Center.get_y()) / r, 0); 
  p->inc_u(InjectSpeed * cos(phi), InjectSpeed * sin(phi), 0); 
  p->set_m(rho * p->get_vol());
  computeEnergy(p, rho * IsothermalSoundSpeedSq);

  p->set_A(computeA(p, r));
}

// Solar Wind in meridional (XY) plane

/**
  We expect the length in the inputfile to be normalized to 1 AU.
  Velocity normalization will be computed given the temperature in Kelvin.

    (u/uc)**2 - 2*ln(u/uc) + 4*ln(rc/r) - 4*rc/r + 3 = 0
    r**2 * rho * u = const
    uc**2 = p / rho = kB * T / mp

  Units for normalization 
  L = 1 AU
  u = uc
  t = L / u
  To find out magnetic field in [SI]:
  B [SI] = (2*mu0*rho*kB*T/beta/mp)**0.5, 
  where plasma beta is found using code units:
  beta = 2*P/B**2

  Composition - 96% protons, 4% alpha-particles.
  rho0 - density at the Earth's orbit, 7 [ions cm^-3] = 1.3e-20 [kg m^-3]

  B at 1 AU - 7 nT, azimuthal angle 45 deg. Hence, magnetic pressure 
  pmag = B**2/(2*mu0) = 7e-9**2 / (2 * 4e-7 * pi) = 1.95e-11 [Pa]
  Alfven speed - Va = B0 / (mu0 * rho0)**0.5 = 5.4e4
  Temperature - 1.5e6 [K]
  pgas = nkT = 1.45e-10 [Pa]
  Thermal speed of protons
  uth = (kB*T/mp)**0.5 = 1.1e5 m/s
  Solar wind speed at 1 AU
  u(1 AU) = 450 km/s = 4.5e5 m/s
  Plasma beta at 1 AU should be ~10

*/
InitialConditionSW1::InitialConditionSW1(ConfigFile * config)
{

  // InitialCondition

  Lxyz[0] = config->read<SlurmDouble>("Lx");
  Lxyz[1] = config->read<SlurmDouble>("Ly");
  Lxyz[2] = config->read<SlurmDouble>("Lz");

  rho0 = config->read<SlurmDouble>("rho0", rho0);
  p0 = config->read<SlurmDouble>("p0", p0);
  set_GasGamma(config->read<SlurmDouble>("gamma", GasGamma));

  // Gravity
  GravityAcceleration[0] = config->read<SlurmDouble>("gravityX", 0.);
  GravityAcceleration[1] = config->read<SlurmDouble>("gravityY", 0.);
  GravityAcceleration[2] = config->read<SlurmDouble>("gravityZ", 0.);
  
  IsothermalSoundSpeedSq = p0 / rho0;
  AdiabaticSoundSpeedSq = GasGamma * IsothermalSoundSpeedSq;

  u0 = config->read<SlurmDouble>("u0", u0);
  B0 = config->read<SlurmDouble>("B0", B0);
  name = config->read <string>("testcase");  

  // InitialConditionIO

  InjectSpeed = config->read<SlurmDouble>("InjectSpeed", u0);
  // dxp / u0
  InjectPeriod = get_Lx() / config->read<SlurmInt>("GridCellsX") / config->read<SlurmDouble>("ParticlesPerCellX") / InjectSpeed;
  LastInjectTime = 0.;
  // Should only shift by a half of a particle before the first new population pops up
  NextInjectTime = 0.5 * InjectPeriod;
  InjectPosition = 0.;  

  // InitialConditionSW

  T = config->read<SlurmDouble>("T", T);
  // [m/s]
  uc = pow(T * BOLTZMANN_CONSTANT / PROTON_MASS, 0.5);
  // rc normalized to 1 AU
  rc = 0.5 * GRAVITATIONAL_CONSTANT * SOLAR_MASS / (uc * uc) / ASTRONOMICAL_UNIT;
  // Solar wind speed at Earth's orbit
  uL = parker_speed(1.);

  // No rotation when modeling meridional plane
  AngularSpeed = 0.;

  // Center of the sphere
  Center.set_xyz(config->read<SlurmDouble>("x0", 0.5 * get_Lx()), config->read<SlurmDouble>("y0", 0.5 * get_Ly()), config->read<SlurmDouble>("z0", 0.5 * get_Lz()));

  // Radius of the internal boundary
  R = config->read<SlurmDouble>("R0");
  InjectSpeed = parker_speed(R / rc);
  // Solar wind density at the inner boundary
  uInner = InjectSpeed;
  rhoInner = rho0 * uL / (uInner * R * R);

  // Estimate the number of particles in the cylindrical coordinate system
  // N = 2/pi * nphi * nr * nz, where nz == ncz * npz. N is the total number of particles.
  SlurmInt N = config->read<SlurmInt>("GridCellsX") * config->read<SlurmDouble>("ParticlesPerCellX") * config->read<SlurmInt>("GridCellsY") * config->read<SlurmDouble>("ParticlesPerCellY");
  SlurmDouble R1 = pow(pow(get_Lx(), 2) + pow(0.5*get_Ly(), 2), 0.5);

  // Put a condition on the distance between particles along phi and r at the outermost corner dr == dphi * R1.
  // Then the number of particles on each circle
  nphi = PI * pow(2 * N * (R1*R1 - R*R) / (get_Lx() * get_Ly()), 0.5);
  dphi = 2 * PI / nphi;
  // Distance between particles along radius
  dr = dphi * R1;
  nr = (R1 - R) / dr;

  // Inject period is different than in the inlet
  InjectPeriod = dr / InjectSpeed;
  NextInjectTime = InjectPeriod;

  messageline("\nSolar Wind in Meridional plane:");
  messageline("T [K] = ", T);
  messageline("Critical radius rc [AU] = ", rc);
  messageline("Critical speed uc [m/s] = ", uc);
  messageline("Internal Radius = ", R);
  messageline("Particle cylindrical distribution dr = ", dr);
  messageline("Particle cylindrical distribution nphi = ", nphi);
  messageline("Injection speed [uc] = ", InjectSpeed);
  messageline("Injection density = ", rhoInner);
  messageline("InjectPosition = ", R);
  messageline("Rotation period = ", 2. * PI / AngularSpeed);
  messageline("Asimuthal speed = ", AngularSpeed * R);
  messageline("Solar dipole B0 = ", B0);
}

/** Injected particles have radial speed */
void InitialConditionSW1::initParticle(Particle * p, Interpolator * interpolator) 
{
  SlurmDouble r, u, rho;
  computeParkerProfile(p, r, u, rho);

  // Fast solar wind is on the poles, slow one on the equator
  // Here I use heuristics. As Parker's profile gives speed ~400 km/s at the Earth's orbit, I multiply it by 2.
  SlurmDouble psi = atan2(p->get_y() - Center.get_y(), p->get_x() - Center.get_x());
  SlurmDouble u1 = InjectSpeed * (2. - pow(cos(psi), 4));

  p->set_u(u1 * (p->get_x() - Center.get_x()) / r, u1 * (p->get_y() - Center.get_y()) / r, 0); 
  p->set_m(rho * p->get_vol());
  computeEnergy(p, rho * IsothermalSoundSpeedSq);

  p->set_A(computeA(p, r));

  p->set_color(0);
}

/** 
  Injected particles have radial speed. 
  It expects phi instead of X, and r is computed here.
*/
void InitialConditionSW1::initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t) 
{
  /*
  // Shift the particle acording to InjectPosition
  SlurmDouble r = (R + InjectPosition) / R;
  p->set_x(Center.get_x() + (p->get_x() - Center.get_x()) * r);
  p->set_y(Center.get_y() + (p->get_y() - Center.get_y()) * r);
  */

  p->set_color((short) (128. * p->get_x() / PI));
  
  // In case we have received phi from ParticleManager
  SlurmDouble psi = p->get_x() + t * AngularSpeed;

  SlurmDouble r = (R + InjectPosition);
  // Add asimuthal component?
  p->set_u(0, - AngularSpeed * r * sin(psi));
  p->set_u(1, AngularSpeed * r * cos(psi));

  p->set_x(Center.get_x() + r * cos(psi));
  p->set_y(Center.get_y() + r * sin(psi));

  SlurmDouble u, rho;
  computeParkerProfile(p, r, u, rho);

  // Fast solar wind is on the poles, slow one on the equator
  // Here I use heuristics. As Parker's profile gives speed ~400 km/s at the Earth's orbit, I multiply it by 2.
  SlurmDouble u1 = InjectSpeed * (2. - pow(cos(psi), 4));

  p->inc_u(u1 * cos(psi), u1 * sin(psi), 0); 
  p->set_m(rho * p->get_vol());
  computeEnergy(p, rho * IsothermalSoundSpeedSq);

  p->set_A(computeA(p, r));
}


// Brio-Wu shock

InitialConditionBrioWu::InitialConditionBrioWu(ConfigFile * config) : InitialCondition(config) 
{
  p0 = config->read<SlurmDouble>("p0", 1.0);
  rho0 = config->read<SlurmDouble>("rho0", 1.0);
  set_GasGamma(config->read<SlurmDouble>("gamma", 2.0));
  B0 = config->read<SlurmDouble>("B0", 1.0);
}

void InitialConditionBrioWu::initCell(GridCell * c)
{
  // Normalized x
  SlurmDouble x = c->get_x()/get_Lx();

  if ((x < 0.25) || (x >= 0.75))
  {
    c->set_rho(rho0);
    c->set_p(p0);
  }
  else
  {
    c->set_rho(0.125 * rho0);
    c->set_p(0.1 * p0);
  }

  c->set_e(c->get_p() / (GasGamma_1 * c->get_rho()));
}

void InitialConditionBrioWu::initNode(GridNode * n) 
{
  n->set_u(0, 0, 0);
  // Normalized x
  SlurmDouble x = n->get_x()/get_Lx();
  if (x < 0.25)    
    n->set_A(0, 0, - B0 * x * get_Lx());
  else if (x >= 0.75)
    n->set_A(0, 0, B0 * (1 - x) * get_Lx());
  else
    n->set_A(0, 0, B0 * (x - 0.5) * get_Lx());
}

void InitialConditionBrioWu::initParticle(Particle * ptcl, Interpolator * interpolator) 
{
  ptcl->set_u(0, 0, 0);
  // Normalized x
  SlurmDouble x = ptcl->get_x() / get_Lx();
  SlurmDouble rho = 0., P = 0.;

  if (x < 0.25)    
  {
    ptcl->set_A(0, 0, -B0 * x * get_Lx());
    rho = rho0;
    P = p0;
    ptcl->set_color(0);
  }
  else if (x >= 0.75)
  {
    ptcl->set_A(0, 0, B0 * (1.0 - x) * get_Lx());
    rho = rho0;
    P = p0;
    ptcl->set_color(1);
  }
  else
  {
    ptcl->set_A(0, 0, B0 * (x - 0.5) * get_Lx());
    rho = 0.125 * rho0;
    P = 0.1 * p0;
    ptcl->set_color(2);
  }

  ptcl->set_m(rho * ptcl->get_vol());
  computeEnergy(ptcl, P);
}

// Two blast waves

InitialConditionTB::InitialConditionTB(ConfigFile * config) : InitialCondition(config) 
{
  p0 = config->read<SlurmDouble>("p0", 1.0);
  rho0 = config->read<SlurmDouble>("rho0", 1.0);
  set_GasGamma(config->read<SlurmDouble>("gamma", 1.4));
}

void InitialConditionTB::initCell(GridCell * c)
{
  c->set_rho(rho0);

  // Normalized x
  SlurmDouble x = c->get_x()/get_Lx();

  if (x < 0.1)
    c->set_p(1e3 * p0);
  else if (x > 0.9)
    c->set_p(100 * p0);
  else
    c->set_p(0.01 * p0);

  c->set_e(c->get_p() / (GasGamma_1 * c->get_rho()));
}

void InitialConditionTB::initNode(GridNode * n) 
{
  n->set_u(0, 0, 0);
}

void InitialConditionTB::initParticle(Particle * ptcl, Interpolator * interpolator) 
{
  ptcl->set_u(0, 0, 0);
  // Normalized x
  SlurmDouble x = ptcl->get_x() / get_Lx();
  SlurmDouble P = 0.;

  if (x < 0.1)    
  {
    P = 1e3 * p0;
    ptcl->set_color(0);
  }
  else if (x > 0.9)
  {
    P = 100 * p0;
    ptcl->set_color(2);
  }
  else
  {
    P = 0.01 * p0;
    ptcl->set_color(1);
  }

  ptcl->set_m(rho0 * ptcl->get_vol());
  computeEnergy(ptcl, P);
}


// Two blast waves with periodic bounds

InitialConditionTBPeriodic::InitialConditionTBPeriodic(ConfigFile * config) : InitialCondition(config) 
{
  p0 = config->read<SlurmDouble>("p0", 1.0);
  rho0 = config->read<SlurmDouble>("rho0", 1.0);
  set_GasGamma(config->read<SlurmDouble>("gamma", 1.4));
}

void InitialConditionTBPeriodic::initCell(GridCell * c)
{
  c->set_rho(rho0);

  // Normalized x
  SlurmDouble x = 2 * c->get_x()/get_Lx();

  if ((x < 0.1) || (x > 1.9))
    c->set_p(1e3 * p0);
  else if ((x > 0.9) && (x < 1.1))
    c->set_p(100 * p0);
  else
    c->set_p(0.01 * p0);

  c->set_e(c->get_p() / (GasGamma_1 * c->get_rho()));
}

void InitialConditionTBPeriodic::initNode(GridNode * n) 
{
  n->set_u(0, 0, 0);
}

void InitialConditionTBPeriodic::initParticle(Particle * ptcl, Interpolator * interpolator) 
{
  ptcl->set_u(0, 0, 0);
  // Normalized x
  SlurmDouble x = 2 * ptcl->get_x() / get_Lx();
  SlurmDouble P = 0.;

  if ((x < 0.1) || (x > 1.9))
  {
    P = 1e3 * p0;
    ptcl->set_color(0);
  }
  else if ((x > 0.9) && (x < 1.1))
  {
    P = 100 * p0;
    ptcl->set_color(2);
  }
  else
  {
    P = 0.01 * p0;
    ptcl->set_color(1);
  }

  ptcl->set_m(rho0 * ptcl->get_vol());
  computeEnergy(ptcl, P);
}

// Double Harris

InitialConditionDH::InitialConditionDH(ConfigFile * config) : InitialCondition(config) 
{
  B0 = config->read<SlurmDouble>("B0", 1.0);
  delta = config->read<SlurmDouble>("delta", 1.0);
  A0 = config->read<SlurmDouble>("A0", 0.1*B0);
}

SlurmDouble InitialConditionDH::computeAz(GridPoint * p)
{
  // Coordinate relative to the bottom sheet
  SlurmDouble yb = (p->get_y() - 0.25 * get_Ly()) / delta;
  // Coordinate relative to the top sheet
  SlurmDouble yt = (p->get_y() - 0.75 * get_Ly()) / delta;

  SlurmDouble A = B0 * delta * (p->get_y()/delta - log(cosh(yb)) + log(cosh(yt)));

  // Add perturbation
  SlurmDouble x = p->get_x() - 0.5 * get_Lx();
  SlurmDouble y = yb * delta;
  SlurmDouble sigma2 = pow(0.1 * delta_perp, 2);
  A += A0 * cos(2 * PI * x / delta_perp) * cos(PI * y / delta_perp) * exp(-(x*x + y*y) / sigma2);

  return A;
}

/// Compute and return magnetic pressure
SlurmDouble InitialConditionDH::computeB(GridPoint * p)
{
  // Coordinate relative to the bottom sheet
  SlurmDouble yb = (p->get_y() - 0.25 * get_Ly()) / delta;
  // Coordinate relative to the top sheet
  SlurmDouble yt = (p->get_y() - 0.75 * get_Ly()) / delta;

  SlurmDouble B = B0 * (1 - tanh(yb) + tanh(yt));

  return B*B;
}

void InitialConditionDH::initNode(GridNode * n) 
{
  n->set_u(0, 0, 0);

  // B0x * (-1.0 + tanh(yBd) - tanh(yTd))
  n->set_A(0, 0, computeAz(n));
}

void InitialConditionDH::initParticle(Particle * ptcl, Interpolator * interpolator) 
{
  ptcl->set_u(0, 0, 0);
  ptcl->set_m(rho0 * ptcl->get_vol());
  computeEnergy(ptcl, p0);

  ptcl->set_A(0, 0, computeAz(ptcl));
}

/** 
  Balance magnetic and gas pressures
*/
void InitialConditionDH::initCell(GridCell * c)
{
  // density, momentum, energy
  c->set_rho(rho0);
  c->set_p(p0 + B0*B0 - computeB(c));
  c->set_e(p0 / (GasGamma_1 * rho0));
};
