#include "Interpolators.h"

using namespace std;

/// Returns the interpolation scheme given its name
InterpolationSchemes Interpolator::getSchemeByName(string name)
{
  if (boost::iequals(name, "Linear"))
    return isLinear;
  else if (boost::iequals(name, "Quadratic"))
    return isQuadratic;
  else 
    return isUnknown;
}

/// Returns the interpolation scheme name
string Interpolator::getInterpolationSchemeName(InterpolationSchemes scheme)
{
  if (scheme == isLinear) 
    return string("Linear");
  else if (scheme == isQuadratic) 
    return string("Quadratic");
  else 
    return string("Unknown");
}

/// Factory method
Interpolator * Interpolator::makeInterpolator(GridCommon * grid, InterpolationSchemes scheme, MFEvolutionMethods MFEvolution)
{
  messageline("Creating interpolator: ", getInterpolationSchemeName(scheme));
  if (scheme == isLinear)
  {
    if (MFEvolution == mfemOff) 
      return new LinearInterpolator(grid);
    else if (MFEvolution == mfemAdvectParticles) 
      return new LinearInterpolatorAP(grid);
    else if ((MFEvolution == mfemInterpolateParticles) || ((MFEvolution == mfemPreserveParticles))) 
      return new LinearInterpolatorIP(grid);
    else 
    {
      message("ERROR: Unknown interpolation method is specified!");
      throw SLURM_ERROR_INTERPOLATION;
    }
  }
  else if (scheme == isQuadratic)
  {
    if (MFEvolution == mfemOff) 
    {
      //return new QuadraticInterpolatorHD(grid);
      return new QuadraticInterpolatorCC(grid);
    }
    else if (MFEvolution == mfemAdvectParticles) 
      return new QuadraticInterpolatorAP(grid);
    else if ((MFEvolution == mfemInterpolateParticles) || ((MFEvolution == mfemPreserveParticles))) 
    {
      return new QuadraticInterpolatorCC(grid);
      //return new QuadraticInterpolatorIP(grid);
    }
    else 
    {
      message("ERROR: Unknown interpolation method is specified!");
      throw SLURM_ERROR_INTERPOLATION;
    }
  }

  return NULL;
}

/// Factory method
Interpolator * Interpolator::makeInterpolator(GridCommon * grid, string scheme, MFEvolutionMethods MFEvolution)
{
  return Interpolator::makeInterpolator(grid, Interpolator::getSchemeByName(scheme), MFEvolution);
}

/** 
  Find the point's adjacent cells and interpolation weights for them.
  Corners are not neighbors, there are 8 weights.

  adj_cells - already allocated array of GridCell* pointers
  weights - array of the same size with weights

  4-cell linear interpolation; periodic in X and Y.
  Find the nearest node, and, via its neighbors, the adjacent cells.

*/
inline void Interpolator::getInterpolationCellsLinear(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[])
{
  GridNode* nn = grid->getClosestNode(p);

  // The weights are computed using the geometrically true neighbors, not accounting for BCs (i.e., flipping over in the periodic case).
  SlurmDouble wx1 = (p->get_x() - nn->get_c000()->get_x()) / (nn->get_c100()->get_x() - nn->get_c000()->get_x());
  SlurmDouble wx0 = 1. - wx1;  
  SlurmDouble wy1 = (p->get_y() - nn->get_c000()->get_y()) / (nn->get_c010()->get_y() - nn->get_c000()->get_y());
  SlurmDouble wy0 = 1. - wy1;  
  SlurmDouble wz1 = (p->get_z() - nn->get_c000()->get_z()) / (nn->get_c001()->get_z() - nn->get_c000()->get_z());
  SlurmDouble wz0 = 1. - wz1;

  // The cells to interpolate to are accounting fro BCs, therefore we need node's "acxxx". 
  adj_cells[0] = nn->get_c000()->get_advCell();       
  weights[0] = wx0 * wy0 * wz0;
  adj_cells[1] = nn->get_c100()->get_advCell();
  weights[1] = wx1 * wy0 * wz0;
  adj_cells[2] = nn->get_c110()->get_advCell();
  weights[2] = wx1 * wy1 * wz0;
  adj_cells[3] = nn->get_c010()->get_advCell();
  weights[3] = wx0 * wy1 * wz0;

  adj_cells[4] = nn->get_c001()->get_advCell();
  weights[4] = wx0 * wy0 * wz1;
  adj_cells[5] = nn->get_c101()->get_advCell();
  weights[5] = wx1 * wy0 * wz1;
  adj_cells[6] = nn->get_c111()->get_advCell();
  weights[6] = wx1 * wy1 * wz1;
  adj_cells[7] = nn->get_c011()->get_advCell();
  weights[7] = wx0 * wy1 * wz1;
}

/// Far-side piece of the Quadratic interpolation
inline SlurmDouble Interpolator::weight1(SlurmDouble x1, SlurmDouble x2, SlurmDouble dxInv)
{
  SlurmDouble d = fabs((x1 - x2) * dxInv);
  return(0.5 * d * d - 1.5 * d + 1.125);
};

/// Central quadriliteral piece of the Quadratic interpolation
inline SlurmDouble Interpolator::weight2(SlurmDouble x1, SlurmDouble x2, SlurmDouble dxInv)
{
  SlurmDouble d = fabs((x1 - x2) * dxInv);
  return(0.75 - d * d);
};

/** 
  Find the point's adjacent cells and interpolation weights for them.
  Use the quadratic interpolation. In this case, there should be 27 weights.
  
  Weights:
  
    1/2x^2 + 3/2x + 9/8  if -3/2 <= x < -1/2
    -x^2 + 3/4           if -1/2 <= x < 1/2
    1/2x^2 - 3/2x + 9/8  if 1/2 <= x < 3/2
  
  adj_cells - already allocated array of GridCell* pointers
  weights - array of the same size with weights

  NOTE, assumes the neighbor cells have the same extent dx, dy, dz. This is necessary to correctly interpolate on ghost cells.
 
*/
inline void Interpolator::getInterpolationCellsQuadratic(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[])
{
  GridCell * cc = grid->getClosestCell(p);

  SlurmDouble dx = cc->get_dx();
  SlurmDouble dy = cc->get_dy();
  SlurmDouble dz = cc->get_dz();

  // This interpolation assumes that neighbor cells have the same sizes!
  SlurmDouble wx0 = weight1(p->get_x(), cc->get_x() - dx, cc->get_dxInv());
  SlurmDouble wx1 = weight2(p->get_x(), cc->get_x(), cc->get_dxInv());
  SlurmDouble wx2 = weight1(p->get_x(), cc->get_x() + dx, cc->get_dxInv());    
  SlurmDouble wy0 = weight1(p->get_y(), cc->get_y() - dy, cc->get_dyInv());
  SlurmDouble wy1 = weight2(p->get_y(), cc->get_y(), cc->get_dyInv());
  SlurmDouble wy2 = weight1(p->get_y(), cc->get_y() + dy, cc->get_dyInv());
  SlurmDouble wz0 = weight1(p->get_z(), cc->get_z() - dz, cc->get_dzInv());
  SlurmDouble wz1 = weight2(p->get_z(), cc->get_z(), cc->get_dzInv());
  SlurmDouble wz2 = weight1(p->get_z(), cc->get_z() + dz, cc->get_dzInv());

  // Assign weights to the cells from bottom to top,
  // Bottom level of cells
  adj_cells[0] = cc->get_n000()->get_c000()->get_advCell();
  weights[0] = wx0 * wy0 * wz0;
  adj_cells[1] = cc->get_n000()->get_c100()->get_advCell();  // 100
  weights[1] = wx1 * wy0 * wz0;
  adj_cells[2] = cc->get_n100()->get_c100()->get_advCell();  // 200
  weights[2] = wx2 * wy0 * wz0;
  adj_cells[3] = cc->get_n000()->get_c010()->get_advCell();  // 010
  weights[3] = wx0 * wy1 * wz0;
  adj_cells[4] = cc->get_n000()->get_c110()->get_advCell();  // 110
  weights[4] = wx1 * wy1 * wz0;
  adj_cells[5] = cc->get_n100()->get_c110()->get_advCell();  // 210
  weights[5] = wx2 * wy1 * wz0;
  adj_cells[6] = cc->get_n010()->get_c010()->get_advCell();  // 020
  weights[6] = wx0 * wy2 * wz0;
  adj_cells[7] = cc->get_n010()->get_c110()->get_advCell();  // 120
  weights[7] = wx1 * wy2 * wz0;
  adj_cells[8] = cc->get_n110()->get_c110()->get_advCell();  // 220
  weights[8] = wx2 * wy2 * wz0;
  // Middle level
  adj_cells[9] = cc->get_n000()->get_c001()->get_advCell();  // 001
  weights[9] = wx0 * wy0 * wz1;
  adj_cells[10] = cc->get_n000()->get_c101()->get_advCell();  // 101
  weights[10] = wx1 * wy0 * wz1;
  adj_cells[11] = cc->get_n100()->get_c101()->get_advCell();  // 201
  weights[11] = wx2 * wy0 * wz1;
  adj_cells[12] = cc->get_n000()->get_c011()->get_advCell();  // 011
  weights[12] = wx0 * wy1 * wz1;
  adj_cells[13] = cc->get_advCell();              // 111 == cc
  weights[13] = wx1 * wy1 * wz1;
  adj_cells[14] = cc->get_n100()->get_c111()->get_advCell();  // 211
  weights[14] = wx2 * wy1 * wz1;
  adj_cells[15] = cc->get_n010()->get_c011()->get_advCell();  // 021
  weights[15] = wx0 * wy2 * wz1;
  adj_cells[16] = cc->get_n010()->get_c111()->get_advCell();  // 121
  weights[16] = wx1 * wy2 * wz1;
  adj_cells[17] = cc->get_n110()->get_c111()->get_advCell();  // 221
  weights[17] = wx2 * wy2 * wz1;
  // Top level
  adj_cells[18] = cc->get_n001()->get_c001()->get_advCell();  // 002
  weights[18] = wx0 * wy0 * wz2;
  adj_cells[19] = cc->get_n001()->get_c101()->get_advCell();  // 102
  weights[19] = wx1 * wy0 * wz2;
  adj_cells[20] = cc->get_n101()->get_c101()->get_advCell();  // 202
  weights[20] = wx2 * wy0 * wz2;
  adj_cells[21] = cc->get_n001()->get_c011()->get_advCell();  // 012
  weights[21] = wx0 * wy1 * wz2;
  adj_cells[22] = cc->get_n001()->get_c111()->get_advCell();  // 112
  weights[22] = wx1 * wy1 * wz2;
  adj_cells[23] = cc->get_n101()->get_c111()->get_advCell();  // 212
  weights[23] = wx2 * wy1 * wz2;
  adj_cells[24] = cc->get_n011()->get_c011()->get_advCell();  // 022
  weights[24] = wx0 * wy2 * wz2;
  adj_cells[25] = cc->get_n011()->get_c111()->get_advCell();  // 122
  weights[25] = wx1 * wy2 * wz2;
  adj_cells[26] = cc->get_n111()->get_c111()->get_advCell();  // 222
  weights[26] = wx2 * wy2 * wz2;
}

/** 
  Find adjacent nodes and interpolation weights for them. 
  
  adj_cells - already allocated array of GridCell* pointers
  weights - array of the same size with weights
 
  returns a pointer to the closest cell
*/
void Interpolator::getInterpolationNodes(GridPoint* p, GridNode* adj_nodes[], SlurmDouble weights[])
{
  // Closest Cell always returns a cell inside the main domain. Therefore the neighbors are very simple: cc->n###.
  GridCell* cc = grid->getClosestCell(p);
  
  // Compute weights basing on the lower left neighbor
  SlurmDouble wx1 = (p->get_x() - cc->get_n000()->get_x()) / (cc->get_n100()->get_x() - cc->get_n000()->get_x());
  SlurmDouble wx0 = 1. - wx1;
  SlurmDouble wy1 = (p->get_y() - cc->get_n000()->get_y()) / (cc->get_n010()->get_y() - cc->get_n000()->get_y());
  SlurmDouble wy0 = 1. - wy1;
  SlurmDouble wz1 = (p->get_z() - cc->get_n000()->get_z()) / (cc->get_n001()->get_z() - cc->get_n000()->get_z());
  SlurmDouble wz0 = 1. - wz1;
  
  weights[0] = wx0 * wy0 * wz0;
  weights[1] = wx1 * wy0 * wz0;
  weights[2] = wx1 * wy1 * wz0;
  weights[3] = wx0 * wy1 * wz0;
  weights[4] = wx0 * wy0 * wz1;
  weights[5] = wx1 * wy0 * wz1;
  weights[6] = wx1 * wy1 * wz1;
  weights[7] = wx0 * wy1 * wz1;
  
  // The neighbors have been taken care about on initialization
  adj_nodes[0] = cc->get_an000();
  adj_nodes[1] = cc->get_an100();
  adj_nodes[2] = cc->get_an110();
  adj_nodes[3] = cc->get_an010();    
  adj_nodes[4] = cc->get_an001();
  adj_nodes[5] = cc->get_an101();
  adj_nodes[6] = cc->get_an111();
  adj_nodes[7] = cc->get_an011();    
}

/// Formally add values to the grid cell. Weights are supplied from getInterpolationCells().
void Interpolator::advectToGridCell(Particle* source, GridCell* dest, SlurmDouble w, unsigned long quants)
{
  //QUANT_ENERGY | QUANT_MASS | QUANT_VOL | QUANT_VX | QUANT_VY | QUANT_VZ | QUANT_WEIGHT | QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ;
  dest->lock();
  //dest->rho += source->rho * w;
  if ((quants & QUANT_ENERGY) == QUANT_ENERGY) 
    dest->inc_e(source->get_e() * w);
  if ((quants & QUANT_MASS) == QUANT_MASS) 
    dest->inc_m(source->get_m() * w);
  if ((quants & QUANT_VOL) == QUANT_VOL) 
    dest->inc_vol(source->get_vol() * w);
  if ((quants & QUANT_WEIGHT) == QUANT_WEIGHT) 
    dest->inc_weight(w);
  dest->unlock();
}

/** Formally add values to the grid cell. Weights are supplied from getInterpolationCells(). */
void Interpolator::advectToGridCell(Particle* source, GridCell* dest, SlurmDouble w)
{
  dest->lock();
  //dest->rho += source->rho * w;
  dest->inc_e(source->get_e() * w);
  dest->inc_m(source->get_m() * w);
  dest->inc_vol(source->get_vol() * w);
  dest->inc_weight(w);
  dest->inc_phi(source->get_phi() * source->get_vol() * w);
  dest->unlock();
}

/** Interpolate from a particle onto the specified node; override to have different magnetic field evolutions. */
void Interpolator::advectToGridNode(Particle* source, GridNode* dest, SlurmDouble w, unsigned long quants)
{
  dest->lock();
  // Advect particle's momentum, not velocity
  if (((quants & QUANT_VX) == QUANT_VX) || ((quants & QUANT_VY) == QUANT_VY) || ((quants & QUANT_VZ) == QUANT_VZ))
    dest->inc_u(source->get_ux() * source->get_m() * w, source->get_uy() * source->get_m() * w, source->get_uz() * source->get_m() * w);
  if ((quants & QUANT_MASS) == QUANT_MASS) 
    dest->inc_m(source->get_m() * w);
  if ((quants & QUANT_VOL) == QUANT_VOL) 
    dest->inc_vol(source->get_vol() * w);
  if ((quants & QUANT_WEIGHT) == QUANT_WEIGHT) 
    dest->inc_weight(w);
  if (((quants & QUANT_MAGNX) == QUANT_MAGNX) || ((quants & QUANT_MAGNY) == QUANT_MAGNY) || ((quants & QUANT_MAGNZ) == QUANT_MAGNZ))
    dest->inc_A(source->get_Ax() * w, source->get_Ay() * w, source->get_Az() * w);
  dest->unlock();
}

/** 
  Interpolate from a particle onto the specified node; override to have different magnetic field evolutions. 
  QUANT_VX | QUANT_VY | QUANT_VZ | QUANT_WEIGHT | QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ | QUANT_MASS;
*/
void Interpolator::advectToGridNode(Particle* source, GridNode* dest, SlurmDouble w)
{
  dest->lock();
  // Advect particle's momentum, not velocity
  dest->inc_u(source->get_ux() * source->get_m() * w, source->get_uy() * source->get_m() * w, source->get_uz() * source->get_m() * w);
  dest->inc_m(source->get_m() * w);
  dest->inc_vol(source->get_vol() * w);
  dest->inc_weight(w);
  advectNodeField(source, dest, w);
  dest->unlock();
}

/** Interpolates cell center quantities on the given point, and returns the corresponding GridCell object. */
GridCell* Interpolator::interpolateCells(GridPoint* p, unsigned long quants)
{
  GridCell* c = new GridCell(); // all quantities should be zero
  GridCell* my_cell;
  SlurmDouble w; // total interpolation weight

  SlurmDouble weights[get_InterpolCellsNumber()];
  GridCell* adj_cells[get_InterpolCellsNumber()];

  getInterpolationCells(p, adj_cells, weights);
  for (SlurmInt i = 0; i < get_InterpolCellsNumber(); ++i)
  {
    my_cell = adj_cells[i];
    w = weights[i];
    if ((quants & QUANT_RHO) == QUANT_RHO) 
      c->inc_rho(my_cell->get_rho() * w);
    if ((quants & QUANT_ENERGY) == QUANT_ENERGY) 
      c->inc_e(my_cell->get_e() * w);
    // Quantities needed during particles advancement
    if ((quants & QUANT_INTERNAL) == QUANT_INTERNAL) 
    {
      c->inc_de(my_cell->get_de() * w);
      c->inc_gradu(my_cell, w);
    }
  }

  return c;
}

/** 
  Interpolates energy change and velocity gradient (de, gradu) during particle advancement. 
*/
GridCell* Interpolator::interpolateCells(GridPoint* p)
{
  GridCell* c = new GridCell(); // all quantities should be zero
  GridCell* my_cell;
  SlurmDouble w; // total interpolation weight

  SlurmDouble weights[get_InterpolCellsNumber()];
  GridCell* adj_cells[get_InterpolCellsNumber()];

  getInterpolationCells(p, adj_cells, weights);
  for (SlurmInt i = 0; i < get_InterpolCellsNumber(); ++i)
  {
    my_cell = adj_cells[i];
    w = weights[i];
    c->inc_de(my_cell->get_de() * w);
    c->inc_gradu(my_cell, w);
  }

  return c;
}

/** 
  Advect from the given particle onto adjacent 8 cells and 8 nodes (in 3D)
  When advecting to the cells, it accounts for the boundary-related connectivity which is set in the Grid object.
  For instance, the 'advection cell' for a ghost cell on a reflective boundary is its corresponding neighbor inside the normal (non-ghost) grid.
*/
void Interpolator::advectToGrid(Particle* p, unsigned long quants)
{
  SlurmDouble c_weights[get_InterpolCellsNumber()];
  GridCell* adj_cells[get_InterpolCellsNumber()];
  SlurmDouble n_weights[get_InterpolNodesNumber()];
  GridNode* adj_nodes[get_InterpolNodesNumber()];

  // Find the cells to advect to, and the corresponding weights, including the boundary-related connectivity.
  getInterpolationCells(p, adj_cells, c_weights);
  // The neighbor nodes are flipped over the edges according to periodic BCs. 
  for (SlurmInt i = 0; i < get_InterpolCellsNumber(); ++i) advectToGridCell(p, adj_cells[i], c_weights[i], quants);

  getInterpolationNodes(p, adj_nodes, n_weights);
  for (SlurmInt i = 0; i < get_InterpolNodesNumber(); ++i) advectToGridNode(p, adj_nodes[i], n_weights[i], quants);
}

/** Advect from the given particle onto adjacent 8 cells and 8 nodes (in 3D)
  When advecting to the cells, it accounts for the boundary-related connectivity which is set in the Grid object.
  For instance, the 'advection cell' for a ghost cell on a reflective boundary is its corresponding neighbor inside the normal (non-ghost) grid.
*/
void Interpolator::advectToGrid(Particle* p)
{
  SlurmDouble c_weights[get_InterpolCellsNumber()];
  GridCell* adj_cells[get_InterpolCellsNumber()];
  SlurmDouble n_weights[get_InterpolNodesNumber()];
  GridNode* adj_nodes[get_InterpolNodesNumber()];

  // Find the cells to advect to, and the corresponding weights, including the boundary-related connectivity.
  getInterpolationCells(p, adj_cells, c_weights);
  // The neighbor nodes are flipped over the edges according to periodic BCs. 
  for (SlurmInt i = 0; i < get_InterpolCellsNumber(); ++i) advectToGridCell(p, adj_cells[i], c_weights[i]);

  getInterpolationNodes(p, adj_nodes, n_weights);
  for (SlurmInt i = 0; i < get_InterpolNodesNumber(); ++i) advectToGridNode(p, adj_nodes[i], n_weights[i]);
}


// Linear interpolator

/** 
  A general routine that interpolates the specified quantities from grid nodes.
  It is slower, therefore normally it is called only on Particles initialization.
  Interpolates node quantities on the given point, and returns the corresponding GridNode object. 

  Note, when mfemAdvectParticles is used, set the magnetic field quantities in quants!
  For mfemInterpolateParticles it is not needed.

  The caller is responsible to free the new node's memory afterwards.
*/
GridNode* LinearInterpolator::interpolateNodes(GridPoint* p, unsigned long quants)
{
  GridNode* n = new GridNode(); // all quantities should be zero
  GridNode* my_node;
  SlurmDouble w; // Total interpolation weight

  SlurmDouble weights[get_InterpolNodesNumber()];
  GridNode* adj_nodes[get_InterpolNodesNumber()];
  
  getInterpolationNodes(p, adj_nodes, weights);
  for (SlurmInt i = 0; i < get_InterpolNodesNumber(); ++i)
  {
    my_node = adj_nodes[i];
    w = weights[i];
  
    if (((quants & QUANT_VX) == QUANT_VX) || ((quants & QUANT_VY) == QUANT_VY) || ((quants & QUANT_VZ) == QUANT_VZ))
    {
      n->inc_u(my_node->get_ux() * w, my_node->get_uy() * w, my_node->get_uz() * w);
      n->inc_du(my_node->get_dux() * w, my_node->get_duy() * w, my_node->get_duz() * w);
    }
    if ((quants & QUANT_MASS) == QUANT_MASS) 
      n->inc_m(my_node->get_m() * w);
    if ((quants & QUANT_WEIGHT) == QUANT_WEIGHT) 
      n->inc_weight(w);

    // Magnetic field, if specified
    if (((quants & QUANT_MAGNX) == QUANT_MAGNX) || ((quants & QUANT_MAGNY) == QUANT_MAGNY) || ((quants & QUANT_MAGNZ) == QUANT_MAGNZ))
    {
      n->inc_A(my_node->get_Ax() * w, my_node->get_Ay() * w, my_node->get_Az() * w);
      n->inc_dA(my_node->get_dAx() * w, my_node->get_dAy() * w, my_node->get_dAz() * w);
    }
  }

  return n;
}

/** 
  Interpolates the default node quantities to prticle when magnetic field evolution is off. 
  The caller is responsible to free the new node's memory afterwards.

  Only u and du are needed to advance the particles when magnetic field evolution is off.
*/
GridNode* LinearInterpolator::interpolateNodes(GridPoint* p)
{
  GridNode* n = new GridNode(); // all quantities should be zero
  GridNode* my_node;
  SlurmDouble w; // Total interpolation weight

  SlurmDouble weights[get_InterpolNodesNumber()];
  GridNode* adj_nodes[get_InterpolNodesNumber()];
  
  getInterpolationNodes(p, adj_nodes, weights);
  
  for (SlurmInt i = 0; i < get_InterpolNodesNumber(); ++i)
  {
    my_node = adj_nodes[i];
    w = weights[i];
  
    n->inc_u(my_node->get_ux() * w, my_node->get_uy() * w, my_node->get_uz() * w);
    n->inc_du(my_node->get_dux() * w, my_node->get_duy() * w, my_node->get_duz() * w);
  }
  return n;
}


// Linear interpolator MHD

/** 
  Here, interpolation of dA is added on top of u and du.
 
  Interpolates the default node quantities (u, du, dA) when magnetic field evolution is used.
  Returns a new GridNode object. The caller is responsible to free the new node's memory afterwards.
*/
GridNode* LinearInterpolatorMHD::interpolateNodes(GridPoint* p)
{
  GridNode* n = new GridNode(); // all quantities should be zero
  GridNode* my_node;
  SlurmDouble w; // Total interpolation weight

  SlurmDouble weights[get_InterpolNodesNumber()];
  GridNode* adj_nodes[get_InterpolNodesNumber()];
  
  getInterpolationNodes(p, adj_nodes, weights);
  
  for (SlurmInt i = 0; i < get_InterpolNodesNumber(); ++i)
  {
    my_node = adj_nodes[i];
    w = weights[i];
  
    n->inc_u(my_node->get_ux() * w, my_node->get_uy() * w, my_node->get_uz() * w);
    n->inc_du(my_node->get_dux() * w, my_node->get_duy() * w, my_node->get_duz() * w);
    // Interpolate the change of A
    n->inc_dA(my_node->get_dAx() * w, my_node->get_dAy() * w, my_node->get_dAz() * w);
  }

  return n;
}


// Linear interpolator that tracks neighbor particles

/// Advect from the given particle onto adjacent 8 cells and 8 nodes (in 3D)
void LinearInterpolatorIP::advectToGrid(Particle* p)
{
  SlurmDouble c_weights[get_InterpolCellsNumber()];
  GridCell* adj_cells[get_InterpolCellsNumber()];
  SlurmDouble n_weights[get_InterpolNodesNumber()];
  GridNode* adj_nodes[get_InterpolNodesNumber()];

  // Find the cells to advect to, and the corresponding weights, including the boundary-related connectivity.
  getInterpolationCells(p, adj_cells, c_weights);
  // The neighbor nodes are flipped over the edges according to periodic BCs. 
  for (SlurmInt i = 0; i < get_InterpolCellsNumber(); ++i) advectToGridCell(p, adj_cells[i], c_weights[i]);

  getInterpolationNodes(p, adj_nodes, n_weights);
  for (SlurmInt i = 0; i < get_InterpolNodesNumber(); ++i) advectToGridNode(p, adj_nodes[i], n_weights[i]);

  // Update the neighbor particles if needed. Not flipped around the periodic BCs here.
  get_grid()->updateParticleNodeNeighbors(p);
}


// Quadratic interpolator which uses grid cells

/// Constructor
QuadraticInterpolatorCC::QuadraticInterpolatorCC(GridCommon *grid): LinearInterpolatorMHD(grid) 
{
  set_InterpolCellsNumber(InterpolWeightsNumberQuadratic);
  set_InterpolNodesNumber(InterpolWeightsNumberLinear);
}

/** 
  Interpolates the cell quantities during particle advancement.
  It uses particle's geometric neighbor cells to advect from cells.
  cells -> particles: de, velocity gradient
*/
GridCell* QuadraticInterpolatorCC::interpolateCells(GridPoint* p)
{
  GridCell* c = grid->getClosestCell(p)->interpolateToParticle(p);
  return c;
}

/** 
  Interpolates the cell quantities on initialization
*/
GridCell* QuadraticInterpolatorCC::interpolateCells(GridPoint* p, unsigned long quants)
{
  GridCell* c = grid->getClosestCell(p)->interpolateToParticle(p, quants);
  return c;
}

/** 
  Advect from the given particle onto adjacent 27 cells and 8 nodes.
  When advecting to the cells, it does it into 'logical' neighbors advCell, to account for periodic or reflective BCs.
  For instance, the 'advection cell' for a ghost cell on a reflective boundary is its corresponding neighbor inside the normal (non-ghost) grid.

  particle->nodes: momentum, mass, volume, weight, vector potential A.
  particle->cells: energy, mass, volume, weight.
*/
void QuadraticInterpolatorCC::advectToGrid(Particle* p)
{
  // Find the cells to advect to, and the corresponding weights, including the boundary-related connectivity.
  grid->getClosestCell(p)->advectFromParticle(p);

  // For nodes, use traditional way for now
  SlurmDouble n_weights[get_InterpolNodesNumber()];
  GridNode* adj_nodes[get_InterpolNodesNumber()];

  getInterpolationNodes(p, adj_nodes, n_weights);
  for (SlurmInt i = 0; i < get_InterpolNodesNumber(); ++i) advectToGridNode(p, adj_nodes[i], n_weights[i]);

  // Update the neighbor particles if needed. Not flipped around the periodic BCs here.
  get_grid()->updateParticleNodeNeighbors(p);
}
