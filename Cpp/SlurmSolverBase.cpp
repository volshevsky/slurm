/** 
  A base for Slurm solvers.
  Implements output, in particular.

  Started in April 2015
*/
#include "SlurmSolverBase.h"

using namespace std;

/** Read the most basic and common inputfile parameters */
SlurmSolverBase::SlurmSolverBase(ConfigFile *config)
{
  // Initial condition
  this->initialCondition = InitialCondition::makeInitial(config);

  // Fields object
  fields = new Fields(config, initialCondition);
  
  // We want to save magnetic fields only when the field is enabled
  if (fields->get_FieldEvolutionMethod() != mfemOff)
  {
    NodeOutputQuantities |= QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ | QUANT_MAGNETIC;
    CellOutputQuantities |= QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ | QUANT_MAGNETIC | QUANT_SCALARPOTENTIAL;
    ParticleOutputQuantities |= QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ;
  }
  
  // Time settings
  this->itStart = config->read<SlurmInt>("itStart", 0);
  this->dt = config->read <SlurmDouble>("dt", 0.001);
  this->nt = config->read <SlurmInt>("nt", 10);
  this->OutputCycle = config->read<SlurmInt>("OutputCycle", 1);
  this->GridOutputCycle = config->read<SlurmInt>("GridOutputCycle", OutputCycle);
  this->ParticlesOutputCycle = config->read<SlurmInt>("ParticlesOutputCycle", OutputCycle);
  this->CurrTime = dt * itStart;
  this->MaxTime = dt * (nt + 1);
  this->SaveGhost = config->read<SlurmInt>("SaveGhost", 0) != 0;

  // Create log file. Always append to the existing one?
  if (this->OutputCycle > 0)
  {
    ofstream log_file(OutputFileNameLog.c_str());
    log_file << "timestep" << "," << "CurrentTime" << "," << "ParticleIntEnergy" << "," << "ParticleKinEnergy" << "," << "CellsIntEnergy" << "," << "CellsKinEnergy" << ","  << "CellsMagEnergy" << endl;
    log_file.close();
  }
}


/** 
  Save grid data to VTI (nodes [mass, vx, vy, vz]; cells [mass, vol, rho, p, Bx, By, Bz]).

  The solver object gathers data from the grid (which doesn't know about output),
  and passes it to the IOManager that saves the data into a file.
*/
void SlurmSolverBase::outputGrid(IOManager* io, const string suffix)
{
  messageline("SlurmSolverBase: saving grid.");
  vector<string> node_names;
  vector<string> cell_names;
  vector<string> cell_vectors;
  vector<string> node_vectors;
  if (((NodeOutputQuantities & QUANT_VX) == QUANT_VX) && ((NodeOutputQuantities & QUANT_VY) == QUANT_VY) && ((NodeOutputQuantities & QUANT_VZ) == QUANT_VZ))
    node_vectors.push_back("u");
  if (((NodeOutputQuantities & QUANT_MAGNX) == QUANT_MAGNX) && ((NodeOutputQuantities & QUANT_MAGNY) == QUANT_MAGNY) && ((NodeOutputQuantities & QUANT_MAGNZ) == QUANT_MAGNZ))
    node_vectors.push_back("A");
  if (((CellOutputQuantities & QUANT_MAGNX) == QUANT_MAGNX) && ((CellOutputQuantities & QUANT_MAGNY) == QUANT_MAGNY) && ((CellOutputQuantities & QUANT_MAGNZ) == QUANT_MAGNZ))
    cell_vectors.push_back("B");
  SlurmDoubleArray4D node_data = grid->getNodeScalarsArray(NodeOutputQuantities, &node_names, SaveGhost);
  SlurmDoubleArray4D cell_data = grid->getCellScalarsArray(CellOutputQuantities, &cell_names, SaveGhost);

  // Starting coordinates for the output arrays
  SlurmDouble x0 = SaveGhost ? -grid->get_dx() : 0;
  SlurmDouble y0 = SaveGhost ? -grid->get_dy() : 0;
  SlurmDouble z0 = SaveGhost ? -grid->get_dz() : 0;
  io->writeVTR(OutputFileNameGrid + suffix, cell_data, cell_names, node_data, node_names, grid->get_dx(), grid->get_dy(), grid->get_dz(), cell_vectors, node_vectors, x0, y0, z0);
}


/** This one converts the timestep into filename suffix */
void SlurmSolverBase::outputGrid(IOManager* io, SlurmInt it)
{
  this->outputGrid(io, NumberToFixedLengthString(it, 6));
}


/** 
  Save particle data to an unstructured grid file.

  The solver object gathers data from particles (which doesn't know about output),
  and passes it to the IOManager that saves the data into a VTU/VTK file.
*/
void SlurmSolverBase::outputParticles(IOManager* io, const string suffix)
{
  messageline("SlurmSolverBase: saving particles.");
  vector<string> scalar_names;
  vector<string> vector_names;
  if (((ParticleOutputQuantities & QUANT_VX) == QUANT_VX) && ((ParticleOutputQuantities & QUANT_VY) == QUANT_VY) && ((ParticleOutputQuantities & QUANT_VZ) == QUANT_VZ))
    vector_names.push_back("u");
  if (((ParticleOutputQuantities & QUANT_MAGNX) == QUANT_MAGNX) && ((ParticleOutputQuantities & QUANT_MAGNY) == QUANT_MAGNY) && ((ParticleOutputQuantities & QUANT_MAGNZ) == QUANT_MAGNZ))
    vector_names.push_back("A");

  SlurmDoubleArray2D data = particles->exportAsArray(ParticleOutputQuantities, &scalar_names);
  io->writeVTU(OutputFileNameParticles + suffix, data, scalar_names, vector_names);
}


/** This one converts the timestep into filename suffix */
void SlurmSolverBase::outputParticles(IOManager* io, SlurmInt it)
{
  this->outputParticles(io, NumberToFixedLengthString(it, 6));
}


/** Conditionally output particles and grid. */
void SlurmSolverBase::doOutput(IOManager* io, SlurmInt it)
{
  if ((OutputCycle > 0) && (it > 0) && (it % OutputCycle == 0))
  {
    // Write log
    ofstream log_file(OutputFileNameLog.c_str(), fstream::app);
    log_file << it << "," << CurrTime << "," << particles->get_InternalEnergy() << "," << particles->get_KineticEnergy() << "," << grid->get_InternalEnergy() << "," << grid->get_KineticEnergy() << ","  << grid->get_MagneticEnergy() << endl;
    log_file.close();    
  }
  if ((GridOutputCycle > 0) && (it % GridOutputCycle == 0))
  {
     outputGrid(io, it);
  }
  if ((ParticlesOutputCycle > 0) && (it % ParticlesOutputCycle == 0))
  {
     outputParticles(io, it);
  }
}


/** What are the conditions to finish? Exceeding time limit, exceeding timestep number limit, etc. */
bool SlurmSolverBase::isDone()
{
  if (CurrTime > MaxTime) 
  {
    return true;
  }
  else 
  {
    return false;
  }
}

/** What are the conditions to finish? Exceeding time limit, exceeding timestep number limit, etc. */
bool SlurmSolverBase::isDone(SlurmInt it)
{
  if (it > nt) 
  {
    return true;
  }
  else 
  {
    return false;
  }
}
