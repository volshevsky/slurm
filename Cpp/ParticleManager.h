#pragma once

#include <list>
#include "Grid.h"
#include "Interpolators.h"
#include "ConfigFile.h"

using namespace std;

/** Data structure for keeping particles. Instead a list there might be, e.g., a vector. A vector is faster to operate, but insertion/deletion in the middle is tough. */
typedef list<Particle> ParticleList;
/** Iterator over the particle list. */
typedef list<Particle>::iterator ParticleIterator;

/** 
  Class ParticleManager operates a list of particles in the Slurm.
  It knows the Grid object: all interpolation is handled by the Grid; ParticleManager only provides an interface to it.

  Particles carry all the information; the grid is only used at intermediate steps. 
  ParticleManager operates a list of particles, it uses extra memory, but is more convenient to insert/delete/sort particles.
*/
class ParticleManager {
private:
  /// Particles!
  ParticleList items;
  /// Initial number of particles per cell in X
  SlurmInt npx;
  /// Initial number of particles per cell in Y
  SlurmInt npy;
  /// Initial number of particles per cell in Z
  SlurmInt npz;
  /// Total energy of particles
  SlurmDouble InternalEnergy;
  /// Helps to compute total energy
  omp_lock_t InternalEnergyLock;
  /// Total kinetic energy
  SlurmDouble KineticEnergy;
  /// Helps to compute total kinetic energy
  omp_lock_t KineticEnergyLock;
  /// Each OutputRange's particle will be saved.
  std::list<Particle>::size_type OutputRange = 1;
  /// Interpolator
  Interpolator * interpolator;
  /// Initial condition is worth keeping here too
  InitialCondition * initial;

  /// An array of the starting positions in the list of particles
  ParticleIterator * IteratorsBegin;
  /// Array of end positions in the list of particles
  ParticleIterator * IteratorsEnd;
  /// Current number of OpenMP threads
  int IteratorsNumberOfThreads;
  /// Recompute the ranges that each process cycles over particles  
  void computeOMPRanges();
  /// Compute particle range for this thread
  void computeOMPThreadRange();
  /// Current number of particles
  std::list<Particle>::size_type CurrentNumber;
  /// Lock for the CurrentNumber
  omp_lock_t CurrentNumberLock;
  /// Number of particles deleted since last recomputation of ranges
  std::list<Particle>::size_type DeletedNumber; 
  /// Lock for the DeletedNumber
  omp_lock_t DeletedNumberLock;

  /// Spatial extent of a single particle on its creation. Normally nxc/npx
  SlurmDouble dxp, dyp, dzp;

  /// Cylindrical distribution/injection of particles.
  SlurmDouble CylInjectRadius = 0.;
  /// Azimuthal distance between injected particles
  SlurmDouble CylInjectDPsi = 0.;
  /// Number of particles in the azimuthal direction
  SlurmInt CylInjectNPsi = 0;
  /// The radial distance between injected particles
  SlurmDouble CylInjectDr = 0.;
  /// Number of particles in the radial direction
  SlurmInt CylInjectNr = 0;
  /// Center of the Sun coordinates
  GridPoint CylInitCenter;

public:
  // Property getters/setters
  /// Returns total internal energy
  SlurmDouble get_InternalEnergy() { return InternalEnergy; }
  /// Total kinetic energy
  SlurmDouble get_KineticEnergy() { return KineticEnergy; }

  // Initial conditions
  /// Creates the particles and distributes them uniformly.
  void initUniform(Grid* grid);
  /// Creates the particles and distributes them uniformly.
  void initCylindrical(Grid* grid, ConfigFile *config);
  /// Sets initial condition for the grid, then interpolates on particles 
  void init(Grid *grid, ConfigFile *config);

  // In the interpolation, the Manager only provides an interface; the math is done by the Grid.
  /// Particles -> grid interpolation (both cells and nodes in one loop). 
  void interpolateToGrid(Grid *grid);
  /// Interpolate from grid, compute gradients, advance the particles, and impose boundary conditions.
  void advanceExplicit(SlurmDouble dt, Grid *grid);
  /** Advance particle's position in one dimension. */
  bool moveParticle(BoundaryCondition* bc_left, BoundaryCondition* bc_right, SlurmDouble new_position, SlurmDouble& pposition, SlurmDouble& pvelocity);
  /// Inject particles
  void injectParticles(Grid *grid, SlurmDouble t, SlurmDouble dt);

  // Import/export
  /// Exports all particle data as a boost array
  SlurmDoubleArray2D exportAsArray(unsigned long quants, vector<string> *quant_names);

  // Particle list manipulation
  /// Returns the real number of items in the list. This is very complex, use get_CurrentNumber instead!
  std::list<Particle>::size_type get_count() { return items.size(); }
  /// Returns the real number of items in the list. This is very complex, use get_CurrentNumber instead!
  std::list<Particle>::size_type get_CurrentNumber() { return CurrentNumber; }
  /// Adds the already created particle to the list
  std::list<Particle>::size_type addParticle(Particle* p);
  /// Create a new particle at a given location, and increase the number of particles
  Particle * addParticle(SlurmDouble x, SlurmDouble y, SlurmDouble z);
  /// Removes a particle
  std::list<Particle>::size_type deleteParticle(ParticleIterator ip);
  /// CurrentNumber setter
  void set_CurrentNumber(std::list<Particle>::size_type value);
  /// Increments the current number of particles
  void inc_CurrentNumber();
  /// Decrements the number of particles
  void dec_CurrentNumber();
  /// DeletedNumber setter
  void set_DeletedNumber(std::list<Particle>::size_type value);
  /// Increments the current number of particles
  void inc_DeletedNumber();
  /// Remove the particles marked for deletion
  void cleanDeleted();
  
  /** Print particle info to STDOUT */
  void print();

  // Volume Evolution
  /// vemMaterialPoint strategy of volume evolution (Sadeghirad et al. 2011)
  static void updateVolumeMP(Particle * p, GridCell * c, SlurmDouble dt)
  {
    // Volume evolution. Traditional way. Vp^n+1=Vp^0*det(J)
    p->set_vol(p->get_vol0() * (p->get_J11() * p->get_J22() * p->get_J33() + p->get_J12() * p->get_J23() * p->get_J31() + p->get_J13() * p->get_J21() * p->get_J32() - p->get_J13() * p->get_J22() * p->get_J31() - p->get_J12() * p->get_J21() * p->get_J33() - p->get_J11() * p->get_J23() * p->get_J32()));    
  };
  /// vemVelocityGradient strategy of volume evolution
  static void updateVolumeVG(Particle * p, GridCell * c, SlurmDouble dt)
  {
    p->inc_vol(p->get_vol() * dt * c->get_divu());
  }; 
  /// vemOff, no volume evolution. Do nothing
  static void updateVolumeOff(Particle * p, GridCell * c, SlurmDouble dt) {};
  /// The method currently used to update particle's volume in advanceExplicit()
  void (*updateParticleVolume)(Particle *, GridCell *, SlurmDouble);

  // Magnetization evolution
  /// mfemAdvectParticles strategy of magnetic field evolution. A^n + 1 = A^n + dt*sum_g(-(E+Eext) - eta*J + (u.nabla)A)
  static void updateMagnetizationAP(Particle * p, GridNode * n)
  {
    // We interpolated the change of A from the grid!
    p->inc_A(n->get_dA());
  }; 
  /// mfemOff, no field. Do nothing
  static void updateMagnetizationOff(Particle * p, GridNode * n) {};
  /// The method currently used to update particle's magnetization in advanceExplicit()
  void (*updateParticleMagnetization)(Particle *, GridNode *);

  void set_InternalEnergy(SlurmDouble value);
  void set_KineticEnergy(SlurmDouble value);

  /// Constructor
  ParticleManager(Grid* grid, string scheme, MFEvolutionMethods MFEvolution);
  ParticleManager();
  ~ParticleManager();
};

