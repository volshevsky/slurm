cmake_minimum_required(VERSION 2.8)

project (Slurm)

set (Slurm_VERSION_MAJOR 1)
set (Slurm_VERSION_MINOR 1)

# Compiler settings

# So far tested on g++ and clang (LLVM)
set (CMAKE_CXX_COMPILER "g++")
#In debug mode, profiling options are added automatically
set (CMAKE_CXX_FLAGS "-Wall -std=c++11 -O3 -fopenmp -lpthread")

# Build type. Possible values: empty, Debug, Release, RelWithDebInfo and MinSizeRel
# Enable debug symbols by default
set (CMAKE_BUILD_TYPE Debug)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  if ("${CMAKE_BUILD_TYPE}" MATCHES Debug)
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopt-info-vec -pg")
  endif()
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  #set (CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -l /usr/lib/gcc/x86_64-linux-gnu/5/include/" )
endif()
set (CMAKE_CXX_FLAGS "${OpenMP_CXX_FLAGS} ${CMAKE_CXX_FLAGS}")

# Slurm libraries and executables
include_directories("${PROJECT_SOURCE_DIR}")

add_executable(Slurm Slurm.cpp)

add_library(ConfigFile ConfigFile.cpp)
add_library(IOManager IOManager.cpp)
add_library(SlurmSolverBase SlurmSolverBase.cpp)
add_library(SlurmSolverExplicit SlurmSolverExplicit.cpp)
add_library(ParticleManager ParticleManager.cpp)
add_library(Grid Grid.cpp)
add_library(GridCommon GridCommon.cpp)
add_library(Fields Fields.cpp)
add_library(SlurMath SlurMath.cpp)
add_library(InitialConditions InitialConditions.cpp)
add_library(Interpolators Interpolators.cpp)
add_library(BoundaryConditions BoundaryConditions.cpp)

# VTK libraries. Comment the below string if there is no VTK installed.
#set (VTK_ENABLED)
if (DEFINED VTK_ENABLED)
  set (VTK_DIR /home/sya/VTK-build)
  find_package(VTK REQUIRED)
  include(${VTK_USE_FILE})
else()
  set (VTK_LIBRARIES "")
endif (DEFINED VTK_ENABLED)

target_include_directories (Slurm PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries (Slurm SlurmSolverExplicit SlurmSolverBase IOManager ConfigFile ParticleManager Grid GridCommon Fields SlurMath InitialConditions Interpolators BoundaryConditions ${VTK_LIBRARIES})
