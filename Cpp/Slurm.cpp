/** 
  Main program for the 2D fluid particle-in-cell solver Slurm.

  Vyacheslav Olshevsky, Nov 2014 (sya@mao.kiev.ua)
*/

#include "utils.h"
#include "SlurmSolverExplicit.h"
#include "IOManager.h"
#include "ConfigFile.h"

#include <time.h>

using namespace std;

int main(int argc, char **argv)
{
  // Time control
  struct timespec tstart={0,0}, tend={0,0};

  messageline("===========================================================");
  messageline("Slurm started\n");
  
  // Read the configuration file
  string configname;
  if (argc < 2) configname = "config";
  else configname = argv[1];

  // Read the config
  ConfigFile *config = new ConfigFile(configname);

  // Limit the number of OpenMP threads if needed
  if (config->read<SlurmInt>("NumberOfThreads", -1) > 0) {
    omp_set_dynamic(config->read<SlurmInt>("DynamicThreads", 0));
    omp_set_num_threads(config->read<SlurmInt>("NumberOfThreads"));
  }
  messageline("Requested number of OMP threads: ", omp_get_max_threads());
  messageline("Maximum (system) number of processes: ", omp_get_num_procs());
  messageline("Dynamic thread allocation? ", omp_get_dynamic());
  messageline("===========================================================\n");
   
  // Create the solver
  SlurmSolverExplicit *solver = new SlurmSolverExplicit(config);

  // Create the I/O manager
  IOManager *io = new IOManager();
  
  // Run the solver
  clock_gettime(CLOCK_MONOTONIC, &tstart);
  int result = solver->run(io);  
  clock_gettime(CLOCK_MONOTONIC, &tend);

  messageline("\n===========================================================");
  if (result == 0)
    message("Slurm simulation successfully finished in ");
  else
    messageline("Slurm solver returned an exit code ", result);
  messageline(((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec), " seconds");
  messageline("===========================================================");

  return 0;
}

