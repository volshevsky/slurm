/**
  Basic definitions and algorithms for individual grid elements in the Slurm project.

  V. Olshevsky, Jan 2016, sya@mao.kiev.ua.
*/
#pragma once

#include <cstddef>
#include "omp.h"
#include "utils.h"
#include "SlurMath.h"

/// Grid dimansionality, must be 3 in this version of Slurm
const int GridDimensionality = 3;

/// Defines possible roles of grid elements
enum GridElementRole {gerNormal = 1, gerBoundary = 2, gerGhost = 3};

/// Irregular grid interpolation tolerance
const SlurmDouble TIITolerance = 1e-14;
/// Upper limit for a, b, c coefficients in trilinear irregular interpolation. In the original bc3wind scheme it equals 5.
const SlurmDouble TIIUpperLimit = 2.0;
/// Maximum number of iterations when computing trilinear irregular interpolation weights
const SlurmInt TIIMaxIterations = 20;
/// Small float for trilinear interpolation
const SlurmDouble TIISmallDOuble = 1e-20;

// Forward declarations
class GridNode;
class GridCell;
class BoundaryCondition;

// Arrays for the rectilinear grids

/// Array of cells. Size of this array is 160 bytes + elements, i.e., sizeof(CellArray) + sizeof(GridCell)*nx*ny*nz
typedef boost::multi_array<GridCell, GridDimensionality> CellArray;

/// Array of cell pointers
typedef boost::multi_array<GridCell*, GridDimensionality> CellRefArray;

/// Array of nodes
typedef boost::multi_array<GridNode, GridDimensionality> NodeArray;

/// Array of node pointers
typedef boost::multi_array<GridNode*, GridDimensionality> NodeRefArray;

/// Extent range for cell array
typedef CellArray::extent_range CellExtentRange;

/// Extent range for node array
typedef NodeArray::extent_range NodeExtentRange;


/** Just a point with coordinates */
class GridPoint 
{
protected:
  /// Coordinates in the 3D space: X
  SlDoubleVector xyz = {0., 0., 0.};
public:
  inline SlurmDouble get_x() {return xyz[0];}
  inline SlurmDouble get_y() {return xyz[1];}
  inline SlurmDouble get_z() {return xyz[2];}
  inline SlurmDouble& fetch_x() {return xyz[0];}
  inline SlurmDouble& fetch_y() {return xyz[1];}
  inline SlurmDouble& fetch_z() {return xyz[2];}
  inline SlDoubleVector get_xyz() {return xyz;}
  inline SlurmDouble get_xyz(SlurmInt i) {return xyz[i];}
  inline void set_x(SlurmDouble value) {xyz[0] = value;}
  inline void set_y(SlurmDouble value) {xyz[1] = value;}
  inline void set_z(SlurmDouble value) {xyz[2] = value;}
  void set_xyz(SlurmInt i, SlurmDouble value) { xyz[i] = value; }
  void set_xyz(SlDoubleVector& value) { xyz = value; }
  void set_xyz(SlurmDouble x, SlurmDouble y, SlurmDouble z) 
  { 
    xyz[0] = x; 
    xyz[1] = y; 
    xyz[2] = z; 
  }

  GridPoint() {}

  GridPoint(SlurmDouble x, SlurmDouble y, SlurmDouble z)
  {
    xyz[0] = x;
    xyz[1] = y;
    xyz[2] = z;
  }

  GridPoint(SlDoubleVector coords) 
  {
    xyz = coords;
  }

  /** Create a new object with the same coordinates */
  GridPoint * clone()
  {
    GridPoint *dest = new GridPoint();
    dest->set_xyz(xyz);
    return dest;    
  }

  /// Compute distance to another point
  inline SlurmDouble distance(GridPoint* p)
  {
    return pow(pow(xyz[0] - p->get_x(), 2) + pow(xyz[1] - p->get_y(), 2) + pow(xyz[2] - p->get_z(), 2), 0.5);
  }

  /// Compute the distance to the point given an array of x, y, z
  inline SlurmDouble distance(SlDoubleVector p)
  {
    return pow(pow(xyz[0] - p[0], 2) + pow(xyz[1] - p[1], 2) + pow(xyz[2] - p[2], 2), 0.5);
  }

  /// Compute the distance to the point given its x, y, z
  inline SlurmDouble distance(SlurmDouble x, SlurmDouble y, SlurmDouble z)
  {
    return pow(pow(xyz[0] - x, 2) + pow(xyz[1] - y, 2) + pow(xyz[2] - z, 2), 0.5);
  }

  /// Return the length is the point represents a physical vector
  inline SlurmDouble get_length()
  {
    return pow(xyz[0]*xyz[0] + xyz[1]*xyz[1] + xyz[2]*xyz[2], 0.5);
  }

  /// Distance to a line represented by two points on it
  SlurmDouble distanceToLine(SlDoubleVector A1, SlDoubleVector A2);

  /// Rotate the coordinate system around the specified axis
  GridPoint * rotate_coords(SlurmInt normal, SlurmDouble theta);

  /// Shift the coords right
  void shift_coords(GridPoint * center);

  /// Shift the coords left
  void shift_coords_inv(GridPoint * center);

  virtual ~GridPoint() {}
};

/** MaterialPoint declares common properties of nodes, cells and particles. */
class MaterialPoint : public GridPoint
{
protected:

  /// Element's physical mass
  SlurmDouble mass = 0.;
  /// Element's physical volume
  SlurmDouble vol = 0.;

public:

  inline SlurmDouble get_m() {return mass;}
  inline void set_m(SlurmDouble value) {mass = value;}
  inline void inc_m(SlurmDouble value) {mass += value;}

  inline SlurmDouble get_vol() {return vol;}
  inline void set_vol(SlurmDouble value) {vol = value;}
  inline void inc_vol(SlurmDouble value) {vol += value;}

  MaterialPoint() : GridPoint() {}
  virtual ~MaterialPoint() {}
};

/** Defines properties common to nodes and particles */
class PointVelocity
{
protected:

  /// Velocity components 
  SlDoubleVector u = {0., 0., 0.};

  /// Particles and nodes carry electromagnetic vector potential
  SlDoubleVector A = {0., 0., 0.};

public:

  inline SlurmDouble get_ux() {return u[0];}
  inline SlurmDouble get_uy() {return u[1];}
  inline SlurmDouble get_uz() {return u[2];}
  inline SlurmDouble get_u(SlurmInt i) {return u[i];}
  inline SlDoubleVector get_u() {return u;}
  inline SlurmDouble& fetch_u(SlurmInt i) {return u[i];}
  inline SlurmDouble& fetch_ux() {return u[0];}
  inline SlurmDouble& fetch_uy() {return u[1];}
  inline SlurmDouble& fetch_uz() {return u[2];}
  inline void set_u(SlDoubleVector value) { u = value; }
  inline void set_u(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    u[0] = _vx;
    u[1] = _vy;
    u[2] = _vz;
  }
  inline void set_u(SlurmInt i, SlurmDouble value) {u[i] = value;}
  inline void set_ux(SlurmDouble value) {u[0] = value;}
  inline void set_uy(SlurmDouble value) {u[1] = value;}
  inline void set_uz(SlurmDouble value) {u[2] = value;}
  inline void inc_u(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    u[0] += _vx;
    u[1] += _vy;
    u[2] += _vz;
  }

  inline SlurmDouble get_Ax() {return A[0];}
  inline SlurmDouble get_Ay() {return A[1];}
  inline SlurmDouble get_Az() {return A[2];}
  inline SlurmDouble get_A(SlurmInt i) {return A[i];}
  inline SlDoubleVector get_A() {return A;}
  inline SlurmDouble& fetch_A(SlurmInt i) {return A[i];}
  inline void set_Ax(SlurmDouble value) {A[0] = value;}
  inline void set_Ay(SlurmDouble value) {A[1] = value;}
  inline void set_Az(SlurmDouble value) {A[2] = value;}
  inline void set_A(SlDoubleVector value) { A = value; }
  inline void set_A(SlurmInt i, SlurmDouble value) {A[i] = value;}
  inline void set_A(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    A[0] = _vx;
    A[1] = _vy;
    A[2] = _vz;
  }
  inline void inc_A(SlDoubleVector value) 
  {
    for (SlurmInt i = 0; i < 3; i++)
      A[i] += value[i];
  }
  inline void inc_A(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    A[0] += _vx;
    A[1] += _vy;
    A[2] += _vz;
  }

  PointVelocity() {}

  /// Returns kinetic energy
  virtual SlurmDouble get_KineticEnergy() {return 0;}

  virtual ~PointVelocity() {}
};

/** Defines properties common to cells and particles */
class PointEnergy
{
protected:

  /// For a particle, it is energy [J]. For a grid cell, it is mass density of energy, i.e., [J/kg].
  SlurmDouble e = 0;
  /// Scalar potential used to clean div(A)
  SlurmDouble phi = 0.;

public:

  SlurmDouble get_e() {return e;}
  void set_e(SlurmDouble value) {e = value;}
  void inc_e(SlurmDouble value) {e += value;}

  SlurmDouble get_phi() {return phi;}
  SlurmDouble& fetch_phi() {return phi;}
  void set_phi(SlurmDouble value) {phi = value;}
  void inc_phi(SlurmDouble value) {phi += value;}

  virtual ~PointEnergy() {}
};

/** 
  Particle has color, id and Jacobian.
  Jacobian is obsoleted if we don't use MaterialPoit volume evolution anymore?
*/
class Particle : public MaterialPoint, PointEnergy, PointVelocity 
{
protected:

  /// Particle's unique ID
  long long id = 0;

  /// The components of the Jacobian matrix of the space transformation between particles and grid. J = dxp / dx = [J11, J12, J13, J21, J22, J23, J31, J32, J33] 
  SlDoubleTensor Jac = {1., 0., 0., 0., 1., 0., 0., 0., 1.};
 
  /// Particle's 'color'
  short color = 0;
  /// Volume at creation
  SlurmDouble vol0 = 0.;

public:

  // Inherited from PointEnergy

  SlurmDouble get_e() {return PointEnergy::get_e();}
  void set_e(SlurmDouble value) {PointEnergy::set_e(value);}
  void inc_e(SlurmDouble value) {PointEnergy::inc_e(value);}

  SlurmDouble get_phi() {return PointEnergy::get_phi();}
  SlurmDouble& fetch_phi() {return PointEnergy::fetch_phi();}
  void set_phi(SlurmDouble value) {PointEnergy::set_phi(value);}
  void inc_phi(SlurmDouble value) {PointEnergy::inc_phi(value);}

  // Inherited from PointVelocity

  SlurmDouble get_Ax() {return PointVelocity::get_Ax();}
  SlurmDouble get_Ay() {return PointVelocity::get_Ay();}
  SlurmDouble get_Az() {return PointVelocity::get_Az();}
  SlurmDouble get_A(SlurmInt i) {return PointVelocity::get_A(i);}
  SlDoubleVector get_A() {return PointVelocity::get_A();}
  SlurmDouble& fetch_A(SlurmInt i) {return PointVelocity::fetch_A(i);}
  void set_A(SlDoubleVector value) { PointVelocity::set_A(value); }
  void set_A(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) {PointVelocity::set_A(_vx, _vy, _vz);}
  void set_A(SlurmInt i, SlurmDouble value) {PointVelocity::set_A(i, value);}
  void inc_A(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) {PointVelocity::inc_A(_vx, _vy, _vz);}
  void inc_A(SlDoubleVector value) {PointVelocity::inc_A(value);}

  SlurmDouble get_ux() {return PointVelocity::get_ux();}
  SlurmDouble get_uy() {return PointVelocity::get_uy();}
  SlurmDouble get_uz() {return PointVelocity::get_uz();}
  SlurmDouble get_u(SlurmInt i) {return PointVelocity::get_u(i);}
  SlDoubleVector get_u() {return PointVelocity::get_u();}
  SlurmDouble& fetch_ux() {return PointVelocity::fetch_ux();}
  SlurmDouble& fetch_uy() {return PointVelocity::fetch_uy();}
  SlurmDouble& fetch_uz() {return PointVelocity::fetch_uz();}
  SlurmDouble& fetch_u(SlurmInt i) {return PointVelocity::fetch_u(i);}
  void set_ux(SlurmDouble value) {PointVelocity::set_ux(value);}
  void set_uy(SlurmDouble value) {PointVelocity::set_uy(value);}
  void set_uz(SlurmDouble value) {PointVelocity::set_uz(value);}
  void set_u(SlDoubleVector value) { PointVelocity::set_u(value); }
  void set_u(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) {PointVelocity::set_u(_vx, _vy, _vz);}
  void set_u(SlurmInt i, SlurmDouble value) {PointVelocity::set_u(i, value);}
  void inc_u(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) {PointVelocity::inc_u(_vx, _vy, _vz);}

  // Getters/setters

  long long get_id() {return id;}
  void set_id(long long value) {id = value;}
  short get_color() {return color;}
  void set_color(short value) {color = value;}

  // Deformation Jacobian
  SlurmDouble get_J11() {return Jac[0];}
  SlurmDouble get_J12() {return Jac[1];}
  SlurmDouble get_J13() {return Jac[2];}
  SlurmDouble get_J21() {return Jac[3];}
  SlurmDouble get_J22() {return Jac[4];}
  SlurmDouble get_J23() {return Jac[5];}
  SlurmDouble get_J31() {return Jac[6];}
  SlurmDouble get_J32() {return Jac[7];}
  SlurmDouble get_J33() {return Jac[8];}
  void set_J11(SlurmDouble value) {Jac[0] = value;}
  void set_J12(SlurmDouble value) {Jac[1] = value;}
  void set_J13(SlurmDouble value) {Jac[2] = value;}
  void set_J21(SlurmDouble value) {Jac[3] = value;}
  void set_J22(SlurmDouble value) {Jac[4] = value;}
  void set_J23(SlurmDouble value) {Jac[5] = value;}
  void set_J31(SlurmDouble value) {Jac[6] = value;}
  void set_J32(SlurmDouble value) {Jac[7] = value;}
  void set_J33(SlurmDouble value) {Jac[8] = value;}

  /// Update deformation Jacobian
  void update_Jac(GridCell * c, SlurmDouble dt);

  SlurmDouble get_vol0() {return vol0;}
  void set_vol0(SlurmDouble value) {vol0 = value;}

  /// Returns this particle's kinetic energy
  SlurmDouble get_KineticEnergy() {return 0.5 * mass * (pow(get_ux(), 2) + pow(get_uy(), 2) + pow(get_uz(), 2));}
};

/** Basic grid element, parent for nodes and cells. */
class GridElement: public MaterialPoint
{
protected:
  /// OMP Lock
  omp_lock_t lc;
  /// Role
  GridElementRole role = gerNormal;
  /// Mass density 
  SlurmDouble rho = 0;
  /// Interpolation weight, a helper property
  SlurmDouble weight = 0; 
  /// Indices on the grid, if applicable
  SlurmInt ijk[3] = {0, 0, 0};
  /// Grid element extents
  SlDoubleVector dxyz = {0, 0, 0};
  /// Inverted sizes for derivatives
  SlDoubleVector dxyzInv = {0, 0, 0};

  /// returns the index of the element of a 3x3x3 cube in 1D array: n = 9 * k + 3 * j + i
  inline int ind333(int i, int j, int k) {return (9 * k + 3 * j + i);}

public:

  /// Constructor only inits lock?
  GridElement() : MaterialPoint() {omp_init_lock(&(lc));}

  /// Lock this element
  void lock() {omp_set_lock(&(lc));}
  /// Unlock this element
  void unlock() {omp_unset_lock(&(lc));}

  // Getters/setters

  GridElementRole get_role() {return role;}
  void set_role(GridElementRole value) {role = value;}

  SlurmDouble get_weight() {return weight;}
  void set_weight(SlurmDouble value) {weight = value;}
  void inc_weight(SlurmDouble value) {weight += value;}

  SlurmInt get_i() {return ijk[0];}
  SlurmInt get_j() {return ijk[1];}
  SlurmInt get_k() {return ijk[2];}
  void set_i(SlurmInt value) {ijk[0] = value;}
  void set_j(SlurmInt value) {ijk[1] = value;}
  void set_k(SlurmInt value) {ijk[2] = value;}
  void set_ijk(SlurmInt i, SlurmInt j, SlurmInt k)
  {
    ijk[0] = i;
    ijk[1] = j;
    ijk[2] = k;
  }

  SlurmDouble get_dx() {return dxyz[0];}
  SlurmDouble get_dy() {return dxyz[1];}
  SlurmDouble get_dz() {return dxyz[2];}
  SlurmDouble get_dxInv() {return dxyzInv[0];}
  SlurmDouble get_dyInv() {return dxyzInv[1];}
  SlurmDouble get_dzInv() {return dxyzInv[2];}
  void set_dx(SlurmDouble value) {dxyz[0] = value;}
  void set_dy(SlurmDouble value) {dxyz[1] = value;}
  void set_dz(SlurmDouble value) {dxyz[2] = value;}

  SlurmDouble get_rho() {return rho;}
  SlurmDouble& fetch_rho() {return rho;}
  void set_rho(SlurmDouble value) {rho = value;}
  void inc_rho(SlurmDouble value) {rho += value;}

  /// Also set the inverse 1/dx, 1/dy, 1/dz
  void set_dxyz(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz);
  
  /// Copy the specified node quantities from another node
  virtual void copyFrom(GridElement *source, unsigned long quants) {}

  /// Copy from advNode/Cell
  virtual void copyFromAdv(unsigned long quants) {}

  /// Copy to advNode/Cell
  virtual void copyToAdv(unsigned long quants) {}

  /// Fills given quants with given value
  virtual void fill_quants(SlurmDouble value, unsigned long quants) {}

  /// Fills the quants needed by the explicit solver with zeros
  virtual void fill_zeros() {}
  
  /// Save to "old" quants
  virtual void saveToOld() {}

  /// Computes 3 components of the gradient of the given quantity
  void DirectionalDerivQuantity(SlurmDouble q000, SlurmDouble q100, SlurmDouble q010, SlurmDouble q110, SlurmDouble q001, SlurmDouble q101, SlurmDouble q011, SlurmDouble q111, SlurmDouble& derivx, SlurmDouble& derivy, SlurmDouble& derivz);
};

/** GridCell carries most field information: mass, pressure and magnetic field. */
class GridCell : public GridElement, PointEnergy
{
protected:

  /// Gas pressure
  SlurmDouble p = 0.; 
  /// "Old" energy mass density
  SlurmDouble eold = 0.; 
  /// Change in energy used by particles
  SlurmDouble de = 0.;
  /// Advance of the electromagnetic vector potential
  SlurmDouble dphi = 0;
  /// Magnetic field 
  SlDoubleVector B = {0., 0., 0.};
  /// Squared magnetic field 
  SlDoubleVector B2 = {0., 0., 0.};
  /// Velocity gradient gradu_ij = du_i/dx_j
  SlDoubleTensor gradu = {0., 0., 0., 0., 0., 0., 0., 0., 0.};
  /// Viscous stress tensor
  SlDoubleTensor visc_stress = {0., 0., 0., 0., 0., 0., 0., 0., 0.};
  /// Electromagnetic vector potential gradient. gradA_ij = dAi/dxj
  SlDoubleTensor gradA = {0., 0., 0., 0., 0., 0., 0., 0., 0.};
  /// ArtificialViscosity
  SlurmDouble viscosity = 0.;
  /// Full stress tensor, including Maxwell
  SlDoubleSymmetricTensor stress = {0, 0, 0, 0, 0, 0};

  /** 
    Geometrical neighbors, a 2x2x2 cube.
    In each nXYZ first index corresponds to the index in X, second to the index in Y, third to the index in Z direction.
    So that n000 is the "left closer bottom" corner of the cubical cell, n011 is the "left backward upper" corner. 

    Used in averageNodes, DirectionalDeriv.

    GridNode *n000, *n100, *n010, *n110, *n001, *n101, *n011, *n111; 
  */
  GridNode * nn[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

  /** 
    Logical neighbors: nodes to which particle data is advected.  
    an != nn ONLY in preiodic boundaries!
   
    GridNode *an000, *an100, *an010, *an110, *an001, *an101, *an011, *an111; 
  */
  GridNode * an[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

  /** 
    For a ghost cell, this is the cell where the particle data is advected, i.e., this cell's geometrical boundary neighbor. 
    In periodic BC, the advCell is found on the opposite side of the domain, i.e., flip over both left and right edges.
    For an ordinary cell, advCell is a pointer to this cell.
    Used ONLY in PERIODIC and REFLECTIVE boundaries!
  */
  GridCell *advCell = NULL;

  /** 
    Cell's 27 logical neighbor cells (cube 3x3x3 with indices from 0 to 2 in each dimension). 
    Each one is the corresponging geometrical neighbor's advCell.
    Logical neighbors are the cells where the data should be interpolated to/from according to boundary conditions.
    Used ONLY in PERIODIC and REFLECTIVE boundaries!

    For instance, this cell corresponds to indices [1, 1, 1] or, in the 1D array, 13. 
    Therefore this cell's "advection" cell is cnc[13]. 

    Advection cells are used, for instance in periodic and reflective boundary: the ghost's "advection cell" is its neighbor inside the main domain.
  */
  GridCell * cnc[27] = {};

  /// Cells which share a face with this one [left, right, front, back, top, bottom]
  GridCell * FaceNeighbors[6] = {};

  /// Far-side piece of the Quadratic interpolation
  inline SlurmDouble outerWeight(SlurmDouble x1, SlurmDouble x2, SlurmDouble dxInv);

  /// Central quadriliteral piece of the Quadratic interpolation
  inline SlurmDouble innerWeight(SlurmDouble x1, SlurmDouble x2, SlurmDouble dxInv);

  /// Compute 9 weights for quadratic interpolation
  inline void computeWeights(GridPoint * p, SlurmDouble * w);

public:

  // Inherited from PointEnergy

  SlurmDouble get_e() {return PointEnergy::get_e();}
  void set_e(SlurmDouble value) {PointEnergy::set_e(value);}
  void inc_e(SlurmDouble value) {PointEnergy::inc_e(value);}

  SlurmDouble get_phi() {return PointEnergy::get_phi();}
  SlurmDouble& fetch_phi() {return PointEnergy::fetch_phi();}
  void set_phi(SlurmDouble value) {PointEnergy::set_phi(value);}
  void inc_phi(SlurmDouble value) {PointEnergy::inc_phi(value);}
  
  // Inherited from GridElement

  SlurmDouble get_rho() {return GridElement::get_rho();}
  SlurmDouble& fetch_rho() {return GridElement::fetch_rho();}
  void set_rho(SlurmDouble value) {GridElement::set_rho(value);}
  void inc_rho(SlurmDouble value) {GridElement::inc_rho(value);}
  
  // Getters/setters

  /// normal = 0, 1, 2 correspond to x, y, z; side = 0 is left, side = 1 is right.
  GridCell * get_FaceNeighbor(SlurmInt normal, SlurmInt side) {return FaceNeighbors[normal*2 + side];}
  void set_FaceNeighbor(SlurmInt normal, SlurmInt side, GridCell * value) {FaceNeighbors[normal*2 + side] = value;}

  SlurmDouble get_p() {return p;}
  SlurmDouble& fetch_p() {return p;}
  void set_p(SlurmDouble value) {p = value;}

  SlurmDouble get_eold() {return eold;}
  void set_eold(SlurmDouble value) {eold = value;}

  SlurmDouble get_de() {return de;}
  void set_de(SlurmDouble value) {de = value;}
  void update_de() {de = e - eold;}
  void inc_de(SlurmDouble value) {de += value;}

  SlurmDouble get_dphi() {return dphi;}
  void set_dphi(SlurmDouble value) {dphi = value;}
  void inc_dphi(SlurmDouble value) {dphi += value;}

  SlurmDouble get_visc() {return viscosity;}
  SlurmDouble& fetch_visc() {return viscosity;}
  void set_visc(SlurmDouble value) {viscosity = value;}

  GridCell * get_advCell() {return advCell;}
  void set_advCell(GridCell * value) {advCell = value;}

  SlurmDouble get_Bx() {return B[0];}
  SlurmDouble get_By() {return B[1];}
  SlurmDouble get_Bz() {return B[2];}
  void set_Bx(SlurmDouble value) {B[0] = value;}
  void set_By(SlurmDouble value) {B[1] = value;}
  void set_Bz(SlurmDouble value) {B[2] = value;}
  SlurmDouble get_B(SlurmInt i) {return B[i];}
  SlDoubleVector get_B() {return B;}
  SlurmDouble& fetch_B(SlurmInt i) {return B[i];}
  void set_B(SlurmInt i, SlurmDouble value) {B[i] = value;}
  void set_B(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    B[0] = _vx;
    B[1] = _vy;
    B[2] = _vz;
  }
  void inc_B(SlDoubleVector value) {for (SlurmInt i = 0; i < 3; i++) B[i] += value[i];}

  SlurmDouble get_Bx2() {return B2[0];}
  SlurmDouble get_By2() {return B2[1];}
  SlurmDouble get_Bz2() {return B2[2];}
  SlurmDouble get_B2(SlurmInt i) {return B2[i];}
  SlurmDouble& fetch_B2(SlurmInt i) {return B2[i];}

  // SlurmDouble gradu11, gradu12, gradu13, gradu21, gradu22, gradu23, gradu31, gradu32, gradu33;
  SlurmDouble get_gradu11() {return gradu[0];}
  SlurmDouble get_gradu12() {return gradu[1];}
  SlurmDouble get_gradu13() {return gradu[2];}
  SlurmDouble get_gradu21() {return gradu[3];}
  SlurmDouble get_gradu22() {return gradu[4];}
  SlurmDouble get_gradu23() {return gradu[5];}
  SlurmDouble get_gradu31() {return gradu[6];}
  SlurmDouble get_gradu32() {return gradu[7];}
  SlurmDouble get_gradu33() {return gradu[8];}
  SlurmDouble get_gradu(SlurmInt i) {return gradu[i];}
  SlDoubleTensor get_gradu() {return gradu;}
  void set_gradu11(SlurmDouble value) {gradu[0] = value;}
  void set_gradu12(SlurmDouble value) {gradu[1] = value;}
  void set_gradu13(SlurmDouble value) {gradu[2] = value;}
  void set_gradu21(SlurmDouble value) {gradu[3] = value;}
  void set_gradu22(SlurmDouble value) {gradu[4] = value;}
  void set_gradu23(SlurmDouble value) {gradu[5] = value;}
  void set_gradu31(SlurmDouble value) {gradu[6] = value;}
  void set_gradu32(SlurmDouble value) {gradu[7] = value;}
  void set_gradu33(SlurmDouble value) {gradu[8] = value;}
  void set_gradu(SlurmInt i, SlurmDouble value) {gradu[i] = value;}
  void set_gradu(GridNode * gradx, GridNode * grady, GridNode * gradz);
  void inc_gradu(GridCell * c, SlurmDouble w)
  {
     for (SlurmInt i = 0; i < 9; i++)
        gradu[i] += w * c->get_gradu(i);
  }

  // Stress tensor is symmetric
  SlurmDouble get_stress12() {return stress[0];}
  SlurmDouble get_stress21() {return stress[0];}
  SlurmDouble get_stress13() {return stress[1];}
  SlurmDouble get_stress31() {return stress[1];}
  SlurmDouble get_stress23() {return stress[2];}
  SlurmDouble get_stress32() {return stress[2];}
  SlurmDouble get_stress11() {return stress[3];}
  SlurmDouble get_stress22() {return stress[4];}
  SlurmDouble get_stress33() {return stress[5];}
  SlurmDouble get_stress(SlurmInt i) {return stress[i];}
  SlDoubleSymmetricTensor get_stress() {return stress;}
  SlurmDouble& fetch_stress(SlurmInt i) {return stress[i];}

  /// Compute the full stress tensor given bulk viscosity coefficients
  void set_stress(SlurmDouble mu, SlurmDouble mu2);

  /// Divergence of velocity
  SlurmDouble get_divu() {return get_gradu11() + get_gradu22() + get_gradu33();}

  /// Velocity variation. Needed by, e.g., Kuropatenko artificial viscosity.
  SlurmDouble get_VelocityVariation() {return get_gradu11()*dxyz[0] + get_gradu22()*dxyz[1] + get_gradu33()*dxyz[2];}

  // Tensor Gradient of vector potential
  SlurmDouble get_gradA11() {return gradA[0];}
  SlurmDouble get_gradA12() {return gradA[1];}
  SlurmDouble get_gradA13() {return gradA[2];}
  SlurmDouble get_gradA21() {return gradA[3];}
  SlurmDouble get_gradA22() {return gradA[4];}
  SlurmDouble get_gradA23() {return gradA[5];}
  SlurmDouble get_gradA31() {return gradA[6];}
  SlurmDouble get_gradA32() {return gradA[7];}
  SlurmDouble get_gradA33() {return gradA[8];}
  void set_gradA11(SlurmDouble value) {gradA[0] = value;}
  void set_gradA12(SlurmDouble value) {gradA[1] = value;}
  void set_gradA13(SlurmDouble value) {gradA[2] = value;}
  void set_gradA21(SlurmDouble value) {gradA[3] = value;}
  void set_gradA22(SlurmDouble value) {gradA[4] = value;}
  void set_gradA23(SlurmDouble value) {gradA[5] = value;}
  void set_gradA31(SlurmDouble value) {gradA[6] = value;}
  void set_gradA32(SlurmDouble value) {gradA[7] = value;}
  void set_gradA33(SlurmDouble value) {gradA[8] = value;}
  SlurmDouble get_gradA(SlurmInt i) {return gradA[i];}
  SlDoubleTensor get_gradA() {return gradA;}
  void set_gradA(SlurmInt i, SlurmDouble value) {gradA[i] = value;}
  void set_gradA(GridNode * gradx, GridNode * grady, GridNode * gradz);

  /// Divergence of vector potential
  SlurmDouble get_divA() {return get_gradA11() + get_gradA22() + get_gradA33();}

  //GridNode *n000, *n100, *n010, *n110, *n001, *n101, *n011, *n111;
  GridNode * get_n000() {return nn[0];}
  GridNode * get_n100() {return nn[1];}
  GridNode * get_n010() {return nn[2];}
  GridNode * get_n110() {return nn[3];}
  GridNode * get_n001() {return nn[4];}
  GridNode * get_n101() {return nn[5];}
  GridNode * get_n011() {return nn[6];}
  GridNode * get_n111() {return nn[7];}
  GridNode * get_nn(SlurmInt i) {return nn[i];}
  void set_n000(GridNode * value) {nn[0] = value;}
  void set_n100(GridNode * value) {nn[1] = value;}
  void set_n010(GridNode * value) {nn[2] = value;}
  void set_n110(GridNode * value) {nn[3] = value;}
  void set_n001(GridNode * value) {nn[4] = value;}
  void set_n101(GridNode * value) {nn[5] = value;}
  void set_n011(GridNode * value) {nn[6] = value;}
  void set_n111(GridNode * value) {nn[7] = value;}

  GridNode * get_an000() {return an[0];}
  GridNode * get_an100() {return an[1];}
  GridNode * get_an010() {return an[2];}
  GridNode * get_an110() {return an[3];}
  GridNode * get_an001() {return an[4];}
  GridNode * get_an101() {return an[5];}
  GridNode * get_an011() {return an[6];}
  GridNode * get_an111() {return an[7];}
  GridNode * get_an(SlurmInt i) {return an[i];}
  void set_an000(GridNode * value) {an[0] = value;}
  void set_an100(GridNode * value) {an[1] = value;}
  void set_an010(GridNode * value) {an[2] = value;}
  void set_an110(GridNode * value) {an[3] = value;}
  void set_an001(GridNode * value) {an[4] = value;}
  void set_an101(GridNode * value) {an[5] = value;}
  void set_an011(GridNode * value) {an[6] = value;}
  void set_an111(GridNode * value) {an[7] = value;}
  void set_an(GridNode * _n000, GridNode * _n100, GridNode * _n010, GridNode * _n110, GridNode * _n001, GridNode * _n101, GridNode * _n011, GridNode * _n111) 
  {
    an[0] = _n000;
    an[1] = _n100;
    an[2] = _n010;
    an[3] = _n110;
    an[4] = _n001;
    an[5] = _n101;
    an[6] = _n011;
    an[7] = _n111;
  }

  /// Average specified quants over the 'geometrical' neighbor nodes
  void averageNodes(unsigned long quants, GridNode* avgn);

  /// "Directional derivative" or gradient. Computed over this cells's eight neighbor nodes. Returns through a grid node object.
  void DirectionalDeriv(unsigned long quants, GridNode* dirdernx, GridNode* dirderny, GridNode* dirdernz);

  /// Copy another cell's quantities
  void copyFrom(GridCell *source, unsigned long quants);

  /// Copy from advCell
  void copyFromAdv(unsigned long quants) {copyFrom(advCell, quants);}

  /// Copy to advCell
  void copyToAdv(unsigned long quants) {advCell->copyFrom(this, quants);}

  /// Returns this cell's kinetic energy
  SlurmDouble get_MagneticEnergy() {return 0.5 * vol * (pow(B[0], 2) + pow(B[1], 2) + pow(B[2], 2));}
  
  /// Returns cell's internal energy, e*m
  SlurmDouble get_InternalEnergy() {return e * mass;}

  /// Returns this cell's kinetic energy using velocities averaged over neighbor nodes
  SlurmDouble get_KineticEnergy();

  /// Fills given quants
  void fill_quants(SlurmDouble value, unsigned long quants);
  /// Fills the quants needed by the explicit solver with zeros
  void fill_zeros();

  /// Save to "old" quants
  void saveToOld() 
  { 
    eold = e;
  }

  /// Initialize neighbor cells used by quadratic interpolation
  void initNeighborCells();

  /// Fills neighbor cells from the given 3x3x3 array
  void setNeighborCells(CellRefArray nbrs);

  /// interpolate to particles
  GridCell* interpolateToParticle(GridPoint * p);

  /// interpolate to particles only the specific quantities
  GridCell* interpolateToParticle(GridPoint * p, unsigned long quants);

  /// Interpolate from particles
  void advectFromParticle(Particle * p);

  /// Constructor
  GridCell() : GridElement() {}
};

/** GridNode represents a single node on the grid. In principle, only carries velocity. */
class GridNode : public GridElement, PointVelocity
{
protected: 

  /// "Old" velocity
  SlDoubleVector uold = {0., 0., 0.};
  /// Particle velocity change
  SlDoubleVector du = {0., 0., 0.};
  /// Divergence of B, just a diagnostic quantity.
  SlurmDouble divB = 0.;
  /// "old" Electromagnetic vector potential
  SlDoubleVector Aold = {0., 0., 0.};
  /// Electromagnetic vector potential increment, is projected to particles
  SlDoubleVector dA = {0., 0., 0.};
  /// Current
  SlDoubleVector J = {0., 0., 0.};

  /** 
    Geometrical neighbor cells. 
    In each cXYZ first index corresponds to the index in X, second to the index in Y, third to the index in Z direction. 
    These cells are only used by interpolator to compute weights and distances. 
    GridCell *c000, *c100, *c010, *c110, *c001, *c101, *c011, *c111; 
  */
  GridCell * nc[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

  /** 
    Neighbor cells into which the particle data is advected.
    These cells are connected to this node logically, and not necessariy geometrically. 
    So far, used for interpolation ONLY in PERIODIC boundaries! 
    GridCell *ac000, *ac100, *ac010, *ac110, *ac001, *ac101, *ac011, *ac111; 
  */
  GridCell * ac[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

  /// Nodes which "share a face" with this one, i.e., the ones directly above, below, left, right, behind, front... [left, right, front, back, top, bottom]
  GridNode * FaceNeighbors[6] = {};

  /// This node is used to, e.g., impose periodic BCs.
  GridNode *advNode = NULL;
  /// Closest particles that enclose the node. We look for particles in the above neighbor cells.
  Particle * ptcl[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
  /// Boundary conditions in three dimensions
  BoundaryCondition * bc[3] = {NULL, NULL, NULL};

public:

  // Inherited from PointVelocity

  SlurmDouble get_Ax() {return PointVelocity::get_Ax();}
  SlurmDouble get_Ay() {return PointVelocity::get_Ay();}
  SlurmDouble get_Az() {return PointVelocity::get_Az();}
  SlurmDouble get_A(SlurmInt i) {return PointVelocity::get_A(i);}
  SlDoubleVector get_A() {return PointVelocity::get_A();}
  SlurmDouble& fetch_A(SlurmInt i) {return PointVelocity::fetch_A(i);}
  void set_A(SlDoubleVector value) { PointVelocity::set_A(value); }
  void set_A(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) {PointVelocity::set_A(_vx, _vy, _vz);}
  void set_A(SlurmInt i, SlurmDouble value) {PointVelocity::set_A(i, value);}
  void inc_A(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) {PointVelocity::inc_A(_vx, _vy, _vz);}
  void inc_A(SlDoubleVector value) {PointVelocity::inc_A(value);}

  SlDoubleVector get_Aold() {return Aold;}
  void set_Aold(SlDoubleVector value) { Aold = value; }

  SlDoubleVector get_uold() {return uold;}

  SlurmDouble get_ux() {return PointVelocity::get_ux();}
  SlurmDouble get_uy() {return PointVelocity::get_uy();}
  SlurmDouble get_uz() {return PointVelocity::get_uz();}
  SlurmDouble get_u(SlurmInt i) {return PointVelocity::get_u(i);}
  SlDoubleVector get_u() {return PointVelocity::get_u();}
  SlurmDouble& fetch_u(SlurmInt i) {return PointVelocity::fetch_u(i);}
  void set_ux(SlurmDouble value) {PointVelocity::set_ux(value);}
  void set_uy(SlurmDouble value) {PointVelocity::set_uy(value);}
  void set_uz(SlurmDouble value) {PointVelocity::set_uz(value);}
  void set_u(SlDoubleVector value) { PointVelocity::set_u(value); }
  void set_u(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) {PointVelocity::set_u(_vx, _vy, _vz);}
  void set_u(SlurmInt i, SlurmDouble value) {PointVelocity::set_u(i, value);}
  void inc_u(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) {PointVelocity::inc_u(_vx, _vy, _vz);}

  
  // Inherited from GridElement

  SlurmDouble get_rho() {return GridElement::get_rho();}
  SlurmDouble& fetch_rho() {return GridElement::fetch_rho();}
  void set_rho(SlurmDouble value) {GridElement::set_rho(value);}
  void inc_rho(SlurmDouble value) {GridElement::inc_rho(value);}

  // Getters/setters

  /// normal = 0, 1, 2 correspond to x, y, z; side = 0 is left, side = 1 is right.
  GridNode * get_FaceNeighbor(SlurmInt normal, SlurmInt side) {return FaceNeighbors[normal*2 + side];}
  void set_FaceNeighbor(SlurmInt normal, SlurmInt side, GridNode * value) {FaceNeighbors[normal*2 + side] = value;}

  SlurmDouble get_divB() {return divB;}
  void set_divB(SlurmDouble value) {divB = value;}
  /// gradx, grady, gradz are pre-computed derivatives. GridCells.
  void set_divB(GridCell * gradx, GridCell * grady, GridCell * gradz) {divB = gradx->get_Bx() + grady->get_By() + gradz->get_Bz();}
  GridNode * get_advNode() {return advNode;}
  void set_advNode(GridNode * value) {advNode = value;}

  SlurmDouble get_uxold() {return uold[0];}
  SlurmDouble get_uyold() {return uold[1];}
  SlurmDouble get_uzold() {return uold[2];}
  void set_uxold(SlurmDouble value) {uold[0] = value;}
  void set_uyold(SlurmDouble value) {uold[1] = value;}
  void set_uzold(SlurmDouble value) {uold[2] = value;}

  SlurmDouble get_Axold() {return Aold[0];}
  SlurmDouble get_Ayold() {return Aold[1];}
  SlurmDouble get_Azold() {return Aold[2];}
  void set_Axold(SlurmDouble value) {Aold[0] = value;}
  void set_Ayold(SlurmDouble value) {Aold[1] = value;}
  void set_Azold(SlurmDouble value) {Aold[2] = value;}

  SlDoubleVector get_dA() {return dA;}
  SlurmDouble get_dA(SlurmInt i) {return dA[i];}
  SlurmDouble get_dAx() {return dA[0];}
  SlurmDouble get_dAy() {return dA[1];}
  SlurmDouble get_dAz() {return dA[2];}
  void set_dA(SlurmInt i, SlurmDouble value) {dA[i] = value;}
  void set_dA(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    dA[0] = _vx;
    dA[1] = _vy;
    dA[2] = _vz;
  }
  void update_dA() {for (SlurmInt i = 0; i < 3; i++) dA[i] = A[i] - Aold[i];}
  void inc_dA(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    dA[0] += _vx;
    dA[1] += _vy;
    dA[2] += _vz;
  }

  SlurmDouble get_Jx() {return J[0];}
  SlurmDouble get_Jy() {return J[1];}
  SlurmDouble get_Jz() {return J[2];}  
  void set_Jx(SlurmDouble value) {J[0] = value;}
  void set_Jy(SlurmDouble value) {J[1] = value;}
  void set_Jz(SlurmDouble value) {J[2] = value;}
  SlurmDouble get_J(SlurmInt i) {return J[i];}
  SlDoubleVector get_J() {return J;}
  void set_J(SlurmInt i, SlurmDouble value) {J[i] = value;}
  void set_J(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    J[0] = _vx;
    J[1] = _vy;
    J[2] = _vz;
  }

  inline SlurmDouble get_dux() {return du[0];}
  inline SlurmDouble get_duy() {return du[1];}
  inline SlurmDouble get_duz() {return du[2];}
  inline SlurmDouble get_du(SlurmInt i) {return du[i];}
  inline void set_dux(SlurmDouble value) {du[0] = value;}
  inline void set_duy(SlurmDouble value) {du[1] = value;}
  inline void set_duz(SlurmDouble value) {du[2] = value;}
  inline void set_du(SlurmInt i, SlurmDouble value) {du[i] = value;}
  inline void set_du(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    du[0] = _vx;
    du[1] = _vy;
    du[2] = _vz;
  }
  inline void update_du() {for (SlurmInt i=0; i < 3; i++) du[i] = u[i] - uold[i];}
  inline void inc_du(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
  {
    du[0] += _vx;
    du[1] += _vy;
    du[2] += _vz;
  }

  inline Particle * get_p000() {return ptcl[0];}
  inline Particle * get_p100() {return ptcl[1];}
  inline Particle * get_p010() {return ptcl[2];}
  inline Particle * get_p110() {return ptcl[3];}
  inline Particle * get_p001() {return ptcl[4];}
  inline Particle * get_p101() {return ptcl[5];}
  inline Particle * get_p011() {return ptcl[6];}
  inline Particle * get_p111() {return ptcl[7];}
  inline Particle * get_pi(SlurmInt i) {return ptcl[i];}
  inline Particle **get_ptcls() {return ptcl;}
  inline void set_p000(Particle * value) {ptcl[0] = value;}
  inline void set_p100(Particle * value) {ptcl[1] = value;}
  inline void set_p010(Particle * value) {ptcl[2] = value;}
  inline void set_p110(Particle * value) {ptcl[3] = value;}
  inline void set_p001(Particle * value) {ptcl[4] = value;}
  inline void set_p101(Particle * value) {ptcl[5] = value;}
  inline void set_p011(Particle * value) {ptcl[6] = value;}
  inline void set_p111(Particle * value) {ptcl[7] = value;}
  inline void set_ptcls(Particle * _p000, Particle * _p100, Particle * _p010, Particle * _p110, Particle * _p001, Particle * _p101, Particle * _p011, Particle * _p111)
  {
    ptcl[0] = _p000;
    ptcl[1] = _p100;
    ptcl[2] = _p010;
    ptcl[3] = _p110;
    ptcl[4] = _p001;
    ptcl[5] = _p101;
    ptcl[6] = _p011;
    ptcl[7] = _p111;
  }

  GridCell * get_c000() {return nc[0];}
  GridCell * get_c100() {return nc[1];}
  GridCell * get_c010() {return nc[2];}
  GridCell * get_c110() {return nc[3];}
  GridCell * get_c001() {return nc[4];}
  GridCell * get_c101() {return nc[5];}
  GridCell * get_c011() {return nc[6];}
  GridCell * get_c111() {return nc[7];}
  GridCell * get_nc(SlurmInt i) {return nc[i];}
  void set_c000(GridCell * value) {nc[0] = value;}
  void set_c100(GridCell * value) {nc[1] = value;}
  void set_c010(GridCell * value) {nc[2] = value;}
  void set_c110(GridCell * value) {nc[3] = value;}
  void set_c001(GridCell * value) {nc[4] = value;}
  void set_c101(GridCell * value) {nc[5] = value;}
  void set_c011(GridCell * value) {nc[6] = value;}
  void set_c111(GridCell * value) {nc[7] = value;}
  void set_nc(GridCell * _c000, GridCell * _c100, GridCell * _c010, GridCell * _c110, GridCell * _c001, GridCell * _c101, GridCell * _c011, GridCell * _c111) 
  {
    nc[0] = _c000;
    nc[1] = _c100;
    nc[2] = _c010;
    nc[3] = _c110;
    nc[4] = _c001;
    nc[5] = _c101;
    nc[6] = _c011;
    nc[7] = _c111;
  }

  GridCell * get_ac000() {return ac[0];}
  GridCell * get_ac100() {return ac[1];}
  GridCell * get_ac010() {return ac[2];}
  GridCell * get_ac110() {return ac[3];}
  GridCell * get_ac001() {return ac[4];}
  GridCell * get_ac101() {return ac[5];}
  GridCell * get_ac011() {return ac[6];}
  GridCell * get_ac111() {return ac[7];}
  void set_ac000(GridCell * value) {ac[0] = value;}
  void set_ac100(GridCell * value) {ac[1] = value;}
  void set_ac010(GridCell * value) {ac[2] = value;}
  void set_ac110(GridCell * value) {ac[3] = value;}
  void set_ac001(GridCell * value) {ac[4] = value;}
  void set_ac101(GridCell * value) {ac[5] = value;}
  void set_ac011(GridCell * value) {ac[6] = value;}
  void set_ac111(GridCell * value) {ac[7] = value;}
  void set_ac(GridCell * _c000, GridCell * _c100, GridCell * _c010, GridCell * _c110, GridCell * _c001, GridCell * _c101, GridCell * _c011, GridCell * _c111) 
  {
    ac[0] = _c000;
    ac[1] = _c100;
    ac[2] = _c010;
    ac[3] = _c110;
    ac[4] = _c001;
    ac[5] = _c101;
    ac[6] = _c011;
    ac[7] = _c111;
  }

  BoundaryCondition * get_bc(SlurmInt i) {return bc[i];}
  void set_bc(SlurmInt i, BoundaryCondition * value) {bc[i] = value;}

  /// Average quants over the 'logical' neighbor cells. The neighbors should account for BCs.
  void averageCells(unsigned long quants, GridCell* avgc);

  /// Former DirectionalDerivCell
  void DirectionalDeriv(unsigned long quants, GridCell* dirdercx, GridCell* dirdercy, GridCell* dirdercz);

  /// Directional derivative of all components of the stress tensor
  void DirectionalDerivStress(GridCell* dirdercx, GridCell* dirdercy, GridCell* dirdercz);

  /// Copy the specified node quantities from another node
  void copyFrom(GridNode *source, unsigned long quants);

  /// Copy from advNode
  void copyFromAdv(unsigned long quants) {copyFrom(advNode, quants);}

  /// Copy to advNode
  void copyToAdv(unsigned long quants) {advNode->copyFrom(this, quants);}

  /// Check if the given particle is closer to the node than any of its ptcl
  void updateNeighborParticle(Particle* p);

  /// Fills given quants
  void fill_quants(SlurmDouble value, unsigned long quants);
  /// Fills the quants needed by the explicit solver with zeros
  void fill_zeros();

  /// Save to "old" quants. 
  void saveToOld() { uold = u; }

  /// Save A to "old" quants
  void saveToOldA() { Aold = A; }

  /// Returns true only if all neighbor particles are non-NULL
  bool has_all_ptcls()
  {
    if (ptcl[0] && ptcl[1] && ptcl[2] && ptcl[3] && ptcl[4] && ptcl[5] && ptcl[6] && ptcl[7])
      return true;
    else 
      return false;
  }

};

// Helper functions

/// Length of a vector represented by a GridPoint object
SlurmDouble vectorLength(GridPoint* p);
/// Distance between two grid points 
SlurmDouble computeDistance(GridPoint* A, GridPoint* B);
/// Difference between two vectors represented by GridPoint objects
GridPoint * pointDifference(GridPoint* A, GridPoint* B);
/// Cross-product of two vectors represented by GridPoint objects
GridPoint * pointCrossProduct(GridPoint* A, GridPoint* B);
/// Cross-product of two 3D vectors given their coordinates
void crossProduct(SlurmDouble x1, SlurmDouble y1, SlurmDouble z1, SlurmDouble x2, SlurmDouble y2, SlurmDouble z2, SlurmDouble& x, SlurmDouble& y, SlurmDouble& z);

/** Class GridCommon incapsulates basic (geometrical?) functionality of grids, e.g., copying nodes */
class GridCommon
{
protected:

public:

  // Stuff moved here from Grid in order to have particle initialization and interpolation in InitialConditions

  /// The node closest to the given point
  virtual GridNode* getClosestNode(GridPoint* p) {return NULL;}
  /// The cell including the given point
  virtual GridCell* getClosestCell(GridPoint* p) {return NULL;}
  /// Look for the closest nodes and update their neighbor particles accordingly.
  virtual void updateParticleNodeNeighbors(Particle* p) {};

  // Interpolation

  /** 
    Returns interpolation weights for 4 corners of a rectangle 
    Allocate memory for weights prior to calling this function!
    Note the order of the bloody points!
  
    4(p01) *-----* 3(p11)
           |  p  |
    1(p00) *-----* 2(p10)
  
  */
  static void BilinearInterpolationWeights(GridPoint *p, GridPoint *p00, GridPoint *p10, GridPoint *p11, GridPoint *p01, SlurmDouble *weights);

  /** 
    Converts coordinates from "Physical" to "Logical" space.
    The return values are basically interpolation weights along the corresponding axes (wx and wy in BilinearInterpolationWeights).
    https://www.particleincell.com/2012/quad-interpolation/
  
    p01(4) *--------* p11(3)
            \       |
             \  p   |
              \     |
        p00(1) *----* p10(2)
  
  */
  static void Physical2Logical(GridPoint *p, GridPoint *p00, GridPoint *p10, GridPoint *p11, GridPoint *p01, SlurmDouble *coords);

  /** 
    Computes interpolations weights for a point inside an arbitrary convex quadriliteral as described in
    https://www.particleincell.com/2012/quad-interpolation/
  
    Allocate memory for weights prior to calling this function!
    Note the order of the bloody points!
  
    p01(4) *--------* p11(3)
            \       |
             \  p   |
              \     |
        p00(1) *----* p10(2)

  */
  static void QuadriliteralInterpolationWeights(GridPoint *p, GridPoint *p00, GridPoint *p10, GridPoint *p11, GridPoint *p01, SlurmDouble *weights);

  /// Interpolation weights from irregular octahedron
  static SlurmDouble * TrilinearIrregularInterpolationWeights(GridPoint *p, GridPoint *p000, GridPoint *p100, GridPoint *p010, GridPoint *p110, GridPoint *p001, GridPoint *p101, GridPoint *p011, GridPoint *p111);

  /// Interpolation weights from irregular octahedron with pre-allocated arrays. This one is faster when applied to grid or particles.
  static SlurmDouble * TrilinearIrregularInterpolationWeights(GridPoint *p, GridPoint *p000, GridPoint *p100, GridPoint *p010, GridPoint *p110, GridPoint *p001, GridPoint *p101, GridPoint *p011, GridPoint *p111, SlurmDoubleArray2D fgh, SlurmDoubleArray2D coefs);

  /// Destructor
  ~GridCommon();
};
