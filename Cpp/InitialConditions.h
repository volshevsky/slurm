/**
  Initial condition is a placeholder for some physics as well:
  - GasGamma
  - Equation of state (EOS)
  - Gravity has been moved here from grid

  To introduce a new initial condition:
  - Create a new descendant of the InitialCondition class.
  - Realize at least: the constructor, initCell() and initNode() methods.
  - Add a new line into the makeInitial() factory method (in the InitialConditions.cpp file).

*/
#pragma once

#include "Interpolators.h"
#include "ConfigFile.h"

/** 
  Base class for all initial conditions.

  Normally you don't instantiate this class directly. Use one of its descendants.

  To introduce a new initial condition:
  - Create a new inheritor of the InitialCondition class.
  - Realize at least: the constructor, initCell() and initNode() methods.
  - Add a new line into the makeInitial() factory method (in the InitialConditions.cpp file).

*/
class InitialCondition
{
protected:

  /// Textual name of the initial condition
  string name;
  /// Adiabatic constant
  SlurmDouble GasGamma = 5./3.;
  /// Gamma - 1
  SlurmDouble GasGamma_1 = GasGamma - 1;  
  /// Grid dimensions.
  SlurmDouble Lxyz[3];
  /// Default initial pressure
  SlurmDouble p0 = 1;
  /// Default initial density
  SlurmDouble rho0 = 1;
  /// Sound speed squared. Used in isothermal MHD.
  SlurmDouble IsothermalSoundSpeedSq;
  /// Adiabatic sound speed squared. 
  SlurmDouble AdiabaticSoundSpeedSq;
  /// Default velocity amplitude
  SlurmDouble u0 = 0;
  /// Default MF amplitude
  SlurmDouble B0 = 0;
  /// Gravity vector
  SlurmDouble GravityAcceleration[3] = {0., 0., 0.};

  /// Reads default pressure and density and computes sound speed  
  void initDefaultParameters(ConfigFile * config, SlurmDouble _p0, SlurmDouble _rho0, SlurmDouble _gamma);
  /// Brackbill & Ruppel (JCP 1986) energy consists of several components. Here it's different, simplified.
  // Fabio's way to evolve energy
  void updateParticleEnergyAdiabatic(Particle * p, GridCell * c) { p->inc_e(p->get_e() * c->get_de()); } 
  // My way to evolve energy
  //void updateParticleEnergyAdiabatic(Particle * p, GridCell * c) { p->inc_e(p->get_m() * c->get_de()); } 
  /// Setter for gas gamma
  void set_GasGamma(SlurmDouble value);
  /// Set particle energy given pressure
  void computeEnergyAdiabatic(Particle * pt, SlurmDouble press) { pt->set_e(press * pt->get_vol() / GasGamma_1); }
  
public:

  /// Initializes node quantities according to the initial condition. 
  virtual void initNode(GridNode * n) {}
  /// Initializes node quantities according to the initial condition at given time. 
  virtual void initNode(GridNode * n, SlurmDouble t) {}
  /// Initializes cell quantities according to the initial condition.
  virtual void initCell(GridCell * c);
  /// Default initialization is based on advection of the grid quantities to the particle
  virtual void initParticleInterpolate(Particle * p, Interpolator * interpolator);
  /// Default initialization is based on advection of the grid quantities to the particle
  virtual void initParticle(Particle * p, Interpolator * interpolator);
  /// Initialization of injected particles
  virtual void initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t);
  /// Check if it is time to inject particles
  virtual bool checkInjectMoment(SlurmDouble t) {return false;}
  /// Reset the last inject moment
  virtual void resetInjectMoment(SlurmDouble t) {}
  /// Add gravity
  virtual void addGravity(GridNode * n, SlurmDouble dt);
  
  /// Isothermal EOS
  void computePressureIsothermal(GridCell * c);
  /// Adiabatic EOS
  void computePressureAdiabatic(GridCell * c);
  /// Equation of state is adiabatic by default
  virtual void computePressure(GridCell * c) { computePressureAdiabatic(c); }
  /// Set particle energy (or cell's mass energy density) given pressure
  virtual void computeEnergy(Particle * pt, SlurmDouble press) { computeEnergyAdiabatic(pt, press); }
  /// Compute particle's energy
  virtual void updateParticleEnergy(Particle * p, GridCell * c) { updateParticleEnergyAdiabatic(p, c); }

  // Getters/setters
  inline SlurmDouble get_GasGamma() { return(GasGamma); }
  inline string get_name() { return name; }
  inline SlurmDouble get_Lx() {return Lxyz[0];}
  inline SlurmDouble get_Ly() {return Lxyz[1];}
  inline SlurmDouble get_Lz() {return Lxyz[2];}
  inline SlurmDouble get_L(SlurmInt i) {return Lxyz[i];}
  inline SlurmDouble get_AdiabaticSoundSpeedSq() { return AdiabaticSoundSpeedSq; }
  inline SlurmDouble get_IsothermalSoundSpeedSq() { return IsothermalSoundSpeedSq; }
 
  /// Constructor
  InitialCondition(ConfigFile * config);
  /// Default constructor
  InitialCondition() {}
  /// Destructor
  ~InitialCondition() {}
  /// Factory method
  static InitialCondition * makeInitial(ConfigFile * config);
};

/** 
  MHD Orszag-Tang.
  Initialization of the grid, Orszag-Tang initial condition.
    p0 = 5. / 12. / PI, 
    rho0 = 25. / 36. / PI, 
    B0 = 1.0 / sqrt(4. * PI)
  are the parameters of the initial state.

  Magnetic field in classical formulation
  c->Bx = -B0 * sin(2. * PI * c->get_y());
  c->By = B0 * sin(4. * PI * c->get_x());

*/
class InitialConditionOT: public InitialCondition
{
protected:

  SlurmDouble rho0 = 25. / 36. / PI;
  SlurmDouble p0 = 5. / 12. / PI;

public:

  InitialConditionOT(ConfigFile * config) : InitialCondition(config) {}
  void initNode(GridNode * n);
  void initParticle(Particle * p, Interpolator * interpolator);
};

/** 
  MHD 3D Orszag-Tang.
  Initialization of the grid, 3D Orszag-Tang initial condition.
    p0 = 5. / 12. / PI, 
    rho0 = 25. / 36. / PI, 
    B0 = 1.0 / sqrt(4. * PI)
  are the parameters of the initial state.
*/
class InitialConditionOT3D: public InitialCondition
{

protected:

  SlurmDouble rho0 = 25. / 36. / PI;
  SlurmDouble p0 = 5. / 12. / PI;

public:

  InitialConditionOT3D(ConfigFile * config) :  InitialCondition(config) {}
  void initNode(GridNode * n);

};

/** 
  A single-point pressure perturbation in the middle of the uniform domain.
  How to add magnetic field?
*/
class InitialConditionPerturbation: public InitialCondition
{
protected:
  
  SlurmDouble p0 = 1.666;
  SlurmDouble rho1;
  SlurmInt i0, j0, k0;  // perturbation coords

public:

  InitialConditionPerturbation(ConfigFile * config);
  void initCell(GridCell * c);
};

/** 
  Hydrodynamic Kelvin-Helmholtz instability as in
  
  A Well-Posed Kelvin-Helmholtz Instability Test and Comparison
  Colin P. McNally, Wladimir Lyra, Jean-Claude Passy
  The Astrophysical Journal Supplement, Volume 201, Issue 2, article id. 18, 17 pp. (2012)
  
  In Athena, the initiation is somewhat different
  http://www.astro.princeton.edu/~jstone/Athena/tests/kh/kh.html
  Defaults: p0 = 2.5 (uniform), rho0 = 1, rho1 = 2, u0 = 0.5.
*/
class InitialConditionKH: public InitialCondition
{
protected:

  SlurmDouble rho1 = 2;
  SlurmDouble u0 = 0.5;
  SlurmDouble p0 = 2.5;
  SlurmDouble rho0 = 1.0;
  SlurmDouble rhom;
  SlurmDouble L = 0.025;

public:

  InitialConditionKH(ConfigFile * config);
  void initCell(GridCell * c);
  void initNode(GridNode * n);
  void initParticle(Particle * p, Interpolator * interpolator);
};

/** 
  2D MHD Magnetic Loop, supposed to propagate with a speed inclined by 30 degrees to Y axis.
  It has only Bx and By, hence only Az. Note that in the initial condition div(A) != 0!
  To make it fully 3D, add an arbitrary Z component of the speed.

  It is important that the loop keeps zero vertical component of the magnetic field.

  http://www.astro.princeton.edu/~jstone/Athena/tests/field-loop/Field-loop.html
*/
class InitialConditionML: public InitialCondition
{
protected:
  SlurmDouble LoopRadius = 0.15;
  SlDoubleVector u0 = {0.577, 0.577, 0};
  SlurmDouble B0 = 0.001;

  SlurmDouble computeAz(GridPoint * p);

public:
  InitialConditionML(ConfigFile * config);
  void initNode(GridNode * n);
  void initParticle(Particle * p, Interpolator * interpolator);
};

/** 
  This is a fully 3D magnetic loop inclined by 45 degrees.
  Basically, along the XZ diagonal of the cube.
  It is supposed to propagate with L/sqrt(3) speed in all three dimensions.

  http://www.astro.princeton.edu/~jstone/Athena/tests/field-loop/Field-loop.html
*/
class InitialConditionML3D: public InitialCondition
{
protected:
  SlurmDouble LoopRadius = 0.3;
  SlDoubleVector u0 = {0.577, 0.577, 0.577};
  SlurmDouble y0;
  GridPoint *A1, *A2; // Loop axis
  SlurmDouble B0 = 0.001;

  SlDoubleVector computeA(GridPoint * p);

public:
  InitialConditionML3D(ConfigFile * config);
  void initNode(GridNode * n);
  void initParticle(Particle * p, Interpolator * interpolator);
  ~InitialConditionML3D();
};

/** 
  Initialize HD/MHD Rayleigh-Taylor instability as in
  http://www.astro.princeton.edu/~jstone/Athena/tests/rt/rt.html
  MODIFICATION FOR 3D: ADDED 3RD DIMENSION Z, PARAMETERS ARE THE SAME AS FOR X
  VELOCITY MODIFIED ACCORDING TO http://www.cs.nyu.edu/courses/fall12/CSCI-GA.2945-001/dl/xiaoyi-report.pdf
  
  For the single-mode test, we use a rectangular domain, -0.25 <= x <= 0.25; -0.75 <= y <= 0.75. The boundary conditions are periodic at |x| = 0.25, and reflecting walls at |y| = 0.75. 
  For y>0 the density is 2.0, while for y <= 0 it is one. A constant gravitational acceleration g = 0.1 must be added to the equations of motion. 
  The pressure is given by the condition of hydrostatic equilibrium, that is P = P0 - 0.1rhoy, where P0=2.5, and gamma = 1.4. This gives a sound speed of 3.5 in the low density medium at the interface.
  
  The structures which appear in the nonlinear regime are very sensitive to the nature of the perturbations used to seed the intability. To avoid gridding errors associated with perturbing the interface, we instead perturb the velocities. 
  For the single-mode perturbation, we set Vy = 0.01[1 + cos(4pix)][1 + cos(3piy)]/4.
*/
class InitialConditionRT: public InitialCondition
{
protected:
  SlurmDouble u0 = 0.01;
  SlurmDouble p0 = 2.5;
  SlurmDouble GasGamma = 1.4;

public:
  InitialConditionRT(ConfigFile * config);
  void initCell(GridCell * c);
  void initNode(GridNode * n);
  /// Two particle colors?
  void initParticle(Particle * p, Interpolator * interpolator);
};

/** 
  MHD magnetic reconnection in double-periodic current sheets.
  Also from Athena.
  TODO: parameterize L
*/
class InitialConditionMR: public InitialCondition
{
protected:
  SlurmDouble u0 = 0.1;
  SlurmDouble p0 = 2.5;

public:
  InitialConditionMR(ConfigFile * config) : InitialCondition(config) {}
  void initNode(GridNode * n);
};

/** 
  Inits the hydrodynamic implosion test case (2D version of a Sod shock tube).
  Requires reflecting BCs.

  Reference: http://www.astro.princeton.edu/~jstone/Athena/tests/implode/Implode.html
*/
class InitialConditionImplosion: public InitialCondition
{
protected:

  SlurmDouble rho0 = 0.125;
  SlurmDouble p0 = 0.14;

public:

  InitialConditionImplosion(ConfigFile * config) : InitialCondition(config) {}
  void initCell(GridCell * c);
  void initNode(GridNode * n);
};

/** 1D hydrodynamical shock */
class InitialConditionShock1D: public InitialCondition
{
public:
  InitialConditionShock1D(ConfigFile * config) : InitialCondition(config) {}
  void initCell(GridCell * c);
  void initNode(GridNode * n);
};

/** 
  Hydrodynamic spherical blast test case
  Reference: http://www.astro.princeton.edu/~jstone/Athena/tests/blast/blast.html
*/
class InitialConditionSB: public InitialCondition
{
protected:

  SlurmDouble p0 = 0.1;

public:

  InitialConditionSB(ConfigFile * config) : InitialCondition(config) {}
  void initCell(GridCell * c);
  void initNode(GridNode * n);
  void initParticle(Particle * ptcl, Interpolator * interpolator);
};

/** 
  Magnetic reconnection in double-periodic current sheets one of which is shrinking.
  As proposed by Gianni, September 2016. This is a modified version of the setup used in Yan, Otto, Muzzell, & Lee (1994), JGR.
*/
class InitialConditionSS: public InitialCondition
{
protected:

  SlurmDouble a0 = 5; 
  SlurmDouble D = 5;
  SlurmDouble u0 = 0.1;
  SlurmDouble B0 = 1;

public:

  InitialConditionSS(ConfigFile * config);
  void initNode(GridNode * n);
  void initParticle(Particle * p, Interpolator * interpolator);
};

/** 
  (Magnetized) Taylor-Green vortex.
  Three different magnetic configurations for Taylor-Green from E. Lee at al. (2010, PhysRev E 81)

  When setting == 0 pure hydrodynamic problem.

  For studies and comparison with kinetic simulations.
*/
class InitialConditionTG: public InitialCondition
{
protected:

  SlurmInt setting = 1;
  SlurmDouble u0 = 1;
  SlurmDouble B0 = 1;

public:

  InitialConditionTG(ConfigFile * config);
  void initNode(GridNode * n);
};

/** 
  Inlet/outlet testing
*/
class InitialConditionIO: public InitialCondition
{
protected:
  /// Default initial pressure
  SlurmDouble p0 = 2.5;
  /// Default initial density
  SlurmDouble rho0 = 1;  
  /// Infow velocity
  SlurmDouble u0 = 0.2;
  /// Initial speed of injected particles
  SlurmDouble InjectSpeed;
  /// Injection period. Dt has to stay constant?! Or be much smaller than the injection period?
  SlurmDouble InjectPeriod; // dxp/vinj
  /// Last time particles were injected
  SlurmDouble LastInjectTime;
  /// Next time when particles should appear at X=0.
  SlurmDouble NextInjectTime;
  /// Time is discrete, and new particles do not necessarily appear at X=0.
  SlurmDouble InjectPosition;

public:
  ///
  InitialConditionIO(ConfigFile * config);
  /// Default constructor
  InitialConditionIO() {}
  ///
  void initCell(GridCell * c);
  ///
  void initNode(GridNode * n);
  ///
  void initNode(GridNode * n, SlurmDouble t);
  /// In the beginning, all particles have the same color
  void initParticle(Particle * p, Interpolator * interpolator);
  /// Injected particles have a different color
  void initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t);
  /// t_last is the time when last injection has happened
  virtual bool checkInjectMoment(SlurmDouble t);
  /// Set the moment of this injection and of the next injection
  virtual void resetInjectMoment(SlurmDouble t);
};


/** 
  Fan & Gibson, 2004 (ApJ 609:1123-1133) rising flux rope.
  The arcade is taken from Ashchwanden's book. See initNode().

  Boundary conditions derived from FlipMHD (Anna Lisa Restante & Giovanni Lapenta)

  For velocity:
  In Z - routine bc_wall() is called with params:
    boundary_top = 4 - open
    boundary_bottom = 1 - line tying
  In Y - periodic
  In X - bc_open()

  Specifically,
  X
    u - copy from the previous layer (bc_open())
    B = 0 (bc_ghost())
  Y
    periodic

  Bottom and top BCs (sub bc_wall()) are applied twice in Flip: before and after other BCs.
  Makes sense to override the walls, I guess. In Slurm, Z BCs are applied at the end anyway.

  Z bottom
    u = 0
    B - copy from previous layer
  Z top 
    u - copy
    B - copy

*/
class InitialConditionFG: public InitialConditionIO
{
protected:
  /// Loop injection speed
  SlurmDouble u0 = 0.0125;
  /// Default MF amplitude
  SlurmDouble B0 = 1.0;
  ///
  SlurmDouble p0 = 1e-3;
  /// Arcade parameters k, l, a
  SlurmDouble K, L, alpha;
  /// Torus parameters
  SlurmDouble Bt, a, q, Rg, xc, yc, zc;

public:
  ///
  InitialConditionFG(ConfigFile * config);
  ///
  void initCell(GridCell * c);
  ///
  void initNode(GridNode * n);
  /// Fix bottom boundary for the emerging torus
  void initNode(GridNode * n, SlurmDouble t);
  /// In the beginning, all particles have the same color
  void initParticle(Particle * p, Interpolator * interpolator);
  /// Injected particles have a different color
  void initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t);
};

/**
  Quiet solar wind #1.
  Uniform magnetic field in the XZ plane.

  Density
  r**2 * rho * u = const
  
  Parker's solution
  (u/uc)**2 - 2*ln(u/uc) + 4*ln(rc/r) - 4*rc/r + 3 = 0

  TODO: Perhaps, we should pre-compute a vector of speeds and densities and interpolate from it?

  Parameters of the system.
  L = 1 AU = 216 Rsun
  U = uc (Isothermal speed of sound == critical speed); uc^2 = p/rho
  T = 10^6 K

  At r = rc p/rho == 1.

*/
class InitialConditionSW: public InitialConditionIO
{
protected:

  /// Center of the Sun coordinates
  GridPoint Center;  
  /// Solar wind speed [Code units]
  SlurmDouble u0 = 0.1;
  /// SW temperature [K]
  SlurmDouble T = 1e6;
  /// Critical speed
  SlurmDouble uc;
  /// Critical radius, the radius where u == uc. Normalized to 1 AU.
  SlurmDouble rc;
  /// Speed at the inner boundary
  SlurmDouble uInner = 0.;
  /// Density at the inner boundary
  SlurmDouble rhoInner = 0.;
  /// Speed at Earth's orbit
  SlurmDouble uL;
  /// Number of particles along phi
  SlurmInt nphi = 0;
  /// Number of particles along radius
  SlurmInt nr = 0;
  /// Distance between particles
  SlurmDouble dphi = 0.;
  /// Distance between particles
  SlurmDouble dr = 0.;  
  /// Radius of the internal boundary
  SlurmDouble R = 0.;
  /// Equatorial rotation period
  SlurmDouble AngularSpeed = 0.;
  /// Dipole's vector potential
  SlurmDouble A0 = 0.1;
  /// Inclination of the solar dipole axis to the ecliptic Z
  SlurmDouble Inclination = 7.5/180 * PI;

  /// Compute speed and density of Parker solar wind at given point
  void computeParkerProfile(GridPoint * p, SlurmDouble & r, SlurmDouble & u, SlurmDouble & rho);
  /// Compute dipolar field
  SlDoubleVector computeA(GridPoint * p, SlurmDouble r = -1);

public:

  ///
  InitialConditionSW(ConfigFile * config);
  /// Default constructor
  InitialConditionSW() {}
  ///
  void initCell(GridCell * c);
  ///
  void initNode(GridNode * n) { initNode(n, 0.); };
  /// Fix bottom boundary for the emerging torus
  void initNode(GridNode * n, SlurmDouble t);
  /// In the beginning, all particles have the same color
  void initParticle(Particle * p, Interpolator * interpolator);
  /// Injected particles have a different color
  void initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t);
  /// Isothermal EOS in this case
  void computePressure(GridCell * c) { computePressureIsothermal(c); }
  /// Just do nothing for isothermal EOS
  void updateParticleEnergy(Particle * p, GridCell * c) { }
};

/**
  Solar wind in meridional plane (XY here) as described by
    Chun Xia, J. Teunissen, I. Mellah, E. Chane & Mr. Keppens, 2017 (ArXiv preprint on MPI-AMRVAC).
  Dipolar magnetic field

  Density
  r**2 * rho * u = const
  
  Parker's solution
  (u/uc)**2 - 2*ln(u/uc) + 4*ln(rc/r) - 4*rc/r + 3 = 0

  TODO: Perhaps, we should pre-compute a vector of speeds and densities and interpolate from it?

  Parameters of the system.
  L = 1 AU = 216 Rsun
  U = uc (Isothermal speed of sound == critical speed); uc^2 = p/rho
  T = 10^6 K

  At r = rc p/rho == 1.

*/
class InitialConditionSW1: public InitialConditionSW
{
protected:

  /// Solar wind speed [Code units]
  SlurmDouble u0 = 0.1;
  /// SW temperature [K]
  SlurmDouble T = 1.5e6;
  /// Inclination of the solar dipole axis to the ecliptic Z
  SlurmDouble Inclination = 0.5 * PI; // 7.5/180 * PI;

public:

  /// Even in C++ 11 it is tricky to inherit constructors from non-direct parents, 
  /// therefore best is to define constructors explicitly.
  InitialConditionSW1(ConfigFile * config);
  /// In the beginning, all particles have the same color
  void initParticle(Particle * p, Interpolator * interpolator);
  /// Injected particles have a different color
  void initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t);
};


/**
  InitialConditionFluxRope.
  Flux rope moving through uniform magnetic field.
  Inflowing magnetic loop from the ML test.
*/
class InitialConditionFR: public InitialConditionIO
{
protected:
  /// Loop injection speed
  SlurmDouble u0 = 0.1;
  /// Loop amplitude
  SlurmDouble B0 = 0.;
  /// Background magnetic field vector
  SlDoubleVector B0xyz = {0, 0, 0};
  ///
  SlurmDouble p0 = 1.0;
  /// Magnetic loop radius
  SlurmDouble LoopRadius = 0.15;
  /// Initial coordinates of the loop
  SlurmDouble xyzc[3] = {0, 0, 0};
  /// Compute loop field
  SlurmDouble computeLoopA(GridPoint * p, SlurmDouble t);

public:
  ///
  InitialConditionFR(ConfigFile * config);
  ///
  void initCell(GridCell * c);
  ///
  void initNode(GridNode * n) { initNode(n, 0.); };
  /// Fix bottom boundary for the emerging torus
  void initNode(GridNode * n, SlurmDouble t);
  /// In the beginning, all particles have the same color
  void initParticle(Particle * p, Interpolator * interpolator) { initParticle(p, interpolator, 0.); };
  /// Injected particles have a different color
  void initParticle(Particle * p, Interpolator * interpolator, SlurmDouble t);
  /// Isothermal EOS in this case
  void computePressure(GridCell * c) { computePressureIsothermal(c); }
  /// Just do nothing for isothermal EOS
  void updateParticleEnergy(Particle * p, GridCell * c) { }
};


/** 
  Test laboratory for testing different tests.
  Use setting to switch between tests.
*/
class InitialConditionTest: public InitialCondition
{
protected:

  SlurmInt setting = 1;
  SlurmDouble u0 = 0.1;

public:

  InitialConditionTest(ConfigFile * config);
  void initNode(GridNode * n);
};

/** 
  1D MHD Brio and Wu Shock Tube
  http://www.astro.princeton.edu/~jstone/Athena/tests/brio-wu/Brio-Wu.html
  Brio, M. & C.C. Wu, "An Upwind Differencing Scheme for the Equations of Ideal Magnetohydrodynamics", Journal of Computational Physics, 75, 400-422 (1988). The test is described in Section V.
*/
class InitialConditionBrioWu: public InitialCondition
{
public:

  InitialConditionBrioWu(ConfigFile * config);
  void initCell(GridCell * c);
  void initNode(GridNode * n);
  void initParticle(Particle * ptcl, Interpolator * interpolator);
};


/** 
  Two interactig blast waves (HD).
  http://www.astro.princeton.edu/~jstone/Athena/tests/twoibw/TwoIBW.html
  Woodward & Colella, The Numerical Simulation of Two-Dimensional Fluid Flow with Strong Shocks. JCP 54, 115 -- 173 (1984).
*/
class InitialConditionTB: public InitialCondition
{
public:

  InitialConditionTB(ConfigFile * config);
  void initCell(GridCell * c);
  void initNode(GridNode * n);
  void initParticle(Particle * ptcl, Interpolator * interpolator);
};

/** 
  Two interactig blast waves (HD) with periodic boundary conditions.
  http://www.astro.princeton.edu/~jstone/Athena/tests/twoibw/TwoIBW.html
  Woodward & Colella, The Numerical Simulation of Two-Dimensional Fluid Flow with Strong Shocks. JCP 54, 115 -- 173 (1984).
*/
class InitialConditionTBPeriodic: public InitialCondition
{
public:

  InitialConditionTBPeriodic(ConfigFile * config);
  void initCell(GridCell * c);
  void initNode(GridNode * n);
  void initParticle(Particle * ptcl, Interpolator * interpolator);
};


/** 
  Double periodic Harris current sheet (well, 2 current sheets).
  
*/
class InitialConditionDH: public InitialCondition
{
protected:

  /// Current sheet thickness
  SlurmDouble delta = 1; 
  /// Distance from the current sheets to the center of the domain. Obsolete?
  SlurmDouble D = 5;
  /// Magnetic field amplitude
  SlurmDouble B0 = 1;
  /// Perturbation amplitude
  SlurmDouble A0 = 0.01*B0;
  /// Perturbation wavelength?
  SlurmDouble delta_perp = 5;

  /// Compute vector potential
  SlurmDouble computeAz(GridPoint * p);
  /// Compute magnetic pressure
  SlurmDouble computeB(GridPoint * p);

public:

  InitialConditionDH(ConfigFile * config);
  void initNode(GridNode * n);
  void initParticle(Particle * ptcl, Interpolator * interpolator);
  void initCell(GridCell * c);
};
