#include "Fields.h"
#include "GridCommon.h"
#include "BoundaryConditions.h"
#include <boost/algorithm/string/predicate.hpp>

using namespace std;

/** 
  The constructor should read the initial conditions and physics, e.g., gamma and gravity?
*/
Fields::Fields(ConfigFile * config, InitialCondition * initial) : config(config)
{
  // Some physis first
  this->Resistivity = config->read<SlurmDouble>("Resistivity", 0.);

  // Read magnetic field evolution strategy
  FieldEvolutionMethod = getMethodByName(config->read<string>("MagneticFieldEvolution", "Off"));

  messageline("Initializing Fields object");
  messageline("Magnetic field evolution strategy: ", getFieldEvolutionMethodName(FieldEvolutionMethod));

  // Init external fields if needed
  /*TODO: create a factory for the external fields? */
  if (boost::iequals(initial->get_name(), "ShrinkingSheet")) 
    this->ExternalFields = new ExternalFieldsShrinkingSheets(config);
  else if (boost::iequals(initial->get_name(), "FanGibson")) 
    this->ExternalFields = new ExternalFieldsFG(config);
  else if (boost::iequals(initial->get_name(), "FluxRope")) 
    this->ExternalFields = new ExternalFieldsUniform(config);
  else if (boost::iequals(initial->get_name(), "BrioWu")) 
    this->ExternalFields = new ExternalFieldsUniform(config);
}

/// Returns the interpolation scheme given its name
string Fields::getFieldEvolutionMethodName(MFEvolutionMethods method)
{
  if (method == mfemOff) 
    return(string("Off"));
  else if (method == mfemInterpolateParticles) 
    return(string("InterpolateParticles"));
  else if (method == mfemAdvectParticles) 
    return(string("AdvectParticles"));
  else if (method == mfemPreserveParticles) 
    return(string("PreserveParticles"));
  else 
    return string("Unknown");
}

/// Return field evolution method by name
MFEvolutionMethods Fields::getMethodByName(string name)
{
  if (boost::iequals(name, "Off"))
    return(mfemOff);
  else if (boost::iequals(name, "InterpolateParticles"))
    return(mfemInterpolateParticles);
  else if (boost::iequals(name, "AdvectParticles"))
    return(mfemAdvectParticles);
  else if (boost::iequals(name, "RemapGrid"))
    return(mfemRemapGrid);
  else if (boost::iequals(name, "PreserveParticles")) 
    return(mfemPreserveParticles);
  else 
    return(mfemUnknown);
}

/** 
  Compute B (including Bexternal) and gradA on a cell center using cell neighbor's vector potential.
  Boundary conditions on nodes A MUST be imposed prior to calling this.

  B = nabla x A
  dA/dt = nabla x B (= -E)

*/
void Fields::computeBfromA(GridCell * c)
{
  // Gradients of the node quantities: cell centered
  GridNode* gradCCx = new GridNode();
  GridNode* gradCCy = new GridNode();
  GridNode* gradCCz = new GridNode();

  // Compute the gradients
  c->DirectionalDeriv(QUANT_MAGNETIC, gradCCx, gradCCy, gradCCz);
 
  // Compute field and add external
  // Derive B
  c->set_B(gradCCy->get_Az() - gradCCz->get_Ay(), gradCCz->get_Ax() - gradCCx->get_Az(), gradCCx->get_Ay() - gradCCy->get_Ax());

  // Fabio's way to add external fields
  if (ExternalFields)
    ExternalFields->addCellExternalFields(c);

  // Vector potential gradients
  c->set_gradA(gradCCx, gradCCy, gradCCz);

  // Cleanup
  delete gradCCx;
  delete gradCCy;
  delete gradCCz;  
}

/** 
  Update scalar potential of the electromagnetic field on cells.
  It is used for divergence-cleaning of A.

  dphi/dt = -c^2 * div(A) - c*phi/h - 0.5 * div(u) * phi,
  TODO: is it div(u) * phi, or div(u*phi)?

  where h is the smoothing length. 
  TODO: what is h? 5 * (dx + dy + dz)?

  Stasyszyn & Elstner "A Vector Potential implementation for Smoothed Particle Magnetohydrodynamics". ArXiv https://arxiv.org/abs/1411.3290v1
*/
void Fields::advanceCellScalarPotential(GridCell * c, SlurmDouble dt)
{
  SlurmDouble cs2 = c->get_p() / c->get_rho();
  c->inc_phi(dt * (-cs2 * c->get_divA() - pow(cs2, 0.5)*c->get_phi() / (5 * c->get_dx()) - 0.5 * c->get_phi() * c->get_divu()));
}

/** 
  Fetches from particles or computes node vector potential according to FieldEvolutionMethod.
  Also computes currents on the nodes.
*/
void Fields::prepareNodeFields(GridNode* n, BoundaryCondition* bcx1, BoundaryCondition* bcy1, BoundaryCondition* bcz1)
{
  if (FieldEvolutionMethod == mfemAdvectParticles)
  {
    // Only normalize to volume here, because what we've got from particles is A*vol
    n->set_A(n->get_Ax() / n->get_vol(), n->get_Ay() / n->get_vol(), n->get_Az() / n->get_vol());
  }
  if ((FieldEvolutionMethod == mfemInterpolateParticles) || (FieldEvolutionMethod == mfemPreserveParticles))
  {
    // To get correct interpolation weights we need to flip neighbor particles coordinates over the periodic walls.
    if (n->has_all_ptcls())
    {
      GridPoint * p000 = n->get_p000()->clone();
      GridPoint * p100 = n->get_p100()->clone();
      GridPoint * p010 = n->get_p010()->clone();
      GridPoint * p110 = n->get_p110()->clone();
      GridPoint * p001 = n->get_p001()->clone();
      GridPoint * p101 = n->get_p101()->clone();
      GridPoint * p011 = n->get_p011()->clone();
      GridPoint * p111 = n->get_p111()->clone();
      
      if (bcx1->get_type() == btPeriodic)
      {
        if (p000->get_x() > n->get_x()) p000->set_x(p000->get_x() - bcx1->get_end());
        if (p010->get_x() > n->get_x()) p010->set_x(p010->get_x() - bcx1->get_end());
        if (p100->get_x() < n->get_x()) p100->set_x(p100->get_x() + bcx1->get_end());
        if (p110->get_x() < n->get_x()) p110->set_x(p110->get_x() + bcx1->get_end());
        if (p001->get_x() > n->get_x()) p001->set_x(p001->get_x() - bcx1->get_end());
        if (p011->get_x() > n->get_x()) p011->set_x(p011->get_x() - bcx1->get_end());
        if (p101->get_x() < n->get_x()) p101->set_x(p101->get_x() + bcx1->get_end());
        if (p111->get_x() < n->get_x()) p111->set_x(p111->get_x() + bcx1->get_end());
      }
      
      if (bcy1->get_type() == btPeriodic)
      {
        if (p000->get_y() > n->get_y()) p000->set_y(p000->get_y() - bcy1->get_end());
        if (p100->get_y() > n->get_y()) p100->set_y(p100->get_y() - bcy1->get_end());
        if (p010->get_y() < n->get_y()) p010->set_y(p010->get_y() + bcy1->get_end());
        if (p110->get_y() < n->get_y()) p110->set_y(p110->get_y() + bcy1->get_end());
        if (p001->get_y() > n->get_y()) p001->set_y(p001->get_y() - bcy1->get_end());
        if (p101->get_y() > n->get_y()) p101->set_y(p101->get_y() - bcy1->get_end());
        if (p011->get_y() < n->get_y()) p011->set_y(p011->get_y() + bcy1->get_end());
        if (p111->get_y() < n->get_y()) p111->set_y(p111->get_y() + bcy1->get_end());
      }
      
      if (bcz1->get_type() == btPeriodic)
      {
        if (p000->get_z() > n->get_z()) p000->set_z(p000->get_z() - bcz1->get_end());
        if (p100->get_z() > n->get_z()) p100->set_z(p100->get_z() - bcz1->get_end());
        if (p010->get_z() > n->get_z()) p010->set_z(p010->get_z() - bcz1->get_end());
        if (p110->get_z() > n->get_z()) p110->set_z(p110->get_z() - bcz1->get_end());
        if (p001->get_z() < n->get_z()) p001->set_z(p001->get_z() + bcz1->get_end());
        if (p101->get_z() < n->get_z()) p101->set_z(p101->get_z() + bcz1->get_end());
        if (p011->get_z() < n->get_z()) p011->set_z(p011->get_z() + bcz1->get_end());
        if (p111->get_z() < n->get_z()) p111->set_z(p111->get_z() + bcz1->get_end());
      }
      
      // 3D interpolation
      SlurmDouble * weights = GridCommon::TrilinearIrregularInterpolationWeights(n, p000, p100, p010, p110, p001, p101, p011, p111);
      for (int i = 0; i < 3; i++)
      {
        SlurmDouble A = 0;    
        for (int j = 0; j < 8; j++)
          A += n->get_pi(j)->get_A(i) * weights[j]; // Particle * _p000, Particle * _p100, Particle * _p010, Particle * _p110, Particle * _p001, Particle * _p101, Particle * _p011, Particle * _p111
        n->set_A(i, A);
      }
      
      delete weights;
      delete p000;
      delete p100;
      delete p010;
      delete p110;
      delete p001;
      delete p101;
      delete p011;
      delete p111;
    }
    else
    {
      // Simple average of the non-null neighbors?
      SlurmDouble A[3] = {0., 0., 0.};
      Particle ** ptcl = n->get_ptcls();
      SlurmInt nonzero = 0;
      for (SlurmInt i = 0; i < 8; i++)
        if (ptcl[i])
        {
          for (SlurmInt j = 0; j < 3; j++)
            A[j] += ptcl[i]->get_A(j);
          ++nonzero;
        }
      if (nonzero > 0)
      {
        for (SlurmInt j = 0; j < 3; j++)
          n->set_A(j, A[j] / nonzero);
      }  
    }
  }
}

/** 
  Advance electromagnetic vector potential on the given node 
    dA/dt = -E - eta*J + (u.nabla)A
  or
    dA/dt = (u.nabla)A + [u x Bext] - resistivity*J,
  where [u x Bext] == -Eext.
    J = curl(B)  
*/
void Fields::advanceNodeVectorPotential(GridNode* n, SlurmDouble t, SlurmDouble dt)
{ 
  // Update electromagnetic vector potential using the cell-centered derivatives
  //if ((FieldEvolutionMethod == mfemAdvectParticles) || (FieldEvolutionMethod == mfemInterpolateParticles) || (FieldEvolutionMethod == mfemPreserveParticles))
  {
    // Averaged cell
    GridCell* avgc = new GridCell();

    // Save to old
    n->saveToOldA();

    // A evolution
    n->averageCells(QUANT_INTERNAL | QUANT_MAGNETIC, avgc);
    n->inc_A(dt * (n->get_ux() * avgc->get_gradA11() + n->get_uy() * avgc->get_gradA21() + n->get_uz() * avgc->get_gradA31() - Resistivity * n->get_Jx()),
             dt * (n->get_ux() * avgc->get_gradA12() + n->get_uy() * avgc->get_gradA22() + n->get_uz() * avgc->get_gradA32() - Resistivity * n->get_Jy()),
             dt * (n->get_ux() * avgc->get_gradA13() + n->get_uy() * avgc->get_gradA23() + n->get_uz() * avgc->get_gradA33() - Resistivity * n->get_Jz()));

    // Subtract grad(phi)
    GridCell* gradNCx = new GridCell();
    GridCell* gradNCy = new GridCell();
    GridCell* gradNCz = new GridCell();

    n->DirectionalDeriv(QUANT_SCALARPOTENTIAL | QUANT_MAGNETIC, gradNCx, gradNCy, gradNCz);
    n->inc_A(-dt * gradNCx->get_phi(), -dt * gradNCy->get_phi(), -dt * gradNCz->get_phi());

    // Subtract Eext
    if (ExternalFields)
      ExternalFields->addNodeExternalFields(n, avgc, dt);
    
    // Save A increments to old
    n->update_dA();

    // This can be commented out to improve performance (when divB check is not needed!)
    n->set_divB(gradNCx, gradNCy, gradNCz);
  
    delete avgc;
    delete gradNCx;
    delete gradNCy;
    delete gradNCz;
  }
}

/** 
  Compute electric currents on nodes
    J = [nabla x B]
*/
void Fields::computeNodeCurrents(GridNode* n)
{
  GridCell* gradNCx = new GridCell();
  GridCell* gradNCy = new GridCell();
  GridCell* gradNCz = new GridCell();

  n->DirectionalDeriv(QUANT_MAGNETIC, gradNCx, gradNCy, gradNCz);
  n->set_J(gradNCy->get_Bz() - gradNCz->get_By(), gradNCz->get_Bx() - gradNCx->get_Bz(), gradNCx->get_By() - gradNCy->get_Bx());

  delete gradNCx;
  delete gradNCy;
  delete gradNCz;
}

/** Compute the divergence of B */
void Fields::computeDivB(GridNode* n)
{
  if (n->get_role() != gerGhost)
  {
    // Node-centered gradients of cell quantities.
    GridCell* gradNCx = new GridCell();
    GridCell* gradNCy = new GridCell();
    GridCell* gradNCz = new GridCell();
  
    n->DirectionalDeriv(QUANT_MAGNETIC, gradNCx, gradNCy, gradNCz);
    n->set_divB(gradNCx->get_Bx() + gradNCy->get_By() + gradNCz->get_Bz());
  
    delete gradNCx;
    delete gradNCy;
    delete gradNCz;
  }
  else
    n->set_divB(0);
}

/**
  Add resistivity to internal energy. 
    de/dt = -(p + artificial_viscosity)*grad(u) + resistivity*|[nabla x B]|^2

  Currents (J = [nabla x B]) must be computed on the nodes prior to calling this!
*/
void Fields::addCellResistivity(GridCell * c)
{
  if (Resistivity > TIISmallDOuble)
  {
    // Averaged nodes
    GridNode* avgn = new GridNode();

    c->averageNodes(QUANT_CURRENT, avgn);
    c->set_e(c->get_e() + Resistivity * (pow(avgn->get_Jx(), 2) + pow(avgn->get_Jy(), 2) + pow(avgn->get_Jz(), 2)));

    delete avgn;  
  }
}

Fields::~Fields()
{
  // Is ExternalFields object destroyed automatically?
}

// External fields shrinking sheets

/**
  External Ez (node quantity) for double shrinking current sheets:

  Ezext=-1.d0/a/tscal*a1*(((x-Lx/2.d0)+D)*tanh(((x-Lx/2.d0)+D)/a)-a*log(cosh(((x-Lx/2.d0)+D)/a)) - (-Lx/2.d0+D)*tanh((-Lx/2.d0+D)/a)+a*log(cosh((-Lx/2.d0+D)/a)))
  if (x>Lx/2.d0)
    node%Ezext=1.d0/a/tscal*a1*(((x-Lx/2.d0)-D)*tanh(((x-Lx/2.d0)-D)/a)-a*log(cosh(((x-Lx/2.d0)-D)/a)) - ((Lx/2.d0-D)*tanh((Lx/2.d0-D)/a)-a*log(cosh((Lx/2.d0-D)/a))))
  
  Where a=a0-a1*t/tscal, a0=a1, D, and tscal constants defined in the cfg.
  
  For the resistivity: dA/dt=(u x B)+(u . nabla)A + Eext -eta*J, with (u x B)+(u . nabla)A already included in the code.
  Also, de/dt=-(p+q)div(u) + eta*J^2
*/
void ExternalFieldsShrinkingSheets::addNodeExternalFields(GridNode* n, SlurmDouble t, SlurmDouble dt, InitialCondition * initial)
{
  SlurmDouble a = a0 - a1 * t / tscal;

  if (n->get_x() > x0)
  {
    SlurmDouble dm = n->get_x() - x0 - D;
    n->set_A(2, n->get_Az() + dt / a / tscal * a1 * (dm * tanh(dm / a) - a * log(cosh(dm / a)) - (-d1 * tanh(-d1 / a) - a * log(cosh(-d1/a)))));
  }
  else
  {
    SlurmDouble dp = n->get_x() - x0 + D;
    n->set_A(2, n->get_Az() - dt / a / tscal * a1 * (dp * tanh(dp / a) - a * log(cosh(dp / a)) - d1 * tanh(d1 / a) + a * log(cosh(d1 / a))));
  }
}

ExternalFieldsShrinkingSheets::ExternalFieldsShrinkingSheets(ConfigFile * config) : ExternalFieldsGenerator(config)
{
  this->tscal = config->read<SlurmDouble>("tscal", 100);
  this->D = config->read<SlurmDouble>("D", 5.0);
  this->a0 = config->read<SlurmDouble>("a0", 5.0);
  this->a1 = this->a0;
  this->x0 = 0.5 * config->read<SlurmDouble>("Lx");
  this->d1 = -this->x0 + this->D;
}

// Magnetic arcade

ExternalFieldsFG::ExternalFieldsFG(ConfigFile * config) : ExternalFieldsGenerator(config)
{
  // Domain dimensions
  this->Lx = config->read<SlurmDouble>("Lx", 1.5);
  this->Ly = config->read<SlurmDouble>("Ly", 1.0);
  this->Lz = config->read<SlurmDouble>("Lz", 1.25);
  // This is only used to compute Bt...
  this->B0 = config->read<SlurmDouble>("B0", 1.0);
  this->Bt = 9 * B0;
  // Normalization factor inside the exponent of the loop. Basically, toroidal loop diameter.
  this->a = config->read<SlurmDouble>("a", 0.1);
  // Radius of the torus, I guess
  this->Rg = config->read<SlurmDouble>("Rg", 0.375);
  // wat is tis?
  this->q = config->read<SlurmDouble>("Q", -1);
  // Loop vertical velocity?
  this->u0 = config->read<SlurmDouble>("u0", 0.0125);

  // Center of the torus
  this->xc = 0.5 * Lx;
  this->yc = 0.5 * Ly;
  // Initially, loop is under the bottom. However, in F&G paper it is at -0.675.
  this->zc = -Rg - 3 * a;  

  messageline("\nFan & Gibson loop parameters:");
  messageline("Rg = ", Rg);
  messageline("zc = ", zc);
  messageline("u0 = ", u0);
}

/** 
  External field is needed within 3*a far from the toroidal axis.
  Here, the particles need to be injected as well.
*/
bool ExternalFieldsFG::isExternalFieldNeeded(GridPoint* p, SlurmDouble t, SlurmDouble dt)
{
  // Coordinates of the node ralitive to the emerging loop's center
  SlurmDouble x = p->get_x() - xc;
  SlurmDouble y = p->get_y() - yc;
  SlurmDouble z = p->get_z() - (zc + u0 * t);
 
  // Squared distance to the center of the torus
  SlurmDouble r2 = pow(x, 2) + pow(y, 2) + pow(z, 2);
  SlurmDouble r = pow(r2, 0.5);

  SlurmDouble CosTheta = y / (r + 1.e-10);
  SlurmDouble SinTheta = pow(1 - CosTheta * CosTheta, 0.5);

  // Omega is the distance to the axis of the toroidal tube.
  SlurmDouble Omega2 = r2 + Rg*Rg - 2. * r * Rg * SinTheta * SinTheta;
  SlurmDouble Omega = pow(Omega2, 0.5);
 
  return (Omega < 3 * a);
}

/**
  A torus moving in from below.
  dA = -dt * E = (u0 * dt * By, -u0 * dt * Bx, 0)

  E = [u0z x B]
  Ex = - u0 * By
  Ey = u0 * Bx

  Center of spherical coordinate system (r, Phi, Theta is in the center of the torus. 
  Its polar axis is the torus axis, hence the coordinates.

  Magnetic field is given by
    Btube = rot[A(r, Theta) / (r * sin(Theta)) * ephi] + Bphi(r, Theta) * ephi
  where ephi is a unit vector in Phi direction,
    A(r, Theta) = 0.5 * q * a^2 * Bt * exp(-Omega(r, Theta)^2 / a^2)
    Bphi(r, Theta) = a * Bt / (r * sin(Theta)) * exp(-Omega(r, Theta)^2 / a^2)
  where Omega is the distance to the tube axis:
    Omega = (r^2 + R^2 - 2 * r * R * sin(Theta)^2)^0.5
  
  R = 0.375 * Ly
  Bt = 9 * B0
  q = -1
  a = 0.1 * Ly

  At time t z

*/
void ExternalFieldsFG::addNodeExternalFields(GridNode* n, SlurmDouble t, SlurmDouble dt, InitialCondition * initial)
{
  // Coordinates of the node ralitive to the emerging loop's center
  SlurmDouble x = n->get_x() - xc;
  SlurmDouble y = n->get_y() - yc;
  SlurmDouble z = n->get_z() - (zc + u0 * t);
 
  // Squared distance to the center of the torus
  SlurmDouble r2 = pow(x, 2) + pow(y, 2) + pow(z, 2);
  SlurmDouble r = pow(r2, 0.5);

  SlurmDouble CosTheta = y / (r + 1.e-10);
  SlurmDouble SinTheta = pow(1 - CosTheta * CosTheta, 0.5);

  SlurmDouble CosPhi = x / (r * SinTheta + 1e-10);
  SlurmDouble SinPhi = z / (r * SinTheta + 1e-10);

  // Omega is the distance to the axis of the toroidal tube.
  SlurmDouble Omega2 = r2 + Rg*Rg - 2. * r * Rg * SinTheta * SinTheta;
  SlurmDouble Omega = pow(Omega2, 0.5);

  // Partial derivatives of the torus vector potential
  SlurmDouble dAdOmega = -q * Bt * Omega * exp(-Omega2 / (a*a));
  // ?
  SlurmDouble pOmpth = -2. / Omega * r * Rg * SinTheta * CosTheta;
  // ?
  SlurmDouble pOmpr = 1. / Omega * (r - Rg * SinTheta * SinTheta);
  
  SlurmDouble Bphi = a * Bt / (r * SinTheta + 1e-10) * exp(-Omega2 / (a*a));
  SlurmDouble Br = dAdOmega * pOmpth / (r2 * SinTheta + 1e-10);
  SlurmDouble Btheta = -dAdOmega * pOmpr / (r * SinTheta + 1e-10);

  /**
  // Field as defined in FlipMHD setup
  bx_ini = -Bphi * SinPhi +  Btheta * CosTheta * CosPhi + Br * SinTheta * CosPhi;
  // In the below expression the exponent is required if the loop is fully inside the domain in the beginning?
  //by_ini = -SinTheta * Btheta + CosTheta * Br - exp(-0.0625 * (n->get_z() - zc) / (a * a));
  // I guess if we are moving the loop inside, esponent is unnecessary??
  by_ini = -Btheta * SinTheta + Br * CosTheta - exp(-0.0625 * (n->get_z() - zc) / (a * a));
  bz_ini = Bphi * CosPhi + Btheta * CosTheta * SinPhi + Br * SinTheta * SinPhi;
  */

  // Truncate the field outside the toroidal tube
  if (Omega < 3 * a)
  {
    // dAx = u0 * By * dt, dAy = -u0 * Bx * dt
    n->inc_A(u0 * dt * (-Btheta * SinTheta + Br * CosTheta - exp(-0.0625 * (n->get_z() - zc) / (a * a))), -u0 * dt * (-Bphi * SinPhi +  Btheta * CosTheta * CosPhi + Br * SinTheta * CosPhi), 0); 
    // Is it legitimate to set loop's speed here?
    n->set_u(0, 0, u0);
  }
}

// Uniform background field

ExternalFieldsUniform::ExternalFieldsUniform(ConfigFile * config) : ExternalFieldsGenerator(config)
{
  Bext[0] = config->read<SlurmDouble>("B0x", config->read<SlurmDouble>("B0", 0.));
  Bext[1] = config->read<SlurmDouble>("B0y", 0.);
  Bext[2] = config->read<SlurmDouble>("B0z", 0.);
  message("Uniform background magnetic field Bxyz = ", Bext[0]);
  message(", ", Bext[1]);
  messageline(", ", Bext[2]);
}

/** 
  External field is needed everywhere.
*/
bool ExternalFieldsUniform::isExternalFieldNeeded(GridPoint* p, SlurmDouble t, SlurmDouble dt)
{
  return true;
}

/**
   Subtract external electric field
                            |uy*Bz - uz*By|
     dA = -Eext = [u x B] = |uz*Bx - ux*Bz|
                            |ux*By - uy*Bx|
        
   avgc - precomputed average over node's neighbor cells. It is used to fetch B.
*/
void ExternalFieldsUniform::addNodeExternalFields(GridNode* n, GridCell * avgc, SlurmDouble dt)
{ 
  n->inc_A(dt * (n->get_u(1)*Bext[2] - n->get_u(2)*Bext[1]), dt * (n->get_u(2)*Bext[0] - n->get_u(0)*Bext[2]), dt * (n->get_u(0)*Bext[1] - n->get_u(1)*Bext[0]));
}

/** 
  Add Bext as Fabio suggests.
  If this method is used, do not use addNodeExternalFields.
*/
void ExternalFieldsUniform::addCellExternalFields(GridCell* c) 
{
  c->inc_B(Bext);
}
