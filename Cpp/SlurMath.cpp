#include "SlurMath.h"

/** 
  Compute matrix determinant recursively
  
  Parameters:
    a - the matrix
    m - the matrice's order

  det(a) = sum((-1)^j * a0j * det(M0j)),
  where Mij is a minor (matrix obtained by crossing out the i-th row and j-th column out of a).

  nomenclature: a[row_index][column_index]?

*/
SlurmDouble SlDeterminant(SlurmDoubleArray2D a, SlurmInt m)
{
  // The resulting determinant
  SlurmDouble D = 0.;

  /// As we most often encounter 3x3 matrices, put it as the first choice here
  if (m == 3)
  {
    D = a[0][0] * (a[1][1]*a[2][2] - a[1][2]*a[2][1]) + a[1][0] * (a[2][1]*a[0][2] - a[0][1]*a[2][2]) + a[2][0] * (a[0][1]*a[1][2] - a[0][2]*a[1][1]);
  }
  else if (m == 1) 
  {
    D = a[0][0];
  }
  else if (m == 2) 
  {
    D = a[0][0] * a[1][1] - a[1][0] * a[0][1];
  }
  else if (m > 3)
  {
    // Matrice's a minor
    SlurmDoubleArray2D minor;
    minor.resize(boost::extents[m-1][m-1]);
    // The sign of the next minor's determinant
    short s = 1;
    // Just indices
    SlurmInt i, j, im;
    
    // Sum all minors
    for (SlurmInt row_index = 0; row_index < m; ++row_index)
    {
      // Indices in the minor matrix
      im = 0;
      // Construct the minor matrix
      for (i = 0; i < m; ++i)
      {
        // Skip the current row
        if (i == row_index) continue;
        // Set the minor matrix' row members
        for (j = 1; j < m; ++j) minor[im][j-1] = a[i][j];
        // Increment the row index in the minor matrix
        ++im;
      }
      // Add to the determinant
      D += s * a[row_index][0] * SlDeterminant(minor, m-1);
      s *= -1;
    }
  }

  return(D);
}

/**
  Solve linear system using Kramer's method

  Parameters:
    a - matrix of coefficients
    r - vector of free terms
    solution - the resulting array. Must be initialized before
    m - the order of the system (size of the matrix)

*/
void SolveLinearSystem(SlurmDoubleArray2D a, SlurmDouble * r, SlurmDouble * solution, SlurmInt m)
{
  SlurmDouble D = SlDeterminant(a, m);
  if (D != 0)
  {
    SlurmDouble temp_column[m];
    
    for (SlurmInt j = 0; j < m; ++j)
    {
      for (SlurmInt i = 0; i < m; ++i) 
      {
        temp_column[i] = a[i][j];
        a[i][j] = r[i];
      }
    
      solution[j] = SlDeterminant(a, m) / D;
    
      for (SlurmInt i = 0; i < m; ++i) a[i][j] = temp_column[i];
    }
  }
  else
  {
    for (SlurmInt i = 0; i < m; ++i) solution[i] = 0.;
  }
}