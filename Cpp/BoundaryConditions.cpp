#include "BoundaryConditions.h"

using namespace std;

/// Factory
BoundaryCondition * BoundaryCondition::makeBoundaryCondition(ConfigFile * config, string btname, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial)
{
  BoundaryType bt = BoundaryCondition::stringToBT(btname);

  if (bt == btPeriodic)
    return new BoundaryConditionPeriodic(config, bt, side, normal, begin, end, initial);
  else if (bt == btReflective)
    return new BoundaryConditionReflective(config, bt, side, normal, begin, end, initial);
  else if (bt == btInlet)
    return new BoundaryConditionInlet(config, bt, side, normal, begin, end, initial);
  else if (bt == btOutlet)
    return new BoundaryConditionOutlet(config, bt, side, normal, begin, end, initial);
  else if (bt == btFixed)
    return new BoundaryConditionFixed(config, bt, side, normal, begin, end, initial);
  else if (bt == btRigidWall)
    return new BoundaryConditionRigidWall(config, bt, side, normal, begin, end, initial);
  else if (bt == btSphere)
    return new BoundaryConditionSphere(config, bt, side, normal, begin, end, initial);
  else if (bt == btSupersonicOutlet)
    return new BoundaryConditionSupersonicOutlet(config, bt, side, normal, begin, end, initial);
  else 
    return NULL;
}

/// Main constructor
BoundaryCondition::BoundaryCondition(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : type(bt), side(side), normal(normal), begin(begin), end(end), initial(initial)
{
  thickness = fabs(end - begin);
  messageline(btToString(bt));
}

/** Returns Boundary Condition name */
string BoundaryCondition::btToString(BoundaryType t)
{
  if (t == btPeriodic)
    return string("periodic");
  else if (t == btReflective) 
    return string("reflective");
  else if (t == btOutlet) 
    return string("outlet");
  else if (t == btInlet) 
    return string("inlet");
  else if (t == btFixed) 
    return string("fixed");
  else if (t == btRigidWall) 
    return string("rigidwall");
  else if (t == btSphere) 
    return string("sphere");
  else if (t == btSupersonicOutlet) 
    return string("SupersonicOutlet");
  else
    return string("unknown");
}

/** Returns Boundary condition type given name */
BoundaryType BoundaryCondition::stringToBT(string name)
{
  if (boost::iequals(name, "periodic"))
    return btPeriodic;
  else if (boost::iequals(name, "reflective"))
    return btReflective;
  else if (boost::iequals(name, "outlet"))
    return btOutlet;
  else if (boost::iequals(name, "inlet"))
    return btInlet;
  else if (boost::iequals(name, "fixed"))
    return btFixed;
  else if (boost::iequals(name, "RigidWall"))
    return btRigidWall;
  else if (boost::iequals(name, "Sphere"))
    return btSphere;
  else if (boost::iequals(name, "SupersonicOutlet"))
    return btSupersonicOutlet;
  else
    return btUnknown;
}

/// Check if certain cell is the ghost of this one
bool BoundaryCondition::cellIsGhost(GridCell * c)
{
  if (((normal == 0) && (side == 0) && (c->get_x() < begin)) ||
     ((normal == 0) && (side == 1) && (c->get_x() > end)) || 
     ((normal == 1) && (side == 0) && (c->get_y() < begin)) ||
     ((normal == 1) && (side == 1) && (c->get_y() > end)) || 
     ((normal == 2) && (side == 0) && (c->get_z() < begin)) ||
     ((normal == 2) && (side == 1) && (c->get_z() > end)))
    return true;
  else
    return false;
}

/** used by Grid::imposeBoundaryConditions() */
void BoundaryCondition::imposeOnCell(GridCell * c, SlurmDouble t) 
{
  if (cellIsGhost(c))
    c->copyFromAdv(QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNETIC);
};

/** used by Grid::computeArtificialViscosity() */
void BoundaryCondition::imposeOnCell(GridCell * c, unsigned long quants, SlurmDouble t) 
{
  if (cellIsGhost(c))
    c->copyFromAdv(quants);
};

/** Copy from advNode */
void BoundaryCondition::imposeOnNode(GridNode * n, SlurmDouble t)
{
  n->copyFromAdv(QUANT_VELOCITY | QUANT_MASS | QUANT_MAGNETIC | QUANT_WEIGHT | QUANT_VOL | QUANT_CURRENT);
}

/** Copy from advNode */
void BoundaryCondition::imposeOnNode(GridNode * n, unsigned long quants, SlurmDouble t)
{
  n->copyFromAdv(quants);
}

/** 
  Set A according to PMC (Perfect Magnetic Conductor) condition on cell's corners.
  In PMC, the tangential magnetic field on the wall is zero, while the normal B component is unchanged (openly stretches through the wall).

  Normal A on boundary nodes == 0.
  Normal A on ghost nodes = - their real counterparts.
  Tangential A on boundary nodes doesn't matter.
  Tangential A on ghost nodes = their real counterparts.
*/
void BoundaryCondition::setPerfectMagneticConductor(GridNode * n)
{  
  // Normal - opposite sign, tangential - same.
  if (n->get_role() == gerGhost)
  {
    // Real node, mirror of this one across the boundary
    GridNode * rn = n->get_FaceNeighbor(normal, !side)->get_FaceNeighbor(normal, !side);

    SlDoubleVector A = rn->get_A();
    A[normal] = - A[normal];

    n->set_A(A);
  }
  else if (n->get_role() == gerBoundary)
  {
    n->set_A(normal, 0);
  }
}

/**   
  Reverse the normal velocity, and kick the particle back into domain.

  Advance particle's position in one dimension (X, Y, or Z).
  Returns false if the particle should be deleted.
  new_position - the precomputed new coordinate of a particle.
  pposition - particle's current coordinate.
  pvelocity - particle's velocity (is reversed when reflected).

  Same reflective condition should be also for the outlet?
*/
inline bool BoundaryCondition::reflectParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity)
{
  if (((side == 1) && (new_position[normal] > end)) || ((side == 0) && (new_position[normal] < end)))
  {
    new_position[normal] = 2 * end - new_position[normal];
    new_velocity[normal] = -new_velocity[normal];
  }
  return true;
}

/**   
  Transmit a particle through an open wall.

  Advance particle's position in one dimension (X, Y, or Z).
  Returns false if the particle should be deleted.
  new_position - the precomputed new coordinate of a particle.
  pposition - particle's current coordinate.
  pvelocity - particle's velocity (is reversed when reflected).

  By default, delete the particle if it moves out of the domain and return false.
*/
inline bool BoundaryCondition::transmitParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity)
{
  if (((side == 1) && (new_position[normal] >= end)) || ((side == 0) && (new_position[normal] < end)))
    return false;
  return true;
}

/**   
  In periodic BC, flip the particle's position around the domain.

  Advance particle's position in one dimension (X, Y, or Z).
  new_position - the precomputed new coordinate of a particle.
  pposition - particle's current coordinate.
  pvelocity - particle's velocity (is reversed when reflected).

  returns false if the particle should be deleted.
*/
inline bool BoundaryCondition::flipParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity)
{
  // Note, on the right side it is >=, while on the left side it is <. This is correct.
  if ((side == 1) && (new_position[normal] >= end))
    new_position[normal] = fmod(new_position[normal], end);
  else if ((side == 0) && (new_position[normal] < end))
    new_position[normal] = initial->get_L(normal) - fmod(abs(new_position[normal]), initial->get_L(normal));
  return true;
}

// Periodic

/** Copy from advNode */
void BoundaryConditionPeriodic::imposeOnNode(GridNode * n, SlurmDouble t)
{
  n->copyFromAdv(QUANT_VELOCITY | QUANT_MASS | QUANT_MAGNETIC | QUANT_WEIGHT | QUANT_VOL | QUANT_CURRENT);
}

/** Ensure the new velocities are OK on ghost nodes */
void BoundaryConditionPeriodic::adjustBoundaryNode(GridNode * n, SlurmDouble t)
{
  n->copyFromAdv(QUANT_VELOCITY);
}

// reflective

/** 
  In ghost, set the same values as in the adjacent cell from the real domain
  QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ | QUANT_PMAG;
*/
void BoundaryConditionReflective::imposeOnCell(GridCell * c, SlurmDouble t) 
{
  //TODO: Shall I copy weight here? How to deal with magnetic field?
  if (cellIsGhost(c))
  {
    //c->copyFromAdv(QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNETIC);
    c->copyFrom(c->get_FaceNeighbor(normal, !side), QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNETIC);
  }
}

/** 
  Sets reflective conditions on the specified node.
  In our concept of reflective boundaries, normal velocity is zero. The tangential is doubled.
  The contribution of particles to other quantities (e.g. mass) is doubled (sort of "ghost particle" contribution).
*/
void BoundaryConditionReflective::imposeOnNode(GridNode * n, SlurmDouble t)
{
  for (SlurmInt i = 0; i < 3; i++)
    if (i == normal)
      n->set_u(normal, 0);
    else
      n->set_u(i, 2 * n->get_u(i));

  n->set_m(2 * n->get_m());
}

/**
  This method is called once grid nodes are advanced to impose additional restrictions on BCs.
  Set normal component of velocity on the node to zero.
*/
void BoundaryConditionReflective::adjustBoundaryNode(GridNode * n, SlurmDouble t)
{
  n->set_u(normal, 0);
  n->set_du(normal, 0);
}

// Inlet

BoundaryConditionInlet::BoundaryConditionInlet(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : BoundaryCondition(config, bt, side, normal, begin, end, initial)
{ 
  u0 = config->read<SlurmDouble>("u0", 1.0);
}

/** 
  Constant velocity?
*/
void BoundaryConditionInlet::imposeOnNode(GridNode * n, SlurmDouble t)
{
  n->set_vol(n->get_dx() * n->get_dy() * n->get_dz());
  initial->initNode(n, t);
}

/** 
  Here, we should also initialize the boundary layer of cells?
*/
void BoundaryConditionInlet::imposeOnCell(GridCell * c, SlurmDouble t) 
{
  if (cellIsGhost(c))
  {
    c->set_vol(c->get_dx() * c->get_dy() * c->get_dz());
    initial->initCell(c);
    GridCell * boundary_cell = c->get_FaceNeighbor(normal, !side);
    boundary_cell->set_vol(boundary_cell->get_dx() * boundary_cell->get_dy() * boundary_cell->get_dz());
    initial->initCell(boundary_cell);
  }
}

/**
  This method is called once grid nodes are advanced to impose additional restrictions on BCs.
  Set constant velocity on the node. Set zero velocity increment.
*/
void BoundaryConditionInlet::adjustBoundaryNode(GridNode * n, SlurmDouble t)
{
  n->set_u(0, 0, 0);
  n->set_u(normal, u0);
  n->set_du(0, 0, 0);
}

// Outlet

BoundaryConditionOutlet::BoundaryConditionOutlet(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : BoundaryCondition(config, bt, side, normal, begin, end, initial)
{ 
  u0 = config->read<SlurmDouble>("u0", 0.1);
}

/** 
  Constant velocity?
*/
void BoundaryConditionOutlet::imposeOnNode(GridNode * n, SlurmDouble t)
{
  // This way, vector potential is only projected from particles to boundary nodes, and they are not fixed.
  if (n->get_role() == gerBoundary)
  {
    /*
    // This works with IO and FR initial condition.    
    n->copyFrom(n->get_FaceNeighbor(normal, !side), QUANT_VELOCITY | QUANT_MASS);
    n->set_u(0, 0, 0);
    n->set_u(normal, u0);
    */
    
    // This works with IO and SW initial conditions. Does not work with FR!
    n->set_vol(n->get_dx() * n->get_dy() * n->get_dz());
    n->copyFrom(n->get_FaceNeighbor(normal, !side), QUANT_VELOCITY | QUANT_MASS);
  }
  else
  {
    // In case of solar wind, PMC makes field lines open; otherwise they close in the domain.
    setPerfectMagneticConductor(n);
  }
}

/** 
  The outlet BC should be imposed also on the boundary layer of cells because of the lack of particles in the ghost cell.
  1) Copy to the boundary cell from its face neighbor.
  2) Copy from the boundary cell to this ghost cell.
*/
void BoundaryConditionOutlet::imposeOnCell(GridCell * c, SlurmDouble t) 
{
  if (cellIsGhost(c))
  {
    // Works with IO boundary condition
    // In SW problem, leads to pressure build-up on the boundary
    // Locate the cell's boundary neighbor. Should we check if its role is gerBoundary?
    GridCell * boundary_cell = c->get_FaceNeighbor(normal, !side);
    // Copy info to the boundary cell.
    boundary_cell->copyFrom(boundary_cell->get_FaceNeighbor(normal, !side), QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNETIC);
    // Copy info from the boundary cell to this ghost cell.
    c->copyFrom(boundary_cell, QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNETIC);
  }
}

/**
  This method is called once grid nodes are advanced to impose additional restrictions on BCs.
  Set constant velocity on the node. Set zero velocity increment.
*/
void BoundaryConditionOutlet::adjustBoundaryNode(GridNode * n, SlurmDouble t)
{
  if (n->get_role() == gerBoundary)
  {
    // Works with IO and SW
    n->copyFrom(n->get_FaceNeighbor(normal, !side), QUANT_MASS);
    n->set_du(normal, 0);
  }
}

// BoundaryConditionFixed

BoundaryConditionFixed::BoundaryConditionFixed(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : BoundaryCondition(config, bt, side, normal, begin, end, initial)
{ 
  u0 = config->read<SlurmDouble>("u0", 0.0);
}

/** 
  Fixed values on a wall
*/
void BoundaryConditionFixed::imposeOnNode(GridNode * n, SlurmDouble t)
{
  /*
  if (n->get_role() == gerBoundary)
  {
    n->copyFrom(n->get_FaceNeighbor(normal, !side), QUANT_VELOCITY | QUANT_MASS);
    n->set_u(0, 0, 0);
  }
  */
  n->set_vol(n->get_dx() * n->get_dy() * n->get_dz());
  initial->initNode(n);
}

/** Dirichlet? */
void BoundaryConditionFixed::imposeOnCell(GridCell * c, SlurmDouble t) 
{
  if (cellIsGhost(c))
  {
    //c->copyFrom(c->get_FaceNeighbor(normal, !side), QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNETIC);
    c->set_vol(c->get_dx() * c->get_dy() * c->get_dz());
    initial->initCell(c);    
  }
}

/**
  This method is called once grid nodes are advanced to impose additional restrictions on BCs.
  Set constant velocity on the node. Set zero velocity increment.
*/
void BoundaryConditionFixed::adjustBoundaryNode(GridNode * n, SlurmDouble t)
{
  n->set_u(normal, u0);
  n->set_du(0, 0, 0);
}


// BoundaryConditionRigidWall

/** 
  Should be a no-slip wall. 
  Following the FlipMHD convention for Fan & Gibson problem, copy u from the previous layer of the main domain.
*/
void BoundaryConditionRigidWall::imposeOnNode(GridNode * n, SlurmDouble t)
{
  // This way, vector potential is only projected from particles to boundary nodes, and they are not fixed.
  if (n->get_role() == gerBoundary)
  {
    n->copyFrom(n->get_FaceNeighbor(normal, !side), QUANT_VELOCITY | QUANT_MASS);

    n->set_u(normal, 0);
  }
  else if (n->get_role() == gerGhost)
  {
    //n->copyFrom(n->get_FaceNeighbor(normal, !side), QUANT_VELOCITY | QUANT_MASS | QUANT_MAGNETIC);
  }

  //setPerfectMagneticConductor(n);
}

/** Dirichlet? */
void BoundaryConditionRigidWall::imposeOnCell(GridCell * c, SlurmDouble t) 
{
  if (cellIsGhost(c))
  {
    // This way, magnetic field can be computed inside the Grid
    c->copyFrom(c->get_FaceNeighbor(normal, !side), QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNETIC);
    c->set_B(0, 0, 0);
  }
}

/**
  This method is called once grid nodes are advanced to impose additional restrictions on BCs.
  Set constant velocity on the node. Set zero velocity increment.
*/
void BoundaryConditionRigidWall::adjustBoundaryNode(GridNode * n, SlurmDouble t)
{
  if (n->get_role() == gerGhost)
  {
  }
  else
  {
    n->set_u(0, 0, 0);
    n->set_du(0, 0, 0);
  }
}


// BoundaryConditionSphere

/** Get the center from initial? */
BoundaryConditionSphere::BoundaryConditionSphere(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : BoundaryCondition(config, bt, side, normal, begin, end, initial)
{ 
  Center.set_xyz(config->read<SlurmDouble>("x0", 0.5 * initial->get_Lx()), config->read<SlurmDouble>("y0", 0.5 * initial->get_Ly()), config->read<SlurmDouble>("z0", 0.5 * initial->get_Lz()));
  R = config->read<SlurmDouble>("R0");
  messageline("\nSphere radius = ", R);
  message("Sphere center = ", Center.get_x());
  message(", ", Center.get_y());
  messageline(", ", Center.get_z());
}

/** Delete all particles that get into the sphere */
bool BoundaryConditionSphere::imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) 
{ 
  if (Center.distance(new_position) < R)
    return false;
  return true;
}

/** 
  Fixed values
*/
void BoundaryConditionSphere::imposeOnNode(GridNode * n, SlurmDouble t)
{
  n->set_vol(n->get_dx() * n->get_dy() * n->get_dz());
  initial->initNode(n);
}

/** Fixed value */
void BoundaryConditionSphere::imposeOnCell(GridCell * c, SlurmDouble t) 
{
  if (cellIsGhost(c))
  {
    c->set_vol(c->get_dx() * c->get_dy() * c->get_dz());
    initial->initCell(c);    
  }
}

/**
  This method is called once grid nodes are advanced to impose additional restrictions on BCs.
  Set constant velocity on the node. Set zero velocity increment.
*/
void BoundaryConditionSphere::adjustBoundaryNode(GridNode * n, SlurmDouble t)
{
  // We should only set the outflow speed here
  // initial->initNodeSpeed ? initial->get
  n->set_du(0, 0, 0);
}


// Outlet with PML

BoundaryConditionSupersonicOutlet::BoundaryConditionSupersonicOutlet(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : BoundaryCondition(config, bt, side, normal, begin, end, initial)
{ 
}

/** 
  This routine is only used for vector potential after it has been advanced.
*/
void BoundaryConditionSupersonicOutlet::imposeOnNode(GridNode * n, unsigned long quants, SlurmDouble t)
{
  n->set_dA(0, 0, 0);
  // This way, vector potential is only projected from particles to boundary nodes, and they are not fixed.
  if (n->get_role() == gerGhost)
  {
    // In case of solar wind, PMC makes field lines open; otherwise they close in the domain.
    setPerfectMagneticConductor(n);
    n->set_divB(0);
  }
}

/** 
  BC is imposed first on all boundary nodes, then on all ghost nodes.
*/
void BoundaryConditionSupersonicOutlet::imposeOnNode(GridNode * n, SlurmDouble t)
{
  //n->copyFromAdv(QUANT_VELOCITY | QUANT_MASS | QUANT_VOL);
  // This way, vector potential is only projected from particles to boundary nodes, and they are not fixed.
  if (n->get_role() == gerGhost)
  {
    n->copyFrom(n->get_FaceNeighbor(normal, !side), QUANT_VELOCITY | QUANT_MASS | QUANT_VOL);
    // In case of solar wind, PMC makes field lines open; otherwise they close in the domain.
    setPerfectMagneticConductor(n);
    n->set_divB(0);
  }
}

/** 
  The outlet BC should be imposed also on the boundary layer of cells because of the lack of particles in the ghost cell.
  1) Copy to the boundary cell from its face neighbor.
  2) Copy from the boundary cell to this ghost cell.
*/
void BoundaryConditionSupersonicOutlet::imposeOnCell(GridCell * c, SlurmDouble t) 
{
  if (cellIsGhost(c))
  {
    // Locate the cell's boundary neighbor. Should we check if its role is gerBoundary?
    GridCell * boundary_cell = c->get_FaceNeighbor(normal, !side);
    // Copy info to the boundary cell.
    boundary_cell->copyFrom(boundary_cell->get_FaceNeighbor(normal, !side), QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL);
    // Copy info from the boundary cell to this ghost cell. Why copy B? B must've been computed from A.
    c->copyFrom(boundary_cell, QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL);
    //c->copyFromAdv(QUANT_ENERGY | QUANT_PRESS | QUANT_RHO | QUANT_MASS | QUANT_WEIGHT | QUANT_VOL | QUANT_MAGNETIC);
  }
}

/** 
  The outlet BC should be imposed also on the boundary layer of cells because of the lack of particles in the ghost cell.
  1) Copy to the boundary cell from its face neighbor.
  2) Copy from the boundary cell to this ghost cell.
  3) Magnetic field on the ghost cell is computed from the boundary cell's field using the Perfect Magnetic Conductor.

  TODO: what to do with B field?
  TODO: what to do with current?
*/
void BoundaryConditionSupersonicOutlet::imposeOnCell(GridCell * c, unsigned long quants, SlurmDouble t) 
{
  if (cellIsGhost(c))
  {
    // Locate the cell's boundary neighbor. Should we check if its role is gerBoundary?
    GridCell * boundary_cell = c->get_FaceNeighbor(normal, !side);
    // Copy info to the boundary cell. Do not copy magnetic?
    boundary_cell->copyFrom(boundary_cell->get_FaceNeighbor(normal, !side), quants);
    // Copy info from the boundary cell to this ghost cell. 
    c->copyFrom(boundary_cell, quants); // & ~QUANT_MAGNETIC);
    //c->copyFromAdv(quants);
  }
}

/**
  This method is called once grid nodes are advanced to impose additional restrictions on BCs.
  Set constant velocity on the node. Set zero velocity increment.
*/
void BoundaryConditionSupersonicOutlet::adjustBoundaryNode(GridNode * n, SlurmDouble t)
{
  if (n->get_role() == gerBoundary)
  {
    n->copyFrom(n->get_FaceNeighbor(normal, !side), QUANT_VELOCITY);
    //n->copyFromAdv(QUANT_VELOCITY);
    //n->set_du(0, 0, 0);
  }
}
