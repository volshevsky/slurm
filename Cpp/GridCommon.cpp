/** 
  Basic functionality of the grids/geometries of the Slurm

*/
#include "GridCommon.h"

using namespace std;

/** Compute distance between two points */
SlurmDouble computeDistance(GridPoint* A, GridPoint* B)
{
  return pow(pow(A->get_x() - B->get_x(), 2) + pow(A->get_y() - B->get_y(), 2) + pow(A->get_z() - B->get_z(), 2), 0.5);
}

/** Difference between two vectors represented by GridPoint objects */
GridPoint * pointDifference(GridPoint* A, GridPoint* B)
{
  GridPoint* result = new GridPoint();
  result->set_xyz(A->get_x() - B->get_x(), A->get_y() - B->get_y(), A->get_z() - B->get_z());
  return result;
}

/** Cross-product of two vectors represented by GridPoint objects */
GridPoint * pointCrossProduct(GridPoint* A, GridPoint* B)
{
  return new GridPoint(A->get_y() * B->get_z() - A->get_z() * B->get_y(), A->get_z() * B->get_x() - A->get_x() * B->get_z(), A->get_x() * B->get_y() - A->get_y() * B->get_x());
}

/** Cross-product of two 3D vectors given their coordinates */
void crossProduct(SlurmDouble x1, SlurmDouble y1, SlurmDouble z1, SlurmDouble x2, SlurmDouble y2, SlurmDouble z2, SlurmDouble& x, SlurmDouble& y, SlurmDouble& z)
{
  x = y1 * z2 - z1 * y2;
  y = z1 * x2 - x1 * z2;
  z = x1 * y2 - y1 * x2;
}

/** Length of a vector represented by a GridPoint object */
SlurmDouble vectorLength(GridPoint* p)
{
  return pow(pow(p->get_x(), 2) + pow(p->get_y(), 2) + pow(p->get_z(), 2), 0.5);
}

// Grid point

/** 
  Distance from a grid point to a line represented by two other grid points 

  distance = |(r - A1) x (r - A2)| / |A1 - A2|

  From
  http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html

*/
SlurmDouble GridPoint::distanceToLine(SlDoubleVector A1, SlDoubleVector A2)
{
  // Vector X - A1
  SlDoubleVector d1 = {xyz[0] - A1[0], xyz[1] - A1[1], xyz[2] - A1[2]};
  // Vector X - A2
  SlDoubleVector d2 = {xyz[0] - A2[0], xyz[1] - A2[1], xyz[2] - A2[2]};
  // Absolute value of the cross-product of the above divided by the distance between A1 and A2
  return pow(pow(d1[1]*d2[2] - d1[2]*d2[1], 2) + pow(d1[2]*d2[0] - d1[0]*d2[2], 2) + pow(d1[0]*d2[1] - d1[1]*d2[0], 2), 0.5) / pow(pow(A1[0]-A2[0], 2) + pow(A1[1]-A2[1], 2) + pow(A1[2]-A2[2], 2), 0.5);
}

/** 
  Rotate the coordinate system around the specified axis
*/
GridPoint * GridPoint::rotate_coords(SlurmInt normal, SlurmDouble theta)
{
  GridPoint * pt = this->clone();
  SlurmInt i1 = (normal + 1) % 3;
  SlurmInt i2 = (i1 + 1) % 3;
  SlurmDouble st = sin(theta);
  SlurmDouble ct = cos(theta);
  pt->set_xyz(i1, xyz[i1] * ct + xyz[i2] * st);
  pt->set_xyz(i2, -xyz[i1] * st + xyz[i2] * ct);
  return pt;
}

/**
  Shift the coords right
*/
void GridPoint::shift_coords(GridPoint * center)
{
  for (SlurmInt i = 0; i < 3; i ++)
    xyz[i] = xyz[i] + center->get_xyz(i);
}

/**
  Shift the coords right
*/
void GridPoint::shift_coords_inv(GridPoint * center)
{
  for (SlurmInt i = 0; i < 3; i ++)
    xyz[i] = xyz[i] - center->get_xyz(i);
}

// Particle

/** 
  This routine is used to prepare for updating particle volume
  when MaterialPoint method is used.
*/
void Particle::update_Jac(GridCell * c, SlurmDouble dt)
{
  // Save velocity and Jacobian to temporary variables
  SlDoubleTensor Jold = Jac;

  set_J11((1. + dt * c->get_gradu11()) * Jold[0] + dt * c->get_gradu21() * Jold[3] + dt * c->get_gradu31() * Jold[6]);
  set_J12((1. + dt * c->get_gradu11()) * Jold[1] + dt * c->get_gradu21() * Jold[4] + dt * c->get_gradu31() * Jold[7]);
  set_J13((1. + dt * c->get_gradu11()) * Jold[2] + dt * c->get_gradu21() * Jold[5] + dt * c->get_gradu31() * Jold[8]);
  set_J21(dt * c->get_gradu12() * Jold[0] + (1. + dt * c->get_gradu22()) * Jold[3] + dt * c->get_gradu32() * Jold[6]);
  set_J22(dt * c->get_gradu12() * Jold[1] + (1. + dt * c->get_gradu22()) * Jold[4] + dt * c->get_gradu32() * Jold[7]);
  set_J23(dt * c->get_gradu12() * Jold[2] + (1. + dt * c->get_gradu22()) * Jold[5] + dt * c->get_gradu32() * Jold[8]);
  set_J31(dt * c->get_gradu13() * Jold[0] + dt * c->get_gradu23() * Jold[3] + (1. + dt * c->get_gradu33()) * Jold[6]);
  set_J32(dt * c->get_gradu13() * Jold[1] + dt * c->get_gradu23() * Jold[4] + (1. + dt * c->get_gradu33()) * Jold[7]);
  set_J33(dt * c->get_gradu13() * Jold[2] + dt * c->get_gradu23() * Jold[5] + (1. + dt * c->get_gradu33()) * Jold[8]);
}

// Grid element

/**
  Computes 3 components of the gradient of the given node/cell quantity.
  The gradient is computed as the average over the gradients taken along the corresponding ridges of the cube formed by vertices where q000, q100, ... are given.
  Operation is the same for nodes and cells.
  dxInv = 1/dx
*/
void GridElement::DirectionalDerivQuantity(SlurmDouble q000, SlurmDouble q100, SlurmDouble q010, SlurmDouble q110, SlurmDouble q001, SlurmDouble q101, SlurmDouble q011, SlurmDouble q111, SlurmDouble& derivx, SlurmDouble& derivy, SlurmDouble& derivz)
{
  derivx = 0.25 * dxyzInv[0] * (q100 - q000 + q110 - q010 + q101 - q001 + q111 - q011); 
#ifdef IGNORE_Y
  derivy = 0.;
#else
  derivy = 0.25 * dxyzInv[1] * (q010 - q000 + q110 - q100 + q011 - q001 + q111 - q101);   
#endif
#ifdef IGNORE_Z
  derivz = 0;
#else
  derivz = 0.25 * dxyzInv[2] * (q001 - q000 + q101 - q100 + q011 - q010 + q111 - q110);   
#endif
}

/** Set dx, dy, dz & inverted values */
void GridElement::set_dxyz(SlurmDouble _vx, SlurmDouble _vy, SlurmDouble _vz) 
{
  dxyz[0] = _vx;
  dxyz[1] = _vy;
  dxyz[2] = _vz;
  for (SlurmInt i = 0; i < 3; i++) 
  {
    if (dxyz[i] != 0)
      dxyzInv[i] = 1./dxyz[i];
    else
      dxyzInv[i] = 0;
  }
}

// GridCell

/** 
  Average cell's neighbor nodes.
  This is normally called after BCs have been imposed, therefore no need for 'logical' neighbors here.
*/
void GridCell::averageNodes(unsigned long quants, GridNode* avgn)
{  
  if (((quants & QUANT_VX) == QUANT_VX) || ((quants & QUANT_VY) == QUANT_VY) || ((quants & QUANT_VZ) == QUANT_VZ))
  {
    for (int i = 0; i < 3; i++) 
    {
      SlurmDouble v = 0.;
      for (int j = 0; j < 8; j++) v += nn[j]->get_u(i);
      avgn->set_u(i, 0.125 * v);
    }
  }
  if (((quants & QUANT_MAGNX) == QUANT_MAGNX) || ((quants & QUANT_MAGNY) == QUANT_MAGNY) || ((quants & QUANT_MAGNZ) == QUANT_MAGNZ) || ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC))
  {
    for (int i = 0; i < 3; i++) 
    {
      SlurmDouble v = 0.;
      for (int j = 0; j < 8; j++) v += nn[j]->get_A(i);
      avgn->set_A(i, 0.125 * v);
    }
  }
  if ((quants & QUANT_CURRENT) == QUANT_CURRENT)
  {
    for (int i = 0; i < 3; i++) 
    {
      SlurmDouble v = 0.;
      for (int j = 0; j < 8; j++) v += nn[j]->get_J(i);
      avgn->set_J(i, 0.125 * v);
    }
  }
}

/** 
  Cell-centered gradient of a node quantity ("directional derivative"). 
  Is computed by averaging the differences of the 8 neighbor nodes.
  Returns via dirdernx, dirderny, dirdernz.
  dirdernx and dirderny MUST BE initialized outside of this method!

  This method is normally called after BCs have been imposed, therefore no need for 'logical' neughbors here.
  TODO: make dirders a vector.
*/
void GridCell::DirectionalDeriv(unsigned long quants, GridNode* dirdernx, GridNode* dirderny, GridNode* dirdernz)
{
  //GridNode *n000, *n100, *n010, *n110, *n001, *n101, *n011, *n111;
  if ((quants & QUANT_VELOCITY) == QUANT_VELOCITY)
  {
    for (int i = 0; i < 3; i++)
      DirectionalDerivQuantity(nn[0]->get_u(i), nn[1]->get_u(i), nn[2]->get_u(i), nn[3]->get_u(i), nn[4]->get_u(i), nn[5]->get_u(i), nn[6]->get_u(i), nn[7]->get_u(i), dirdernx->fetch_u(i), dirderny->fetch_u(i), dirdernz->fetch_u(i));
  }
  // Electromagnetic vector potential
  if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC)
  {
    for (int i = 0; i < 3; i++)
      DirectionalDerivQuantity(nn[0]->get_A(i), nn[1]->get_A(i), nn[2]->get_A(i), nn[3]->get_A(i), nn[4]->get_A(i), nn[5]->get_A(i), nn[6]->get_A(i), nn[7]->get_A(i), dirdernx->fetch_A(i), dirderny->fetch_A(i), dirdernz->fetch_A(i));
  }
}

/** 
  Returns this cell's kinetic energy using velocities averaged over neighbor nodes.
*/
SlurmDouble GridCell::get_KineticEnergy()
{
  // Average velocities over the geometrical neighbor nodes
  SlurmDouble un[3] = {0, 0, 0};
  for (SlurmInt i = 0; i < 8; i++) 
  {
    un[0] += nn[i]->get_ux();
    un[1] += nn[i]->get_uy();
    un[2] += nn[i]->get_uz();
  }
  for (SlurmInt i = 0; i < 3; i++) 
    un[i] *= 0.125;
  return 0.5 * mass * (pow(un[0], 2) + pow(un[1], 2) + pow(un[2], 2));
}

/** Assigns the value to the specified quants */
void GridCell::fill_quants(SlurmDouble value, unsigned long quants)
{
  if ((quants & QUANT_RHO) == QUANT_RHO) 
    rho = value;
  if ((quants & QUANT_ENERGY) == QUANT_ENERGY) 
    e = value;
  if ((quants & QUANT_MASS) == QUANT_MASS) 
    mass = value;
  if ((quants & QUANT_VOL) == QUANT_VOL) 
    vol = value;
  // Number of particles in the cell
  if ((quants & QUANT_WEIGHT) == QUANT_WEIGHT) 
    weight = value;
}

/** Fills with zeros the quantities needed by the explicit solver */
void GridCell::fill_zeros()
{
  e = 0.;
  phi = 0.;
  mass = 0.;
  vol = 0.;
  // Number of particles in the cell
  weight = 0.;  
}

/** 
  Set the gradient using computed (outside of this method) cell-centered directional derivatives.
  gradu_ij = du_i/dx_j
*/
void GridCell::set_gradu(GridNode * gradx, GridNode * grady, GridNode * gradz) 
{
  set_gradu11(gradx->get_ux());
  set_gradu12(grady->get_ux());
  set_gradu13(gradz->get_ux());
  set_gradu21(gradx->get_uy());
  set_gradu22(grady->get_uy());
  set_gradu23(gradz->get_uy());
  set_gradu31(gradx->get_uz());
  set_gradu32(grady->get_uz());
  set_gradu33(gradz->get_uz());
}

/** 
  Compute the full stress tensor, including viscous and Maxwell stresses.
    mu - shear viscosity coefficient
    mu2 = mu_bulk - 2/3*mu, so it is not exactly the bulk viscosity coefficient as defined by Kundu & Kohen.
  
    tau_ij = 
  
*/
void GridCell::set_stress(SlurmDouble mu, SlurmDouble mu2)
{
  // Divergence of velocity
  SlurmDouble divu = get_divu();

  // First 3 elements are tau_12, tau_13, tau_23
  stress[0] = mu * (get_gradu12() + get_gradu21()) + B[0] * B[1];
  stress[1] = mu * (get_gradu13() + get_gradu31()) + B[0] * B[2];
  stress[2] = mu * (get_gradu23() + get_gradu32()) + B[1] * B[2];
  
  // Full pressure
  SlurmDouble p_full = - p - viscosity + mu2 * divu - 0.5 * (B[0]*B[0] + B[1]*B[1] + B[2]*B[2]);
  // Last three elements are tau_11, tau_22, tau_23
  stress[3] = 2 * mu * get_gradu11() + B[0]*B[0] + p_full;
  stress[4] = 2 * mu * get_gradu22() + B[1]*B[1] + p_full;
  stress[5] = 2 * mu * get_gradu33() + B[2]*B[2] + p_full;
}

/** 
  Set the gradient using computed (outside of this method) cell-centered directional derivatives.
  gradA_ij = dA_i/dx_j
*/
void GridCell::set_gradA(GridNode * gradx, GridNode * grady, GridNode * gradz) 
{
  set_gradA11(gradx->get_Ax());
  set_gradA12(grady->get_Ax());
  set_gradA13(gradz->get_Ax());
  set_gradA21(gradx->get_Ay());
  set_gradA22(grady->get_Ay());
  set_gradA23(gradz->get_Ay());
  set_gradA31(gradx->get_Az());
  set_gradA32(grady->get_Az());
  set_gradA33(gradz->get_Az());
}

/** 
  I AM NOT SURE THIS ONE WORKS! There must be problems with ghost nodes, etc.. Use setNeighborCells() instead.

  Initialize neighbor cells used by quadratic interpolation.
  Should look for all neighbor nodes (they MUST be filled in!).

  Index of the neighbor in the 27-element array:
    n = 9 * k + 3 * j + i
  Where i, j, k vary from 0 to 2 (from the lower left front corner, to upper right far corner of the neighbor's 3x3x3 cube).

  We are using, say, n000 to deduce 6 neighbors. If there are no ghost nodes, some neighbors will be missing!
  Should the Grid set the neighbor cells?
*/
void GridCell::initNeighborCells()
{
  // Bottom level of cells
  cnc[ind333(0, 0, 0)] = get_n000()->get_c000();  // 000 -> 0
  cnc[ind333(1, 0, 0)] = get_n000()->get_c100();  // 100 -> 1
  cnc[ind333(2, 0, 0)] = get_n100()->get_c100();  // 200 -> 2
  cnc[ind333(0, 1, 0)] = get_n000()->get_c010();  // 010 -> 3
  cnc[ind333(1, 1, 0)] = get_n000()->get_c110();  // 110 -> 4 - Just below this cell
  cnc[ind333(2, 1, 0)] = get_n100()->get_c110();  // 210 -> 5
  cnc[ind333(0, 2, 0)] = get_n010()->get_c010();  // 020 -> 6
  cnc[ind333(1, 2, 0)] = get_n010()->get_c110();  // 120 -> 7
  cnc[ind333(2, 2, 0)] = get_n110()->get_c110();  // 220 -> 8

  // Middle level
  cnc[ind333(0, 0, 1)] = get_n000()->get_c001();  // 001 -> 9
  cnc[ind333(1, 0, 1)] = get_n000()->get_c101();  // 101 -> 10
  cnc[ind333(2, 0, 1)] = get_n100()->get_c101();  // 201 -> 11
  cnc[ind333(0, 1, 1)] = get_n000()->get_c011();  // 011 -> 12
  cnc[ind333(1, 1, 1)] = this;                    // 111 -> 13 == this cell
  cnc[ind333(2, 1, 1)] = get_n100()->get_c111();  // 211 -> 14
  cnc[ind333(0, 2, 1)] = get_n010()->get_c011();  // 021 -> 15
  cnc[ind333(1, 2, 1)] = get_n010()->get_c111();  // 121 -> 16
  cnc[ind333(2, 2, 1)] = get_n110()->get_c111();  // 221 -> 17

  // Top level
  cnc[ind333(0, 0, 2)] = get_n001()->get_c001();  // 002 -> 18
  cnc[ind333(1, 0, 2)] = get_n001()->get_c101();  // 102 -> 19
  cnc[ind333(2, 0, 2)] = get_n101()->get_c101();  // 202 -> 20
  cnc[ind333(0, 1, 2)] = get_n001()->get_c011();  // 012 -> 21
  cnc[ind333(1, 1, 2)] = get_n001()->get_c111();  // 112 -> 22 - Just above this cell
  cnc[ind333(2, 1, 2)] = get_n101()->get_c111();  // 212 -> 23   ----- need ghost nodes for this one, for instance?
  cnc[ind333(0, 2, 2)] = get_n011()->get_c011();  // 022 -> 24
  cnc[ind333(1, 2, 2)] = get_n011()->get_c111();  // 122 -> 25
  cnc[ind333(2, 2, 2)] = get_n111()->get_c111();  // 222 -> 26

  for (int i = 0; i < 27; i++) 
    if (cnc[i]) 
      cnc[i] = cnc[i]->get_advCell();
}

/** 
  Assigns cell's neighbor cells from the given 3x3x3 array of cells
*/
void GridCell::setNeighborCells(CellRefArray nbrs)
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 3; k++)
        if (nbrs[i][j][k] && (nbrs[i][j][k]->get_advCell()))
          cnc[ind333(i, j, k)] = nbrs[i][j][k]->get_advCell();
        else
          cnc[ind333(i, j, k)] = NULL;
}

/** 
  Far-side piece of the Quadratic interpolation. 

  Weights:
  
    1/2x^2 + 3/2x + 9/8  if -3/2 <= x < -1/2
    -x^2 + 3/4           if -1/2 <= x < 1/2
    1/2x^2 - 3/2x + 9/8  if 1/2 <= x < 3/2
  
  adj_cells - already allocated array of GridCell* pointers
  weights - array of the same size with weights

  NOTE, assumes the neighbor cells have the same extent dx, dy, dz. This is necessary to correctly interpolate on ghost cells.
 
*/
SlurmDouble GridCell::outerWeight(SlurmDouble x1, SlurmDouble x2, SlurmDouble dxInv)
{
  SlurmDouble d = fabs((x1 - x2) * dxInv);
  return(0.5 * d * d - 1.5 * d + 1.125);
};

/** 
  Inner piece of the Quadratic interpolation. 

  Weights:
  
    1/2x^2 + 3/2x + 9/8  if -3/2 <= x < -1/2
    -x^2 + 3/4           if -1/2 <= x < 1/2
    1/2x^2 - 3/2x + 9/8  if 1/2 <= x < 3/2
  
  adj_cells - already allocated array of GridCell* pointers
  weights - array of the same size with weights

  NOTE, assumes the neighbor cells have the same extent dx, dy, dz. This is necessary to correctly interpolate on ghost cells.
 
*/
SlurmDouble GridCell::innerWeight(SlurmDouble x1, SlurmDouble x2, SlurmDouble dxInv)
{
  SlurmDouble d = fabs((x1 - x2) * dxInv);
  return(0.75 - d * d);
};

/** 
  Compute 9 weights for quadratic interpolation
  Parameters:
    p - point onto which to interpolate. Assumes that it is inside this cell!
    w - an array of 9 weights. [w0x, w1x, w2x, w0y, w1y, w2y, w0z, w1z, w2z]
*/
void GridCell::computeWeights(GridPoint * p, SlurmDouble * w)
{
  // Cycle over 3 coordinates
  for (SlurmInt i = 0; i < 3; i++)
  {
    SlurmDouble px = p->get_xyz(i);
    w[i*3] = outerWeight(px, xyz[i] - dxyz[i], dxyzInv[i]);
    w[i*3+1] = innerWeight(px, xyz[i], dxyzInv[i]);
    w[i*3+2] = outerWeight(px, xyz[i] + dxyz[i], dxyzInv[i]);
  }
}

/** 
  Quadratic interpolation from this cell and its _geometrical_ neighbors onto given particle.
  To find the cell where this particle lies in, do grid->getClosestCell().

  Cell -> particle projection uses geometrical neighbors to interpolate only de and velocity (deformation) gradient.
  Ghost cells must have zero de and gradu!

  Parameters:
      p - point (particle) on which to project the cell's (and neighbors) data.
 
  The caller SHOULD delete res.
*/
GridCell* GridCell::interpolateToParticle(GridPoint * p)
{
  // Compute 9 weights
  SlurmDouble weights[9];
  computeWeights(p, weights);
  // Result
  GridCell * res = new GridCell();

  // Cycle over 27 logical neighbor cells
  for (SlurmInt i = 0; i < 3; i++)
  for (SlurmInt j = 0; j < 3; j++)
  for (SlurmInt k = 0; k < 3; k++)
  {
    SlurmDouble w = weights[i]*weights[3+j]*weights[6+k];
    GridCell * cc = cnc[ind333(i, j, k)];

    if (cc)
    {
      res->inc_de(cc->get_de() * w);
      res->inc_gradu(cc, w);
      res->inc_phi(cc->get_dphi() * w);
    }
  }

  return res;
}

/** 
  Quadratic interpolation from this cell and its _geometrical_ neighbors onto given particle.
  To find the cell where this particle lies in, do grid->getClosestCell().

  Cell -> particle projection uses geometrical neighbors to interpolate only de and velocity (deformation) gradient.
  Ghost cells must have zero de and gradu!

  Parameters:
      p - point (particle) on which to project the cell's (and neighbors) data.
 
  The caller SHOULD delete res.
*/
GridCell* GridCell::interpolateToParticle(GridPoint * p, unsigned long quants)
{
  // Compute 9 weights
  SlurmDouble weights[9];
  computeWeights(p, weights);
  // Result
  GridCell * res = new GridCell();

  // Cycle over 27 logical neighbor cells
  for (SlurmInt i = 0; i < 3; i++)
  for (SlurmInt j = 0; j < 3; j++)
  for (SlurmInt k = 0; k < 3; k++)
  {
    SlurmDouble w = weights[i]*weights[3+j]*weights[6+k];
    GridCell * cc = cnc[ind333(i, j, k)];

    if (cc)
    {
      if ((quants & QUANT_RHO) == QUANT_RHO) 
        res->inc_rho(cc->get_rho() * w);
      if ((quants & QUANT_ENERGY) == QUANT_ENERGY) 
        res->inc_e(cc->get_e() * w);
      // Quantities needed during particles advancement
      if ((quants & QUANT_INTERNAL) == QUANT_INTERNAL) 
      {
        res->inc_de(cc->get_de() * w);
        res->inc_gradu(cc, w);
      }
    }
  }

  return res;
}

/**
  Gather necessary quantities from particle into 27 logical neighbors.
  Needs energy, mass, volume, weight.

  Projection particles->cells uses "logical" neighbors, i.e., advCell.
 
*/
void GridCell::advectFromParticle(Particle * p)
{
  // Compute 9 weights
  SlurmDouble weights[9];
  computeWeights(p, weights);
  GridCell * cc;

  // Cycle over 27 logical neighbor cells
  for (SlurmInt i = 0; i < 3; i++)
  for (SlurmInt j = 0; j < 3; j++)
  for (SlurmInt k = 0; k < 3; k++)
  {
    cc = cnc[ind333(i, j, k)];
    if (cc)
    {
      SlurmDouble w = weights[i]*weights[3+j]*weights[6+k];
      cc->lock();
      cc->inc_e(p->get_e() * w);
      cc->inc_m(p->get_m() * w);
      cc->inc_vol(p->get_vol() * w);
      cc->inc_phi(p->get_vol() * p->get_phi() * w);
      cc->inc_weight(w);
      cc->unlock();      
    }
  }  
}

// GridNode


/** 
  Node-centered gradient of a cell quantity ("directional derivative").
  Computed over the 8 neighbor cells.
  It uses the 'logical' neighbors of the node, those which include BCs connectivity.

  dirdercx, dirdercy, dirdercz MUST BE initialized prior to calling this function!

  It is more complicated than GridCell::DirectionalDeriv() because the number of cells is smaller than the number of nodes.
  In addition, derivatives of (Bx**2 + By**2), (By**2 + Bz**2), (Bx**2 + Bz**2) are computed when QUANT_PMAG bit is set. These are stored in B2, correspondingly.

  This derivative is used when the grid is advanced, i.e., after boundary conditions were imposed.
*/
void GridNode::DirectionalDeriv(unsigned long quants, GridCell* dirdercx, GridCell* dirdercy, GridCell* dirdercz)
{
  // Use logical neighbors to compute derivatives.
  if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC)
  {
    for (int i = 0; i < 3; i++)
      DirectionalDerivQuantity(ac[0]->get_B(i), ac[1]->get_B(i), ac[2]->get_B(i), ac[3]->get_B(i), ac[4]->get_B(i), ac[5]->get_B(i), ac[6]->get_B(i), ac[7]->get_B(i), dirdercx->fetch_B(i), dirdercy->fetch_B(i), dirdercz->fetch_B(i));
  }
  if ((quants & QUANT_PRESS) == QUANT_PRESS) 
  {
    DirectionalDerivQuantity(ac[0]->get_p(), ac[1]->get_p(), ac[2]->get_p(), ac[3]->get_p(), ac[4]->get_p(), ac[5]->get_p(), ac[6]->get_p(), ac[7]->get_p(), dirdercx->fetch_p(), dirdercy->fetch_p(), dirdercz->fetch_p());
  }
  if ((quants & QUANT_VISCOSITY) == QUANT_VISCOSITY)
  {
    DirectionalDerivQuantity(ac[0]->get_visc(), ac[1]->get_visc(), ac[2]->get_visc(), ac[3]->get_visc(), ac[4]->get_visc(), ac[5]->get_visc(), ac[6]->get_visc(), ac[7]->get_visc(), dirdercx->fetch_visc(), dirdercy->fetch_visc(), dirdercz->fetch_visc());
  }
  // A dirty trick to have the derivatives of (Bx**2 + By**2), (By**2 + Bz**2), (Bx**2 + Bz**2) needed by the explicit scheme.
  if ((quants & QUANT_PMAG) == QUANT_PMAG) 
  {
    for (int i = 0; i < 3; i++)
      DirectionalDerivQuantity(pow(ac[0]->get_B(i), 2), pow(ac[1]->get_B(i), 2), pow(ac[2]->get_B(i), 2), pow(ac[3]->get_B(i), 2), pow(ac[4]->get_B(i), 2), pow(ac[5]->get_B(i), 2), pow(ac[6]->get_B(i), 2), pow(ac[7]->get_B(i), 2), dirdercx->fetch_B2(i), dirdercy->fetch_B2(i), dirdercz->fetch_B2(i));
  }
  if ((quants & QUANT_SCALARPOTENTIAL) == QUANT_SCALARPOTENTIAL)
  {
    DirectionalDerivQuantity(ac[0]->get_phi(), ac[1]->get_phi(), ac[2]->get_phi(), ac[3]->get_phi(), ac[4]->get_phi(), ac[5]->get_phi(), ac[6]->get_phi(), ac[7]->get_phi(), dirdercx->fetch_phi(), dirdercy->fetch_phi(), dirdercz->fetch_phi());
  }
}

/** 
  Node-centered gradient of the full stress tensor.
  Computed over the 8 neighbor cells.
  It uses the 'logical' neighbors of the node, those which include BCs connectivity.

  dirdercx, dirdercy, dirdercz MUST BE initialized prior to calling this function!

  This derivative is used when the grid is advanced.
*/
void GridNode::DirectionalDerivStress(GridCell* dirdercx, GridCell* dirdercy, GridCell* dirdercz)
{
  for (int i = 0; i < 6; i++)
    DirectionalDerivQuantity(ac[0]->get_stress(i), ac[1]->get_stress(i), ac[2]->get_stress(i), ac[3]->get_stress(i), ac[4]->get_stress(i), ac[5]->get_stress(i), ac[6]->get_stress(i), ac[7]->get_stress(i), dirdercx->fetch_stress(i), dirdercy->fetch_stress(i), dirdercz->fetch_stress(i));  
}

/** 
  Average node's neighbor cells. 
  The neighbors should account for BCs, therefore 'logical' neighbors are used.
*/
void GridNode::averageCells(unsigned long quants, GridCell* avgc)
{
  if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC)
  {
    for (int i = 0; i < 3; i++) 
    {
      SlurmDouble v = 0.;
      for (int j = 0; j < 8; j++) v += ac[j]->get_B(i);
      avgc->set_B(i, 0.125 * v);
    }
  }
  if ((quants & QUANT_RHO) == QUANT_RHO)
  {
    SlurmDouble v = 0.;
    for (int j = 0; j < 8; j++) v += ac[j]->get_rho();
    avgc->set_rho(0.125 * v);
  }
  // INTERNAL == gradient of A used by Fields::advanceNodeVectorPotential()
  if ((quants & QUANT_INTERNAL) == QUANT_INTERNAL)
  {
    for (int i = 0; i < 9; i++) 
    {
      SlurmDouble v = 0.;
      for (int j = 0; j < 8; j++) v += ac[j]->get_gradA(i);
      avgc->set_gradA(i, 0.125 * v);
    }
  }
}

/** Copy the specified quantities from one node to another */
void GridNode::copyFrom(GridNode *source, unsigned long quants)
{
  if (source != this)
  {
    source->lock();
    lock();    
    if ((quants & QUANT_VELOCITY) == QUANT_VELOCITY)
      u = source->get_u();
    if ((quants & QUANT_CURRENT) == QUANT_CURRENT)
      J = source->get_J();
    if ((quants & QUANT_MASS) == QUANT_MASS) 
      set_m(source->get_m()); 
    if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC)
      A = source->get_A();
    if ((quants & QUANT_WEIGHT) == QUANT_WEIGHT) 
      set_weight(source->get_weight()); 
    if ((quants & QUANT_VOL) == QUANT_VOL) 
      set_vol(source->get_vol()); 
    source->unlock();
    unlock();
  }
}

/** 
  Check if the particle is closer to the node than any neighbor particles and update it.
  1) Check which quadrant the particle is relative to the node.
*/
void GridNode::updateNeighborParticle(Particle* p)
{
  lock();
  Particle * np;
  SlurmDouble d = distance(p);

  if ((p->get_x() >= get_x()) && (p->get_y() >= get_y()) && (p->get_z() >= get_z())) // 111
  {
    np = get_p111();
    // If node's neighbor has moved from this quadrant, substitute it
    if ((!np) || (np->get_m() < 0) || ((np->get_x() < get_x()) || (np->get_y() < get_y()) || (np->get_z() < get_z())))
      set_p111(p);
    else if (d < distance(np))
      set_p111(p);
  }
  if ((p->get_x() >= get_x()) && (p->get_y() >= get_y()) && (p->get_z() < get_z())) // 110
  {
    np = get_p110();
    // If node's neighbor has moved from this quadrant, substitute it
    if ((!np) || (np->get_m() < 0) || ((np->get_x() < get_x()) || (np->get_y() < get_y()) || (np->get_z() >= get_z())))
      set_p110(p);
    else if (d < distance(np))
      set_p110(p);
  }

  // lower right
  if ((p->get_x() >= get_x()) && (p->get_y() < get_y()) && (p->get_z() >= get_z()))  // 101
  {
    np = get_p101();
    // If node's neighbor has moved from this quadrant, substitute it
    if ((!np) || (np->get_m() < 0) || ((np->get_x() < get_x()) || (np->get_y() >= get_y()) || (np->get_z() < get_z())))
      set_p101(p);
    else if (d < distance(np))
      set_p101(p);
  }
  if ((p->get_x() >= get_x()) && (p->get_y() < get_y()) && (p->get_z() < get_z()))  // 100
  {
    np = get_p100();
    // If node's neighbor has moved from this quadrant, substitute it
    if ((!np) || (np->get_m() < 0) || ((np->get_x() < get_x()) || (np->get_y() >= get_y()) || (np->get_z() >= get_z())))
      set_p100(p);
    else if (d < distance(np))
      set_p100(p);
  }

  if ((p->get_x() < get_x()) && (p->get_y() < get_y()) && (p->get_z() >= get_z()))  // 001
  {
    np = get_p001();    
    // If node's neighbor has moved from this quadrant, substitute it
    if ((!np) || (np->get_m() < 0) || ((np->get_x() >= get_x()) || (np->get_y() >= get_y()) || (np->get_z() < get_z())))
      set_p001(p);
    else if (d < distance(np))
      set_p001(p);
  }
  if ((p->get_x() < get_x()) && (p->get_y() < get_y()) && (p->get_z() < get_z()))  // 000
  {
    np = get_p000();    
    // If node's neighbor has moved from this quadrant, substitute it
    if ((!np) || (np->get_m() < 0) || ((np->get_x() >= get_x()) || (np->get_y() >= get_y()) || (np->get_z() >= get_z())))
      set_p000(p);
    else if (d < distance(np))
      set_p000(p);
  }

  if ((p->get_x() < get_x()) && (p->get_y() >= get_y()) && (p->get_z() >= get_z()))  // 011
  {
    np = get_p011();    
    // If node's neighbor has moved from this quadrant, substitute it
    if ((!np) || (np->get_m() < 0) || ((np->get_x() >= get_x()) || (np->get_y() < get_y()) || (np->get_z() < get_z())))
      set_p011(p);
    else if (d < distance(np))
      set_p011(p);
  }
  if ((p->get_x() < get_x()) && (p->get_y() >= get_y()) && (p->get_z() < get_z()))  // 010
  {
    np = get_p010();    
    // If node's neighbor has moved from this quadrant, substitute it
    if ((!np) || (np->get_m() < 0) || ((np->get_x() >= get_x()) || (np->get_y() < get_y()) || (np->get_z() >= get_z())))
      set_p010(p);
    else if (d < distance(np))
      set_p010(p);
  }

  unlock();
}

/** Assigns the value to the specified quants */
void GridNode::fill_quants(SlurmDouble value, unsigned long quants)
{
  if ((quants & QUANT_VX) == QUANT_VX) 
    set_ux(value);
  if ((quants & QUANT_VY) == QUANT_VY) 
    set_uy(value);
  if ((quants & QUANT_VZ) == QUANT_VZ) 
    set_uz(value);
  if ((quants & QUANT_MASS) == QUANT_MASS) 
    mass = value;
  if ((quants & QUANT_VOL) == QUANT_VOL) 
    vol = value;
  if ((quants & QUANT_MAGNX) == QUANT_MAGNX) 
    set_Ax(value);
  if ((quants & QUANT_MAGNY) == QUANT_MAGNY) 
    set_Ay(value);
  if ((quants & QUANT_MAGNZ) == QUANT_MAGNZ) 
    set_Az(value);
  if ((quants & QUANT_WEIGHT) == QUANT_WEIGHT) 
    weight = value;
}

/** Fills with zeros the quantities needed by the explicit solver */
void GridNode::fill_zeros()
{
  set_u(0, 0, 0);
  mass = 0;
  vol = 0;
  weight = 0;
  set_A(0, 0, 0);
}

/** 
  Copy the specified quantities from one cell to another 
*/
void GridCell::copyFrom(GridCell *source, unsigned long quants)
{
  if (source != this)
  {
    source->lock();
    lock();
    
    if ((quants & QUANT_RHO) == QUANT_RHO) 
      set_rho(source->get_rho());
    if ((quants & QUANT_ENERGY) == QUANT_ENERGY)
    {
      set_e(source->get_e());
      set_de(source->get_de());
    }
    if ((quants & QUANT_VOL) == QUANT_VOL) 
      set_vol(source->get_vol()); 
    if ((quants & QUANT_MASS) == QUANT_MASS) 
      set_m(source->get_m()); 
    if ((quants & QUANT_PRESS) == QUANT_PRESS) 
      set_p(source->get_p()); 
    if ((quants & QUANT_WEIGHT) == QUANT_WEIGHT) 
      set_weight(source->get_weight()); 
    if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC)
    {
      B = source->get_B();
      gradA = source->get_gradA();
      phi = source->get_phi();
    }
    if ((quants & QUANT_VISCOSITY) == QUANT_VISCOSITY) 
    {
      set_visc(source->get_visc()); 
      stress = source->get_stress();
    }
    source->unlock();
    unlock();
  }
}

/** 
  Returns interpolation weights for 4 corners of a rectangle 
  Allocate memory for weights prior to calling this function!
  Note the order of the bloody points!

  4(p01) *-----* 3(p11)
         |  p  |
  1(p00) *-----* 2(p10)

*/
void GridCommon::BilinearInterpolationWeights(GridPoint *p, GridPoint *p00, GridPoint *p10, GridPoint *p11, GridPoint *p01, SlurmDouble *weights)
{
  SlurmDouble wx = (p->get_x() - p00->get_x()) / (p10->get_x() - p00->get_x());
  SlurmDouble wy = (p->get_y() - p00->get_y()) / (p01->get_y() - p00->get_y());
  
  weights[0] = (1. - wx) * (1. - wy); // 00
  weights[1] = wx * (1. - wy); // 10
  weights[2] = wx * wy; // 11
  weights[3] = (1. - wx) * wy; // 01
}

/** 
  Converts coordinates from "Physical" to "Logical" space.
  The return values are basically interpolation weights along the corresponding axes (wx and wy in BilinearInterpolationWeights).
  https://www.particleincell.com/2012/quad-interpolation/

  p01(4) *--------* p11(3)
          \       |
           \  p   |
            \     |
      p00(1) *----* p10(2)

*/
void GridCommon::Physical2Logical(GridPoint *p, GridPoint *p00, GridPoint *p10, GridPoint *p11, GridPoint *p01, SlurmDouble *coords)
{
  /* AI = [[ 1.,  0.,  0.,  0.],
           [-1.,  1.,  0.,  0.],
           [-1.,  0.,  0.,  1.],
           [ 1., -1.,  1., -1.]]     
  */

  SlurmDouble a[4] = {p00->get_x(), -p00->get_x() + p10->get_x(), -p00->get_x() + p01->get_x(), p00->get_x() - p10->get_x() + p11->get_x() - p01->get_x()}; // a = AI * px
  SlurmDouble b[4] = {p00->get_y(), -p00->get_y() + p10->get_y(), -p00->get_y() + p01->get_y(), p00->get_y() - p10->get_y() + p11->get_y() - p01->get_y()}; // b = AI * py

  // quadratic equation coeffs, aa*mm^2+bb*m+cc=0
  SlurmDouble aa = a[3]*b[2] - a[2]*b[3];

  if (fabs(aa) < 1e-30) 
  {
    // Rectangular case
    coords[0] = (p->get_x() - p00->get_x()) / (p10->get_x() - p00->get_x());
    coords[1] = (p->get_y() - p00->get_y()) / (p01->get_y() - p00->get_y());
  }
  else 
  { 
    // This should be non-rectangle
    SlurmDouble bb = a[3]*b[0] - a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + p->get_x() * b[3] - p->get_y() * a[3];
    SlurmDouble cc = a[1]*b[0] - a[0]*b[1] + p->get_x() * b[1] - p->get_y() * a[1];
 
    // compute m = (-b+sqrt(b^2-4ac))/(2a)
    SlurmDouble det = sqrt(bb * bb - 4 * aa * cc);

    // Return via the pre-allocated array
    coords[1] = (-bb + det) / (2 * aa); // m
    coords[0] = (p->get_x() - a[0] - a[2] * coords[1]) / (a[1] + a[3] * coords[1]); // l
  }
}

/** 
  Computes interpolations weights for a point inside an arbitrary convex quadriliteral as described in
  https://www.particleincell.com/2012/quad-interpolation/

  Allocate memory for weights prior to calling this function!
  Note the order of the bloody points!

  p01(4) *--------* p11(3)
          \       |
           \  p   |
            \     |
      p00(1) *----* p10(2)

*/
void GridCommon::QuadriliteralInterpolationWeights(GridPoint *p, GridPoint *p00, GridPoint *p10, GridPoint *p11, GridPoint *p01, SlurmDouble *weights)
{
  // Use the already allocated weights to obtain the "logical coordinates": weights[0] == wx, weights[1] == wy
  Physical2Logical(p, p00, p10, p11, p01, weights);
  SlurmDouble wx = weights[0], wy = weights[1];

  // Now rearrange the weights properly
  weights[0] = (1. - wx) * (1. - wy);
  weights[1] = wx * (1. - wy);
  weights[2] = wx * wy;
  weights[3] = (1. - wx) * wy;
}

/** 
  Interpolation weights from irregular octahedron.
  See the trilinear.txt in our documentation.

   x1 = x(i  ,j  ,k  )  = p000
   x2 = x(i+1,j  ,k  )  = p100
   x3 = x(i  ,j+1,k  )  = p010
   x4 = x(i+1,j+1,k  )  = p110
   x5 = x(i  ,j  ,k+1)  = p001
   x6 = x(i+1,j  ,k+1)  = p101
   x7 = x(i  ,j+1,k+1)  = p011
   x8 = x(i+1,j+1,k+1)  = p111

  Parameters:
   p - point to interpolate on
   p000...p111 - data points. Must represent a convex hexahedron
   fgh - a pre-allocated 2D boost array of size [3][8]: fgh.resize(boost::extents[3][8]);
   coefs - a pre-allocated 2D boost array of size [3][3]: coefs.resize(boost::extents[3][3]);

  The pre-allocation of those arrays must speed up operations dramatically

*/
SlurmDouble * GridCommon::TrilinearIrregularInterpolationWeights(GridPoint *p, GridPoint *p000, GridPoint *p100, GridPoint *p010, GridPoint *p110, GridPoint *p001, GridPoint *p101, GridPoint *p011, GridPoint *p111, SlurmDoubleArray2D fgh, SlurmDoubleArray2D coefs)
{
  // Return value
  SlurmDouble *weights = new SlurmDouble[8];
  
  // f
  fgh[0][0] = 0.125 * (p111->get_x() + p011->get_x() + p101->get_x() + p001->get_x() + p110->get_x() + p010->get_x() + p100->get_x() + p000->get_x()) - p->get_x();
  fgh[0][1] = 0.125 * (p111->get_x() - p011->get_x() + p101->get_x() - p001->get_x() + p110->get_x() - p010->get_x() + p100->get_x() - p000->get_x());
  fgh[0][2] = 0.125 * (p111->get_x() + p011->get_x() - p101->get_x() - p001->get_x() + p110->get_x() + p010->get_x() - p100->get_x() - p000->get_x());     
  fgh[0][3] = 0.125 * (p111->get_x() + p011->get_x() + p101->get_x() + p001->get_x() - p110->get_x() - p010->get_x() - p100->get_x() - p000->get_x());     
  fgh[0][4] = 0.125 * (p111->get_x() - p011->get_x() - p101->get_x() + p001->get_x() + p110->get_x() - p010->get_x() - p100->get_x() + p000->get_x());     
  fgh[0][5] = 0.125 * (p111->get_x() - p011->get_x() + p101->get_x() - p001->get_x() - p110->get_x() + p010->get_x() - p100->get_x() + p000->get_x());     
  fgh[0][6] = 0.125 * (p111->get_x() + p011->get_x() - p101->get_x() - p001->get_x() - p110->get_x() - p010->get_x() + p100->get_x() + p000->get_x());     
  fgh[0][7] = 0.125 * (p111->get_x() - p011->get_x() - p101->get_x() + p001->get_x() - p110->get_x() + p010->get_x() + p100->get_x() - p000->get_x());     
  // g
  fgh[1][0] = 0.125 * (p111->get_y() + p011->get_y() + p101->get_y() + p001->get_y() + p110->get_y() + p010->get_y() + p100->get_y() + p000->get_y()) - p->get_y();
  fgh[1][1] = 0.125 * (p111->get_y() - p011->get_y() + p101->get_y() - p001->get_y() + p110->get_y() - p010->get_y() + p100->get_y() - p000->get_y());
  fgh[1][2] = 0.125 * (p111->get_y() + p011->get_y() - p101->get_y() - p001->get_y() + p110->get_y() + p010->get_y() - p100->get_y() - p000->get_y());     
  fgh[1][3] = 0.125 * (p111->get_y() + p011->get_y() + p101->get_y() + p001->get_y() - p110->get_y() - p010->get_y() - p100->get_y() - p000->get_y());     
  fgh[1][4] = 0.125 * (p111->get_y() - p011->get_y() - p101->get_y() + p001->get_y() + p110->get_y() - p010->get_y() - p100->get_y() + p000->get_y());     
  fgh[1][5] = 0.125 * (p111->get_y() - p011->get_y() + p101->get_y() - p001->get_y() - p110->get_y() + p010->get_y() - p100->get_y() + p000->get_y());     
  fgh[1][6] = 0.125 * (p111->get_y() + p011->get_y() - p101->get_y() - p001->get_y() - p110->get_y() - p010->get_y() + p100->get_y() + p000->get_y());     
  fgh[1][7] = 0.125 * (p111->get_y() - p011->get_y() - p101->get_y() + p001->get_y() - p110->get_y() + p010->get_y() + p100->get_y() - p000->get_y());     
  // h
  fgh[2][0] = 0.125 * (p111->get_z() + p011->get_z() + p101->get_z() + p001->get_z() + p110->get_z() + p010->get_z() + p100->get_z() + p000->get_z()) - p->get_z();
  fgh[2][1] = 0.125 * (p111->get_z() - p011->get_z() + p101->get_z() - p001->get_z() + p110->get_z() - p010->get_z() + p100->get_z() - p000->get_z());
  fgh[2][2] = 0.125 * (p111->get_z() + p011->get_z() - p101->get_z() - p001->get_z() + p110->get_z() + p010->get_z() - p100->get_z() - p000->get_z());     
  fgh[2][3] = 0.125 * (p111->get_z() + p011->get_z() + p101->get_z() + p001->get_z() - p110->get_z() - p010->get_z() - p100->get_z() - p000->get_z());     
  fgh[2][4] = 0.125 * (p111->get_z() - p011->get_z() - p101->get_z() + p001->get_z() + p110->get_z() - p010->get_z() - p100->get_z() + p000->get_z());     
  fgh[2][5] = 0.125 * (p111->get_z() - p011->get_z() + p101->get_z() - p001->get_z() - p110->get_z() + p010->get_z() - p100->get_z() + p000->get_z());     
  fgh[2][6] = 0.125 * (p111->get_z() + p011->get_z() - p101->get_z() - p001->get_z() - p110->get_z() - p010->get_z() + p100->get_z() + p000->get_z());     
  fgh[2][7] = 0.125 * (p111->get_z() - p011->get_z() - p101->get_z() + p001->get_z() - p110->get_z() + p010->get_z() + p100->get_z() - p000->get_z());     

  // Initial guess a = b = c = 0
  SlurmDouble abc[3] = {0., 0., 0.};
  SlurmDouble solution[3] = {-fgh[0][0], -fgh[1][0], -fgh[2][0]};

  /* 
    Initial guess a, b, c == 0, hence
      -f0 = f1 * da + f2 * db + f3*db
      -g0 = g1 * da + g2 * db + g3*db
      -h0 = h1 * da + h2 * db + h3*db
  */
  for (short i = 0; i < 3; ++i)
  {
    coefs[i][0] = fgh[i][1];
    coefs[i][1] = fgh[i][2];
    coefs[i][2] = fgh[i][3];
  }

  SlurmDouble delta_abc[3];
  SolveLinearSystem(coefs, solution, delta_abc, 3);
  
  // Now we start Newton iterations using our initial guess. Check first whether all three a, b, c are zero.
  if ((delta_abc[0]*delta_abc[0] + delta_abc[1]*delta_abc[1] + delta_abc[2]*delta_abc[2]) < TIISmallDOuble)
    for (SlurmInt i = 0; i < 8; ++i) weights[i] = 0.125;
  else 
  {
    // Newton's iteration number
    SlurmInt iter = 0;

    // Newton iterations
    while (((delta_abc[0] / abc[0] > TIITolerance) || (delta_abc[1] / abc[1] > TIITolerance) || (delta_abc[2] / abc[2] > TIITolerance)) && 
           (abc[0] < TIIUpperLimit) && (abc[1] < TIIUpperLimit) && (abc[2] < TIIUpperLimit) && (iter < TIIMaxIterations))
    {
      for (SlurmInt i = 0; i < 3; ++i) 
      {
        // increase a, b, c
        abc[i] += delta_abc[i];

        /* 
          Compute f, g, h
            f = f0 + f1*a + f2*b + f3*� + f4*a*b + f5*a*� + f6*b*� + f7*a*b*�
            g = g0 + g1*a + g2*b + g3*� + g4*a*b + g5*a*� + g6*b*� + g7*a*b*�
            h = h0 + h1*a + h2*b + h3*� + h4*a*b + h5*a*� + h6*b*� + h7*a*b*�
        */
        solution[i] = -(fgh[i][0] + fgh[i][1]*abc[0] + fgh[i][2]*abc[1] + fgh[i][3]*abc[2] + fgh[i][4]*abc[0]*abc[1] + fgh[i][5]*abc[0]*abc[2] + fgh[i][6]*abc[1]*abc[2] + fgh[i][7]*abc[0]*abc[1]*abc[2]);

        /* 
          Coefs of the linear system
            (f1 + f4*b + f5*c + f7*b*�)*da + (f2 + f4*a + f6*c + f7*a*c)*db + (f3 + f5*a + f6*b + f7*a*b)*dc = -f
            (g1 + g4*b + g5*c + g7*b*c)*da + (g2 + g4*a + g6*c + g7*a*c)*db + (g3 + g5*a + g6*b + g7*a*b)*dc = -g
            (h1 + h4*b + h5*c + h7*b*c)*da + (h2 + h4*a + h6*c + h7*a*c)*db + (h3 + h5*a + h6*b + h7*a*b)*dg = -h
        */
        coefs[i][0] = fgh[i][1] + fgh[i][4]*abc[1] + fgh[i][5]*abc[2] + fgh[i][7]*abc[1]*abc[2];
        coefs[i][1] = fgh[i][2] + fgh[i][4]*abc[0] + fgh[i][6]*abc[2] + fgh[i][7]*abc[0]*abc[2];
        coefs[i][2] = fgh[i][3] + fgh[i][5]*abc[0] + fgh[i][6]*abc[1] + fgh[i][7]*abc[0]*abc[1];
      }

      // Obtain the a, b, c increments
      SolveLinearSystem(coefs, solution, delta_abc, 3);

      ++iter;      
    }

    // Interpolation has failed? Maybe our particle is outside the hexahedron, or the hexahedron is degenerate...
    if ((fabs(abc[0]) > TIIUpperLimit) || (fabs(abc[1]) > TIIUpperLimit) || (fabs(abc[2]) > TIIUpperLimit) || (iter >= TIIMaxIterations))
    {
      for (SlurmInt i = 0; i < 8; ++i) weights[i] = 0.125;
    }
    else
    {
      // If interpolation is successful, compute weights
      weights[0] = 0.125 * (1 - abc[2]) * (1 - abc[1]) * (1 - abc[0]);
      weights[1] = 0.125 * (1 - abc[2]) * (1 - abc[1]) * (1 + abc[0]);
      weights[2] = 0.125 * (1 - abc[2]) * (1 + abc[1]) * (1 - abc[0]);
      weights[3] = 0.125 * (1 - abc[2]) * (1 + abc[1]) * (1 + abc[0]);
      weights[4] = 0.125 * (1 + abc[2]) * (1 - abc[1]) * (1 - abc[0]);
      weights[5] = 0.125 * (1 + abc[2]) * (1 - abc[1]) * (1 + abc[0]);
      weights[6] = 0.125 * (1 + abc[2]) * (1 + abc[1]) * (1 - abc[0]);
      weights[7] = 0.125 * (1 + abc[2]) * (1 + abc[1]) * (1 + abc[0]);
    }
  }

  return(weights);
}

/** 
  Interpolation weights from irregular octahedron.
  See the trilinear.txt in our documentation.

   x1 = x(i  ,j  ,k  )  = p000
   x2 = x(i+1,j  ,k  )  = p100
   x3 = x(i  ,j+1,k  )  = p010
   x4 = x(i+1,j+1,k  )  = p110
   x5 = x(i  ,j  ,k+1)  = p001
   x6 = x(i+1,j  ,k+1)  = p101
   x7 = x(i  ,j+1,k+1)  = p011
   x8 = x(i+1,j+1,k+1)  = p111

  Weights should already be allocated!

  TODO:
    - allocation of boost arrays is quite slow, move it outside?
*/
SlurmDouble* GridCommon::TrilinearIrregularInterpolationWeights(GridPoint *p, GridPoint *p000, GridPoint *p100, GridPoint *p010, GridPoint *p110, GridPoint *p001, GridPoint *p101, GridPoint *p011, GridPoint *p111)
{
  // the arrays for f0, ... f7
  SlurmDoubleArray2D fgh;
  fgh.resize(boost::extents[3][8]);

  SlurmDoubleArray2D coefs;
  coefs.resize(boost::extents[3][3]);

  return(TrilinearIrregularInterpolationWeights(p, p000, p100, p010, p110, p001, p101, p011, p111, fgh, coefs));
}

GridCommon::~GridCommon()
{
}
