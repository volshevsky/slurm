/** 
  Various math utils for Slurm project
*/
#pragma once
#include <utils.h>

/// Compute matrix determinant recursively
SlurmDouble SlDeterminant(SlurmDoubleArray2D a, SlurmInt m);

/// Solve linear system using Kramer's method
void SolveLinearSystem(SlurmDoubleArray2D a, SlurmDouble * r, SlurmDouble * solution, SlurmInt m);