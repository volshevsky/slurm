/**
  Various utilities for the Slurm project.
  (c) V. Olshevsky, Nov 2014
*/
#pragma once
#include <assert.h>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <math.h>
// Unlike containers from the standard library, operator[] checks whether an index is valid. If an index is not valid, the program exits with std::abort(). 
// If you don't want the validity of indexes to be checked, define the macro BOOST_DISABLE_ASSERTS before you include boost/multi_array.hpp.
//#define DEBUG

// Define a condition to ignore certain dimension for 2D simulation
//#define IGNORE_Y
//#define IGNORE_Z

#ifndef DEBUG 
#define BOOST_DISABLE_ASSERTS
#endif

#include "boost/multi_array.hpp"
#include "boost/array.hpp"
// This one is for string comparison
#include <boost/algorithm/string/predicate.hpp>

// Data types
typedef int SlurmInt;
typedef double SlurmDouble;
typedef std::array<SlurmDouble, 3> SlDoubleVector;
typedef std::array<SlurmDouble, 6> SlDoubleSymmetricTensor;
typedef std::array<SlurmDouble, 9> SlDoubleTensor;
typedef boost::multi_array<double, 2> SlurmDoubleArray2D;
typedef boost::multi_array<double, 3> SlurmDoubleArray3D;
typedef boost::multi_array<double, 4> SlurmDoubleArray4D;

//#define PI 4. * atan(1.0);
#define PI 3.14159265358979323846
// [kg]
#define PROTON_MASS 1.6726219e-27
// [m^3 kg^-1 s^-2]
#define GRAVITATIONAL_CONSTANT 6.67408e-11 
// [m^2 kg s^-2 K^-1]
#define BOLTZMANN_CONSTANT 1.38064852e-23
// [kg]
#define SOLAR_MASS 1.989e30
// [m]
#define ASTRONOMICAL_UNIT 149597870700.
// [s] = 24.7 days
#define SOLAR_EQUATORIAL_PERIOD 2114208.0

/// Interpolation schemes
enum InterpolationSchemes {isUnknown = -1, isLinear = 1, isQuadratic = 2};

/// Boundary conditions
enum BoundaryType {btUnknown = -1, btPeriodic = 1, btReflective = 2, btOutlet = 3, btInlet = 4, btFixed = 5, btRigidWall = 6, btSphere = 7, btSupersonicOutlet = 8};

/// Artificial viscosity modes
enum ArtificialViscosityModes {avmOff = 0, avmKuropatenko = 1};

/** Magnetic field evolution strategies
  mfemOff - hydrodynamic case
  mfemInterpolateParticles - vector potential is carried by particles. It is not advected, but is interpolated on each node from the 8 node neighbor particles in Fields::prepareNodeFields()
                             Vector potential is then updated its change is interpolated to the particles before they are advanced.
  mfemAdvectParticles - vector potential is carried by particles. A*vol is advected between the grid and the particles like a conserved quentity.
  mfemRemapGrid - obsolete
  mfemPreserveParticles - like mfemInterpolateParticles, but vector potential IS NOT advected back to particles. This is exact solution in 2D ideal MHD (dAz/dt == 0).
*/
enum MFEvolutionMethods {mfemUnknown = -1, mfemOff = 0, mfemInterpolateParticles = 1, mfemAdvectParticles = 2, mfemRemapGrid = 2, mfemPreserveParticles = 3};

/// Volume evolution ways
enum VolumeEvolutionMethods {vemOff = 0, vemMaterialPoint = 1, vemVelocityGradient = 2};

/** 
  Flags defining physical quantitites, e.g., for interpolation between particles and grids. 
  A selection of multiple quantities can be combined in a bit mask, e.g.:
    quants = QUANT_RHO | QUANT_MASS;
  selects density and mass (to be interpolated).
*/
#define QUANT_RHO 1
#define QUANT_ENERGY 2
#define QUANT_VX 4
#define QUANT_VY 8
#define QUANT_VZ 16
#define QUANT_MAGNX 32
#define QUANT_MAGNY 64
#define QUANT_MAGNZ 128
#define QUANT_MASS 256
#define QUANT_VOL 512
#define QUANT_PRESS 1024
#define QUANT_PMAG 2048 // Magnetic pressure
#define QUANT_INTERNAL 4096 // Defines multiple quantities depending on the situation. For instance, all "internal" quantities that are interpolated g->p after grid is advanced.
#define QUANT_VISCOSITY 0x2000 // Artificial viscosity
#define QUANT_WEIGHT 0x4000 // Helper interpolation weight of a GridElement
#define QUANT_CURRENT 0x8000   // Current X
#define QUANT_VELOCITY 0x10000 // Velocty vector
#define QUANT_MAGNETIC 0x20000 // Magnetic field vector / vector potential
#define QUANT_SCALARPOTENTIAL 0x40000 // Electromagnetic scalar potential

/// Flags defining dimensions
#define DIM_X 1
#define DIM_Y 2
#define DIM_Z 4

/// Error codes
#define SLURM_ERROR_BASE 1000
#define SLURM_ERROR_WRONG_BC (SLURM_ERROR_BASE + 1)
#define SLURM_ERROR_OUT_OF_BOUNDS (SLURM_ERROR_BASE + 2)
#define SLURM_ERROR_NO_PARTICLES_IN_CELL (SLURM_ERROR_BASE + 3)
#define SLURM_ERROR_INTERPOLATION (SLURM_ERROR_BASE + 4)
#define SLURM_ERROR_WRONG_IC (SLURM_ERROR_BASE + 5)

/// Error messages
#define SLURM_ERROR_TEXT_WRONG_BC "ERROR. The operation is not defined for the specified boundary condition."
#define SLURM_ERROR_TEXT_NO_PARTICLES_IN_CELL "WARNING. No particles in cell/node."
#define SLURM_ERROR_TEXT_OUT_OF_BOUNDS "ERROR. Some point is out of bounds of the computational grid."


using namespace std;

/** 
  Counting bits set in a bit mask.

  http://graphics.stanford.edu/~seander/bithacks.html
*/
inline unsigned long NumberOfBitsSet(unsigned long bitmask)
{
  unsigned long c;
  unsigned long v = bitmask;
  /*cout << v << endl;
  for (c = 0; v; c++)
  {
    v &= v - 1; // clear the least significant bit set
  }
  return v; */
  for (c = 0; v; v >>= 1)
  {
    c += v & 1;
  }
  return c;
} 

/** String manipulation routines */
template <typename T>
string NumberToFixedLengthString (T Number, int Length)
{
  stringstream ss;
  ss << setw(Length) << setfill('0') << Number;
  return ss.str();
}

/** Taken from http://www.cplusplus.com/forum/articles/9645/ */
template <typename T>
string NumberToString ( T Number )
{
  stringstream ss;
  ss << Number;
  return ss.str();
}

/** Taken from http://www.cplusplus.com/forum/articles/9645/ */
template <typename T>
T StringToNumber ( const string &Text )//Text not by const reference so that the function can be used with a 
{                               //character array as argument
  stringstream ss(Text);
  T result;
  return ss >> result ? result : 0;
}

// Input/output
template <typename T>
void message(const T s)
{
  cout << s;
}

template <typename T1, typename T2>
void message(const T1 s1, const T2 s2)
{
  cout << s1 << s2;
}

template <typename T>
void messageline(const T s)
{
  cout << s << endl;
}

template <typename T1, typename T2>
void messageline(const T1 s1, const T2 s2)
{
  cout << s1 << s2 << endl;
}
