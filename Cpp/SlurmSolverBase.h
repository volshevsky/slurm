#pragma once
#include "Grid.h"
#include "ParticleManager.h"
#include "IOManager.h"
#include "ConfigFile.h"
#include "Fields.h"

/** 
  Base class for the Slurm solvers.
  The solver has a Grid object, and a ParticleManager object and just controls the flow of the simulation.
  The Grid doesn't know about any particles, and performs the math.
  ParticleManager moves particles, and knows about the grid when needed to interact.

  Introduced in March 2015.
*/
class SlurmSolverBase
{

protected:

  /// File name to save field data
  string OutputFileNameGrid = "grid";
  /// File name to save particle data
  string OutputFileNameParticles = "particles";
  /// File name to save conserved variables (energy densities)
  string OutputFileNameLog = "conserved.csv";
  /// What to save on nodes?
  unsigned long NodeOutputQuantities = QUANT_VX | QUANT_VY | QUANT_VZ | QUANT_MASS;
  /// What to save on cells?
  unsigned long CellOutputQuantities = QUANT_VOL | QUANT_MASS | QUANT_RHO | QUANT_PRESS | QUANT_VISCOSITY;
  /// What to save on particles?
  unsigned long ParticleOutputQuantities = QUANT_MASS | QUANT_VOL | QUANT_INTERNAL | QUANT_VX | QUANT_VY | QUANT_VZ;

  /// Time step
  SlurmDouble dt;
  /// Current time
  SlurmDouble CurrTime;

  /// Until when shall the simulation run
  SlurmDouble MaxTime;
  /// Number of iterations; careful when adjusting the timestep
  SlurmInt nt;
  /// Which iteration/cycle to start from
  SlurmInt itStart;
  /// Output cycle for textual information
  SlurmInt OutputCycle;
  /// How many cycles to save the grid
  SlurmInt GridOutputCycle;
  /// How many cycles to save the particles
  SlurmInt ParticlesOutputCycle;
  /// Condition to run finalize()
  bool done;

  /// Carries info about electromagnetic field
  Fields *fields;
  /// Object responsible for holding cell/node data
  Grid *grid;
  /// Object that controls particles
  ParticleManager *particles;
  /// Initial condition
  InitialCondition * initialCondition;

  /// Whether to save ghost cells and nodes
  bool SaveGhost = false;

public:

  /// Returns the initial condition
  InitialCondition * get_initialCondition() { return(initialCondition); }

  /** Main flow of the solver */
  virtual int run(IOManager* io) = 0;
  /** Last bits / cleanup before closing */
  virtual int finalize() = 0;
  /** Checks whether it is time to finalize */
  bool isDone();
  /** Checks whether it is time to finalize */
  bool isDone(SlurmInt it);
  /** Produce output: fields, particles */
  virtual void doOutput(IOManager* io, SlurmInt it);

  /** Output only grid */
  void outputGrid(IOManager* io, SlurmInt it);
  /** Output only grid */
  void outputGrid(IOManager* io, const string suffix);
  /** Output only particles */
  void outputParticles(IOManager* io, const SlurmInt it);
  /** Output only particles */
  void outputParticles(IOManager* io, const string suffix);

  SlurmSolverBase(ConfigFile *config);
  SlurmSolverBase() {};
  ~SlurmSolverBase() {};
};

