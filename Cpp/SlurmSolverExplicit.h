/* An explicit solver class for the Slurm project.
The solver has a Grid object, and a ParticleManager object and just controls the flow of the simulation.
The Grid doesn't know about any particles, and performs the math.
ParticleManager moves particles, and knows about the grid when needed to interact.

Started in March 2015
*/
#pragma once
#include "SlurmSolverBase.h"
#include <time.h>

class SlurmSolverExplicit: public SlurmSolverBase
{
private:
  /** Which quantities are interpolated between particles and grid */
  unsigned long InterpolationQuants = QUANT_ENERGY | QUANT_MASS | QUANT_VOL | QUANT_VX | QUANT_VY | QUANT_VZ | QUANT_WEIGHT | QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ;

public:
  int run(IOManager* io);
  int finalize() {return 0;}

  /** Adjust dt according to the grid's CFL condition */
  void adjustTimestep();

  SlurmSolverExplicit(ConfigFile *config);
  ~SlurmSolverExplicit();
};

