#include "IOManager.h"

const string IOManager::ExtensionVTI = ".vti";  
const string IOManager::ExtensionVTR = ".vtr";  
const string IOManager::ExtensionVTU = ".vtu";
const string IOManager::ExtensionVTK = ".vtk";  

/** 
  Write a VTK Unstructured file (used to store particle data normally).

  First 3 columns in data should be the X, Y, Z coordinates of the points

  NOTE, if using VTK prior to 7.0, to be able to open the file in ParaView, change the following attribute in the VTI's header:
  version="2.0" to "1.0"
*/
int IOManager::writeVTU(const string filename, SlurmDoubleArray2D data, vector<string> scalar_names, vector<string> vector_names)
{
#ifdef VTK_ENABLED
  // Create the grid object
  vtkSmartPointer<vtkUnstructuredGrid> ugrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
  // The points of the grid
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

  // TODO: if data.shape()[1] == 3 do not assign additional attributes, just point locations are enough.
  // Arrays carrying additional point attributes (e.g., velocity)
  vtkSmartPointer<vtkDoubleArray> vtk_point_data[data.shape()[1]-3];
  for (unsigned long q = 3; q < data.shape()[1]; ++q)
  {
    vtk_point_data[q] = vtkSmartPointer<vtkDoubleArray>::New();
    vtk_point_data[q]->SetNumberOfComponents(1);
    vtk_point_data[q]->SetNumberOfTuples(data.shape()[0]);
    vtk_point_data[q]->SetName(scalar_names[q].c_str());
  }
  
  // Main cycle: assign point coordinates and additional attributes.
  for (unsigned long ip = 0; ip < data.shape()[0]; ip++)
  {
    // First set points coordinates
    points->InsertNextPoint(data[ip][0], data[ip][1], data[ip][2]);
    // Copy additional attributes to our VTK arrays
    for (unsigned long q = 0; q < data.shape()[1] - 3; ++q)
    {
      vtk_point_data[q]->SetTuple1(ip, data[ip][q+3]);
    }
  } 

  // Set the points
  ugrid->SetPoints(points);

  // Set point data
  vtkPointData* pts = ugrid->GetPointData();

  for (unsigned long q = 0; q < data.shape()[1] - 3; ++q)
  {
    if (q == 0) 
    {
      // Set scalars in the point data
      pts->SetScalars(vtk_point_data[q]);
    }
    else 
    {
      // Add a new array to the point data. 
      pts->AddArray(vtk_point_data[q]);
    }
  }
 
  // Write file
  vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
  writer->SetFileName((filename + ExtensionVTU).c_str());
#if VTK_MAJOR_VERSION <= 5
  writer->SetInput(ugrid);
#else
  writer->SetInputData(ugrid);
#endif
  writer->Write();

#else
  writeUnstructuredVTK(filename, data, scalar_names, vector_names);

#endif // VTK_ENABLED

  return 0;
}

/** 
  Write a VTK Rectilinear grid file.
  In VTR terminology "points" correspond to the nodes of our grid. Cells correspond to the cells.

  Parameters
    filename - output filename
    cell_data - a [nx, ny, nz, nq] array with cell data where q corresponds to the quantity: ux, uy, uz, mass,...
    point_data - same for nodes (points)
    cell_name - [nq] array with names of the quantities
    point_names - same for nodes (points)
    dx, dy, dz - grid steps
    cell_vectors - names of (3-component) vectors in the 4D arrays. Vector components should be placed in the beginning, before the scalars.
                   if empty, all data is considered as scalars.
    point_vectors - same for points (nodes).

  NOTE, if using VTK library, vector output is not implemented yet. TODO!

*/
int IOManager::writeVTR(const string filename, SlurmDoubleArray4D cell_data, vector<string> cell_names, SlurmDoubleArray4D point_data, vector<string> point_names, SlurmDouble dx, SlurmDouble dy, SlurmDouble dz, vector<string> cell_vectors, vector<string> point_vectors, SlurmDouble xstart, SlurmDouble ystart, SlurmDouble zstart)
{
#ifdef VTK_ENABLED
  // Create the rectilinear grid object
  vtkSmartPointer<vtkRectilinearGrid> rgrid = vtkSmartPointer<vtkRectilinearGrid>::New();

  // Before doing anything, specify the grid's extent in X, Y, Z order.
  //rectilinearGrid.SetExtent(0, data.shape()[0] - 1, 0, data.shape()[1] - 1, 0, 1);
  rgrid->SetDimensions(point_data.shape()[0], point_data.shape()[1], point_data.shape()[2]);

  // Create the coordinates and set data values
  vtkSmartPointer<vtkDoubleArray> xCoords = vtkSmartPointer<vtkDoubleArray>::New();
  xCoords->SetNumberOfComponents(1);
  for (unsigned long i = 0; i < point_data.shape()[0]; i++) xCoords->InsertNextTuple1((double)i * dx);
 
  // Create the coordinates and set data values
  vtkSmartPointer<vtkDoubleArray> yCoords = vtkSmartPointer<vtkDoubleArray>::New();
  yCoords->SetNumberOfComponents(1);
  for (unsigned long i = 0; i < point_data.shape()[1]; i++) yCoords->InsertNextTuple1((double)i * dy);

  // Create the coordinates and set data values
  vtkSmartPointer<vtkDoubleArray> zCoords = vtkSmartPointer<vtkDoubleArray>::New();
  zCoords->SetNumberOfComponents(1);
  // In 2D case, there should be 2 layers of nodes (points), because VTKRectilinearGrid is 3D by design
  for (unsigned long i = 0; i < point_data.shape()[2]; i++) zCoords->InsertNextTuple1((double)i * dz);

  // Set the coordinates
  rgrid->SetXCoordinates(xCoords);
  rgrid->SetYCoordinates(yCoords);
  rgrid->SetZCoordinates(zCoords);

  // Grid dimensions for future use. Returns the number of points (i.e., nodes)
  int* dims = rgrid->GetDimensions();

  // Populate cell data
  vtkSmartPointer<vtkDoubleArray> vtk_cell_data[cell_data.shape()[3]];
  // Get a reference to the grid's cell data
  vtkCellData* cells = rgrid->GetCellData();

  for (unsigned long q = 0; q < cell_data.shape()[3]; ++q)
  {
    vtk_cell_data[q] = vtkSmartPointer<vtkDoubleArray>::New();
    vtk_cell_data[q]->SetNumberOfComponents(1);
    vtk_cell_data[q]->SetNumberOfTuples(rgrid->GetNumberOfCells());
    vtk_cell_data[q]->SetName(cell_names[q].c_str());

    // Cycle over all rgrid's points, get their indices and copy the data.
    // We want to have only one cycle over point's ID to efficiently use multi-threading.
    for (long cId = 0; cId < rgrid->GetNumberOfCells(); ++cId)
    {
      // Get cells's indices i, j , k
      unsigned long k = cId / ((dims[0] - 1) * (dims[1] - 1));
      unsigned long j = (cId - k * (dims[0] - 1) * (dims[1] - 1)) / (dims[0] - 1);
      unsigned long i = cId - k * (dims[0] - 1) * (dims[1] - 1) - j * (dims[0] - 1);

      // CAUTION!!! K should be always zero in the 2D case?
      vtk_cell_data[q]->SetTuple1(cId, cell_data[i][j][k][q]);
    }

    // Set scalars in the point data
    if (q == 0) cells->SetScalars(vtk_cell_data[q]);
    // Add a new array to the point data. 
    else cells->AddArray(vtk_cell_data[q]);
  }

  // Populate point (node) data. Assume only 1 layer of nodes for 2D?
  vtkSmartPointer<vtkDoubleArray> vtk_point_data[point_data.shape()[3]];
  // Get a reference to the grid's cell data
  vtkPointData* points = rgrid->GetPointData();

  for (unsigned long q = 0; q < point_data.shape()[3]; ++q)
  {
    vtk_point_data[q] = vtkSmartPointer<vtkDoubleArray>::New();
    vtk_point_data[q]->SetNumberOfComponents(1);
    vtk_point_data[q]->SetNumberOfTuples(rgrid->GetNumberOfPoints());
    vtk_point_data[q]->SetName(point_names[q].c_str());

    // Cycle over all rgrid's points, get their indices and copy the data.
    // We want to have only one cycle over point's ID to efficiently use multi-threading.
    for (long pId = 0; pId < rgrid->GetNumberOfPoints(); ++pId)
    {
      // Get cells's indices i, j , k
      unsigned long k = pId / (dims[0] * dims[1]);
      unsigned long j = (pId - k * dims[0] * dims[1]) / dims[0];
      unsigned long i = pId - k * dims[0] * dims[1] - j * dims[0];

      // CAUTION!!! K should be always zero in the 2D case?
      vtk_point_data[q]->SetTuple1(pId, point_data[i][j][k][q]);
    }

    // Set scalars in the point data
    if (q == 0) points->SetScalars(vtk_point_data[q]);
    // Add a new array to the point data. 
    else points->AddArray(vtk_point_data[q]);
  }
    
  vtkSmartPointer<vtkXMLRectilinearGridWriter> writer = vtkSmartPointer<vtkXMLRectilinearGridWriter>::New();
  writer->SetFileName((filename + ExtensionVTR).c_str());
#if VTK_MAJOR_VERSION <= 5
  writer->SetInputConnection(rgrid->GetProducerPort());
#else
  writer->SetInputData(rgrid);
#endif
  writer->Write();

#else
  writeRectilinearVTK(filename, cell_data, cell_names, point_data, point_names, dx, dy, dz, cell_vectors, point_vectors, xstart, ystart, zstart);

#endif // VTK_ENABLED


  return 0;
}

/** 
  Write a VTK Image file with multiple components at each data point.
  NOTE, to be able to open the file in ParaView, change the following attribute in the VTI's header:
  version="2.0" to "1.0"
*/
int IOManager::writeVTI(const string filename, SlurmDoubleArray3D data, SlurmDouble dx, SlurmDouble dy, SlurmDouble x0, SlurmDouble y0)
{
#ifdef VTK_ENABLED
  vtkSmartPointer<vtkImageData> imageData = vtkSmartPointer<vtkImageData>::New();
  imageData->SetDimensions(data.shape()[0], data.shape()[1], 1);
  imageData->SetSpacing(dx, dy, 1.0);
  imageData->SetOrigin(x0, y0, 0.);
  
#if VTK_MAJOR_VERSION <= 5
  imageData->SetNumberOfScalarComponents(data.shape()[2]);
  imageData->SetScalarTypeToDouble();
#else
  imageData->AllocateScalars(VTK_DOUBLE, data.shape()[2]);
#endif
  int* dims = imageData->GetDimensions();
 
  // Fill the image data
  for (int j = 0; j < dims[1]; j++)
  {
    for (int i = 0; i < dims[0]; i++)
    {
      double* pixel = static_cast<double*>(imageData->GetScalarPointer(i, j, 0));
      for (unsigned long k = 0; k < data.shape()[2]; k++)
      {
        pixel[k] = data[i][j][k];
      }
    }
  }
 
  vtkSmartPointer<vtkXMLImageDataWriter> writer = vtkSmartPointer<vtkXMLImageDataWriter>::New();
  writer->SetFileName((filename + ExtensionVTI).c_str());
#if VTK_MAJOR_VERSION <= 5
  writer->SetInputConnection(imageData->GetProducerPort());
#else
  writer->SetInputData(imageData);
#endif
  writer->Write();

#endif //VTK_ENABLED
  return 0;
}

/** 
  TODO: replace this dinosaur with a proper method from boost?
*/
void IOManager::ByteSwap(unsigned char * b, int n)
{
  //TODO: Check endian. Using boost?
  register int i = 0;
  register int j = n-1;
  while (i<j)
  {
     std::swap(b[i], b[j]);
     i++, j--;
  }
}

/** 
  Write an old-format simple VTK file. Doesn't need VTK library. Writes point data only.
  In VTR terminology "points" correspond to the nodes of our grid. Cells correspond to the cells.
  Be very, very careful with the order: all vectors SHOULD be in the beginning!

  Parameters
    filename - output filename
    cell_data - a [nx, ny, nz, nq] array with cell data where q corresponds to the quantity: ux, uy, uz, mass,...
    point_data - same for nodes (points)
    cell_name - [nq] array with names of the quantities
    point_names - same for nodes (points)
    dx, dy, dz - grid steps
    cell_vectors - names of (3-component) vectors in the 4D arrays. Vector components should be placed in the beginning, before the scalars.
                   if empty, all data is considered as scalars.
    point_vectors - same for points (nodes).

  So far, saves in single-precision.

  Data types: float or double?
*/
int IOManager::writeRectilinearVTK(const string filename, SlurmDoubleArray4D cell_data, vector<string> cell_names, SlurmDoubleArray4D point_data, vector<string> point_names, SlurmDouble dx, SlurmDouble dy, SlurmDouble dz, vector<string> cell_vectors, vector<string> point_vectors, SlurmDouble xstart, SlurmDouble ystart, SlurmDouble zstart)
{
  std::ofstream vtkstream((filename + ExtensionVTK).c_str(), std::ios::out | std::ios::trunc | std::ios::binary);

  // Header
  vtkstream << "# vtk DataFile Version 2.0\n";
  vtkstream << "Slurm fields\n";
  vtkstream << "BINARY\n";
  vtkstream << "DATASET RECTILINEAR_GRID\n";
  vtkstream << "DIMENSIONS " << point_data.shape()[0] << " " << point_data.shape()[1] << " " << point_data.shape()[2] << "\n";

  // Coordinates
  vtkstream << "X_COORDINATES " << point_data.shape()[0] << " float\n";
  float x_coords[point_data.shape()[0]];
  for (unsigned long i = 0; i < point_data.shape()[0]; i++) x_coords[i] = (float)(dx * i + xstart); //vtkstream << (float)(dx * i) << " ";
  FloatArraySwap(x_coords, point_data.shape()[0]);
  vtkstream.write((char*)x_coords, point_data.shape()[0] * sizeof(float));
  vtkstream << endl;

  vtkstream << "Y_COORDINATES " << point_data.shape()[1] << " float\n";
  float y_coords[point_data.shape()[1]];
  for (unsigned long i = 0; i < point_data.shape()[1]; i++) y_coords[i] = (float)(dy * i + ystart); //vtkstream << (float)(dy * i) << " ";
  FloatArraySwap(y_coords, point_data.shape()[1]);
  vtkstream.write((char*)y_coords, point_data.shape()[1] * sizeof(float));
  vtkstream << endl;

  vtkstream << "Z_COORDINATES " << point_data.shape()[2] << " float\n";
  float z_coords[point_data.shape()[2]];
  for (unsigned long i = 0; i < point_data.shape()[2]; i++) z_coords[i] = (float)(dz * i + zstart); //vtkstream << (float)(dz * i) << " ";
  FloatArraySwap(z_coords, point_data.shape()[2]);
  vtkstream.write((char*)z_coords, point_data.shape()[2] * sizeof(float));
  vtkstream << endl;

  // Allocate a buffer for writing
  unsigned long nPoints = point_data.shape()[0] * point_data.shape()[1] * point_data.shape()[2];
  unsigned long nCells = cell_data.shape()[0] * cell_data.shape()[1] * cell_data.shape()[2];
  unsigned long buffer_size = std::max(nPoints, nCells);
  // If there are vectors, allocate the buffer for 3 components at a time
  if ((point_vectors.size() > 0) || (cell_vectors.size() > 0))
    buffer_size *= 3;
  float * writebuffer = new float[buffer_size];

  // Point (node) data
  vtkstream << "POINT_DATA " << nPoints << endl;

  // Point (node) Vectors
  if (point_vectors.size() > 0)
  {
    for (unsigned long q = 0; q < point_vectors.size(); ++q)
    {
      vtkstream << "VECTORS " << point_vectors[q].c_str() << " float\n";
  
      // Cycle over all rgrid's points, get their indices and copy the data.
      // We want to have only one cycle over point's ID to efficiently use multi-threading.
      for (unsigned long pId = 0; pId < nPoints; ++pId)
      {
        // Get cells's indices i, j , k
        unsigned long k = pId / (point_data.shape()[0] * point_data.shape()[1]);
        unsigned long j = (pId - k * point_data.shape()[0] * point_data.shape()[1]) / point_data.shape()[0];
        unsigned long i = pId - k * point_data.shape()[0] * point_data.shape()[1] - j * point_data.shape()[0];
  
        for (int comp = 0; comp < 3; comp++)
          writebuffer[3*pId + comp] = point_data[i][j][k][q*3 + comp];
      }
  
      // Swap endian and dump to file
      FloatArraySwap(writebuffer, 3 * nPoints);
      vtkstream.write((char*)writebuffer, 3 * nPoints * sizeof(float));
      vtkstream << endl;
    }
  }  

  // Point (node) scalars
  for (unsigned long q = 3 * point_vectors.size(); q < point_data.shape()[3]; ++q)
  {
    vtkstream << "SCALARS " << point_names[q].c_str() << " float\n";
    vtkstream << "LOOKUP_TABLE default\n";

    // Cycle over all rgrid's points, get their indices and copy the data.
    // We want to have only one cycle over point's ID to efficiently use multi-threading.
    for (unsigned long pId = 0; pId < nPoints; ++pId)
    {
      // Get cells's indices i, j , k
      unsigned long k = pId / (point_data.shape()[0] * point_data.shape()[1]);
      unsigned long j = (pId - k * point_data.shape()[0] * point_data.shape()[1]) / point_data.shape()[0];
      unsigned long i = pId - k * point_data.shape()[0] * point_data.shape()[1] - j * point_data.shape()[0];

      writebuffer[pId] = point_data[i][j][k][q];
    }

    // Swap endian and dump to file
    FloatArraySwap(writebuffer, nPoints);
    vtkstream.write((char*)writebuffer, nPoints * sizeof(float));
    vtkstream << endl;
  }

  // Cell data. We can use the same writebuffer as the number of cells is smaller by 1 in each dimension?
  vtkstream << "CELL_DATA " << nCells << endl;

  // cell vectors first
  if (point_vectors.size() > 0)
  {
    for (unsigned long q = 0; q < cell_vectors.size(); ++q)
    {
      vtkstream << "VECTORS " << cell_vectors[q].c_str() << " float\n";
  
      // Cycle over all cells, get their indices and copy the data.
      // We want to have only one cycle over point's ID to efficiently use multi-threading.
      for (unsigned long cId = 0; cId < nCells; ++cId)
      {
        // Get cells's indices i, j , k
        unsigned long k = cId / (cell_data.shape()[0] * cell_data.shape()[1]);
        unsigned long j = (cId - k * cell_data.shape()[0] * cell_data.shape()[1]) / cell_data.shape()[0];
        unsigned long i = cId - k * cell_data.shape()[0] * cell_data.shape()[1] - j * cell_data.shape()[0];
  
        for (int comp = 0; comp < 3; comp++)
          writebuffer[cId*3 + comp] = cell_data[i][j][k][q*3 + comp];
      }
  
      // Swap endian and dump to file
      FloatArraySwap(writebuffer, 3 * nCells);
      vtkstream.write((char*)writebuffer, 3 * nCells * sizeof(float));
      vtkstream << endl;
    }
  }

  // cell scalars
  for (unsigned long q = 3 * cell_vectors.size(); q < cell_data.shape()[3]; ++q)
  {
    vtkstream << "SCALARS " << cell_names[q].c_str() << " float\n";
    vtkstream << "LOOKUP_TABLE default\n";

    // Cycle over all cells, get their indices and copy the data.
    // We want to have only one cycle over point's ID to efficiently use multi-threading.
    for (unsigned long cId = 0; cId < nCells; ++cId)
    {
      // Get cells's indices i, j , k
      unsigned long k = cId / (cell_data.shape()[0] * cell_data.shape()[1]);
      unsigned long j = (cId - k * cell_data.shape()[0] * cell_data.shape()[1]) / cell_data.shape()[0];
      unsigned long i = cId - k * cell_data.shape()[0] * cell_data.shape()[1] - j * cell_data.shape()[0];

      writebuffer[cId] = cell_data[i][j][k][q];
    }

    // Swap endian and dump to file
    FloatArraySwap(writebuffer, nCells);
    vtkstream.write((char*)writebuffer, nCells * sizeof(float));
    vtkstream << endl;
  }

  vtkstream.close();

  delete [] writebuffer;

  return(0);
}

/** 
  Write an old-fasioned VTK file with unstructured grid (used to store particle data normally).

  Parameters:
    data - 2D array with points data. First 3 columns are the point's X, Y, Z.
    scalar_names - a vector smaller by 3 than the number of columns in data. Contains the names of points attributes.

*/
int IOManager::writeUnstructuredVTK(const string filename, SlurmDoubleArray2D data, vector<string> scalar_names, vector<string> vector_names)
{
  std::ofstream vtkstream((filename + ExtensionVTK).c_str(), std::ios::out | std::ios::trunc | std::ios::binary);

  // Header
  vtkstream << "# vtk DataFile Version 2.0\n";
  vtkstream << "Slurm particles\n";
  vtkstream << "BINARY\n";
  vtkstream << "DATASET UNSTRUCTURED_GRID\n";

  // Write point coordinates
  unsigned long nPoints = data.shape()[0];
  float coords[3];
  vtkstream << "POINTS " << nPoints << " float\n";
  for (unsigned long ip = 0; ip < nPoints; ip++)
  {
    for (int i = 0; i < 3; i++) coords[i] = (float)data[ip][i];
    FloatArraySwap(coords, 3);
    vtkstream.write((char*)coords, 3 * sizeof(float));
  }
  vtkstream << endl;

  // Write point data
  float * writebuffer = new float[(vector_names.size() > 0) ? (3 * nPoints) : nPoints];
  vtkstream << "POINT_DATA " << nPoints << endl;

  // Write vectors first
  if (vector_names.size() > 0)
  {
    for (unsigned long q = 0; q < vector_names.size(); q++)
    {
      vtkstream << "VECTORS " << vector_names[q].c_str() << " float\n";
      
      for (unsigned long ip = 0; ip < nPoints; ip++) 
        for (int comp = 0; comp < 3; comp++)
          writebuffer[3 * ip + comp] = (float)data[ip][3*(q+1) + comp];
    
      FloatArraySwap(writebuffer, 3 * nPoints);
      vtkstream.write((char*)writebuffer, 3 * nPoints * sizeof(float));    
      vtkstream << endl;
    }
  }

  // Write scalars
  for (unsigned long q = 3 * (vector_names.size() + 1); q < data.shape()[1]; q++)
  {
    vtkstream << "SCALARS " << scalar_names[q].c_str() << " float\n";
    vtkstream << "LOOKUP_TABLE default\n";

    for (unsigned long ip = 0; ip < nPoints; ip++) 
      writebuffer[ip] = (float)data[ip][q];

    FloatArraySwap(writebuffer, nPoints);
    vtkstream.write((char*)writebuffer, nPoints * sizeof(float));    
    vtkstream << endl;
  }

  vtkstream.close();
  delete [] writebuffer;
  return 0;
}


IOManager::IOManager()
{
  //
}

IOManager::~IOManager()
{
  //
}
