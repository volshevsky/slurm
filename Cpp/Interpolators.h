#pragma once

#include "GridCommon.h"

/// Specifies the number of weights used by the LinearInterpolator class
const int InterpolWeightsNumberLinear = pow(2, GridDimensionality);
const int InterpolWeightsNumberQuadratic = pow(3, GridDimensionality);

/**
  Interpolator presents an interface for various interpolator classes in Slurm.  
*/
class Interpolator
{

protected:

  GridCommon *grid;
  /// Number of neighbor cells used for interpolation: differs in quadratic and linear
  SlurmInt InterpolCellsNumber = InterpolWeightsNumberLinear;
  /// Number of neighbor nodes used for interpolation. Always 8 (linear)
  SlurmInt InterpolNodesNumber = InterpolWeightsNumberLinear;

  /// Far-side piece of the Quadratic interpolation
  SlurmDouble weight1(SlurmDouble x1, SlurmDouble x2, SlurmDouble dx);
  /// Central quadriliteral piece of the Quadratic interpolation
  SlurmDouble weight2(SlurmDouble x1, SlurmDouble x2, SlurmDouble dx);

public:

  // Getters/setters

  int get_InterpolCellsNumber() { return InterpolCellsNumber; }
  void set_InterpolCellsNumber(int value) { InterpolCellsNumber = value; }
  int get_InterpolNodesNumber() { return InterpolNodesNumber; }
  void set_InterpolNodesNumber(int value) { InterpolNodesNumber = value; }
  GridCommon * get_grid() { return grid; }

  /// Add values from a particle to the adjacent cells and nodes
  virtual void advectToGrid(Particle* p, unsigned long quants);

  /// Add values from a particle to the adjacent cells and nodes
  virtual void advectToGrid(Particle* p);

  /// Interpolates cell center quantities on the given point, and returns the corresponding GridCell object.
  virtual GridCell* interpolateCells(GridPoint* p, unsigned long quants);

  /// Interpolates the quantities during particle advancement. 
  virtual GridCell* interpolateCells(GridPoint* p);

  /// Interpolates node quantities on the given point, and generates the GridNode object with data
  virtual GridNode* interpolateNodes(GridPoint* p, unsigned long quants) {return new GridNode();};

  /// Interpolates node quantities during particle advancement
  virtual GridNode* interpolateNodes(GridPoint* p) {return new GridNode();};

  /// Use 8 neighbors to compute linear interpolation weights.
  void getInterpolationCellsLinear(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[]);

  /// Use second-order spline to compute interpolation weights for cells. Also the number of weights is 27.
  void getInterpolationCellsQuadratic(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[]);

  /// Compute the interpolation weights for nodes.
  void getInterpolationNodes(GridPoint* p, GridNode* adj_nodes[], SlurmDouble weights[]);

  /// Find neighbor cells when projecting to/from cells, and the corresponding weights
  virtual void getInterpolationCells(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[]) {}

  /// Formally add values to the grid cell. Weights are supplied from getInterpolationCells().
  virtual void advectToGridCell(Particle* source, GridCell* dest, SlurmDouble w, unsigned long quants);

  /// Formally add values to the grid cell. Weights are supplied from getInterpolationCells().
  virtual void advectToGridCell(Particle* source, GridCell* dest, SlurmDouble w);

  /// This interpolator is used whenever particles carry A and phi
  static void advectNodeFieldAP(Particle* source, GridNode* dest, SlurmDouble w)
  {
    dest->inc_A(source->get_Ax() * source->get_vol() * w, source->get_Ay() * source->get_vol() * w, source->get_Az() * source->get_vol() * w);
    //TODO: interpolate phi!
  }

  /// mfemInterpolateParticles, mfemPreserveParticles, mfemOff
  static void advectNodeFieldOff(Particle* source, GridNode* dest, SlurmDouble w) {}

  /// Obsolete?
  static void advectNodeFieldDefault(Particle* source, GridNode* dest, SlurmDouble w) 
  {
    dest->inc_A(source->get_Ax() * w, source->get_Ay() * w, source->get_Az() * w);
  }

  /// Currently used method which advects magnetic field to the node
  void (*advectNodeField)(Particle *, GridNode *, SlurmDouble);

  /// Formally add to the specified node quantities
  virtual void advectToGridNode(Particle* source, GridNode* dest, SlurmDouble w, unsigned long quants);

  /// Formally add the node quantities that are used in interpolateParticles
  virtual void advectToGridNode(Particle* source, GridNode* dest, SlurmDouble w);

  /// Returns the scheme by name
  static InterpolationSchemes getSchemeByName(string name);

  /// Returns the name of the scheme
  static string getInterpolationSchemeName(InterpolationSchemes scheme);

  /// Factory
  static Interpolator * makeInterpolator(GridCommon * grid, InterpolationSchemes scheme, MFEvolutionMethods MFEvolution);

  /// Factory
  static Interpolator * makeInterpolator(GridCommon * grid, string scheme, MFEvolutionMethods MFEvolution);

  /// Constructor
  Interpolator(GridCommon *grid) : grid(grid)
  {
    advectNodeField = &Interpolator::advectNodeFieldDefault;
  }
  /// Destructor
  virtual ~Interpolator() {}
};


/**
  Traditional, old linear interpolator.
*/
class LinearInterpolator : public Interpolator
{

public:

  /// Constructor
  LinearInterpolator(GridCommon *grid): Interpolator(grid)
  {
    set_InterpolNodesNumber(InterpolWeightsNumberLinear);
    set_InterpolCellsNumber(InterpolWeightsNumberLinear);
  };

  virtual void getInterpolationCells(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[]) 
  {
    getInterpolationCellsLinear(p, adj_cells, weights);
  };

  /// A general routine that interpolates the specified quantities onto grid nodes.
  virtual GridNode* interpolateNodes(GridPoint* p, unsigned long quants);

  /// Interpolates the default node quantities when magnetic field evolution is off. 
  virtual GridNode* interpolateNodes(GridPoint* p);

};


/// Adds vector potential interpolation from nodes to particles
class LinearInterpolatorMHD : public LinearInterpolator
{

public:

  /// Constructor
  LinearInterpolatorMHD(GridCommon *grid): LinearInterpolator(grid) {};

  /// Interpolates the default node quantities when magnetic field evolution is used.
  virtual GridNode* interpolateNodes(GridPoint* p);
};


/** 
  Linear Interpolator which tracks neighbor particles (mfemInterpolateParticles).
*/
class LinearInterpolatorIP : public LinearInterpolatorMHD 
{
public:
  /// Constructor
  LinearInterpolatorIP(GridCommon *grid): LinearInterpolatorMHD(grid) {};

  /// Advect from the given particle onto adjacent 8 cells and 8 nodes (in 3D)
  void advectToGrid(Particle* p);
};

/** 
  Linear Interpolator with mfemAdvectParticles. Change the way A is advected. 
*/
class LinearInterpolatorAP : public LinearInterpolatorMHD
{
public:

  /// Constructor
  LinearInterpolatorAP(GridCommon *grid): LinearInterpolatorMHD(grid) 
  {
    advectNodeField = &Interpolator::advectNodeFieldAP;
  };
};

/** Quadratic Interpolator with mfemOff */
class QuadraticInterpolatorHD : public LinearInterpolator
{
public:
  /// Constructor
  QuadraticInterpolatorHD(GridCommon *grid): LinearInterpolator(grid) 
  {
    set_InterpolCellsNumber(InterpolWeightsNumberQuadratic);
    set_InterpolNodesNumber(InterpolWeightsNumberLinear);
  };

  virtual void getInterpolationCells(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[]) 
  {
    getInterpolationCellsQuadratic(p, adj_cells, weights);
  };
};

/** Quadratic Interpolator with mfemInterpolateParticles */
class QuadraticInterpolatorIP : public LinearInterpolatorIP
{
public:
  /// Constructor
  QuadraticInterpolatorIP(GridCommon *grid): LinearInterpolatorIP(grid) 
  {
    set_InterpolCellsNumber(InterpolWeightsNumberQuadratic);
    set_InterpolNodesNumber(InterpolWeightsNumberLinear);
  };

  virtual void getInterpolationCells(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[]) 
  {
    Interpolator::getInterpolationCellsQuadratic(p, adj_cells, weights);
  };
};


/** Quadratic Interpolator with mfemAdvectParticles */
class QuadraticInterpolatorAP : public LinearInterpolatorAP
{
public:

  /// Constructor
  QuadraticInterpolatorAP(GridCommon *grid): LinearInterpolatorAP(grid) 
  {
    set_InterpolCellsNumber(InterpolWeightsNumberQuadratic);
    set_InterpolNodesNumber(InterpolWeightsNumberLinear);
  };

  virtual void getInterpolationCells(GridPoint* p, GridCell* adj_cells[], SlurmDouble weights[]) 
  {
    Interpolator::getInterpolationCellsQuadratic(p, adj_cells, weights);
  };
};

/** 
  Quadratic Interpolator which tracks neighbor particles (mfemInterpolateParticles).
  And uses interpolation functionality implemented in gri cells
*/
class QuadraticInterpolatorCC : public LinearInterpolatorMHD 
{
public:
  /// Constructor
  QuadraticInterpolatorCC(GridCommon *grid);

  /// Advect from the given particle onto adjacent 8 cells and 8 nodes (in 3D)
  void advectToGrid(Particle* p);

  /// Use cells to interpolates the quantities during particle advancement. 
  GridCell* interpolateCells(GridPoint* p);

  /// Use cells to interpolates the quantities during particle advancement. 
  GridCell* interpolateCells(GridPoint* p, unsigned long quants);
};
