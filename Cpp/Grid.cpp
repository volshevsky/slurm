#include "Grid.h"

using namespace std;

/** 
  The constructor should set the geometry and physics, e.g., gamma and gravity.
*/
Grid::Grid(MFEvolutionMethods MFEvolution, ConfigFile * config, InitialCondition * initial, Fields * fields) : initial(initial), fields(fields)
{
  // Locks
  omp_init_lock(&InternalEnergyLock);
  omp_init_lock(&KineticEnergyLock);
  omp_init_lock(&MagneticEnergyLock);

  // Magnetic field evolution: HD or MHD?
  if (MFEvolution == mfemOff) 
    FieldsEnabled = false;
  if ((MFEvolution == mfemInterpolateParticles) || (MFEvolution == mfemPreserveParticles)) 
    TrackNodeNeighborParticles = true;

  // Volume evolution method
  string sVolumeEvolution = config->read<string>("VolumeEvolution", "MaterialPoint");
  messageline("Volume evolution strategy: ", sVolumeEvolution);
  this->VolumeEvolution = vemMaterialPoint;
  if (boost::iequals(sVolumeEvolution, "Off")) 
    this->VolumeEvolution = vemOff;
  else if (boost::iequals(sVolumeEvolution, "VelocityGradient")) 
    this->VolumeEvolution = vemVelocityGradient;

  // Geometry
  this->ncx = config->read<SlurmInt>("GridCellsX");
  this->ncy = config->read<SlurmInt>("GridCellsY");
  this->ncz = config->read<SlurmInt>("GridCellsZ");
  this->Lx = config->read<SlurmDouble>("Lx");
  this->Ly = config->read<SlurmDouble>("Ly");
  this->Lz = config->read<SlurmDouble>("Lz", 1.);

  // Number of nodes
  message("Number of cells: ", ncx);
  message(" ", ncy);
  messageline(" ", ncz);
  this->nnx = this->ncx + 1;
  this->nny = this->ncy + 1;
  this->nnz = this->ncz + 1;

  // Grid resolution
  this->dx = this->Lx / SlurmDouble(this->ncx);
  this->dy = this->Ly / SlurmDouble(this->ncy);
  this->dz = this->Lz / SlurmDouble(this->ncz);
  this->dxInv = 1. / this->dx;
  this->dyInv = 1. / this->dy;
  this->dzInv = 1. / this->dz;

  // Read "physical" viscosity coefficients
  KinematicShearViscosity = config->read<SlurmDouble>("KinematicShearViscosity", KinematicShearViscosity);
  KinematicBulkViscosity = config->read<SlurmDouble>("KinematicBulkViscosity", KinematicBulkViscosity);
  messageline("Shear viscosity = ", KinematicShearViscosity);
  messageline("Bulk viscosity = ", KinematicBulkViscosity);

  // Read artificial viscosity type   
  string sArtificialViscosity = config->read<string>("ArtificialViscosity", "Kuropatenko");
  messageline("Artificial viscosity : ", sArtificialViscosity);
  this->ArtificialViscosity = avmOff;
  if (boost::iequals(sArtificialViscosity, "Kuropatenko")) 
    this->ArtificialViscosity = avmKuropatenko;
  else 
    this->ArtificialViscosity = avmOff;

  // Initialize boundary conditions
  this->initBoundaryConditions(config, initial);

  // Initiate the node array and cell array (including ghost cells). No ghost nodes!
  NodeArray::extent_gen node_extents;
  nodes.resize(node_extents[NodeExtentRange(-1, nnx + 1)][NodeExtentRange(-1, nny + 1)][NodeExtentRange(-1, nnz + 1)]);
  //nodes.resize(boost::extents[nnx][nny][nnz]);
  CellArray::extent_gen cell_extents;
  cells.resize(cell_extents[CellExtentRange(-1, ncx + 1)][CellExtentRange(-1, ncy + 1)][CellExtentRange(-1, ncz + 1)]);
 
  // Cycle over all cells, including ghosts and set initial values
  for (CellArray::index ic = -1; ic < ncx + 1; ++ic) 
  for (CellArray::index jc = -1; jc < ncy + 1; ++jc)
  for (CellArray::index kc = -1; kc < ncz + 1; ++kc)
  {
    GridCell* c = &cells[ic][jc][kc];

    c->set_ijk(ic, jc, kc);
    c->set_xyz(dx * (0.5 + SlurmDouble(ic)), dy * (0.5 + SlurmDouble(jc)), dz * (0.5 + SlurmDouble(kc)));
    c->set_dxyz(dx, dy, dz);

    // Distribute roles. In "3D-2D" case when we'll have only 1 layer of cells in Z. All nodes will be boundary??
    if ((ic < 0) || (ic >= ncx) || (jc < 0) || (jc >= ncy) || (kc < 0) || (kc >= ncz))
      c->set_role(gerGhost);
    else if ((ic == 0) || (ic == ncx - 1) || (jc == 0) || (jc == ncy - 1) || (kc == 0) || (kc == ncz - 1))
      c->set_role(gerBoundary);

    // Connectivity. Neighbor cells are not used?
    initCellNeighbors(c);
  }

  // Init quadratic interpolation stuff
  initCellsInterpolationStuff();

  // For now on, there are no ghost nodes, and we can use the define for_node_indices
  for (NodeArray::index in = -1; in < nnx + 1; ++in) 
  for (NodeArray::index jn = -1; jn < nny + 1; ++jn)
  for (NodeArray::index kn = -1; kn < nnz + 1; ++kn)
  {
    GridNode* n = &nodes[in][jn][kn];
  
    n->set_ijk(in, jn, kn);
    n->set_xyz(dx * SlurmDouble(in), dy * SlurmDouble(jn), dz * SlurmDouble(kn));
    n->set_dxyz(dx, dy, dz);

    // Node's role. In the 3D case with only 1 layer of cells in Z, will all nodes be the boundary ones?
    if ((in < 0) || (in > nnx - 1) || (jn < 0) || (jn > nny - 1) || (kn < 0) || (kn > nnz - 1))
    {
      n->set_role(gerGhost);
      // For corner nodes, all three BCs are set...
      if (in < 0) 
        n->set_bc(0, get_bcx0());
      else if (in > nnx-1) 
        n->set_bc(0, get_bcx1());
      if (jn < 0) 
        n->set_bc(1, get_bcy0());
      else if (jn > nny-1) 
        n->set_bc(1, get_bcy1());
      if (kn < 0) 
        n->set_bc(2, get_bcz0());
      else if (kn > nnz-1) 
        n->set_bc(2, get_bcz1());
    }
    else if ((in == 0) || (in == nnx - 1) || (jn == 0) || (jn == nny - 1) || (kn == 0) || (kn == nnz - 1))
    {
      n->set_role(gerBoundary);
      // For corner nodes, all three BCs are set...
      if (in == 0) 
        n->set_bc(0, get_bcx0());
      else if (in == nnx-1) 
        n->set_bc(0, get_bcx1());
      if (jn == 0) 
        n->set_bc(1, get_bcy0());
      else if (jn == nny-1) 
        n->set_bc(1, get_bcy1());
      if (kn == 0) 
        n->set_bc(2, get_bcz0());
      else if (kn == nnz-1) 
        n->set_bc(2, get_bcz1());
    }

    // Connectivity
    initNodeNeighbors(n);
  }

  // Add a sphere and override ce
  if ((initial->get_name()).find("SolarWind") == 0)
  {
    BoundaryCondition * bc_sphere = BoundaryCondition::makeBoundaryCondition(config, "Sphere", 0, 0, Lz - 0.5 * dz, 0, initial);
    add_bc(bc_sphere);
    for_cells
    {
      if (bc_sphere->cellIsGhost(c))
      {
        c->set_role(gerGhost);
        for (SlurmInt in = 0; in < 8; in++)
        {
          GridNode * n = c->get_nn(in);
          n->set_role(gerGhost);
          n->set_bc(0, bc_sphere);
        }
      }
    }
  }

  // Initial condition
  for_nodes
    initial->initNode(n);

  for_cells
  {
    initial->initCell(c);
  }

  prepareCellFields(0.);

  messageline("\nGrid: Grid has been created.\n");
}

/** Interpolate magnetic field according to the MF evolution strategy. */
void Grid::interpolateVectorPotential()
{
  // Tracking neighbor particles is a job of the grid object, not of the boundary condition.
  updateBoundaryNeighborParticles();

  // Fetch vector potential from particles/prepare it for update. The actual advancement happens after cells are advanced.
  // Also here the currents are computed
  if (FieldsEnabled) 
  {
    for_nodes
      if (n->get_role() != gerGhost)
        fields->prepareNodeFields(n, get_bcx1(), get_bcy1(), get_bcz1());
  }
}

/** 
  Compute magnetic field B from vector potential A; 
    B = [nabla x A]
  Compute currents; 
    J = [nabla x B]
  Impose the corresponding BC. 
  Compute div(B) on all nodes.

  This routine is called after BCs on the nodes' A have been imposed.
*/
void Grid::prepareCellFields(SlurmDouble t)
{
  if (!FieldsEnabled)
    return;

  // Gradients of the node quantities: cell centered
  GridNode* gradCCx = new GridNode();
  GridNode* gradCCy = new GridNode();
  GridNode* gradCCz = new GridNode();

  // Compute magnetic field from vector potential
  for_cells
  {
    // normalize interpolated vector potential
    c->set_phi(c->get_phi() / c->get_vol());

    // Compute the directional derivs of A
    c->DirectionalDeriv(QUANT_MAGNETIC, gradCCx, gradCCy, gradCCz);
    // Vector potential gradients
    c->set_gradA(gradCCx, gradCCy, gradCCz);

    // B = nabla x A
    c->set_B(c->get_gradA32() - c->get_gradA23(), c->get_gradA13() - c->get_gradA31(), c->get_gradA21() - c->get_gradA12());

    if (fields->get_ExternalFields())
      fields->get_ExternalFields()->addCellExternalFields(c);
  }

  // Impose BC on magnetic field, and on energy with resistive contribution
  //imposeBCCells(QUANT_MAGNETIC);

  // Currents J = [nabla x B]
  GridCell* gradNCx = new GridCell();
  GridCell* gradNCy = new GridCell();
  GridCell* gradNCz = new GridCell();

  for_nodes
    if (n->get_role() != gerGhost)
    {
      n->DirectionalDeriv(QUANT_MAGNETIC, gradNCx, gradNCy, gradNCz);
      n->set_J(gradNCy->get_Bz() - gradNCz->get_By(), gradNCz->get_Bx() - gradNCx->get_Bz(), gradNCx->get_By() - gradNCy->get_Bx());
    }

  computeDivB();

  // BC on the newly computed currents
  imposeBCNodes(QUANT_CURRENT, t);

  // Cleanup
  delete gradCCx;
  delete gradCCy;
  delete gradCCz;
  delete gradNCx;
  delete gradNCy;
  delete gradNCz;
}

/** 
  Advance the grid by one time step.
  Mind the sequence:
    1. Velocity and electromagnetic vector potential are updated on the nodes.
    2. On cells most quantities use new velocities:
       - e
       - Jacobian
       - Density
       - Magnetic field
    3. On nodes, update the vector potential.
    4. On cells and nodes, prepare the "old" quantities for interpolation on particles

  Equations
    de/dt = -(p + artificial_viscosity)*grad(u) + resistivity*|[nabla B]|^2
    dA/dt = uxB + (u nabla)A + E_external - resistivity*[nabla B]
*/
void Grid::advanceExplicit(SlurmDouble t, SlurmDouble dt)
{
  // Cell-Centered gradients of node quantities.
  GridNode* gradCCx = new GridNode();
  GridNode* gradCCy = new GridNode();
  GridNode* gradCCz = new GridNode();
  // Node-centered gradients of cell quantities.
  GridCell* gradNCx = new GridCell();
  GridCell* gradNCy = new GridCell();
  GridCell* gradNCz = new GridCell();
  // Averaged cell
  GridCell* avgc = new GridCell();
  GridNode* avgn = new GridNode();
  // Helper variable for advancing nodes, dt/m
  SlurmDouble dtrho = 0.;
  
  // Save data to the "old" quantities before advancing
  saveToOld();

  // Compute B from A; compute currents; impose BC on the fields; compute divB.
  prepareCellFields(t);

  // Compute viscosity and velocity gradients. It includes both bulk and shear viscosities.
  computeViscosity();

  // Advance node quantities: velocity; and precompute vector potential.
  for_nodes
  {
    if (n->get_role() != gerGhost)
    {
      /*
      // The old way of velocity equation, without stress tensor
      // The QUANT_PMAG enables computation of the derivatives of Bx**2, By**2, Bz**2 which will be saved to B2.
      n->DirectionalDeriv(QUANT_MAGNETIC | QUANT_PRESS | QUANT_PMAG | QUANT_VISCOSITY, gradNCx, gradNCy, gradNCz);
      n->averageCells(QUANT_MAGNETIC, avgc);
     
      // Advance velocity
      dtrho = dt / n->get_rho();
      n->inc_u(dtrho * (-(gradNCx->get_p() + gradNCx->get_visc() + 0.5 * (gradNCx->get_By2() + gradNCx->get_Bz2())) + avgc->get_By() * gradNCy->get_Bx() + avgc->get_Bz() * gradNCz->get_Bx()),
               dtrho * (-(gradNCy->get_p() + gradNCy->get_visc() + 0.5 * (gradNCy->get_Bx2() + gradNCy->get_Bz2())) + avgc->get_Bx() * gradNCx->get_By() + avgc->get_Bz() * gradNCz->get_By()),
               dtrho * (-(gradNCz->get_p() + gradNCz->get_visc() + 0.5 * (gradNCz->get_Bx2() + gradNCz->get_By2())) + avgc->get_Bx() * gradNCx->get_Bz() + avgc->get_By() * gradNCy->get_Bz()));
      */

      /// New way of updating velocity using stress tensor
      n->DirectionalDerivStress(gradNCx, gradNCy, gradNCz);
      dtrho = dt / n->get_rho();
      n->inc_u(dtrho * (gradNCx->get_stress11() + gradNCy->get_stress12() + gradNCz->get_stress13()),
               dtrho * (gradNCx->get_stress21() + gradNCy->get_stress22() + gradNCz->get_stress23()),
               dtrho * (gradNCx->get_stress31() + gradNCy->get_stress32() + gradNCz->get_stress33()));

      /// end of "new way"

      // Add gravity if needed
      initial->addGravity(n, dt);
     
      // A change of velocity, to be interpolated on particles
      n->update_du();
    }
  }

  // Adjust speeds according to BC     
  adjustBCNodes(t);

  // Set energy counters to zero
  set_InternalEnergy(0);
  set_MagneticEnergy(0);
  set_KineticEnergy(0);

  // Advance cell quantities: e, rho; compute B from A; compute gradu.
  for_cells
  {
    if (c->get_role() != gerGhost)
    {
      dtrho = dt / c->get_rho();
      SlurmDouble de = 0;

      // Get the velocity derivatives
      c->DirectionalDeriv(QUANT_VELOCITY, gradCCx, gradCCy, gradCCz);
      c->set_gradu(gradCCx, gradCCy, gradCCz);
    
      // Add resistive contribution to cell's energy, +eta*<J>^2
      //if (FieldsEnabled)
      //{
        //TODO: this creates a new average node object each time!
        //fields->addCellResistivity(c);

        c->averageNodes(QUANT_CURRENT, avgn);
        de = fields->get_Resistivity() * (pow(avgn->get_Jx(), 2) + pow(avgn->get_Jy(), 2) + pow(avgn->get_Jz(), 2));

        //fields->advanceCellScalarPotential(c, dt);

        // Advance scalar potential for cleaning div(A). What should be the 'smoothing length'? dx or dxp?
        SlurmDouble cs2 = c->get_p() / c->get_rho();
        c->set_dphi(dt * (-cs2 * c->get_divA() - pow(cs2, 0.5) * 5 * c->get_phi() / c->get_dx())); // - 0.5 * c->get_phi() * c->get_divu()));
        c->inc_phi(c->get_dphi()); 
      //}

      // Add hydrodynamic contribution
      SlurmDouble divu = c->get_divu();
      de -= (c->get_p() + c->get_visc() + KinematicBulkViscosity*c->get_rho()*divu) * divu + 2 * KinematicShearViscosity * c->get_rho() * pow(c->get_gradu12() + c->get_gradu21() + c->get_gradu13() + c->get_gradu31() + c->get_gradu23() + c->get_gradu32(), 2);

      // Update cell's energy change - to be interpolated to particles
      c->set_de(de / (c->get_rho() * c->get_eold()));

      // Increase cell's energy. Is it really needed? Only de is used by particles, isn't it? Well, maybe to compute the total energies...
      c->inc_e(de * dtrho);
        
      // Total energies
      inc_InternalEnergy(c->get_InternalEnergy());   
      inc_MagneticEnergy(c->get_MagneticEnergy());
      inc_KineticEnergy(c->get_KineticEnergy());
    }
  }

  // Impose BC on the quantities that will be projected to particles: e, de.
  // Impose BC on magnetic field which is used to advance node's vector potential, to be projected on particles.
  // BC on the stress tensor (to be interpolated on particles) was imposed when imposing BC on viscosity, see cell's copyFrom().
  imposeBCCells(QUANT_ENERGY | QUANT_MAGNETIC);

  // Advance vector potential; copy velocity advance to the "old" velocity. Impose BCs on the nodes.
  if (FieldsEnabled) 
  {
    // Advance vector potential only on the nodes inside the domain, and on boundary nodes, because BCs for B were done; then impose BC?
    for_nodes
      if (n->get_role() != gerGhost)
      {
        //TODO: this computes gradphi and creates three objects each time!
        fields->advanceNodeVectorPotential(n, t, dt);
      }
    imposeBCNodes(QUANT_MAGNETIC, t);
  }

  delete gradCCx;
  delete gradCCy;
  delete gradCCz;
  delete gradNCx;
  delete gradNCy;
  delete gradNCz;
  delete avgc;
  delete avgn;
}

/** Look for the closest nodes and update their neighbor particles accordingly */
void Grid::updateParticleNodeNeighbors(Particle* p)
{
  GridCell* cc = getClosestCell(p);      
  for (SlurmInt i = 0; i < 8; i++)
    cc->get_nn(i)->updateNeighborParticle(p);
}

/** Retrieve the node that is closest to the specified point */
GridNode* Grid::getClosestNode(GridPoint* p)
{
  //This check should be time-consuming!
  /*if ((p->get_x() < bcx0.end) || (p->get_y() < bcy0.end) || (p->get_z() < bcz0.end) || (p->get_x() > bcx1.end) || (p->get_y() > bcy1.end) || (p->get_z() > bcz1.end))
  {
    throw SLURM_ERROR_OUT_OF_BOUNDS;
  }*/

  return &nodes[SlurmInt(round(p->get_x() * dxInv))][SlurmInt(round(p->get_y() * dyInv))][SlurmInt(round(p->get_z() * dzInv))];
}

/** Retrieve the cell that includes the specified point */
GridCell* Grid::getClosestCell(GridPoint* p)
{
  // Check if particle's inside X bounds
  SlurmInt i = SlurmInt(floor(p->get_x() * dxInv)); // left X
  // Sometimes the particle is exactly at the boundary? Then return the index of the cell inside the main domain
  if (p->get_x() == get_bcx1()->get_end())
    i = ncx - 1;

  // Check if particle's inside Y bounds
  SlurmInt j = SlurmInt(floor(p->get_y() * dyInv)); // bottom Y
  if (p->get_y() == get_bcy1()->get_end())
    j = ncy - 1;

  // Check if particle's inside Z bounds
  SlurmInt k = SlurmInt(floor(p->get_z() * dzInv)); // bottom Y
  if (p->get_z() == get_bcz1()->get_end())
    k = ncz - 1;

  return &cells[i][j][k];
}

/** Fills all cells with the specified value for the selected quantities. */
void Grid::fillCells(SlurmDouble value, unsigned long quants)
{
  for_cells
    c->fill_quants(value, quants);
}

/** Fills all nodes with the specified value for the selected quantities. */
void Grid::fillNodes(SlurmDouble value, unsigned long quants)
{
  for_nodes
    n->fill_quants(value, quants);
}

/** 
  Fills default cell quantities. 
  QUANT_ENERGY | QUANT_MASS | QUANT_VOL | QUANT_VX | QUANT_VY | QUANT_VZ | QUANT_WEIGHT | QUANT_MAGNX | QUANT_MAGNY | QUANT_MAGNZ
*/
void Grid::fillZeros()
{
  for_cells
  {
    // Save quantities to old to be able to retrieve them in case no particles in cell
    c->saveToOld();
    c->fill_zeros();
  }
  for_nodes
  {
    // Save quantities to old to be able to retrieve them in case no particles in node
    n->saveToOld();
    n->saveToOldA();
    n->fill_zeros();
  }
}

/** 
  Normalize quantities interpolated from particles. 
  Use this for setting the equation of state.
*/
void Grid::normalizeCells()
{
  for_cells
  {
    // Suppress volume evolution when needed
    //TODO: replace by local dx, dy, dz?
    if (VolumeEvolution == vemOff)
      c->set_vol(dx * dy * dz);

    // When m == 0 it means no particles project into this cell.
    if (c->get_m() > 0)
    {
      // Get energy density from energy because we store energy_density on the grid, and energy on particles
      c->set_e(c->get_e() / c->get_m());        

      // Get mass density from mass & volume
      c->set_rho(c->get_m() / c->get_vol());

      // Apply equation of state
      initial->computePressure(c);
    }
    else if (c->get_role() != gerGhost)
    {
      // Empty boundary cells MUST be filled in here, because afterwards wrong values will be copied to their ghosts.
      // Retrieve from old: rho has not been zeroed; therefore retrieve energy and compute pressure.
      c->set_e(c->get_eold());
      c->set_vol(dx * dy * dz);
      c->set_m(c->get_rho() * c->get_vol());

      // Apply isothermal equation of state to prevent negative pressures.
      initial->computePressureIsothermal(c);
    }
  }
}

/** Normalize quantities interpolated from particles, e.g., momentum -> mass. */
void Grid::normalizeNodes()
{
  for_nodes
  {
    if (VolumeEvolution == vemOff)
      n->set_vol(dx * dy * dz);

    // If m == 0 it means no particles project into this node.
    if (n->get_m() > 0)
    {
      n->set_rho(n->get_m() / n->get_vol());

      // Momentum -> velocity
      for (int i = 0; i < 3; i++) 
        n->set_u(i, n->get_u(i) / n->get_m());
    }
    else if (n->get_role() != gerGhost)
    {
      // Empty boundary nodes MUST be filled in here, because afterwards wrong values will be copied to their ghosts.
      n->set_A(n->get_Aold());

      n->set_vol(dx * dy * dz);
      n->set_m(n->get_rho() * n->get_vol());
      n->set_u(n->get_uold());
    }
  }
}

/** 
 * Run after BC to check for empty cells and nodes and do something about them. 
 * This routine adds two more cycles over all nodes and cells, and is only needed in problems where empty cells/nodes can form.
 * There is not much physics in here, only some heuristics.
*/
void Grid::fillEmptyElements()
{
  SlurmInt empty_nodes = 0;

  for_nodes
  {
    // If m == 0 it means no particles project into this node.
    if (n->get_m() > 0)
    {
      // Do nothing
    }
    else
    {
      // The density has not been zeroed, therefore it stays as old. Retrieve vector potential.
      n->set_A(n->get_Aold());

      n->set_vol(dx * dy * dz);
      n->set_m(n->get_rho() * n->get_vol());
      n->set_u(n->get_uold());

      if (n->get_role() != gerGhost)
        ++empty_nodes;
    }
  }

  SlurmInt empty_cells = 0;

  for_cells
  {
    // When m == 0 it means no particles project into this cell.
    if (c->get_m() > 0)
    {
      // Do nothing
    }
    else
    {
      // Retrieve from old: rho has not been zeroed; therefore retrieve energy and compute pressure.
      c->set_e(c->get_eold());
      c->set_vol(dx * dy * dz);
      c->set_m(c->get_rho() * c->get_vol());

      // Apply equation of state
      initial->computePressureIsothermal(c);

      if (c->get_role() != gerGhost)
        ++empty_cells;
    }
  }

  if (empty_nodes > 0)
    messageline("Empty nodes: ", empty_nodes);
  if (empty_cells > 0)
    messageline("Empty cells: ", empty_cells);
}

/** 
  This method should be called BEFORE adding any user-defined boundary condition.
  It is implicitly assumed that the first 6 items in the boundary_conditions are the walls of the rectangular domain!
*/
void Grid::initBoundaryConditions(ConfigFile * config, InitialCondition * initial)
{
  messageline("\nCreating boundary conditions");
  message("BC X left: ");
  add_bc(BoundaryCondition::makeBoundaryCondition(config, config->read<string>("BoundaryConditionX", "periodic"), 0, 0, 0.5 * dx, 0., initial));
  message("BC X right: ");
  add_bc(BoundaryCondition::makeBoundaryCondition(config, config->read<string>("BoundaryConditionX1", config->read<string>("BoundaryConditionX", "periodic")), 1, 0, Lx - 0.5 * dx, Lx, initial));
  message("BC Y left: ");
  add_bc(BoundaryCondition::makeBoundaryCondition(config, config->read<string>("BoundaryConditionY", "periodic"), 0, 1, 0.5 * dy, 0., initial));  
  message("BC Y right: ");
  add_bc(BoundaryCondition::makeBoundaryCondition(config, config->read<string>("BoundaryConditionY1", config->read<string>("BoundaryConditionY", "periodic")), 1, 1, Ly - 0.5 * dy, Ly, initial));
  message("BC Z left: ");
  add_bc(BoundaryCondition::makeBoundaryCondition(config, config->read<string>("BoundaryConditionZ", "periodic"), 0, 2, 0.5 * dz, 0., initial));
  message("BC Z right: ");
  add_bc(BoundaryCondition::makeBoundaryCondition(config, config->read<string>("BoundaryConditionZ1", config->read<string>("BoundaryConditionZ", "periodic")), 1, 2, Lz - 0.5 * dz, Lz, initial));
}

/** 
  Track neighbor particles in case of periodic BCs.
  Really, in other BCs it is trivial.
*/
void Grid::adjustNodeNeighborParticles(GridNode * n)
{
  GridNode *nl;

  SlurmInt i = n->get_i();
  SlurmInt j = n->get_j();
  SlurmInt k = n->get_k();
  if ((n->get_bc(0)) && (n->get_bc(0)->get_type() == btPeriodic))
  {
    // Corner nodes need to be set up first
    if ((n->get_bc(1)) && (n->get_bc(1)->get_type() == btPeriodic) && (n->get_bc(2)) && (n->get_bc(2)->get_type() == btPeriodic))
    {
      // (_p000, _p100, _p010, _p110, _p001, _p101, _p011, _p111)
      n->set_ptcls(nodes[nnx-1][nny-1][nnz-1].get_p000(), nodes[0][nny-1][nnz-1].get_p100(), nodes[nnx-1][0][nnz-1].get_p010(), nodes[0][0][nnz-1].get_p110(), nodes[nnx-1][nny-1][0].get_p001(), nodes[0][nny-1][0].get_p101(), nodes[nnx-1][0][0].get_p011(), nodes[0][0][0].get_p111());
    }
    // vertical ridges
    else if ((n->get_bc(1)) && (n->get_bc(1)->get_type() == btPeriodic))
    {
      n->set_ptcls(nodes[nnx-1][nny-1][k].get_p000(), nodes[0][nny-1][k].get_p100(), nodes[nnx-1][0][k].get_p010(), nodes[0][0][k].get_p110(), nodes[nnx-1][nny-1][k].get_p001(), nodes[0][nny-1][k].get_p101(), nodes[nnx-1][0][k].get_p011(), nodes[0][0][k].get_p111());
    }
    // horizontal ridges
    else if ((n->get_bc(2)) && (n->get_bc(2)->get_type() == btPeriodic))
    {
      n->set_ptcls(nodes[nnx-1][j][nnz-1].get_p000(), nodes[0][j][nnz-1].get_p100(), nodes[nnx-1][j][nnz-1].get_p010(), nodes[0][j][nnz-1].get_p110(), nodes[nnx-1][j][0].get_p001(), nodes[0][j][0].get_p101(), nodes[nnx-1][j][0].get_p011(), nodes[0][j][0].get_p111());
    }
    else
    {
      nl = &nodes[0][j][k];
      n->set_p100(nl->get_p100());
      n->set_p110(nl->get_p110());
      n->set_p101(nl->get_p101());
      n->set_p111(nl->get_p111());
      nl = &nodes[nnx-1][j][k];
      n->set_p000(nl->get_p000());
      n->set_p010(nl->get_p010());
      n->set_p001(nl->get_p001());
      n->set_p011(nl->get_p011());            
    }
  }
  else if ((n->get_bc(1)) && (n->get_bc(1)->get_type() == btPeriodic))
  {
    // Corners have already been set. Only set the last 4 ridges.
    if ((n->get_bc(2)) && (n->get_bc(2)->get_type() == btPeriodic))
    {
      // (_p000, _p100, _p010, _p110, _p001, _p101, _p011, _p111)
      n->set_ptcls(nodes[i][nny-1][nnz-1].get_p000(), nodes[i][nny-1][nnz-1].get_p100(), nodes[i][0][nnz-1].get_p010(), nodes[i][0][nnz-1].get_p110(), nodes[i][nny-1][0].get_p001(), nodes[i][nny-1][0].get_p101(), nodes[i][0][0].get_p011(), nodes[i][0][0].get_p111());
    }
    else
    {
      nl = &nodes[i][nny-1][k];
      n->set_p000(nl->get_p000());
      n->set_p001(nl->get_p001());
      n->set_p101(nl->get_p101());
      n->set_p100(nl->get_p100());
      nl = &nodes[i][0][k];
      n->set_p010(nl->get_p010());
      n->set_p011(nl->get_p011());
      n->set_p111(nl->get_p111());
      n->set_p110(nl->get_p110());
    }
  }
  else if ((n->get_bc(2)) && (n->get_bc(2)->get_type() == btPeriodic))
  {
    // All corners and ridges have been set?
    nl = &nodes[i][j][nnz-1];
    n->set_p000(nl->get_p000());
    n->set_p010(nl->get_p010());
    n->set_p100(nl->get_p100());
    n->set_p110(nl->get_p110());
    nl = &nodes[i][j][0];
    n->set_p001(nl->get_p001());
    n->set_p011(nl->get_p011());
    n->set_p101(nl->get_p101());
    n->set_p111(nl->get_p111());
  }
}

/** 
  Copies necessary pointers to neighbor particles, e.g., after particles are initialized.
*/
void Grid::updateBoundaryNeighborParticles()
{
  if (TrackNodeNeighborParticles)
  {
    for_nodes
    {
      if (n->get_role() == gerBoundary)
        adjustNodeNeighborParticles(n);
    }
  }
}

/** 
  Impose boundary conditions on all (default) physical quantities.

  Boundary conditions are first applied to all boundary nodes, which is necessary for periodic BCs.
  
  Periodic (nodes only).
  Only impose periodic BCs on the nodes: copy from left to right.

  In Periodic BCs all corners are essentially the same node[0][0][0]
  Care about the X, Y order: the corners are copied for the X boundaries.

  Reflective (nodes and cells). 
  Zero gradients on the boundary nodes. 
  Normal components of velocity are zeros, therefore ghost cells are copied from the adjacent boundary cells.

*/
void Grid::imposeBoundaryConditions(SlurmDouble t)
{
  // Nodes
  for (vector<BoundaryCondition*>::iterator bc = boundary_conditions.begin(); bc != boundary_conditions.end(); ++bc)
  {
    // Impose on boundary nodes first, as for some nodes advNode could be a boundary node.
    for_nodes
    {
      if (n->get_role() == gerBoundary)
        for (SlurmInt j = 0; j < 3; j++)
          if (n->get_bc(j) == (*bc)) 
            (*bc)->imposeOnNode(n, t);
    }

    // Impose on ghost nodes next, as for some nodes advNode could be a boundary node.
    for_nodes
    {
      if (n->get_role() == gerGhost)
        for (SlurmInt j = 0; j < 3; j++)
          if (n->get_bc(j) == (*bc)) 
            (*bc)->imposeOnNode(n, t);
    }
  }

  // Fill ghost cells. The logical connectivity of the cells is set on init.
  // Note, that on periodic boundaries the ghost cells are not used to compute node-centered gradients. Therefore filling ghosts is not needed. But we do it...
  for (vector<BoundaryCondition*>::iterator bc = boundary_conditions.begin(); bc != boundary_conditions.end(); ++bc)
  {
    for_cells
    {
      if (c->get_role() == gerGhost) 
      {
        // TODO: this method is called before prepareCellFields(). Is it necessary to put BCs on the cells here?
        (*bc)->imposeOnCell(c, t);
        // We should compute B in all cases except periodic?
        //if (((*bc)->get_type() == btFixed) || ((*bc)->get_type() == btSphere) || ((*bc)->get_type() == btOutlet) || ((*bc)->get_type() == btSupersonicOutlet))
        //  fields->computeBfromA(c);
      }
    }
  }

  fillEmptyElements();
}

/** 
  Impose boundary condition on the given quantity.
  It is used, for instance, when artificial viscosity or magnetic field is computed.
*/
void Grid::imposeBCCells(unsigned long quants, SlurmDouble t)
{
  // Copy to the ghost nodes
  for_cells
    if (c->get_role() == gerGhost) 
      for (vector<BoundaryCondition*>::iterator bc = boundary_conditions.begin(); bc != boundary_conditions.end(); ++bc)
      {
        (*bc)->imposeOnCell(c, quants, t);
        // TODO: prepareCellFields walks over all cells and all nodes. Do we really need to impose BCs on fields here?
        if (((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC) && (((*bc)->get_type() == btFixed) || ((*bc)->get_type() == btSphere) || ((*bc)->get_type() == btOutlet) || ((*bc)->get_type() == btSupersonicOutlet)))
          fields->computeBfromA(c);
      }
}

/** 
  Impose node boundary condition on the given quantity.
  It is used applied when vector potential has been updated.
*/
void Grid::imposeBCNodes(unsigned long quants, SlurmDouble t)
{
  for (vector<BoundaryCondition*>::iterator bc = boundary_conditions.begin(); bc != boundary_conditions.end(); ++bc)
  {
    // Impose on boundary nodes first, as for some nodes advNode could be a boundary node.
    for_nodes
      if (n->get_role() == gerBoundary)
        for (SlurmInt j = 0; j < 3; j++)
          if (n->get_bc(j) == (*bc)) 
            (*bc)->imposeOnNode(n, quants, t);

    // Impose on ghost nodes next, as for some nodes advNode could be a boundary node.
    for_nodes
      if (n->get_role() == gerGhost)
        for (SlurmInt j = 0; j < 3; j++)
          if (n->get_bc(j) == (*bc)) 
            (*bc)->imposeOnNode(n, quants, t);
  }
}

/** 
  Adjust velocities on the boundaries after they've been updated.
*/
void Grid::adjustBCNodes(SlurmDouble t)
{
  for (vector<BoundaryCondition*>::iterator bc = boundary_conditions.begin(); bc != boundary_conditions.end(); ++bc)
  {
    // For some ghost nodes the advNode is a boundary node, therefore we need to fill boundary nodes first
    for_nodes
      if (n->get_role() == gerBoundary)
        for (SlurmInt j = 0; j < 3; j++)
          if (n->get_bc(j) == (*bc)) 
            (*bc)->adjustBoundaryNode(n, t);

    // For some ghost nodes the advNode is a boundary node, therefore we need to fill ghost nodes after the boundary ones
    for_nodes
      if (n->get_role() == gerGhost)
        for (SlurmInt j = 0; j < 3; j++)
          if (n->get_bc(j) == (*bc)) 
            (*bc)->adjustBoundaryNode(n, t);
  }
}


/** 
  Compute viscosity and velocity gradient at each cell prior to advancing cells.

  We use notation of Kundu & Kohen "Fluid Mechanics" 5th edition (2012), page 113, eqns 4.31, 4.34, 4.36.
  Viscosity consists of the bulk and shear viscosities so that Navier-Stokes equation is
  
    rho*Du_j/dt = -dp/dx_j + d/dx_i [ 2*mu*S_ij + (mu_v - 2/3mu) du_m/dx_m delta_ij],
  
  D/dt - convective derivative, 
  mu - dynamic shear viscosity, mu = KinematicShearViscosity * rho
  mu_v - dynamic bulk viscosity, mu_v = KinematicBulkViscosity * rho + artificialViscosity. Bulk viscosity is important for handling shocks.
  delta_ij - Kronecker's delta

  Artificial viscosities:
  - Kuropatenko. Taken from Caramana et al. (1997, JCP), Equation [8]. Originally by Kuropatenko (1967).
    Kuropatenko's c1 and c2 may be adjusted for different problems.

*/
void Grid::computeViscosity()
{  
  // Cell-Centered gradients of node quantities.
  GridNode* gradCCx = new GridNode();
  GridNode* gradCCy = new GridNode();
  GridNode* gradCCz = new GridNode();

  // Viscosity coefficients must be positive. If no viscosity, just compute the velocity gradient.
  if ((KinematicShearViscosity < TIISmallDOuble) && (KinematicBulkViscosity < TIISmallDOuble) && (ArtificialViscosity == avmOff))
  {
    for_cells
    {
      // Compute and set velocity derivatives
      c->DirectionalDeriv(QUANT_VELOCITY, gradCCx, gradCCy, gradCCz);    
      c->set_gradu(gradCCx, gradCCy, gradCCz);
      c->set_stress(KinematicShearViscosity * c->get_rho(), (KinematicBulkViscosity - 0.667 * KinematicShearViscosity) * c->get_rho());
    }
  }
  else
  {
    // Kuropatenko's formula coefficients. 
    // For all tests except Implosion, c1 = c2 = 1. For Implosion, 0.5 each.
    SlurmDouble c1 = pow(1.0, 2); // It will be used squared!!!
    SlurmDouble c2 = 1.0;
    // Introduce these coefficients for convenience
    SlurmDouble b1 = 0.25 * c2 * (initial->get_GasGamma() + 1);
    SlurmDouble b2 = b1 * b1;
    // Velocity variation used by Kuropatenko's
    SlurmDouble dv = 0;
    // Sound speed used by Kuropatenko's
    SlurmDouble cs = 0;
    
    // div(u)
    SlurmDouble divu = 0;
    
    // Compute viscosity at each cell
    for_cells
    {
      c->set_visc(0.);

      // Compute and set velocity derivatives
      c->DirectionalDeriv(QUANT_VELOCITY, gradCCx, gradCCy, gradCCz);    
      c->set_gradu(gradCCx, gradCCy, gradCCz);
      divu = c->get_divu();
    
      if (c->get_role() != gerGhost)
      {
        // Add artificial viscosity
        if (ArtificialViscosity == avmKuropatenko)
        {
          if (divu < 0)
          {
            // Isothermal sound speed squared
            cs = initial->get_GasGamma() * c->get_p() / c->get_rho(); 
            // Sum of velocity variations over the cell
            dv = abs(c->get_VelocityVariation());
            // Corresponds to Fabio's mu.
            c->set_visc(c->get_rho() * (b1 * dv + sqrt(b2 * dv * dv + c1 * cs)) * dv);
          }
        }
        // Shall we set stress for ghost cells??
        c->set_stress(KinematicShearViscosity * c->get_rho(), (KinematicBulkViscosity - 0.667 * KinematicShearViscosity) * c->get_rho());
      }
      
    }
    
    // Finally, copy viscosity and stress tensor to the ghost cells
    imposeBCCells(QUANT_VISCOSITY);
  }
  delete gradCCx;
  delete gradCCy;
  delete gradCCz;
}

/** 
  Compute divergence of B at all nodes.
  Has to be inside the Grid for performance (not to create/delete 3 gradient cells for each node).
*/
void Grid::computeDivB()
{
  // Node-centered gradients of cell quantities.
  GridCell* gradNCx = new GridCell();
  GridCell* gradNCy = new GridCell();
  GridCell* gradNCz = new GridCell();

  for_nodes
  {
    if (n->get_role() != gerGhost)
    {    
      n->DirectionalDeriv(QUANT_MAGNETIC, gradNCx, gradNCy, gradNCz);
      n->set_divB(gradNCx->get_Bx() + gradNCy->get_By() + gradNCz->get_Bz());    
    }
    else
      n->set_divB(0);
  }

  delete gradNCx;
  delete gradNCy;
  delete gradNCz;
}


/** 
  Set a specific node's neighbor particle p.
  dims - denote which neighbor should be set: 000, 100, 010, 110, etc. 
  i, j, k - the indices of the node to set.
*/
void Grid::set_NodeNeighborParticle(Particle* p, SlurmInt i, SlurmInt j, SlurmInt k, short dims)
{
  if (((dims & DIM_X) == DIM_X) && ((dims & DIM_Y) == DIM_Y) && ((dims & DIM_Z) == DIM_Z))
    nodes[i][j][k].set_p111(p);
  else if (((dims & DIM_X) == DIM_X) && ((dims & DIM_Y) == DIM_Y))
    nodes[i][j][k].set_p110(p);
  else if (((dims & DIM_X) == DIM_X) && ((dims & DIM_Z) == DIM_Z))
    nodes[i][j][k].set_p101(p);
  else if (((dims & DIM_Y) == DIM_Y) && ((dims & DIM_Z) == DIM_Z))
    nodes[i][j][k].set_p011(p);
  else if (((dims & DIM_X) == DIM_X))
    nodes[i][j][k].set_p100(p);
  else if (((dims & DIM_Y) == DIM_Y))
    nodes[i][j][k].set_p010(p);
  else if ((dims & DIM_Z) == DIM_Z)
    nodes[i][j][k].set_p001(p);
  else
    nodes[i][j][k].set_p000(p);
}


/** 
  Store certain quantities in the "old" ones, before time-advancing the grid. 
  - B, magnetization, velocity, rho, e: on the nodes and cell centers
  - Mass is not needed as it doesn't change                          
*/
void Grid::saveToOld()
{
  for_cells
    c->saveToOld();

  for_nodes
    n->saveToOld();
}

/** 
  Find the node's neighbor cells. Should be called only on initialization.
  This one accounts for the periodic boundary conditions, i.e., rightmost's node's right neighbor is the leftmost cell.
*/
void Grid::initNodeNeighbors(GridNode* n)
{
  // Default neighbor's indices
  SlurmInt i = n->get_i(), j = n->get_j(), k = n->get_k();
  SlurmInt i1 = i, j1 = j, k1 = k;
  SlurmInt i0 = i - 1, j0 = j - 1, k0 = k - 1;

  // Assign the 'true' neighbors which could be ghosts
  //set_nc(GridCell * _c000, GridCell * _c100, GridCell * _c010, GridCell * _c110, GridCell * _c001, GridCell * _c101, GridCell * _c011, GridCell * _c111) 
  //n->set_nc(&cells[i0][j0][k0], &cells[i1][j0][k0], &cells[i0][j1][k0], &cells[i1][j1][k0], &cells[i0][j0][k1], &cells[i1][j0][k1], &cells[i0][j1][k1], &cells[i1][j1][k1]);
  n->set_c000(((i0 >= -1) && (i0 <= ncx) && (j0 >= -1) && (j0 <= ncy) && (k0 >= -1) && (k0 <= ncz)) ? &cells[i0][j0][k0] : NULL);
  n->set_c100(((i1 >= -1) && (i1 <= ncx) && (j0 >= -1) && (j0 <= ncy) && (k0 >= -1) && (k0 <= ncz)) ? &cells[i1][j0][k0] : NULL);
  n->set_c010(((i0 >= -1) && (i0 <= ncx) && (j1 >= -1) && (j1 <= ncy) && (k0 >= -1) && (k0 <= ncz)) ? &cells[i0][j1][k0] : NULL);
  n->set_c110(((i1 >= -1) && (i1 <= ncx) && (j1 >= -1) && (j1 <= ncy) && (k0 >= -1) && (k0 <= ncz)) ? &cells[i1][j1][k0] : NULL);
  n->set_c001(((i0 >= -1) && (i0 <= ncx) && (j0 >= -1) && (j0 <= ncy) && (k1 >= -1) && (k1 <= ncz)) ? &cells[i0][j0][k1] : NULL);
  n->set_c101(((i1 >= -1) && (i1 <= ncx) && (j0 >= -1) && (j0 <= ncy) && (k1 >= -1) && (k1 <= ncz)) ? &cells[i1][j0][k1] : NULL);
  n->set_c011(((i0 >= -1) && (i0 <= ncx) && (j1 >= -1) && (j1 <= ncy) && (k1 >= -1) && (k1 <= ncz)) ? &cells[i0][j1][k1] : NULL);
  n->set_c111(((i1 >= -1) && (i1 <= ncx) && (j1 >= -1) && (j1 <= ncy) && (k1 >= -1) && (k1 <= ncz)) ? &cells[i1][j1][k1] : NULL);

  // Account for boundary conditions
  if (get_bcx1()->get_type() == btPeriodic) 
    i1 = i % ncx; // right neighbor's X index
  if ((i <= 0) && (get_bcx0()->get_type() == btPeriodic))
    i0 = ncx - 1;
  if (get_bcy1()->get_type() == btPeriodic)
    j1 = j % ncy; // upper neighbor's Y index
  if ((j <= 0) && (get_bcy0()->get_type() == btPeriodic)) 
    j0 = ncy - 1;
  if (get_bcz1()->get_type() == btPeriodic)
    k1 = k % ncz; // upper neighbor's Z index
  if ((k <= 0) && (get_bcz0()->get_type() == btPeriodic)) 
    k0 = ncz - 1;

  // Assign the neighbors where the particle data is advected
  //n->set_ac(&cells[i0][j0][k0], &cells[i1][j0][k0], &cells[i0][j1][k0], &cells[i1][j1][k0], &cells[i0][j0][k1], &cells[i1][j0][k1], &cells[i0][j1][k1], &cells[i1][j1][k1]);
  n->set_ac000(((i0 >= -1) && (i0 <= ncx) && (j0 >= -1) && (j0 <= ncy) && (k0 >= -1) && (k0 <= ncz)) ? &cells[i0][j0][k0] : NULL);
  n->set_ac100(((i1 >= -1) && (i1 <= ncx) && (j0 >= -1) && (j0 <= ncy) && (k0 >= -1) && (k0 <= ncz)) ? &cells[i1][j0][k0] : NULL);
  n->set_ac010(((i0 >= -1) && (i0 <= ncx) && (j1 >= -1) && (j1 <= ncy) && (k0 >= -1) && (k0 <= ncz)) ? &cells[i0][j1][k0] : NULL);
  n->set_ac110(((i1 >= -1) && (i1 <= ncx) && (j1 >= -1) && (j1 <= ncy) && (k0 >= -1) && (k0 <= ncz)) ? &cells[i1][j1][k0] : NULL);
  n->set_ac001(((i0 >= -1) && (i0 <= ncx) && (j0 >= -1) && (j0 <= ncy) && (k1 >= -1) && (k1 <= ncz)) ? &cells[i0][j0][k1] : NULL);
  n->set_ac101(((i1 >= -1) && (i1 <= ncx) && (j0 >= -1) && (j0 <= ncy) && (k1 >= -1) && (k1 <= ncz)) ? &cells[i1][j0][k1] : NULL);
  n->set_ac011(((i0 >= -1) && (i0 <= ncx) && (j1 >= -1) && (j1 <= ncy) && (k1 >= -1) && (k1 <= ncz)) ? &cells[i0][j1][k1] : NULL);
  n->set_ac111(((i1 >= -1) && (i1 <= ncx) && (j1 >= -1) && (j1 <= ncy) && (k1 >= -1) && (k1 <= ncz)) ? &cells[i1][j1][k1] : NULL);

  // Find the logical counterpart where advection (boundary condition) is happening in periodic BCs
  // For normal nodes it is a pointer to the node itself
  n->set_advNode(n);
  if ((n->get_role() == gerBoundary) || (n->get_role() == gerGhost))
  {
    // Left X boundary
    if ((n->get_bc(0)) && (n->get_bc(0)->get_type() == btPeriodic))
    {
      // corners
      if ((n->get_bc(1)) && (n->get_bc(1)->get_type() == btPeriodic) && (n->get_bc(2)) && (n->get_bc(2)->get_type() == btPeriodic))
        n->set_advNode(&nodes[0][0][0]);
      // Vertical ridges
      else if ((n->get_bc(1)) && (n->get_bc(1)->get_type() == btPeriodic)) 
        n->set_advNode(&nodes[0][0][n->get_k()]);
      // Horizontal ridges
      else if ((n->get_bc(2)) && (n->get_bc(2)->get_type() == btPeriodic))
        n->set_advNode(&nodes[0][n->get_j()][0]);
      else 
        n->set_advNode(&nodes[0][n->get_j()][n->get_k()]);
    }      
    // Y boundary. Only horizontal, X ridges are left
    else if ((n->get_bc(1)) && (n->get_bc(1)->get_type() == btPeriodic))
    {
      if ((n->get_bc(2)) && (n->get_bc(2)->get_type() == btPeriodic))
        n->set_advNode(&nodes[n->get_i()][0][0]);
      else 
        n->set_advNode(&nodes[n->get_i()][0][n->get_k()]);
    }
    // Right Z boundary
    else if ((n->get_bc(2)) && (n->get_bc(2)->get_type() == btPeriodic))
      n->set_advNode(&nodes[n->get_i()][n->get_j()][0]);
  }

  // Set the neighbor nodes sharing a "face" (normal, side)
  n->set_FaceNeighbor(0, 0, ((i - 1) >= -1) ? &nodes[i - 1][j][k] : NULL);
  n->set_FaceNeighbor(0, 1, ((i + 1) <= nnx) ? &nodes[i + 1][j][k] : NULL);
  n->set_FaceNeighbor(1, 0, ((j - 1) >= -1) ? &nodes[i][j - 1][k] : NULL);
  n->set_FaceNeighbor(1, 1, ((j + 1) <= nny) ? &nodes[i][j + 1][k] : NULL);
  n->set_FaceNeighbor(2, 0, ((k - 1) >= -1) ? &nodes[i][j][k - 1] : NULL);
  n->set_FaceNeighbor(2, 1, ((k + 1) <= nnz) ? &nodes[i][j][k + 1] : NULL);
}

/** 
  Assigns the cell's corner nodes.
  This doesn't account for the boundary conditions!
  Boundary conditions on the nodes are enforced by the imposeBoundaryConditions().

  This time-consuming routine should only be called when the grid is initialized.

  When this routine is used, nodes typically don't even have the coordinates set!
*/
void Grid::initCellNeighbors(GridCell *c)
{
  // Be very careful here.
  SlurmInt i0 = c->get_i(); // left X. Shall we check for out-of-bounds here, or maybe just %nnx?
  SlurmInt i1 = (c->get_i() + 1); // % (nnx - 1);
  SlurmInt j0 = c->get_j(); // bottom Y
  SlurmInt j1 = (c->get_j() + 1); // % (nny - 1);
  SlurmInt k0 = c->get_k(); // bottom Z
  SlurmInt k1 = (c->get_k() + 1); // % (nnz - 1);

  // Yes, there are ghost nodes
  c->set_n000(((i0 >= -1) && (j0 >= -1) && (k0 >= -1)) ? &nodes[i0][j0][k0] : NULL);
  c->set_n100(((i1 <= nnx) && (j0 >= -1) && (k0 >= -1)) ? &nodes[i1][j0][k0] : NULL);
  c->set_n010(((i0 >= -1) && (j1 <= nny) && (k0 >= -1)) ? &nodes[i0][j1][k0] : NULL);
  c->set_n110(((i1 <= nnx) && (j1 <= nny) && (k0 >= -1)) ? &nodes[i1][j1][k0] : NULL);
  c->set_n001(((i0 >= -1) && (j0 >= -1) && (k1 <= nnz)) ? &nodes[i0][j0][k1] : NULL);
  c->set_n101(((i1 <= nnx) && (j0 >= -1) && (k1 <= nnz)) ? &nodes[i1][j0][k1] : NULL);
  c->set_n011(((i0 >= -1) && (j1 <= nny) && (k1 <= nnz)) ? &nodes[i0][j1][k1] : NULL);
  c->set_n111(((i1 <= nnx) && (j1 <= nny) && (k1 <= nnz)) ? &nodes[i1][j1][k1] : NULL);

  // Now find the 'logical' neighbor nodes. In periodic BC, flip over the right edge: nodes on the right edge are THE SAME as the leftmost nodes.
  // These neighbors are used for interpolation and advection.
  if (c->get_role() == gerBoundary)
  {
    if (get_bcx1()->get_type() == btPeriodic) 
      i1 = i1 % ncx;
    if (get_bcy1()->get_type() == btPeriodic) 
      j1 = j1 % ncy;
    if (get_bcz1()->get_type() == btPeriodic) 
      k1 = k1 % ncz;

    // GridNode * _n000, GridNode * _n100, GridNode * _n010, GridNode * _n110, GridNode * _n001, GridNode * _n101, GridNode * _n011, GridNode * _n111
    c->set_an(&nodes[i0][j0][k0], &nodes[i1][j0][k0], &nodes[i0][j1][k0], &nodes[i1][j1][k0], &nodes[i0][j0][k1], &nodes[i1][j0][k1], &nodes[i0][j1][k1], &nodes[i1][j1][k1]);
  }
  else
    c->set_an(c->get_n000(), c->get_n100(), c->get_n010(), c->get_n110(), c->get_n001(), c->get_n101(), c->get_n011(), c->get_n111());

  // Set the advection cell. It is used to advect from particles, and to fill ghost cells inside imposeBC().
  if (c->get_role() == gerGhost)
  {
    SlurmInt ia = c->get_i();
    SlurmInt ja = c->get_j();
    SlurmInt ka = c->get_k();

    if (ia < 0)
    {
      BoundaryType b = get_bcx0()->get_type();
      // If periodic, should advect to the rightmost 'normal' cell
      if (b == btPeriodic) 
        ia = ncx - 1;
      // If reflective, advects to the neighbor 'normal' cell. 
      else if ((b == btReflective) || (b == btRigidWall) || (b == btFixed) || (b == btInlet) || (b == btOutlet) || (b == btSupersonicOutlet))
        ia++;
    }
    else if (ia >= ncx) 
    {
      BoundaryType b = get_bcx1()->get_type();
      if (b == btPeriodic) 
        ia = 0;
      else if ((b == btReflective) || (b == btRigidWall) || (b == btFixed) || (b == btInlet) || (b == btOutlet) || (b == btSupersonicOutlet))
        ia--;
    }

    if (ja < 0)
    {
      BoundaryType b = get_bcy0()->get_type();
      if (b == btPeriodic) 
        ja = ncy - 1;
      else if ((b == btReflective) || (b == btRigidWall) || (b == btFixed) || (b == btInlet) || (b == btOutlet) || (b == btSupersonicOutlet))
        ja++;
    }
    else if (ja >= ncy) 
    {
      BoundaryType b = get_bcy1()->get_type();
      if (b == btPeriodic) 
        ja = 0;
      else if ((b == btReflective) || (b == btRigidWall) || (b == btFixed) || (b == btInlet) || (b == btOutlet) || (b == btSupersonicOutlet))
        ja--;
    }

    if (ka < 0)
    {
      BoundaryType b = get_bcz0()->get_type();
      if (b == btPeriodic) 
        ka = ncz - 1;
      else if ((b == btReflective) || (b == btRigidWall) || (b == btFixed) || (b == btInlet) || (b == btOutlet) || (b == btSupersonicOutlet))
        ka++;
    }
    else if (ka >= ncz) 
    {
      BoundaryType b = get_bcz1()->get_type();
      if (b == btPeriodic) 
        ka = 0;
      else if ((b == btReflective) || (b == btRigidWall) || (b == btFixed) || (b == btInlet) || (b == btOutlet) || (b == btSupersonicOutlet))
        ka--;
    }

    c->set_advCell(&cells[ia][ja][ka]);
  }
  else 
    c->set_advCell(c);

  // Set the neighbor cells sharing a face (normal, side)
  c->set_FaceNeighbor(0, 0, ((i0 - 1) >= -1) ? &cells[i0 - 1][j0][k0] : NULL);
  c->set_FaceNeighbor(0, 1, ((i0 + 1) <= ncx) ? &cells[i0 + 1][j0][k0] : NULL);
  c->set_FaceNeighbor(1, 0, ((j0 - 1) >= -1) ? &cells[i0][j0 - 1][k0] : NULL);
  c->set_FaceNeighbor(1, 1, ((j0 + 1) <= ncy) ? &cells[i0][j0 + 1][k0] : NULL);
  c->set_FaceNeighbor(2, 0, ((k0 - 1) >= -1) ? &cells[i0][j0][k0 - 1] : NULL);
  c->set_FaceNeighbor(2, 1, ((k0 + 1) <= ncz) ? &cells[i0][j0][k0 + 1] : NULL);
}

/** 
  Initialize quadratic interpolation stuff.
  The cell itself projects into the element [1][1][1] of the supplied 3x3x3 cube of cells.
*/
void Grid::initCellsInterpolationStuff()
{
  CellRefArray nbrs;
  nbrs.resize(boost::extents[3][3][3]);
  for_cells
  {
    SlurmInt i0 = (c->get_i() > -1) ? -1 : 0;
    SlurmInt i1 = (c->get_i() < ncx) ? 1 : 0;
    SlurmInt j0 = (c->get_j() > -1) ? -1 : 0;
    SlurmInt j1 = (c->get_j() < ncy) ? 1 : 0;
    SlurmInt k0 = (c->get_k() > -1) ? -1 : 0;
    SlurmInt k1 = (c->get_k() < ncz) ? 1 : 0;

    for (GridCell **nc = nbrs.data(); nc < (nbrs.data() + nbrs.num_elements()); ++nc)
      *nc = NULL;

    for (SlurmInt i = i0; i <= i1; i++)
    for (SlurmInt j = j0; j <= j1; j++)
    for (SlurmInt k = k0; k <= k1; k++)
      nbrs[1 + i][1 + j][1 + k] = &cells[i + c->get_i()][j + c->get_j()][k + c->get_k()];

    c->setNeighborCells(nbrs);
  }
}


/**
  Check of CFL condition as suggested by Fabio for the Implosion test
  dt=min(dt,0.5/max(abs(dvx/dx+dvy/dy+dvz/dz)))
*/
SlurmDouble Grid::getCFL()
{
  SlurmDouble cfl = 0., cfl_new = 0.;
  for_cells 
  {
    cfl_new = abs(c->get_divu());
    if (cfl_new > cfl)
      cfl = cfl_new;
  }
  return 0.5 / cfl;
}

/** 
  Save multiple cell-centered quantities into a 4D array.
  Indices are ordered [x][y][z][quantity].
*/
SlurmDoubleArray4D Grid::getCellScalarsArray(unsigned long quants, vector<string> *cell_names, bool export_ghost)
{
  SlurmDoubleArray4D data;
  GridCell *c;

  // Select range of cells for output: with ghosts or without
  SlurmInt i0 = 0, i1 = ncx - 1, j0 = 0, j1 = ncy - 1, k0 = 0, k1 = ncz - 1;
  if (export_ghost)
  {
    i0--;
    i1++;
    j0--;
    j1++;
    k0--;
    k1++;
    data.resize(boost::extents[ncx+2][ncy+2][ncz+2][NumberOfBitsSet(quants)]);
  }
  else
    data.resize(boost::extents[ncx][ncy][ncz][NumberOfBitsSet(quants)]);

  for (CellArray::index ic = i0; ic <= i1; ++ic) 
  for (CellArray::index jc = j0; jc <= j1; ++jc)
  for (CellArray::index kc = k0; kc <= k1; ++kc)
  {
    c = &cells[ic][jc][kc];

    // Indices inside the data array start from 0, not -1
    SlurmInt i = export_ghost ? ic + 1 : ic;
    SlurmInt j = export_ghost ? jc + 1 : jc;
    SlurmInt k = export_ghost ? kc + 1 : kc;

    int iq = 0;
    if ((quants & QUANT_MAGNX) == QUANT_MAGNX) 
    {
      data[i][j][k][iq] = c->get_Bx();
      iq++;
    }
    if ((quants & QUANT_MAGNY) == QUANT_MAGNY) 
    {
      data[i][j][k][iq] = c->get_By();
      iq++;
    }
    if ((quants & QUANT_MAGNZ) == QUANT_MAGNZ) 
    {
      data[i][j][k][iq] = c->get_Bz();
      iq++;
    }
    if ((quants & QUANT_VOL) == QUANT_VOL) 
    {
      data[i][j][k][iq] = c->get_vol();
      iq++;
    }
    if ((quants & QUANT_RHO) == QUANT_RHO) 
    {
      data[i][j][k][iq] = c->get_rho();
      iq++;
    }
    if ((quants & QUANT_PRESS) == QUANT_PRESS) 
    {
      data[i][j][k][iq] = c->get_p();
      iq++;
    }
    if ((quants & QUANT_VISCOSITY) == QUANT_VISCOSITY) 
    {
      data[i][j][k][iq] = c->get_visc();
      iq++;
    }
    if ((quants & QUANT_MASS) == QUANT_MASS) 
    {
      data[i][j][k][iq] = c->get_m();
      iq++;
    }
    if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC) 
    {
      data[i][j][k][iq] = c->get_divA();
      iq++;
    }
    if ((quants & QUANT_SCALARPOTENTIAL) == QUANT_SCALARPOTENTIAL) 
    {
      data[i][j][k][iq] = c->get_phi();
      iq++;
    }
  }

  // Set array names
  if ((quants & QUANT_MAGNX) == QUANT_MAGNX) 
    cell_names->push_back("Bx");
  if ((quants & QUANT_MAGNY) == QUANT_MAGNY) 
    cell_names->push_back("By");
  if ((quants & QUANT_MAGNZ) == QUANT_MAGNZ) 
    cell_names->push_back("Bz");
  if ((quants & QUANT_VOL) == QUANT_VOL) 
    cell_names->push_back("Vol");
  if ((quants & QUANT_RHO) == QUANT_RHO) 
    cell_names->push_back("rho");
  if ((quants & QUANT_PRESS) == QUANT_PRESS) 
    cell_names->push_back("P");
  if ((quants & QUANT_VISCOSITY) == QUANT_VISCOSITY) 
    cell_names->push_back("viscosity");
  if ((quants & QUANT_MASS) == QUANT_MASS) 
    cell_names->push_back("m");
  if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC) 
    cell_names->push_back("divA");
  if ((quants & QUANT_SCALARPOTENTIAL) == QUANT_SCALARPOTENTIAL) 
    cell_names->push_back("phi");
  return data;
}

/** 
  Save multiple node quantities into a 4D array.
  Order: ux, uy, uz, Ax, Ay, Az, ....

  Indices are ordered [x][y][z][quantity]
*/
SlurmDoubleArray4D Grid::getNodeScalarsArray(unsigned long quants, vector<string> *node_names, bool export_ghost)
{
  // First three indices are coordinates, last one is the quantity
  SlurmDoubleArray4D data;
  GridNode *n;

  // Select range of nodes for output: with ghosts or without
  SlurmInt i0 = 0, i1 = nnx - 1, j0 = 0, j1 = nny - 1, k0 = 0, k1 = nnz - 1;
  if (export_ghost)
  {
    i0--;
    i1++;
    j0--;
    j1++;
    k0--;
    k1++;
    data.resize(boost::extents[nnx+2][nny+2][nnz+2][NumberOfBitsSet(quants)]);
  }
  else
    data.resize(boost::extents[nnx][nny][nnz][NumberOfBitsSet(quants)]);

  for (NodeArray::index in = i0; in <= i1; ++in) 
  for (NodeArray::index jn = j0; jn <= j1; ++jn)
  for (NodeArray::index kn = k0; kn <= k1; ++kn)
  {
    n = &nodes[in][jn][kn];

    // Indices inside the data array start from 0, not -1
    SlurmInt i = export_ghost ? in + 1 : in;
    SlurmInt j = export_ghost ? jn + 1 : jn;
    SlurmInt k = export_ghost ? kn + 1 : kn;

    int iq = 0;
    if ((quants & QUANT_VX) == QUANT_VX) 
    {
      data[i][j][k][iq] = n->get_ux();
      iq++;
    }
    if ((quants & QUANT_VY) == QUANT_VY) 
    {
      data[i][j][k][iq] = n->get_uy();
      iq++;
    }
    if ((quants & QUANT_VZ) == QUANT_VZ)
    { 
      data[i][j][k][iq] = n->get_uz();
      iq++;
    }
    if ((quants & QUANT_MAGNX) == QUANT_MAGNX)
    { 
      data[i][j][k][iq] = n->get_Ax();
      iq++;
    }
    if ((quants & QUANT_MAGNY) == QUANT_MAGNY)
    { 
      data[i][j][k][iq] = n->get_Ay();
      iq++;
    }
    if ((quants & QUANT_MAGNZ) == QUANT_MAGNZ)
    { 
      data[i][j][k][iq] = n->get_Az();
      iq++;
    }
    if ((quants & QUANT_MASS) == QUANT_MASS)
    { 
      data[i][j][k][iq] = n->get_m();
      iq++;
    }
    if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC)
    { 
      data[i][j][k][iq] = n->get_divB();
      iq++;
    }
  }

  // Set array names
  if ((quants & QUANT_VX) == QUANT_VX) 
    node_names->push_back("ux");
  if ((quants & QUANT_VY) == QUANT_VY) 
    node_names->push_back("uy");
  if ((quants & QUANT_VZ) == QUANT_VZ)
    node_names->push_back("uz");
  if ((quants & QUANT_MAGNX) == QUANT_MAGNX)
    node_names->push_back("Ax");
  if ((quants & QUANT_MAGNY) == QUANT_MAGNY)
    node_names->push_back("Ay");
  if ((quants & QUANT_MAGNZ) == QUANT_MAGNZ)
    node_names->push_back("Az");
  if ((quants & QUANT_MASS) == QUANT_MASS)
    node_names->push_back("m");
  if ((quants & QUANT_MAGNETIC) == QUANT_MAGNETIC)
    node_names->push_back("divB");
  
  return data;
}

void Grid::set_InternalEnergy(SlurmDouble value)
{
  omp_set_lock(&InternalEnergyLock);
  InternalEnergy = value;
  omp_unset_lock(&InternalEnergyLock);
}

void Grid::set_MagneticEnergy(SlurmDouble value)
{
  omp_set_lock(&MagneticEnergyLock);
  MagneticEnergy = value;
  omp_unset_lock(&MagneticEnergyLock);
}

void Grid::set_KineticEnergy(SlurmDouble value)
{
  omp_set_lock(&KineticEnergyLock);
  KineticEnergy = value;
  omp_unset_lock(&KineticEnergyLock);
}

void Grid::inc_InternalEnergy(SlurmDouble value)
{
  omp_set_lock(&InternalEnergyLock);
  InternalEnergy += value;
  omp_unset_lock(&InternalEnergyLock);
}

void Grid::inc_MagneticEnergy(SlurmDouble value)
{
  omp_set_lock(&MagneticEnergyLock);
  MagneticEnergy += value;
  omp_unset_lock(&MagneticEnergyLock);
}

void Grid::inc_KineticEnergy(SlurmDouble value)
{
  omp_set_lock(&KineticEnergyLock);
  KineticEnergy += value;
  omp_unset_lock(&KineticEnergyLock);
}

void Grid::print()
{
  this->printCells();
  this->printNodes();
}

void Grid::printNodes()
{
  messageline("");
  messageline("Number of nodes: ", nnx*nny);
  for_nodes
  {
    message("x=", n->get_x());
    message(", y=", n->get_y());
    message(", z=", n->get_z());
    message(", vx=", n->get_ux());
    message(", vy=", n->get_uy());
    message(", vz=", n->get_uz());
    message("|  "); 
  }
  messageline("");
}

void Grid::printCells()
{
  messageline("");
  messageline("Number of cells: ", ncx*ncy);
  for_cells
  {
    message("x=", c->get_x());
    message(", y=", c->get_y());
    message(", z=", c->get_z());
    message(", mass=", c->get_m());
    message(", rho=", c->get_rho());
    message(", Bx=", c->get_Bx());
    message(", By=", c->get_By());
    message(", Bz=", c->get_Bz());
    message("|  "); 
  }
  messageline("");
}

Grid::~Grid()
{
}
