/**
  Class boundary conditions for Slurm.

  (c) 2017 V. Olshevsky

*/
#pragma once

#include "GridCommon.h"
#include "ConfigFile.h"
#include "InitialConditions.h"

/** General interface for all BCs */
class BoundaryCondition
{
protected:

  /// Pointer to the initial condition of the problem
  InitialCondition * initial;
  /// The location of the wall: 0., Lx, Ly, Lz, etc.
  SlurmDouble end;  
  /// The beginning of the boundary layer, half-cell before the wall: 0 + dx/2 or Lx - dx/2, etc.
  SlurmDouble begin; 
  /// dx/2, dy/2, etc.
  SlurmDouble thickness; 
  /// The condition's type
  BoundaryType type;
  /// Side: 0 - Left or, 1 - right
  SlurmInt side;
  /// Direction: 0 - X, 1 - Y, 2 - Z
  SlurmInt normal; 

  /// Reflective BC for particles
  bool reflectParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity);
  /// Open BC for particle
  bool transmitParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity);
  /// Periodic BC for particle
  bool flipParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity);

public:

  /// Constructor
  BoundaryCondition(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial);
  /// Factory
  static BoundaryCondition * makeBoundaryCondition(ConfigFile * config, string btname, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial);

  // Getters / setters
  inline BoundaryType get_type() { return type; }
  inline SlurmDouble get_end() { return end; }

  // Checks

  /// Check if certain cell is the ghost of this one
  virtual bool cellIsGhost(GridCell * c);

  // Imposing

  /// used by Grid::imposeBoundaryConditions()
  virtual void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// used by Grid::imposeBCNodes()
  virtual void imposeOnNode(GridNode * n, unsigned long quants, SlurmDouble t = 0);
  /// used by Grid::imposeBoundaryConditions()
  virtual void imposeOnCell(GridCell * c, SlurmDouble t = 0);
  /// used by Grid::computeArtificialViscosity()
  virtual void imposeOnCell(GridCell * c, unsigned long quants, SlurmDouble t = 0);
  /// Adjusts node velocity according to BCs after the grid has been advanced
  virtual void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0) {}
  /// Boundary condition for the particle. Returns false if the particle should be deleted.
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) { return transmitParticle(ptcl, new_position, new_velocity); }

  /// Inlet
  static void setInletNode(GridNode* n, unsigned long quants, BoundaryCondition* bc);

  // Magnetic field conditions

  /// Set A according to PMC (Perfect Magnetic Conductor condition on cell's corners.
  void setPerfectMagneticConductor(GridNode * c);

  /// Returns Boundary Condition name
  static string btToString(BoundaryType t);
  /// Returns Boundary condition type given name
  static BoundaryType stringToBT(string name);

  /// Destructor
  ~BoundaryCondition() {};
};

/** Periodic BC */
class BoundaryConditionPeriodic : public BoundaryCondition
{
public:
  BoundaryConditionPeriodic(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : BoundaryCondition(config, bt, side, normal, begin, end, initial) {};
  /// Just copy from the advNode
  void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// Adjusts node velocity according to BCs after the grid has been advanced
  void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0);
  /// Flip the particle's position across the periodic wall
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) { return flipParticle(ptcl, new_position, new_velocity); }
};

/** Reflective */
class BoundaryConditionReflective : public BoundaryCondition
{
public:
  BoundaryConditionReflective(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : BoundaryCondition(config, bt, side, normal, begin, end, initial) {};
  /// Set both the ghost cell and its neighbor to constant values
  void imposeOnCell(GridCell * c, SlurmDouble t = 0);
  /// set reflective here
  void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// Zero node velocity, zero dv
  virtual void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0);
  /// Reflect particle from the wall
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) { return reflectParticle(ptcl, new_position, new_velocity); }
};

/** Inlet BC */
class BoundaryConditionInlet : public BoundaryCondition
{
private:

  SlurmDouble u0;

public:

  /// Constructor reads the default values
  BoundaryConditionInlet(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial);
  /// Constant speed on nodes?
  void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// Set both the ghost cell and its neighbor to constant values
  void imposeOnCell(GridCell * c, SlurmDouble t = 0);
  /// Constant node velocity, zero velocity increment?
  virtual void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0);
  /// TODO: injected particle should not be moved on the first time step?
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) { return transmitParticle(ptcl, new_position, new_velocity); }
};

/** Outlet BC */
class BoundaryConditionOutlet : public BoundaryCondition
{
protected:

  SlurmDouble u0;

public:

  /// Constructor reads the default values
  BoundaryConditionOutlet(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial);
  /// Constant speed on nodes?
  void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// Set both the ghost cell and its neighbor to constant values
  void imposeOnCell(GridCell * c, SlurmDouble t = 0);
  /// Constant node velocity, zero velocity increment?
  virtual void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0);
  /// Transmit the particle through the wall
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) { return transmitParticle(ptcl, new_position, new_velocity); }
};

/** Fixed BC */
class BoundaryConditionFixed : public BoundaryCondition
{
protected:

  SlurmDouble u0;

public:

  /// Constructor reads the default values
  BoundaryConditionFixed(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial);
  /// Constant speed on nodes?
  void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// Set both the ghost cell and its neighbor to constant values
  void imposeOnCell(GridCell * c, SlurmDouble t = 0);
  /// Constant node velocity, zero velocity increment?
  virtual void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0);
  /// Reflect particle from the wall
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) { return reflectParticle(ptcl, new_position, new_velocity); }
};

/** Combines reflecting no-slip wall and Perfect Magnetic Conductor? */
class BoundaryConditionRigidWall : public BoundaryCondition
{
protected:

  SlurmDouble u0;

public:

  /// Constructor reads the default values
  BoundaryConditionRigidWall(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial) : BoundaryCondition(config, bt, side, normal, begin, end, initial) {};
  /// Constant speed on nodes?
  void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// Set both the ghost cell and its neighbor to constant values
  void imposeOnCell(GridCell * c, SlurmDouble t = 0);
  /// Constant node velocity, zero velocity increment?
  virtual void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0);
  /// Reflect particle from the wall
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) { return reflectParticle(ptcl, new_position, new_velocity); }
};

/** A sphere which can represent a Sun or something similar, where particles disappear %) */
class BoundaryConditionSphere : public BoundaryCondition
{
protected:

  /// The center of the sphere
  GridPoint Center;
  /// The radius of the sphere
  SlurmDouble R;

public:

  /// Constructor reads the default values
  BoundaryConditionSphere(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial);
  /// Constant speed on nodes?
  void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// Set both the ghost cell and its neighbor to constant values
  void imposeOnCell(GridCell * c, SlurmDouble t = 0);
  /// Constant node velocity, zero velocity increment?
  virtual void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0);
  /// Reflect particle from the wall
  /// Reflect particle from the wall
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity);
  /// Check if certain cell is the ghost of this one
  bool cellIsGhost(GridCell * c) { return (Center.distance(c) <= R); }
};

/** Outlet BC with Perfectly Matched Layer */
class BoundaryConditionSupersonicOutlet : public BoundaryCondition
{

public:

  /// Constructor reads the default values
  BoundaryConditionSupersonicOutlet(ConfigFile * config, BoundaryType bt, short side, short normal, SlurmDouble begin, SlurmDouble end, InitialCondition * initial);
  /// Constant speed on nodes?
  void imposeOnNode(GridNode * n, SlurmDouble t = 0);
  /// This is used to zero advanced A?
  void imposeOnNode(GridNode * n, unsigned long quants, SlurmDouble t = 0);
  /// Set both the ghost cell and its neighbor to constant values
  void imposeOnCell(GridCell * c, SlurmDouble t = 0);
  /// used by Grid::computeArtificialViscosity()
  void imposeOnCell(GridCell * c, unsigned long quants, SlurmDouble t = 0);
  /// Constant node velocity, zero velocity increment?
  virtual void adjustBoundaryNode(GridNode * n, SlurmDouble t = 0);
  /// Transmit the particle through the wall
  virtual bool imposeOnParticle(Particle * ptcl, SlDoubleVector& new_position, SlDoubleVector& new_velocity) { return transmitParticle(ptcl, new_position, new_velocity); }
};
