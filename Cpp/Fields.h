#pragma once

#include "GridCommon.h"
#include "ConfigFile.h"
#include "InitialConditions.h"

///  Base class for all external fields.
class ExternalFieldsGenerator;

/**
  Class Field contains functionality related to the electromagnetic fields in the Slurm.

  V. Olshevsky, October 2016, sya@mao.kiev.ua.
*/
class Fields
{
protected:

  /// The configuration file 
  ConfigFile * config;
  /// Magnetic field evolution strategy
  MFEvolutionMethods FieldEvolutionMethod;
  /// Parameters of the external fields
  ExternalFieldsGenerator * ExternalFields = NULL;
  /// Constant resistivity
  SlurmDouble Resistivity = 0.;

public:

  // Property getters/setters
  /// Way the code evolves magnetic field
  MFEvolutionMethods get_FieldEvolutionMethod() { return(FieldEvolutionMethod); }
  /// Getter for the resistivity
  SlurmDouble get_Resistivity() { return(Resistivity); }
  /// ExternalFields getter
  ExternalFieldsGenerator * get_ExternalFields() { return ExternalFields; }

  /// Returns the interpolation scheme given its name
  static string getFieldEvolutionMethodName(MFEvolutionMethods method);
  /// Return field evolution method by name
  static MFEvolutionMethods getMethodByName(string name);

  /// Compute magnetic field B = [nabla x A] on the given cell.
  void computeBfromA(GridCell * c);
  /// Fetch vector potential on nodes and add resistivity?
  void prepareNodeFields(GridNode* n, BoundaryCondition* bcx1, BoundaryCondition* bcy1, BoundaryCondition* bcz1);
  /// Advance scalar potential
  void advanceCellScalarPotential(GridCell * c, SlurmDouble dt);
  /// Advance electromagnetic vector potential on nodes
  void advanceNodeVectorPotential(GridNode* n, SlurmDouble t, SlurmDouble dt);
  /// Compute divergence of B for the given node
  void computeDivB(GridNode* n);
  /// Compute J = [nabla x B] for the given node
  void computeNodeCurrents(GridNode* n);
  /// Add resistivity to internal energy.
  void addCellResistivity(GridCell * c);

  Fields(ConfigFile * config, InitialCondition * initial);
  ~Fields();
};


// External Fields

/** 
  Base class for all external fields.
  Do not instantiate this class directly.
  If you need to add a specific form of external fields, create a new child.
*/
class ExternalFieldsGenerator
{
public:
  /// Add dA = - dt * E to grid node, before B is computed in Grid::advanceExplicit(). 
  virtual void addNodeExternalFields(GridNode* n, SlurmDouble t, SlurmDouble dt, InitialCondition * initial = NULL) {};
  /// Add external electric field E = -[u x Bext]
  virtual void addNodeExternalFields(GridNode* n, GridCell * avgc, SlurmDouble dt) {};
  /// Add Bext
  virtual void addCellExternalFields(GridCell* c) {};
  /// Check if this point needs an external field 
  virtual bool isExternalFieldNeeded(GridPoint* p, SlurmDouble t, SlurmDouble dt) { return false; };

  /// Constructor
  ExternalFieldsGenerator(ConfigFile * config) {};
  /// Destructor
  ~ExternalFieldsGenerator() {};  
};

/** External fields for the shrinking sheets */
class ExternalFieldsShrinkingSheets: public ExternalFieldsGenerator
{
protected:
  /// Time scale
  SlurmDouble tscal;
  /// Parameter D
  SlurmDouble D;
  /// Parameter a0
  SlurmDouble a0;
  /// Parameter a1
  SlurmDouble a1;
  /// Reference coords
  SlurmDouble x0;
  /// Some precomputed param
  SlurmDouble d1;

public:

  void addNodeExternalFields(GridNode* n, SlurmDouble t, SlurmDouble dt, InitialCondition * initial = NULL); 
  ExternalFieldsShrinkingSheets(ConfigFile * config);
};


/** 
  External fields to create a torus-shaped magnetic loop in the sheared arcade fro the Solar Wind book by Markus Aschwanden, Eq. (5.3.14).
  The torus is explained in Fan & Gibson paper (ApJ 609:1123-1133, 2004).
*/
class ExternalFieldsFG: public ExternalFieldsGenerator
{
protected:

  SlurmDouble Lx, Ly, Lz, B0, Bt, q, a, Rg, u0;
  SlurmDouble xc, yc, zc;

public:
  /// Read default params
  ExternalFieldsFG(ConfigFile * config);
  /// Add node fields
  void addNodeExternalFields(GridNode* n, SlurmDouble t, SlurmDouble dt, InitialCondition * initial = NULL); 
  /// Check if this point needs an external field 
  bool isExternalFieldNeeded(GridPoint* p, SlurmDouble t, SlurmDouble dt);
};

/** 
  A uniform field in the XZ plane inclined by Theta to the Z axis.
*/
class ExternalFieldsUniform: public ExternalFieldsGenerator
{
protected:
  /// Background magnetic field vector
  SlDoubleVector Bext = {0, 0, 0};

public:
  /// Read default params
  ExternalFieldsUniform(ConfigFile * config);
  /// Subtract external electric field E = -[u x Bext], i.e., add [u x Bext]
  void addNodeExternalFields(GridNode* n, GridCell * avgc, SlurmDouble dt);
  /// Add Bext
  void addCellExternalFields(GridCell* c);
  /// Check if this point needs an external field 
  bool isExternalFieldNeeded(GridPoint* p, SlurmDouble t, SlurmDouble dt);
};
