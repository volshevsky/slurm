/** 
  Class ParticleManager operates a list of particles in the Slurm.
  It knows the Grid object: all interpolation is handled by the Grid; ParticleManager only provides an interface to it.
*/
#include "ParticleManager.h"

/** Constructor */
ParticleManager::ParticleManager(Grid* grid, string scheme, MFEvolutionMethods MFEvolution)
{
  // Choose volume evolution method
  if (grid->get_VolumeEvolutionMethod() == vemMaterialPoint) 
    updateParticleVolume = &ParticleManager::updateVolumeMP;
  else if (grid->get_VolumeEvolutionMethod() == vemVelocityGradient) 
    updateParticleVolume = &ParticleManager::updateVolumeVG;
  else 
    updateParticleVolume = &ParticleManager::updateVolumeOff; // vemOff

  // Choose magnetic field evolution method
  if (MFEvolution == mfemAdvectParticles) 
    updateParticleMagnetization = &ParticleManager::updateMagnetizationAP;
  else if (MFEvolution == mfemInterpolateParticles) 
    updateParticleMagnetization = &ParticleManager::updateMagnetizationAP;
  else 
    updateParticleMagnetization = &ParticleManager::updateMagnetizationOff;

  // We don't create particles here because spatial distribution is easier to set when cycling over i, j, not over the particles.
  omp_init_lock(&InternalEnergyLock);
  omp_init_lock(&KineticEnergyLock);
  omp_init_lock(&CurrentNumberLock);
  omp_init_lock(&DeletedNumberLock);

  // Interpolators
  interpolator = Interpolator::makeInterpolator(grid, scheme, MFEvolution);

  // Initialize particle ranges for parallel processing
  IteratorsBegin = new ParticleIterator[omp_get_max_threads()];
  IteratorsEnd = new ParticleIterator[omp_get_max_threads()];
}

/** Compute particle range for this thread */
void ParticleManager::computeOMPThreadRange()
{
  // Number of particles per thread
  size_t chunk_size = CurrentNumber / IteratorsNumberOfThreads;

  // Current thread's index
  int thread_num = omp_get_thread_num();

  if (thread_num == 0)
    messageline("Recomputing particle ranges for the number of threads: ", IteratorsNumberOfThreads);

  // Current thread's starting particle
  IteratorsBegin[thread_num] = items.begin();
  std::advance(IteratorsBegin[thread_num], thread_num * chunk_size);

  // Current thread's last particle
  IteratorsEnd[thread_num] = IteratorsBegin[thread_num];

  // last thread iterates the remaining sequence
  if (thread_num == IteratorsNumberOfThreads - 1) // last thread iterates the remaining sequence
    IteratorsEnd[thread_num] = items.end();
  else
    std::advance(IteratorsEnd[thread_num], chunk_size);
} 

/** Compute particle ranges for each thread */
void ParticleManager::computeOMPRanges()
{
  #pragma omp parallel 
  {
    // Total number of threads available now
    IteratorsNumberOfThreads = omp_get_num_threads();

    // Set particle ranges for all threads
    computeOMPThreadRange();

    #pragma omp barrier
  }
} 

/** Sets initial condition for the grid, then interpolates on particles */
void ParticleManager::init(Grid *grid, ConfigFile *config) 
{
  OutputRange = config->read <long long>("ParticlesOutputRange", 1);
  KineticEnergy = 0.;
  InternalEnergy = 0.;
  CurrentNumber = 0;
  DeletedNumber = 0;

  // Particle size
  npx = config->read<SlurmInt>("ParticlesPerCellX");
  npy = config->read<SlurmInt>("ParticlesPerCellY");
  npz = config->read<SlurmInt>("ParticlesPerCellZ", 1);
  dxp = grid->get_dx() / SlurmDouble(npx);
  dyp = grid->get_dy() / SlurmDouble(npy);
  dzp = grid->get_dz() / SlurmDouble(npz);

  initial = grid->get_initial();

  // Create particles and set the quantities such as dx and volume  
  if ((initial->get_name()).find("SolarWind") == 0)
    this->initCylindrical(grid, config);
  else
    this->initUniform(grid);

  // Prepare particles for the solver iteraton
  #pragma omp parallel 
  {
    int thread_num = omp_get_thread_num();
    
    // Make sure particles were split between threads according to the current number of threads
    if (omp_get_num_threads() != IteratorsNumberOfThreads) 
    {
      if (thread_num == 0)
        IteratorsNumberOfThreads = omp_get_num_threads();

      #pragma omp barrier
      computeOMPThreadRange();
    }

    for(ParticleIterator pi = IteratorsBegin[thread_num]; pi != IteratorsEnd[thread_num]; ++pi) 
      initial->initParticle(&(*pi), interpolator);
  }
}
      
/** Distributes particles uniformly in the grid. Sets some default values */
void ParticleManager::initUniform(Grid *grid)
{
  messageline("Initiating uniform particle distribution");
  message("Number of particles per cell: ", npx);
  message(" ", npy);
  messageline(" ", npz);

  Particle *p;

  /// We want to populate cells with particles cell-by-cell, so that particles that are close in the list, are also close in space initially.
  /// This is also good for outputting only each n-th particle, as they will be distributed uniformly.
  for (SlurmInt ic = 0; ic < grid->get_ncx(); ++ic)
  for (SlurmInt jc = 0; jc < grid->get_ncy(); ++jc) 
  for (SlurmInt kc = 0; kc < grid->get_ncz(); ++kc) 
  {
    for (SlurmInt ip = 0; ip < npx; ++ip)
    for (SlurmInt jp = 0; jp < npy; ++jp)
    for (SlurmInt kp = 0; kp < npz; ++kp)
    {
      // Create a new particle and add it to the list
      p = addParticle((0.5 + SlurmDouble(ip + ic * npx)) * dxp, (0.5 + SlurmDouble(jp + jc * npy)) * dyp, (0.5 + SlurmDouble(kp + kc * npz)) * dzp);
    
      // Init node's neighbor particles. We don't flip over the periodic walls, leave it to the grid's imposeBC().
      if (grid->get_TrackNodeNeighborParticles()) 
        grid->updateParticleNodeNeighbors(p);
    }
  } 

  // Set boundary nodes' neighbor particles
  grid->updateBoundaryNeighborParticles();

  // Finally, compute the ranges for parallel particle processing
  computeOMPRanges();
}

/** 
  Distributes particles uniformly in the cylindrical coordinates.
  Z is the axis of the cylinder.
  Other coordinates are psi (0..2pi) and r.
*/
void ParticleManager::initCylindrical(Grid *grid, ConfigFile *config)
{
  CylInjectRadius = config->read<SlurmDouble>("R0");
  CylInitCenter.set_xyz(config->read<SlurmDouble>("x0", 0.5 * grid->get_Lx()), config->read<SlurmDouble>("y0", 0.5 * grid->get_Ly()), config->read<SlurmDouble>("z0", 0.5 * grid->get_Lz()));

  messageline("\nInitiating cylindrical particle distribution");
  message("Center coordinates: ", CylInitCenter.get_x());
  message(" ", CylInitCenter.get_y());
  messageline(" ", CylInitCenter.get_z());
  messageline("Internal radius R0 = ", CylInjectRadius);
  
  // Estimate the number of particles in the cylindrical coordinate system
  // N = 2/pi * CylInjectNPsi * CylInjectNr * nz, where nz == ncz * npz. N is the total number of particles.
  SlurmInt N = npx * npy * grid->get_ncx() * grid->get_ncy();

  // The biggest radius is the maximum between distance to the 4 corners.
  SlurmDouble R1 = std::max(CylInitCenter.get_length(), CylInitCenter.distance(0, grid->get_Ly(), CylInitCenter.get_z()));
  R1 = std::max(R1, CylInitCenter.distance(grid->get_Lx(), grid->get_Ly(), CylInitCenter.get_z()));
  R1 = std::max(R1, CylInitCenter.distance(grid->get_Lx(), 0, CylInitCenter.get_z()));

  // Put a condition on the azimuthal distance (Psi) and radial (r) at the outermost corner dr == CylInjectDPsi * R1.
  // Then the number of particles on each circle
  CylInjectNPsi = (SlurmInt) PI * pow(2 * N * (R1*R1 - CylInjectRadius*CylInjectRadius) / (grid->get_Lx() * grid->get_Ly()), 0.5);
  CylInjectDPsi = 2. * PI / CylInjectNPsi;
  
  // Distance between particles along radius
  CylInjectDr = 0.5 * CylInjectDPsi * R1;
  CylInjectNr = (R1 - CylInjectRadius) / CylInjectDr;
  messageline("dr = ", CylInjectDr);
  messageline("npsi = ", CylInjectNPsi);

  /// We want to populate cells with particles cell-by-cell, so that particles that are close in the list, are also close in space initially.
  /// This is also good for outputting only each n-th particle, as they will be distributed uniformly.
  Particle *p;

  for (SlurmInt ir = 0; ir < CylInjectNr; ++ir)
  for (SlurmInt ipsi = 0; ipsi < CylInjectNPsi; ++ipsi) 
  for (SlurmInt iz = 0; iz < grid->get_ncz() * npz; ++iz) 
  {
    SlurmDouble psi = CylInjectDPsi * ipsi;
    SlurmDouble r = CylInjectRadius + CylInjectDr * ir;
    SlurmDouble x = r * cos(psi) + CylInitCenter.get_x();
    SlurmDouble y = r * sin(psi) + CylInitCenter.get_y();
    if ((x >= 0) && (x <= grid->get_Lx()) && (y >= 0) && (y <= grid->get_Ly()))
    {
      // Create a new particle and add it to the list
      p = addParticle(x, y, (0.5 + iz) * dzp);
      
      // Init node's neighbor particles. We don't flip over the periodic walls, leave it to the grid's imposeBC().
      if (grid->get_TrackNodeNeighborParticles()) 
        grid->updateParticleNodeNeighbors(p);
    }
  } 

  // Set boundary nodes' neighbor particles
  grid->updateBoundaryNeighborParticles();

  // Finally, compute the ranges for parallel particle processing
  computeOMPRanges();
}


/** Inject particles */
void ParticleManager::injectParticles(Grid *grid, SlurmDouble t, SlurmDouble dt)
{
  if (initial->checkInjectMoment(t))
  {
    Particle *p;
    
    if (grid->get_bcx0()->get_type() == btInlet)
    {
      for (SlurmInt jc = 0; jc < grid->get_ncy(); ++jc) 
      for (SlurmInt kc = 0; kc < grid->get_ncz(); ++kc) 
      {
        for (SlurmInt jp = 0; jp < npy; ++jp)
        for (SlurmInt kp = 0; kp < npz; ++kp)
        {
          // Create a new particle and add it to the list
          p = addParticle(0.0, (0.5 + SlurmDouble(jp + jc * npy)) * dyp, (0.5 + SlurmDouble(kp + kc * npz)) * dzp);
      
          // Init node's neighbor particles. We don't flip over the periodic walls, leave it to the grid's imposeBC().
          //if (grid->get_TrackNodeNeighborParticles()) grid->updateParticleNodeNeighbors(p);

          initial->initParticle(p, interpolator, t);
        }
      }
    }
    else if ((boost::iequals(initial->get_name(), "FluxRope") || boost::iequals(initial->get_name(), "FanGibson")) && grid->get_fields()) 
    {
      GridPoint * pt = new GridPoint();

      for (SlurmInt ic = 0; ic < grid->get_ncx(); ++ic) 
      for (SlurmInt jc = 0; jc < grid->get_ncy(); ++jc) 
      {
        for (SlurmInt ip = 0; ip < npx; ++ip)
        for (SlurmInt jp = 0; jp < npy; ++jp)
        {
          // Only inject particles of the torus.
          pt->set_xyz((0.5 + SlurmDouble(ip + ic * npx)) * dxp, (0.5 + SlurmDouble(jp + jc * npy)) * dyp, 0);
 
          if (grid->get_fields()->get_ExternalFields()->isExternalFieldNeeded(pt, t, dt))
          {
            // Create a new particle and add it to the list
            p = addParticle(pt->get_x(), pt->get_y(), pt->get_z());
      
            // Init node's neighbor particles. We don't flip over the periodic walls, leave it to the grid's imposeBC().
            //if (grid->get_TrackNodeNeighborParticles()) grid->updateParticleNodeNeighbors(p);

            initial->initParticle(p, interpolator, t);
          }
        }
      }
      
      delete pt;
    }
    else if ((initial->get_name()).find("SolarWind") == 0)
    {
      Particle *p;
      
      /// We want to populate cells with particles cell-by-cell, so that particles that are close in the list, are also close in space initially.
      /// This is also good for outputting only each n-th particle, as they will be distributed uniformly.
      for (SlurmInt ipsi = 0; ipsi < CylInjectNPsi; ++ipsi) 
      for (SlurmInt iz = 0; iz < grid->get_ncz() * npz; ++iz) 
      {
        SlurmDouble psi = CylInjectDPsi * ipsi;
        SlurmDouble r = CylInjectRadius;
        SlurmDouble x = r * cos(psi) + CylInitCenter.get_x();
        SlurmDouble y = r * sin(psi) + CylInitCenter.get_y();
        // Create a new particle and add it to the list
        if ((x >= 0) && (x <= grid->get_Lx()) && (y >= 0) && (y <= grid->get_Ly()))
        {
          // The psi and r will be transmitted into x, y by the initial condition below.
          p = addParticle(psi, r, (0.5 + iz) * dzp);
        
          // Init node's neighbor particles. We don't flip over the periodic walls, leave it to the grid's imposeBC().
          //if (grid->get_TrackNodeNeighborParticles()) grid->updateParticleNodeNeighbors(p);
          initial->initParticle(p, interpolator, t);
        }
      } 
    }

    initial->resetInjectMoment(t);
    //IteratorsEnd[IteratorsNumberOfThreads - 1] = items.end();
    computeOMPRanges();
  }
}


/** 
  Advance particle position in one dimension (X, Y, or Z).
  bc_left, bc_right - boundary conditions in this dimension.
  new_position - the precomputed new coordinate of a particle (in updateExplicit()).
  pposition - particle's coordinate.
  pvelocity - particle's velocity (is reversed when reflected).

  Returns false if the particle should be deleted.

  This method is obsolete, BoundaryCondition->imposeOnParticle() should be used instead.
*/
inline bool ParticleManager::moveParticle(BoundaryCondition* bc_left, BoundaryCondition* bc_right, SlurmDouble new_position, SlurmDouble& pposition, SlurmDouble& pvelocity)
{
  if (new_position >= bc_right->get_end()) 
  {
    if (bc_right->get_type() == btPeriodic)
    {
      pposition = fmod(new_position, bc_right->get_end());
    }
    else if ((bc_right->get_type() == btReflective) || (bc_right->get_type() == btRigidWall) || (bc_right->get_type() == btFixed))
    {
      pposition = 2 * bc_right->get_end() - new_position;
      pvelocity = -pvelocity;
    }
    else if ((bc_right->get_type() == btOutlet) || (bc_right->get_type() == btSupersonicOutlet))
      return false;
  }
  else if (new_position < bc_left->get_end())
  {
    if (bc_left->get_type() == btPeriodic)
    {
      pposition = bc_right->get_end() - fmod(abs(new_position), bc_right->get_end());
    }
    else if ((bc_left->get_type() == btReflective) || (bc_left->get_type() == btRigidWall) || (bc_left->get_type() == btFixed))
    {
      pposition = 2 * bc_left->get_end() - new_position;
      pvelocity = -pvelocity;
    }
    else if ((bc_left->get_type() == btOutlet) || (bc_left->get_type() == btSupersonicOutlet))
      return false;
  }
  else
  {
    pposition = new_position;
  }
  return true;
}

/**
  Interpolate from grid, compute gradients, advance the particles, and impose boundary conditions.
*/
void ParticleManager::advanceExplicit(SlurmDouble dt, Grid *grid)
{
  set_InternalEnergy(0.);
  set_KineticEnergy(0.);

  #pragma omp parallel 
  {
    int thread_num = omp_get_thread_num();
    
    // Make sure particles were split between threads according to the current number of threads
    if (omp_get_num_threads() != IteratorsNumberOfThreads) 
    {
      if (thread_num == 0)
        IteratorsNumberOfThreads = omp_get_num_threads();

      #pragma omp barrier
      computeOMPThreadRange();
    }

    // Initialize local total energy
    SlurmDouble ThreadInternalEnergy = 0., ThreadKineticEnergy = 0.;
    SlDoubleVector new_position = {0, 0, 0};
    SlDoubleVector new_velocity = {0, 0, 0};

    for(ParticleIterator pi = IteratorsBegin[thread_num]; pi != IteratorsEnd[thread_num]; ++pi)    
    {    
      if (pi->get_m() > 0)
      {        
        //TODO: these guys create and destroy arrays of 8 pointers and 8 doubles for each particle!
        // Fetch de and velocity gradient
        GridCell *c = interpolator->interpolateCells(&(*pi));
        // Fetch u, du, dA
        GridNode *n = interpolator->interpolateNodes(&(*pi));
       
        // Update velocity using the grid's (vx - vxold) value. See Grid::updateExplicit()
        for (SlurmInt i = 0; i < 3; i++)
        {
          new_velocity[i] = pi->get_u(i) + n->get_du(i);
          new_position[i] = pi->get_xyz(i) + dt * n->get_u(i);
#ifdef IGNORE_Y
          new_position[1] = pi->get_xyz(1);
#endif
#ifdef IGNORE_Z
          new_position[2] = pi->get_xyz(2);
#endif
        }       
        
        // Move particles. 
        // TODO: return the bc iterator object from the grid and use it
        bool keep = true;
        SlurmInt bci = 0;
        while (keep && (bci < grid->get_bcCount()))
        {
          keep = grid->get_bc(bci)->imposeOnParticle(&(*pi), new_position, new_velocity);
          bci++;
        }

        if (keep)
        {
          pi->set_xyz(new_position);
          pi->set_u(new_velocity);

          // This is used to update particle's volume in MaterialPoint way.
          //pi->update_Jac(c, dt);

          // Update energy accounting for the equation of state...
          // Brackbill & Ruppel (JCP 1986) energy consists of several components. Here it's different.
          // Change cell's de to evolve in the same way as Fabio does: pi->inc_e(-dt * c->get_de() * pi->get_e());
          c->set_de(c->get_de() * dt);
          initial->updateParticleEnergy(&(*pi), c);
                
          // A^n + 1 = A^n + dt*sum_g(-(E+Eext) - eta*J + (u.nabla)A)
          updateParticleMagnetization(&(*pi), n);
          pi->inc_phi(c->get_phi());
          
          // Update volume
          updateParticleVolume(&(*pi), c, dt);
         
          // Gather conserved quantities
          ThreadInternalEnergy += pi->get_e();
          ThreadKineticEnergy += pi->get_KineticEnergy();
        }
        else
        {
          // Mark particle for deletion. Delete only after the grid has revised node neighbor particles.
          pi->set_m(-1);
          inc_DeletedNumber();
        }
        
        // Clean up
        delete n;
        delete c;
      }
    }
    
    // Save collected quantities
    set_InternalEnergy(InternalEnergy + ThreadInternalEnergy);
    set_KineticEnergy(KineticEnergy + ThreadKineticEnergy);
  }
}

/** 
  Here the particle quantities are delegated to a GridCell (GridNode) objects that are sent to the grid. 
  The Grid does actual interpolation.
*/
void ParticleManager::interpolateToGrid(Grid* grid) 
{
  // Remove particles marked for deletion
  cleanDeleted();

  // Fill grid with zeros before advecting, because particles are added one by one.
  grid->fillZeros();

  #pragma omp parallel 
  {
    int thread_num = omp_get_thread_num();
    
    // Make sure particles were split between threads according to the current number of threads
    if (omp_get_num_threads() != IteratorsNumberOfThreads) 
    {
      if (thread_num == 0)
        IteratorsNumberOfThreads = omp_get_num_threads();

      #pragma omp barrier
      computeOMPThreadRange();
    }
    
    for(ParticleIterator pi = IteratorsBegin[thread_num]; pi != IteratorsEnd[thread_num]; ++pi)
      interpolator->advectToGrid(&(*pi));
  }

  // On the grid, compute densities out of integral quantities projected from particles (energy, mass, momentum)
  grid->normalizeCells();
  grid->normalizeNodes();
  grid->interpolateVectorPotential();
}

/** 
  Export particle data as a 2D array of the size [np, nq+3] where nq is the number of quantities requested.
  Whether to output each i-th particle, or just the specified particles?
*/
SlurmDoubleArray2D ParticleManager::exportAsArray(unsigned long quants, vector<string> *quant_names)
{
  SlurmDoubleArray2D data;
  data.resize(boost::extents[(size_t) ceil(((float)CurrentNumber) / OutputRange)][3 + NumberOfBitsSet(quants)]);

  #pragma omp parallel 
  {
    int thread_num = omp_get_thread_num();

    // Make sure particles were split between threads according to the current number of threads
    if (omp_get_num_threads() != IteratorsNumberOfThreads) 
    {
      if (thread_num == 0)
        IteratorsNumberOfThreads = omp_get_num_threads();

      #pragma omp barrier
      computeOMPThreadRange();
    }

    // ip is used as the index in the array; j is the index of the current particle
    size_t ip = 0;
    size_t jp = thread_num * (CurrentNumber / IteratorsNumberOfThreads);

    for(ParticleIterator pi = IteratorsBegin[thread_num]; pi != IteratorsEnd[thread_num]; ++pi)    
    {
      if (jp % OutputRange == 0)
      {
        // Index in the array
        ip = jp / OutputRange;

        data[ip][0] = pi->get_x();
        data[ip][1] = pi->get_y();
        data[ip][2] = pi->get_z();
        
        int iq = 3;
        if ((quants & QUANT_VX) == QUANT_VX) 
        {
          data[ip][iq] = pi->get_ux();
          iq++;
        }
        if ((quants & QUANT_VY) == QUANT_VY) 
        {
          data[ip][iq] = pi->get_uy();
          iq++;
        }
        if ((quants & QUANT_VZ) == QUANT_VZ) 
        {
          data[ip][iq] = pi->get_uz();
          iq++;
        }
        if ((quants & QUANT_MAGNX) == QUANT_MAGNX) 
        {
          data[ip][iq] = pi->get_Ax();
          iq++;
        }
        if ((quants & QUANT_MAGNY) == QUANT_MAGNY) 
        {
          data[ip][iq] = pi->get_Ay();
          iq++;
        }
        if ((quants & QUANT_MAGNZ) == QUANT_MAGNZ) 
        {
          data[ip][iq] = pi->get_Az();
          iq++;
        }
        if ((quants & QUANT_MASS) == QUANT_MASS) 
        {
          data[ip][iq] = pi->get_m();
          iq++;
        }
        if ((quants & QUANT_VOL) == QUANT_VOL) 
        {
          data[ip][iq] = pi->get_vol();
          iq++;
        }
        if ((quants & QUANT_INTERNAL) == QUANT_INTERNAL) 
        {
          data[ip][iq] = (SlurmDouble) pi->get_color();
          iq++;
        }
      }

      jp++;
    }
  }  
 
  // Names of the columns in the array. The order SHOULD correspond to the above!
  quant_names->push_back("X");
  quant_names->push_back("Y");
  quant_names->push_back("Z");
  if ((quants & QUANT_VX) == QUANT_VX) 
    quant_names->push_back("ux");
  if ((quants & QUANT_VY) == QUANT_VY) 
    quant_names->push_back("uy");
  if ((quants & QUANT_VZ) == QUANT_VZ) 
    quant_names->push_back("uz");
  if ((quants & QUANT_MAGNX) == QUANT_MAGNX) 
    quant_names->push_back("Ax");
  if ((quants & QUANT_MAGNY) == QUANT_MAGNY) 
    quant_names->push_back("Ay");
  if ((quants & QUANT_MAGNZ) == QUANT_MAGNZ) 
    quant_names->push_back("Az");
  if ((quants & QUANT_MASS) == QUANT_MASS) 
    quant_names->push_back("m");
  if ((quants & QUANT_VOL) == QUANT_VOL) 
    quant_names->push_back("V");
  if ((quants & QUANT_INTERNAL) == QUANT_INTERNAL) 
    quant_names->push_back("color");
  
  return data;
}

void ParticleManager::set_CurrentNumber(std::list<Particle>::size_type value)
{
  omp_set_lock(&CurrentNumberLock);
  CurrentNumber = value;
  omp_unset_lock(&CurrentNumberLock);
}

void ParticleManager::inc_CurrentNumber()
{
  omp_set_lock(&CurrentNumberLock);
  ++CurrentNumber;
  omp_unset_lock(&CurrentNumberLock);  
}

void ParticleManager::dec_CurrentNumber()
{
  omp_set_lock(&CurrentNumberLock);
  --CurrentNumber;
  omp_unset_lock(&CurrentNumberLock);  
}

void ParticleManager::set_DeletedNumber(std::list<Particle>::size_type value)
{
  omp_set_lock(&DeletedNumberLock);
  DeletedNumber = value;
  omp_unset_lock(&DeletedNumberLock);
}

void ParticleManager::inc_DeletedNumber()
{
  omp_set_lock(&DeletedNumberLock);
  ++DeletedNumber;
  omp_unset_lock(&DeletedNumberLock);  
}

/** 
  Add a particle to the list and increase particle number.
  Does not set any fields.
*/
std::list<Particle>::size_type ParticleManager::addParticle(Particle *p)
{ 
  items.push_back(*p);
  inc_CurrentNumber();
  return CurrentNumber;
}

/** 
  Add a new particle to the list and increase the particle number.
  Set particle ID, deformation tensor, volume.
*/
Particle * ParticleManager::addParticle(SlurmDouble x, SlurmDouble y, SlurmDouble z)
{ 
  Particle p;

  // Particle's unique ID
  p.set_id(CurrentNumber);
  // Coordinates: uniform distribution in cell centers
  p.set_xyz(x, y, z);
  // Volume
  p.set_vol(dxp * dyp * dzp);
  p.set_vol0(p.get_vol());
  // Jacobian is initiated in GridCommon

  items.push_back(p);
  inc_CurrentNumber();
  return &(items.back());
}

std::list<Particle>::size_type ParticleManager::deleteParticle(ParticleIterator ip) 
{
  items.erase(ip);
  dec_CurrentNumber();
  return CurrentNumber;
}

/* TODO: this is slow. Particled should be marked first, a list of marked particles has to be created, then prticles should be deleted from both lists */
void ParticleManager::cleanDeleted() 
{
  if (DeletedNumber > 0)
  {
    messageline("Deleted particles: ", DeletedNumber);
    ParticleIterator pi = items.begin();
    while (pi != items.end())
    {
      if (pi->get_m() < 0)
        deleteParticle(pi++);
      else
        ++pi;
    }  
    set_DeletedNumber(0);
    computeOMPRanges();
  }
}

void ParticleManager::set_InternalEnergy(SlurmDouble value)
{
  omp_set_lock(&InternalEnergyLock);
  InternalEnergy = value;
  omp_unset_lock(&InternalEnergyLock);
}

void ParticleManager::set_KineticEnergy(SlurmDouble value)
{
  omp_set_lock(&KineticEnergyLock);
  KineticEnergy = value;
  omp_unset_lock(&KineticEnergyLock);
}

void ParticleManager::print() 
{
  messageline("Number of particles: ", this->get_count());
  list<Particle>::size_type i = 0;
  for (ParticleIterator pi = items.begin(), end = items.end(); pi != end; ++pi) 
  {
    message(i);
    message(". x = ", pi->get_x());
    message(", y = ", pi->get_y());
    message(", z = ", pi->get_z());
    message(", mass=", pi->get_m());
    message(", vol=", pi->get_vol());
    message(", vx=", pi->get_ux());
    message(", vy=", pi->get_uy());
    message(", vz=", pi->get_uz());
    message(" |  ");
    ++i;
  }
  messageline("");
}

ParticleManager::~ParticleManager()
{
  items.clear();
}
