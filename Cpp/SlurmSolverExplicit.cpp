/** 
  An explicit solver class for the Slurm project.

  Started in March 2015
*/
#include "SlurmSolverExplicit.h"

using namespace std;

SlurmSolverExplicit::SlurmSolverExplicit(ConfigFile *config) : SlurmSolverBase(config)
{    
  // Create the grid
  messageline("\nSlurmSolverExplicit: creating grid.");
  grid = new Grid(fields->get_FieldEvolutionMethod(), config, get_initialCondition(), fields);

  // Read interpolation scheme
  string scheme = config->read<string>("Interpolation", "Linear");
  messageline("SlurmSolverExplicit: creating particles");
  particles = new ParticleManager(grid, scheme, fields->get_FieldEvolutionMethod());

  messageline("SlurmSolverExplicit: initializing particles");
  particles->init(grid, config);

  messageline("");
#ifdef DEBUG
  messageline("Memory used by GridPoint: ", sizeof(GridPoint));
  messageline("Memory used by MaterialPoint: ", sizeof(MaterialPoint));
  messageline("Memory used by PointVelocity: ", sizeof(PointVelocity));
  messageline("Memory used by PointEnergy: ", sizeof(PointEnergy));
  messageline("Memory used by GridCell: ", sizeof(GridCell));
  messageline("Memory used by GridNode: ", sizeof(GridNode));
  messageline("Memory used by Particle: ", sizeof(Particle));
#endif
  messageline("Memory used by grid cells (MB): ", sizeof(GridCell) * grid->get_ncx() * grid->get_ncy() * grid->get_ncz() / 1048576);
  messageline("Memory used by grid nodes (MB): ", sizeof(GridNode) * grid->get_nnx() * grid->get_nny() * grid->get_nnz() / 1048576);
  messageline("Memory used by particles (MB): ", sizeof(Particle) * particles->get_count() / 1048576);
  done = false;
  messageline("SlurmSolverExplicit: solver initialized.");
}

/** 
  Main solver control 
*/
int SlurmSolverExplicit::run(IOManager* io)
{
  struct timespec tstart={0,0}, tend={0,0};
    
  messageline("\nSlurmSolverExplicit: starting Slurm simulation.");
  messageline("-------------------------------------------------");

  // Computation step
  SlurmInt it = itStart;

  // Main time cycle
  while (!isDone(it)) 
  {
    message("\n---------------------- ", it);
    messageline(" ----------------------");
    messageline("time: ", CurrTime);

    particles->injectParticles(grid, CurrTime, dt);

    // Project conserved quantities from particles to grid. The grid is invoked by the particles.
    clock_gettime(CLOCK_MONOTONIC, &tstart);   
    particles->interpolateToGrid(grid); 
    clock_gettime(CLOCK_MONOTONIC, &tend);
    messageline("Particles interpolated: ", ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec));

    // First normalize the cells, then fill the ghost cells (they have zero mass and weight), then normalize the nodes
    messageline("SlurmSolverExplicit: advancing grid");
    clock_gettime(CLOCK_MONOTONIC, &tstart);   

    // Interpolate magnetic field ans impose BCs
    grid->imposeBoundaryConditions(CurrTime);

    // Check the condition for velocity gradient and adjust the timestep (NOT a CFL really!)
    adjustTimestep();
    // Solve Lagrangian form of MHD equations on the grid.
    grid->advanceExplicit(CurrTime, dt);

    clock_gettime(CLOCK_MONOTONIC, &tend);
    messageline("Grid advanced: ", ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec));

    doOutput(io, it);

    // Project from grid to particles (move particles). Basically, advect the physical quantities. 
    messageline("SlurmSolverExplicit: advancing particles");
    clock_gettime(CLOCK_MONOTONIC, &tstart);   
    particles->advanceExplicit(dt, grid);
    clock_gettime(CLOCK_MONOTONIC, &tend);
    messageline("Particles advanced: ", ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec));

    // Time management
    CurrTime += dt;
    ++it;
  }

  finalize();
  return(0);
}

/** What are the conditions to finish? Exceeding time limit, exceeding timestep number limit, etc. */
void SlurmSolverExplicit::adjustTimestep()
{
  SlurmDouble cfl = grid->getCFL();

  if (dt > cfl)
  {
    dt = cfl;
    messageline("SlurmSolverExplicit: new timestep ", dt);
  }
}

SlurmSolverExplicit::~SlurmSolverExplicit()
{
}
