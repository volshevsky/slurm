/*
  Class IOManager reads parameter file and manages input/output for Slurm.
*/
#pragma once
#include <map>
#include <boost/lexical_cast.hpp>
#include "utils.h"

//#define VTK_ENABLED

#ifdef VTK_ENABLED

#include <vtkCellArray.h>
#include <vtkDataSet.h>
#include <vtkDoubleArray.h>
#include <vtkSmartPointer.h>
#include <vtkProperty.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkXMLImageDataReader.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkProperty.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkVersion.h>
#include <vtkRectilinearGrid.h>
#include <vtkXMLRectilinearGridWriter.h>
#include <vtkCellData.h>

#else

#include <fstream>
//#include <boost/endian/conversion.hpp>

#endif

using namespace std;

class IOManager
{
private:
  map<std::string, std::string> params;
  static const string ExtensionVTI;  
  static const string ExtensionVTU;  
  static const string ExtensionVTR;  
  static const string ExtensionVTK;  

public:
  /// Writes a VTK Image file
  int writeVTI(const string filename, SlurmDoubleArray3D data, SlurmDouble dx, SlurmDouble dy, SlurmDouble x0, SlurmDouble y0);
  /// Writes a VTK Unstructured grid file
  int writeVTU(const string filename, SlurmDoubleArray2D data, vector<string> quant_names, vector<string> scalar_names);
  /// Writes a VTK Rectilinear grid file
  int writeVTR(const string filename, SlurmDoubleArray4D cell_data, vector<string> cell_names, SlurmDoubleArray4D point_data, vector<string> point_names, SlurmDouble dx, SlurmDouble dy, SlurmDouble dz, vector<string> cell_vectors, vector<string> point_vectors, SlurmDouble xstart = 0, SlurmDouble ystart = 0, SlurmDouble zstart = 0);
  /// Simple (old-format) VTK file that doesn't need the VTK library
  int writeRectilinearVTK(const string filename, SlurmDoubleArray4D cell_data, vector<string> cell_names, SlurmDoubleArray4D point_data, vector<string> point_names, SlurmDouble dx, SlurmDouble dy, SlurmDouble dz, vector<string> cell_vectors, vector<string> point_vectors, SlurmDouble xstart = 0, SlurmDouble ystart = 0, SlurmDouble zstart = 0);
  /// Old-format VTK with unstructured grid. Used for points.
  int writeUnstructuredVTK(const string filename, SlurmDoubleArray2D data, vector<string> scalar_names, vector<string> vector_names);

  void ByteSwap(unsigned char * b, int n);
  void FloatArraySwap(float * arr, int n) {for (int i = 0; i < n; i++) ByteSwap((unsigned char*) &arr[i], 4);};

  IOManager();
  ~IOManager();
};

